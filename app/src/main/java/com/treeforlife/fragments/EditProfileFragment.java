package com.treeforlife.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.treeforlife.R;
import com.treeforlife.Utils.AndroidUtil;
import com.treeforlife.Utils.AttachFileUtil;
import com.treeforlife.Utils.DialogUtil;
import com.treeforlife.Utils.ImageLoaderUtil;
import com.treeforlife.Utils.ProgressDialogUtil;
import com.treeforlife.Utils.ValidationUtil;
import com.treeforlife.activities.CountriesListActivity;
import com.treeforlife.commons.CommonAppMethods;
import com.treeforlife.commons.Constants;
import com.treeforlife.commons.FragmentCallBack;
import com.treeforlife.commons.SharedPrefHelper;
import com.treeforlife.customview.CustomEditText;
import com.treeforlife.dataobjects.CountryInfo;
import com.treeforlife.dataobjects.TFLUser;
import com.treeforlife.retrofit.ResponseId;
import com.treeforlife.retrofit.RetrofitManager;
import com.treeforlife.retrofit.RetrofitResponseValidator;
import com.treeforlife.retrofit.responses.EditProfileResponse;

import java.io.File;
import java.net.SocketTimeoutException;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TheAppsmiths on 4/9/2018.
 *
 * @author TheAppsmiths
 */

/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class EditProfileFragment extends Fragment implements View.OnClickListener,
        View.OnTouchListener, AttachFileUtil.AttachFileCallback,
        RetrofitResponseValidator.ValidationListener {

    private static final String TAG = "EditProfileFragment";
    private final int SELECT_COUNTRY_DIAL_CODE = 101;
    private final int SELECT_COUNTRY_ADDRESS = 102;
    private Context mContext;
    private Activity mActivity;
    private Bundle mBundle;
    private FragmentCallBack mCallBack;
    private ImageView mProfileImageView;
    private CustomEditText mFirstNameEditText, mLastNameEditText, mEmailEditText,
            mPhoneDialCodeEditText, mPhoneNumberEditText, mStreetEditText, mCityEditText, mStateEditText,
            mAddressCountryEditText, mBiographyEditText;
    private View mSelectDialCodeView, mSelectAddressCountryView;
    private ImageLoader mImageLoader;
    private AttachFileUtil mAttachFileUtil;
    private SharedPrefHelper mSharedPrefHelper;
    private DialogUtil mDialogUtil;
    private ProgressDialogUtil mProgressDialogUtil;
    private TFLUser mTflUser;
    private Uri mAttachedFileUri;
    private File mAttachedFile;
    private AttachFileUtil.FileType mAttachedFileType;
    private CountryInfo mAddressCountryInfo;
    private String mFirstName, mLastName, mEmail, mPhone, mBiography,
            mPhoneDialCode, mAddressStreet, mAddressCity, mAddressState, mAddressCountry;
    private boolean hasUpdatedProfilePic;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
        this.mActivity = getActivity();
        this.mBundle = getArguments();

        if (mActivity != null && mActivity instanceof FragmentCallBack) {
            this.mCallBack = (FragmentCallBack) mActivity;
        }
        this.mImageLoader = ImageLoaderUtil.getLoader(mContext);
        this.mAttachFileUtil = AttachFileUtil.getInstance();
        this.mSharedPrefHelper = SharedPrefHelper.getInstance();
        this.mDialogUtil = DialogUtil.getInstance();
        this.mProgressDialogUtil = ProgressDialogUtil.getInstance();

        extractDataFromBundle();
        ensureTflUser();

    }

    private void extractDataFromBundle() {
        if (mBundle == null) {
            mBundle = getArguments();
        }

        if (mBundle != null) {
            mTflUser = mBundle.getParcelable(Constants.KEY_TFL_USER);
        }
    }

    private void ensureTflUser() {
        if (mTflUser == null) {
            mTflUser = mSharedPrefHelper.getLoginResponse(mContext);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_edit_profile, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        setData();
    }

    private void initViews(View view) {
        if (view != null) {

            mProfileImageView = (CircleImageView) view.findViewById(R.id.profile_image_view);
            mProfileImageView.setOnClickListener(this);

            mFirstNameEditText = (CustomEditText) view.findViewById(R.id.first_name_edit_text);
            mLastNameEditText = (CustomEditText) view.findViewById(R.id.last_name_edit_text);

            mEmailEditText = (CustomEditText) view.findViewById(R.id.email_edit_text);
            mEmailEditText.setEnabled(false);

            mPhoneDialCodeEditText = view.findViewById(R.id.phone_dial_code_edit_text);
            mPhoneDialCodeEditText.setOnTouchListener(this);
            mSelectDialCodeView = view.findViewById(R.id.select_country_code_view);
            mSelectDialCodeView.setOnClickListener(this);
            mPhoneNumberEditText = (CustomEditText) view.findViewById(R.id.phone_number_edit_text);
            mPhoneNumberEditText.setEnabled(false);

            mStreetEditText = (CustomEditText) view.findViewById(R.id.street_edit_text);
            mCityEditText = (CustomEditText) view.findViewById(R.id.city_edit_text);
            mStateEditText = (CustomEditText) view.findViewById(R.id.state_edit_text);

            mSelectAddressCountryView = view.findViewById(R.id.select_address_country_view);
            mSelectAddressCountryView.setOnClickListener(this);
            mAddressCountryEditText = (CustomEditText) view.findViewById(R.id.edit_text_address_country);
            mAddressCountryEditText.setOnTouchListener(this);

            mBiographyEditText = (CustomEditText) view.findViewById(R.id.biography_edit_text);

            View mChangePictureActionView = view.findViewById(R.id.change_picture_action_view);
            mChangePictureActionView.setOnClickListener(this);

            View mSaveProfileActionView = view.findViewById(R.id.save_profile_action_view);
            mSaveProfileActionView.setOnClickListener(this);

            setListeners();

        }
    }

    private void setListeners() {

        mLastNameEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    AndroidUtil.hideKeyPad(mActivity, mLastNameEditText);
                    if (mEmailEditText.isEnabled()) {
                        setFocusTo(mEmailEditText);
                    } else {
                        if (TextUtils.isEmpty(mPhoneDialCodeEditText.getText())) {
                            mSelectDialCodeView.performClick();
                        } else {
                            setFocusTo(mPhoneNumberEditText);
                        }
                    }

                }
                return true;
            }
        });

        mStateEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    AndroidUtil.hideKeyPad(mActivity, mStateEditText);
                    if (TextUtils.isEmpty(mAddressCountryEditText.getText())) {
                        mSelectAddressCountryView.performClick();
                    } else {
                        setFocusTo(mBiographyEditText);
                    }

                }
                return true;
            }
        });
    }

    private void setFocusTo(CustomEditText editText) {
        if (editText != null) {
            String text = editText.getText().toString();
            if (!TextUtils.isEmpty(text)) {
                editText.setSelection(text.length());
            }
            editText.requestFocus();
            AndroidUtil.showKeyPad(mActivity, editText);
        }
    }

    private void setData() {
        if (mTflUser != null) {
            mImageLoader.displayImage(mTflUser.profileImage, mProfileImageView,
                    ImageLoaderUtil.getDefaultProfileImageOption());

            mFirstNameEditText.setText(mTflUser.firstName);
            mLastNameEditText.setText(mTflUser.lastName);
            mEmailEditText.setText(mTflUser.email);
            String phoneText = mTflUser.phone;
            if (!TextUtils.isEmpty(phoneText) && !TextUtils.isEmpty(mTflUser.phoneDialCode)) {
                phoneText = phoneText.replace(mTflUser.phoneDialCode, "");
            }
            mPhoneNumberEditText.setText(phoneText);

            mStreetEditText.setText(mTflUser.street);
            mCityEditText.setText(mTflUser.city);
            mStateEditText.setText(mTflUser.state);
            mAddressCountryEditText.setText(mTflUser.addressCountryName);
            mBiographyEditText.setText(CommonAppMethods.decodeText(mTflUser.biography));
            mPhoneDialCode = mTflUser.phoneDialCode;
            mPhoneDialCodeEditText.setText(mPhoneDialCode);

            if (!TextUtils.isEmpty(mTflUser.registerBy)) {
                switch (mTflUser.registerBy) {

                    case Constants.REGISTER_BY_EMAIL:
                        mEmailEditText.setEnabled(false);
                        mPhoneNumberEditText.setEnabled(true);
                        mPhoneDialCodeEditText.setEnabled(true);
                        mSelectDialCodeView.setEnabled(true);
                        break;

                    case Constants.REGISTER_BY_PHONE:
                        mEmailEditText.setEnabled(true);
                        mPhoneNumberEditText.setEnabled(false);
                        mPhoneDialCodeEditText.setEnabled(false);
                        mSelectDialCodeView.setEnabled(false);
                        break;

                }
            }

        }

    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        if (view != null && event != null) {
            switch (event.getAction()) {

                case MotionEvent.ACTION_DOWN:
                    switch (view.getId()) {

                        case R.id.edit_text_address_country:
                            mSelectAddressCountryView.performClick();
                            break;


                        case R.id.phone_dial_code_edit_text:
                            mSelectDialCodeView.performClick();
                            break;
                    }
                    break;
            }
        }
        return false;
    }

    @Override
    public void onClick(View view) {
        if (view != null) {

            switch (view.getId()) {

                case R.id.profile_image_view:
                    showProfileImage();
                    break;

                case R.id.change_picture_action_view:
                    boolean showRemoveOption = hasUpdatedProfilePic ? mAttachedFile != null :
                            (mTflUser != null && !TextUtils.isEmpty(mTflUser.profileImage));
                    mAttachFileUtil.start(AttachFileUtil.FileType.FILE_TYPE_IMAGE, mActivity,
                            showRemoveOption, this);

                    break;

                case R.id.save_profile_action_view:
                    callEditProfile();
                    break;

                case R.id.select_country_code_view:
                    Intent intent = new Intent(mContext, CountriesListActivity.class);
                    intent.putExtra(Constants.KEY_REQ_CODE, SELECT_COUNTRY_DIAL_CODE);
                    intent.putExtra(Constants.KEY_PHONE_DIAL_CODE, mPhoneDialCode);
                    mActivity.startActivityForResult(intent, Constants.RC_START_ACTIVITY_SELECT_COUNTRY);
                    mActivity.overridePendingTransition(R.anim.slide_in_from_right, R.anim.fade_in);

                    break;
                case R.id.select_address_country_view:
                    intent = new Intent(mContext, CountriesListActivity.class);
                    intent.putExtra(Constants.KEY_REQ_CODE, SELECT_COUNTRY_ADDRESS);
                    mActivity.startActivityForResult(intent, Constants.RC_START_ACTIVITY_SELECT_COUNTRY);
                    mActivity.overridePendingTransition(R.anim.slide_in_from_right, R.anim.fade_in);

                    break;

            }
        }
    }

    private void showProfileImage() {
        String imageUriOrUrl = "";
        if (hasUpdatedProfilePic) {
            if (mAttachedFileUri != null) {
                imageUriOrUrl = mAttachedFileUri.toString();
            }

        } else {
            if (mTflUser != null) {
                imageUriOrUrl = mTflUser.profileImage;
            }
        }
        if (!TextUtils.isEmpty(imageUriOrUrl)) {
            FragmentManager fragmentManager = getChildFragmentManager();

            if (fragmentManager != null) {
                Bundle bundle = new Bundle();
                bundle.putString(Constants.KEY_CALLER_COMPONENT, TAG);
                bundle.putString(Constants.KEY_IMAGE_URL, imageUriOrUrl);

                ImageViewDialogFragment mImageViewDialogFragment;
                Fragment fragment =
                        fragmentManager.findFragmentByTag(ImageViewDialogFragment.TAG);
                if (fragment != null && fragment instanceof ImageViewDialogFragment) {
                    mImageViewDialogFragment = (ImageViewDialogFragment) fragment;
                    mImageViewDialogFragment.update(bundle);
                } else {
                    mImageViewDialogFragment = new ImageViewDialogFragment();
                    mImageViewDialogFragment.setArguments(bundle);
                }

                mImageViewDialogFragment.show(fragmentManager, ImageViewDialogFragment.TAG);
            }
        }
    }

    private void callEditProfile() {
        if (isValidUser() && isValidProfileData()
                && AndroidUtil.hasInternetConnectivity(mContext)) {
            mProgressDialogUtil.showProgressDialog(mActivity);

            MultipartBody.Builder builder = new MultipartBody.Builder();
            builder.setType(MultipartBody.FORM);

            builder.addFormDataPart("first_name", mFirstName);
            builder.addFormDataPart("last_name", mLastName);
            builder.addFormDataPart("email", mEmail);
            if (!TextUtils.isEmpty(mPhone)) {
                builder.addFormDataPart("phone", mPhoneDialCode + mPhone);
            } else {
                builder.addFormDataPart("phone", mPhone);
            }

            builder.addFormDataPart("phone_dial_code", mPhoneDialCode);
            builder.addFormDataPart("bio", CommonAppMethods.encodeAllCharsToUnicode(mBiography));
            builder.addFormDataPart("street", mAddressStreet);
            builder.addFormDataPart("city", mAddressCity);
            builder.addFormDataPart("state", mAddressState);
            builder.addFormDataPart("address_country_id", mAddressCountryInfo != null ? mAddressCountryInfo.countryId : mTflUser.addressCountryId);
            builder.addFormDataPart("address_country_name", mAddressCountryInfo != null ? mAddressCountryInfo.countryName : mTflUser.addressCountryName);

            builder.addFormDataPart("user_id", mTflUser.userId);
            builder.addFormDataPart("tree_id", mTflUser.treeId);
            builder.addFormDataPart("where_to_plant", mTflUser.whereToPlant);
            builder.addFormDataPart("update_image", String.valueOf(hasUpdatedProfilePic));

            if (hasUpdatedProfilePic && mAttachedFile != null) {
                File file1 = new File(mAttachedFile.getName());
                builder.addFormDataPart("profile_pic",
                        file1.getName(),
                        (RequestBody.create(MediaType.parse("image"),
                                mAttachedFile)));
            }

            MultipartBody requestBody = builder.build();

            Call<EditProfileResponse> mEditProfileCall = RetrofitManager.getRetrofitWebService()
                    .editProfile(requestBody);

            mEditProfileCall.enqueue(new Callback<EditProfileResponse>() {
                @Override
                public void onResponse(@NonNull Call<EditProfileResponse> call, @NonNull Response<EditProfileResponse> response) {
                    new RetrofitResponseValidator(ResponseId.EDIT_PROFILE, response, EditProfileFragment.this);
                }

                @Override
                public void onFailure(@NonNull Call<EditProfileResponse> call, @NonNull Throwable t) {
                    String errorMessage = "";
                    if (t != null) {
                        Log.e(TAG, "onFailure: Error : " + t.getMessage());
                        if (t instanceof SocketTimeoutException) {
                            errorMessage = "Please make sure that your device has an active internet connection.";
                        }
                    }

                    mProgressDialogUtil.dismissProgressDialog();
                    if (!isVisible()) {
                        return;
                    }
                    if (TextUtils.isEmpty(errorMessage)) {
                        AndroidUtil.showErrorToast(mContext);
                    } else {
                        mDialogUtil.showErrorDialog(mContext, errorMessage);
                    }
                }
            });


        }
    }

    private boolean isValidProfileData() {
        mFirstName = mFirstNameEditText.getText().toString().trim();
        mLastName = mLastNameEditText.getText().toString().trim();
        mEmail = mEmailEditText.getText().toString().trim();
        mPhoneDialCode = mPhoneDialCodeEditText.getText().toString().trim();
        mPhone = mPhoneNumberEditText.getText().toString().trim();
        mBiography = mBiographyEditText.getText().toString().trim();
        mAddressStreet = mStreetEditText.getText().toString().trim();
        mAddressCity = mCityEditText.getText().toString().trim();
        mAddressState = mStateEditText.getText().toString().trim();
        String mAddressCountryName = mAddressCountryEditText.getText().toString().trim();


        ValidationUtil.ValidationResult firstNameResult = ValidationUtil.validateName(mFirstName, true);
        ValidationUtil.ValidationResult lastNameResult = ValidationUtil.validateName(mLastName, false);

        if (firstNameResult != null && !firstNameResult.isValidInput) {
            mFirstNameEditText.setError(firstNameResult.errorMessage);
            mFirstNameEditText.requestFocus();
            return false;

        } else if (lastNameResult != null && !lastNameResult.isValidInput) {
            mLastNameEditText.setError(lastNameResult.errorMessage);
            mLastNameEditText.requestFocus();
            return false;
        } else if (!TextUtils.isEmpty(mEmail) && !ValidationUtil.isValidEmail(mEmail)) {
            mEmailEditText.setError(getString(R.string.error_email));
            mEmailEditText.requestFocus();
            return false;

        } else if (!TextUtils.isEmpty(mPhone)) {
            if (TextUtils.isEmpty(mPhoneDialCode)) {
                mPhoneDialCodeEditText.setError(getString(R.string.no_phone_dial_code));
                mPhoneDialCodeEditText.clearFocus();
                AndroidUtil.showToast(mContext, getString(R.string.no_phone_dial_code));
                return false;

            } else if (!ValidationUtil.isValidPhoneNumber(mPhone, false, false)) {
                mPhoneNumberEditText.setError(getString(R.string.error_valid_phone));
                mPhoneNumberEditText.requestFocus();
                return false;

            }
        } else if (!TextUtils.isEmpty(mPhoneDialCode)) {
            if (TextUtils.isEmpty(mPhone)) {
                mPhoneNumberEditText.setError(getString(R.string.empty_phone_number));
                mPhoneNumberEditText.requestFocus();
                return false;

            } else if (!ValidationUtil.isValidPhoneNumber(mPhone, false, false)) {
                mPhoneNumberEditText.setError(getString(R.string.error_valid_phone));
                mPhoneNumberEditText.requestFocus();
                return false;
            }

        }

        if (TextUtils.isEmpty(mAddressStreet)) {
            mStreetEditText.setError(getString(R.string.empty_address_street));
            mStreetEditText.requestFocus();
            return false;

        } else if (TextUtils.isEmpty(mAddressCity)) {
            mCityEditText.setError(getString(R.string.empty_address_city));
            mCityEditText.requestFocus();
            return false;

        } else if (TextUtils.isEmpty(mAddressState)) {
            mStateEditText.setError(getString(R.string.empty_address_state));
            mStateEditText.requestFocus();
            return false;

        } else if (TextUtils.isEmpty(mAddressCountryName)) {
            mAddressCountryEditText.setError(getString(R.string.no_address_country));
            mAddressCountryEditText.clearFocus();
            AndroidUtil.showToast(mContext, getString(R.string.no_address_country));
            return false;

        }

        return true;
    }

    private boolean isValidUser() {

        if (mTflUser == null) {

            AndroidUtil.showToast(mContext, "Invalid User");
            return false;

        } else if (TextUtils.isEmpty(mTflUser.userId)) {

            AndroidUtil.showToast(mContext, "You need to Login !");
            return false;
        }

        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAttachFileUtil != null) {
            mAttachFileUtil.onResume();
        }
        if (mActivity != null) {
            mActivity.setTitle(R.string.edit_profile);
        }
        if (mCallBack != null) {
            Bundle bundle = new Bundle();
            bundle.putString(Constants.KEY_FRAGMENT_CALLBACK, TAG);
            mCallBack.updateBottomBar(bundle);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {

                case Constants.RC_START_IMPLICIT_ACTIVITY_GALLERY:
                case Constants.RC_START_IMPLICIT_ACTIVITY_CAMERA:
                    if (mAttachFileUtil != null) {
                        mAttachFileUtil.onActivityResult(requestCode, resultCode, data);
                    }
                    break;

                case Constants.RC_START_ACTIVITY_SELECT_COUNTRY:
                    if (data != null) {
                        CountryInfo countryInfo = data.getParcelableExtra(Constants.KEY_COUNTRY_INFO);
                        if (countryInfo != null) {
                            int reqCode = data.getIntExtra(Constants.KEY_REQ_CODE, -1);
                            switch (reqCode) {

                                case SELECT_COUNTRY_DIAL_CODE:
                                    mPhoneDialCode = countryInfo.dialCode;
                                    mPhoneDialCodeEditText.setText(mPhoneDialCode);
                                    mPhoneDialCodeEditText.setError(null);
                                    setFocusTo(mPhoneNumberEditText);
                                    break;

                                case SELECT_COUNTRY_ADDRESS:
                                    mAddressCountryInfo = countryInfo;
                                    mAddressCountryEditText.setText(mAddressCountryInfo.countryName);
                                    mAddressCountryEditText.setError(null);
                                    setFocusTo(mBiographyEditText);
                                    break;
                            }
                        }
                    }
                    break;
            }
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (mAttachFileUtil != null) {
            mAttachFileUtil.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onNewFile(@NonNull AttachFileUtil.FileType fileType, String attachedVia, Uri fileUri, File file) {
        this.mAttachedFileType = fileType;
        this.mAttachedFile = file;
        this.mAttachedFileUri = fileUri;
        this.hasUpdatedProfilePic = true;
        mImageLoader.displayImage(String.valueOf(mAttachedFileUri), mProfileImageView,
                ImageLoaderUtil.getDefaultProfileImageOption());
        if (mAttachedFile == null && fileUri != null) {
            AndroidUtil.showToast(mContext, "File path could not be fetched.");
        }

    }

    @Override
    public void onError(@NonNull String errorMessage) {
        AndroidUtil.showToast(mContext, errorMessage);

    }

    @Override
    public void onValidResponse(@NonNull ResponseId responseId, @NonNull Object responseBody, @NonNull Response response) {
        mProgressDialogUtil.dismissProgressDialog();
        switch (responseId) {
            case EDIT_PROFILE:
                if (isVisible()) {
                    final EditProfileResponse editProfileResponse = (EditProfileResponse) responseBody;
                    afterEditProfile(editProfileResponse);

/*
                    CometChatUtil.getInstance().updateCometChatUser(
                            editProfileResponse.tflUser.userId,
                            editProfileResponse.tflUser.firstName + " " +
                                    editProfileResponse.tflUser.lastName,
                            editProfileResponse.tflUser.profileImage,
                            new CometChatResponseListener() {
                                @Override
                                public void onCometChatResponse(ResponseId responseId, boolean isSuccess, JSONObject response) {
                                    Log.d(TAG, "onCometChatResponse() called with: responseId = [" + responseId + "], isSuccess = [" + isSuccess + "], response = [" + response + "]");
                                }
                            });
*/
                }

        }

    }

    private void afterEditProfile(EditProfileResponse response) {
        String responseMessage = response.message;
        if (TextUtils.isEmpty(responseMessage)) {
            responseMessage = "Your profile has been updated successfully.";
        }

        mSharedPrefHelper.setLoginResponse(mContext, response.tflUser);
        mTflUser = mSharedPrefHelper.getLoginResponse(mContext);
        Intent broadCastIntent = new Intent(Constants.ACTION_USER_PROFILE_UPDATED);
        broadCastIntent.putExtra(Constants.KEY_TFL_USER, mTflUser);
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(broadCastIntent);
        mProgressDialogUtil.dismissProgressDialog();
        AndroidUtil.showToast(mContext, responseMessage);
        mActivity.onBackPressed();
    }

    @Override
    public void onInvalidResponse(@NonNull ResponseId responseId, @NonNull String errorMessage, Response response) {
        mProgressDialogUtil.dismissProgressDialog();
        if (isVisible()) {
            if (response == null || !response.isSuccessful()) {
                mDialogUtil.showErrorDialog(mActivity, errorMessage);
            } else {
                mDialogUtil.showOkAlertDialog(mActivity, errorMessage);
            }
        }
    }
}
