package com.treeforlife.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.treeforlife.R;

import java.util.List;
import java.util.Set;

/**
 * Created by TheAppsmiths on 10/16/2017.
 *
 * @author TheAppsmiths
 */

/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class AndroidUtil {

    public static Toast mToast;

    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    public static float convertPXToDP(float px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return dp;
    }

    public static void disableEnableView(@NonNull View view) {
        disableEnableView(view, 400);
    }

    public static void disableEnableView(@NonNull View view, long milliSec) {
        if (view != null) {
            view.setEnabled(false);
            SystemClock.sleep(milliSec);
            view.setEnabled(true);
        }
    }

    public static void hideKeyPad(@NonNull Activity activity) {
        if (activity != null) {
            hideKeyPad(activity, activity.getCurrentFocus());
        }
    }

    public static void hideKeyPad(@NonNull Activity activity, @NonNull View view) {
        if (activity != null) {

            if (view != null) {
                InputMethodManager inputMethodManager =
                        (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                if (inputMethodManager != null) {
                    inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(),
                            InputMethodManager.RESULT_UNCHANGED_SHOWN);
                }
            } else {
                activity.getWindow().setSoftInputMode(
                        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

            }

        }
    }

    public static void hideKeyPad(@NonNull Context context, @NonNull View view) {
        if (context != null && view != null) {

            InputMethodManager inputMethodManager =
                    (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (inputMethodManager != null) {
                inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }

        }
    }

    public static void showKeyPad(@NonNull Activity activity, @NonNull EditText view) {
        if (activity != null) {

            Window window = activity.getWindow();
            if (window != null) {
                window.setSoftInputMode(
                        WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
            }

            if (view != null) {
                InputMethodManager inputMethodManager =
                        (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                if (inputMethodManager != null) {
                    inputMethodManager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
                }
            }
        }
    }

    public static boolean hasInternetConnectivity(@NonNull Activity activity) {
        return hasInternetConnectivity(activity, true);
    }

    public static boolean hasInternetConnectivity(@NonNull Context context) {
        return hasInternetConnectivity(context, true);
    }

    public static boolean hasInternetConnectivity(@NonNull Context context,
                                                  boolean shouldShowNoConnectivityMessage) {
        boolean result = false;
        if (context != null) {
            ConnectivityManager connectivityManager = (ConnectivityManager)
                    context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivityManager != null) {
                NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                if (activeNetworkInfo != null) {
                    int activeNetworkType = activeNetworkInfo.getType();
                    result = (activeNetworkType == ConnectivityManager.TYPE_MOBILE
                            || activeNetworkType == ConnectivityManager.TYPE_WIFI
                            || activeNetworkType == ConnectivityManager.TYPE_BLUETOOTH)

                            &&
                            activeNetworkInfo.isConnected();
                }

                if (!result && shouldShowNoConnectivityMessage && context instanceof Activity
                        && !((Activity) context).isFinishing()) {
                    DialogUtil.getInstance().showOkAlertDialog((Activity) context,
                            context.getString(R.string.error_no_internet));
                }

            }
        }

        return result;
    }

    public static void showErrorToast(@NonNull Context context) {
        String errorMessage = "Sorry, something went wrong, please try again ! ";
        showToast(context, errorMessage, false);
    }

    public static void showToast(@NonNull Context context,
                                 @NonNull String message) {
        showToast(context, message, false);
    }

    public static void showToast(@NonNull Context context,
                                 @NonNull String message,
                                 boolean longDuration) {
        if (context != null && !TextUtils.isEmpty(message)) {
            if (mToast != null) {
                mToast.cancel();
            }
            mToast = Toast.makeText(context, message,
                    longDuration ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT);
            mToast.show();
        }
    }

    //logic to dump the intent to check the data inside it
    public static void dumpIntent(String TAG, Intent intent) {
        if (intent != null) {
            Log.w(TAG, "dumpIntent: Intent Component Name = " + intent.getComponent());
            Log.w(TAG, "dumpIntent: Intent Type = " + intent.getType());
            Log.w(TAG, "dumpIntent: Intent Action = " + intent.getAction());
            Log.w(TAG, "dumpIntent: Intent DataString = " + intent.getDataString());
            Log.w(TAG, "dumpIntent: Intent DataUri = " + intent.getData());
            Log.w(TAG, "dumpIntent: Intent Package = " + intent.getPackage());
            Log.w(TAG, "dumpIntent: Intent Scheme = " + intent.getScheme());
            Set<String> intentCategories = intent.getCategories();
            if (intentCategories != null) {
                int i = 0;
                for (String category : intentCategories) {
                    Log.w(TAG, "dumpIntent: Intent Category[ " + (++i) + " ] = " + category);
                }
            }

            dumpBundle(TAG, intent.getExtras());
        }
    }

    //method to dump the bundle to check data inside it
    public static void dumpBundle(String TAG, Bundle bundle) {
        if (bundle != null) {
            for (String key : bundle.keySet()) {
                Log.w(TAG, " KEY : " + key + " : VALUE : " + bundle.get(key));
            }
        }
    }

    //method to check the information of screen
    public static void dumpScreenInfo(Activity activity) {
        if (activity != null) {

            DisplayMetrics displaymetrics = new DisplayMetrics();
            WindowManager windowManager = activity.getWindowManager();
            if (windowManager != null) {
                Display display = windowManager.getDefaultDisplay();
                if (display != null) {
                    display.getMetrics(displaymetrics);
                    Log.w(display.getName(), " SCREEN INFO => " + displaymetrics.toString());
                }
            }
        }
    }

    public static class IntentHelper {

        public static boolean hasAnActivityToStart(@NonNull Context context, @NonNull Intent intent) {
            if (context == null || intent == null) {
                throw new RuntimeException("Illegal Parameters");
            }
            return intent.resolveActivity(context.getPackageManager()) != null;
        }

        public static int activitiesCountForIntent(@NonNull Context context,
                                                   @NonNull Intent intent) {
            if (context == null || intent == null) {
                throw new RuntimeException("Illegal Parameters");
            }
            PackageManager packageManager = context.getPackageManager();
            List activities = packageManager.queryIntentActivities(intent,
                    PackageManager.MATCH_DEFAULT_ONLY);
            if (activities != null) {
                return activities.size();
            }
            return 0;
        }

        public static Intent createChooserIntent(@NonNull Context context, @NonNull Intent intent) {
            if (context == null || intent == null) {
                throw new RuntimeException("Illegal Parameters");
            }
            if (hasAnActivityToStart(context, intent)
                    && activitiesCountForIntent(context, intent) > 1) {
                String title = "Select";
                return Intent.createChooser(intent, title);
            }
            return intent;
        }

        public static Intent getIntentToOpenAlbum() {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            return intent;
        }

        public static Intent getIntentToOpenGalleryImages() {
            Intent galleryIntent
                    = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            galleryIntent.setType("image/*");
            return galleryIntent;
        }

        public static Intent getIntentToOpenGalleryVideos() {
            Intent galleryIntent
                    = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            galleryIntent.setType("video/*");
            return galleryIntent;
        }

        public static Intent getIntentToOpenCamera() {
            Intent intent = new Intent();
            intent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
            return intent;
        }

        public static Intent getIntentToOpenCamera(@NonNull Uri uri) {
            Intent intent = new Intent();
            intent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            return intent;
        }

        public static Intent getIntentToOpenCameraRecorder(@NonNull Uri uri) {
            Intent intent = new Intent();
            intent.setAction(MediaStore.ACTION_VIDEO_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            return intent;
        }

        public static Intent getIntentToOpenCropImage(@NonNull Uri imageUri) {
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            cropIntent.setData(imageUri);
            cropIntent.putExtra("crop", "true");
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            cropIntent.putExtra("outputX", 96);
            cropIntent.putExtra("outputY", 96);
            cropIntent.putExtra("return-data", true);

            return cropIntent;
        }


    }
}
