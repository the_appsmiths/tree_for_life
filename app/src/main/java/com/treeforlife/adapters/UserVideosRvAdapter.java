package com.treeforlife.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.treeforlife.R;
import com.treeforlife.Utils.ImageLoaderUtil;
import com.treeforlife.customview.CustomTextView;
import com.treeforlife.dataobjects.Video;

import java.util.ArrayList;

/**
 * Created by TheAppsmiths on 2/19/2018.
 * Modified by TheAppsmiths on 28/02/2018
 */

public class UserVideosRvAdapter extends RecyclerView.Adapter<UserVideosRvAdapter.ViewHolder> {
    public static String TAG = "UserVideosRvAdapter";

    private final VideoItemCallBack mCallback;
    private LayoutType mLayoutType;
    private ArrayList<Video> mVideosList;


    public UserVideosRvAdapter(@NonNull UserVideosRvAdapter.LayoutType layoutType, VideoItemCallBack callback) {
        this.mLayoutType = layoutType;
        this.mCallback = callback;
        if (this.mLayoutType == null) {
            this.mLayoutType = UserVideosRvAdapter.LayoutType.LINEAR;
        }
        setHasStableIds(true);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new UserVideosRvAdapter.ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(mLayoutType == LayoutType.LINEAR ? R.layout.item_video :
                                R.layout.item_video_grid,
                        parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bindData(position);
    }

    @Override
    public int getItemCount() {
        return (mVideosList != null) ? mVideosList.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void ensureCommentsList() {
        if (this.mVideosList == null) {
            this.mVideosList = new ArrayList<Video>();
        }
    }

    //clear video listing
    public void clearVideosList() {
        if (mVideosList != null) {
            this.mVideosList.clear();
            notifyDataSetChanged();
        }
    }

    //update list to the adapter
    public void update(ArrayList<Video> videosList) {
        ensureCommentsList();
        this.mVideosList.clear();
        if (videosList != null && !videosList.isEmpty()) {
            this.mVideosList.addAll(videosList);
        }
        notifyDataSetChanged();
    }

    //add list to the adapter
    public void addVideosList(ArrayList<Video> videosList) {
        if (videosList != null && !videosList.isEmpty()) {
            ensureCommentsList();
            int startPosition = mVideosList.size();
            this.mVideosList.addAll(videosList);
            notifyItemRangeInserted(startPosition, mVideosList.size());
        }

    }

    public enum LayoutType {
        LINEAR,
        GRID,
    }

    public interface VideoItemCallBack {
        void videoItemClick(int position, Video video);
    }
    //Holder view to show the data in adapter
    public class ViewHolder extends RecyclerView.ViewHolder {

        private final Context mContext;
        private final ImageLoader mImageLoader;

        private final View mItemView;
        private final CustomTextView mVideoTitleTextView;
        private final ImageView mVideoThumbnailImageView, mPlayIconImageView;
        private Video mVideo;

        public ViewHolder(View itemView) {
            super(itemView);
            this.mItemView = itemView;
            mVideoThumbnailImageView = itemView.findViewById(R.id.video_thumbnail_image_view);
            mPlayIconImageView = itemView.findViewById(R.id.play_icon_image_view);
            mVideoTitleTextView = itemView.findViewById(R.id.video_text);

            this.mContext = itemView.getContext();
            this.mImageLoader = ImageLoaderUtil.getLoader(mContext);

        }

        public void bindData(int position) {
            String videoThumbnailUrl = "";
            if (mVideosList != null && mVideosList.size() > position) {
                mVideo = mVideosList.get(position);

                if (mVideo != null) {
                    String text = mVideo.videoDescription;
                    if (TextUtils.isEmpty(text)) {
                        text = "Video";
                    }
                    mVideoTitleTextView.setText(text);
                    videoThumbnailUrl = mVideo.videoThumbnail;
                }
            }

            if (!TextUtils.isEmpty(videoThumbnailUrl)) {
                mImageLoader.displayImage(videoThumbnailUrl,
                        mVideoThumbnailImageView, ImageLoaderUtil.getDefaultImageOption());
            } else {
                mVideoThumbnailImageView.setImageResource(R.drawable.img_place_holder);
            }
            setListeners(position);

        }


        private void setListeners(final int position) {

            mItemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mCallback != null) {
                        mCallback.videoItemClick(position, mVideo);
                    }
                }
            });

        }

    }

}
