package com.treeforlife.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.treeforlife.R;
import com.treeforlife.Utils.AndroidUtil;
import com.treeforlife.Utils.FragmentUtil;
import com.treeforlife.Utils.ImageLoaderUtil;
import com.treeforlife.commons.Constants;
import com.treeforlife.commons.SharedPrefHelper;
import com.treeforlife.customview.CustomTextView;
import com.treeforlife.dataobjects.TFLUser;

/**
 * Created by TheAppsmiths on 3/23/2018.
 */

/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class MyProfileFragment extends Fragment implements View.OnClickListener {

    public static final String TAG = "MyProfileFragment";

    private Context mContext;
    private Activity mActivity;
    private Bundle mBundle;

    private CustomTextView mProfileNameTextView, mProfileEmailTextView, mProfilePhoneTextView,
            mEmailTextView, mStreetTextView, mCityTextView, mStateTextView, mCountryTextView, mPhoneTextView;
    private ImageView mProfileImageView;

    private BroadcastReceiver mProfileUpdatedReceiver;

    private ImageLoader mImageLoader;
    private SharedPrefHelper mSharedPrefHelper;

    private TFLUser mTflUser;

    private boolean isProfileUpdateReceiverRegistered;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
        this.mActivity = getActivity();
        this.mBundle = getArguments();

        this.mProfileUpdatedReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                afterReceivingBroadcast(context, intent);
            }
        };

        this.mImageLoader = ImageLoaderUtil.getLoader(mContext);
        this.mSharedPrefHelper = SharedPrefHelper.getInstance();

        extractDataFromBundle();
        ensureTflUser();
        registerLocalReceiver();

    }

    private void extractDataFromBundle() {
        if (mBundle == null) {
            mBundle = getArguments();
        }

        if (mBundle != null) {
            mTflUser = mBundle.getParcelable(Constants.KEY_TFL_USER);
        }
    }

    private void ensureTflUser() {
        if (mTflUser == null) {
            mTflUser = mSharedPrefHelper.getLoginResponse(mContext);
        }
    }

    private void registerLocalReceiver() {
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(mActivity);
        if (localBroadcastManager != null) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(Constants.ACTION_USER_PROFILE_UPDATED);
            localBroadcastManager.registerReceiver(mProfileUpdatedReceiver, intentFilter);
            isProfileUpdateReceiverRegistered = true;

        }
    }

    private void unRegisterLocalReceiver() {
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(mActivity);
        if (localBroadcastManager != null && isProfileUpdateReceiverRegistered) {
            localBroadcastManager.unregisterReceiver(mProfileUpdatedReceiver);
            isProfileUpdateReceiverRegistered = false;

        }
    }

    private void afterReceivingBroadcast(Context context, Intent intent) {
        AndroidUtil.dumpIntent(TAG, intent);
        if (intent != null) {
            String mIntentAction = intent.getAction();

            if (!TextUtils.isEmpty(mIntentAction)) {

                switch (mIntentAction) {
                    case Constants.ACTION_USER_PROFILE_UPDATED:
                        mTflUser = intent.getParcelableExtra(Constants.KEY_TFL_USER);
                        if (mTflUser == null) {
                            mTflUser = mSharedPrefHelper.getLoginResponse(mContext);
                            Log.e(TAG, "afterReceivingBroadcast: FETCHING DATA FROM LOCAL DB");
                        }
                        setData();
                        break;
                }
            }
        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_myprofile, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        intiComponent(view);
        setData();
    }


    private void intiComponent(View view) {
        if (view != null) {

            mProfileImageView = view.findViewById(R.id.profile_image_view);
            mProfileImageView.setOnClickListener(this);

            mProfileNameTextView = view.findViewById(R.id.profile_name_text_view);
            mProfileEmailTextView = view.findViewById(R.id.profile_email_text_view);
            mProfilePhoneTextView = view.findViewById(R.id.profile_phone_text_view);

            mEmailTextView = view.findViewById(R.id.email_text_view);
            mCityTextView = view.findViewById(R.id.city_text_view);
            mStreetTextView = view.findViewById(R.id.street_text_view);
            mStateTextView = view.findViewById(R.id.state_text_view);
            mCountryTextView = view.findViewById(R.id.country_text_view);
            mPhoneTextView = view.findViewById(R.id.phone_text_view);

            View mEditProfileView = view.findViewById(R.id.edit_profile_view);
            mEditProfileView.setOnClickListener(this);

        }

    }

    private void setData() {
        if (mProfileNameTextView != null) {
            String email = "", phone = "", userProfileName = "", street = "", city = "", state = "",
                    country = "";

            if (mTflUser != null) {

                mImageLoader.displayImage(mTflUser.profileImage, mProfileImageView,
                        ImageLoaderUtil.getDefaultProfileImageOption());

                email = mTflUser.email;
                phone = mTflUser.phone;
                street = mTflUser.street;
                city = mTflUser.city;
                state = mTflUser.state;
                country = mTflUser.addressCountryName;
                userProfileName = mTflUser.firstName;
                if (!TextUtils.isEmpty(mTflUser.lastName)) {
                    userProfileName += " " + mTflUser.lastName;
                }

                /*String address = CommonAppMethods.getEmoticonText(mTflUser.address);
                if (!TextUtils.isEmpty(address)) {
                    String[] data = address.split(",");

                    for (int i = 0; i < data.length; i++) {
                        data[i] = CommonAppMethods.removeStartingSpaces(data[i]);
                    }

                    int index = data.length - 1;
                    if (index >= 0 && data.length > index) {
                        country = data[index];
                    }

                    index -= 1;
                    if (index >= 0 && data.length > index) {
                        state = data[index];
                    }

                    StringBuilder cityName = new StringBuilder();
                    for (int i = 0; i < index; i++) {
                        if (data.length > i) {
                            if (i == 0) {
                                cityName.append(data[i]);
                            } else {
                                cityName.append(",").append(data[i]);
                            }
                        }
                    }
                    city = cityName.toString();

                }*/

            } else {
                mProfileImageView.setImageResource(R.drawable.dummy_profile_image);
            }

            if (TextUtils.isEmpty(email)) {
                email = "No email ";
            }
            mProfileEmailTextView.setText(email);

            if (TextUtils.isEmpty(phone)) {
                phone = "No phone number ";
            }
            mProfilePhoneTextView.setText(phone);

            if (TextUtils.isEmpty(userProfileName)) {
                userProfileName = "No Name";
            }

            mProfileNameTextView.setText(userProfileName);

            mEmailTextView.setText(email);
            mPhoneTextView.setText(phone);

            if (TextUtils.isEmpty(city)) {
                city = getString(R.string.n_a);
            }
            mCityTextView.setText(city);
            if (TextUtils.isEmpty(street)) {
                street = getString(R.string.n_a);
            }
            mStreetTextView.setText(street);

            if (TextUtils.isEmpty(state)) {
                state = getString(R.string.n_a);
            }
            mStateTextView.setText(state);

            if (TextUtils.isEmpty(country)) {
                country = getString(R.string.n_a);
            }
            mCountryTextView.setText(country);


        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.profile_image_view:
                showProfileImage();
                break;

            case R.id.edit_profile_view:
                Bundle bundle = new Bundle();
                bundle.putParcelable(Constants.KEY_TFL_USER, mTflUser);
                FragmentUtil.loadFragment((AppCompatActivity) mActivity,
                        FragmentUtil.FRAGMENT_EDIT_PROFILE, bundle, true);
                break;
        }
    }

    private void showProfileImage() {
        String imageUriOrUrl = "";

        if (mTflUser != null) {
            imageUriOrUrl = mTflUser.profileImage;
        }

        if (!TextUtils.isEmpty(imageUriOrUrl)) {
            FragmentManager fragmentManager = getChildFragmentManager();

            if (fragmentManager != null) {
                Bundle bundle = new Bundle();
                bundle.putString(Constants.KEY_CALLER_COMPONENT, TAG);
                bundle.putString(Constants.KEY_IMAGE_URL, imageUriOrUrl);

                ImageViewDialogFragment mImageViewDialogFragment;
                Fragment fragment =
                        fragmentManager.findFragmentByTag(ImageViewDialogFragment.TAG);
                if (fragment != null && fragment instanceof ImageViewDialogFragment) {
                    mImageViewDialogFragment = (ImageViewDialogFragment) fragment;
                    mImageViewDialogFragment.update(bundle);
                } else {
                    mImageViewDialogFragment = new ImageViewDialogFragment();
                    mImageViewDialogFragment.setArguments(bundle);
                }

                mImageViewDialogFragment.show(fragmentManager, ImageViewDialogFragment.TAG);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mActivity != null) {
            mActivity.setTitle(R.string.my_profile);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unRegisterLocalReceiver();
    }
}
