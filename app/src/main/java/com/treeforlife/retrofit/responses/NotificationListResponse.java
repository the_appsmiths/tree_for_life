package com.treeforlife.retrofit.responses;

import com.google.gson.annotations.SerializedName;
import com.treeforlife.dataobjects.Notification;

import java.util.ArrayList;

/**
 * Created by TheAppsmiths on 4/24/2018.
 */

public class NotificationListResponse extends WebServiceResponse {

    @SerializedName("data")
    public ArrayList<Notification> notificationList=new ArrayList<Notification>();

    @SerializedName("total_records")
    public int totalRecords;

}
