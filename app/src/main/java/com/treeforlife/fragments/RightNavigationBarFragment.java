package com.treeforlife.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.treeforlife.R;
import com.treeforlife.Utils.ImageLoaderUtil;
import com.treeforlife.adapters.RightNavRvAdapter;
import com.treeforlife.commons.SharedPrefHelper;
import com.treeforlife.customview.CustomTextView;
import com.treeforlife.dataobjects.TFLUser;

/**
 * Created by TheAppsmiths on 3/16/2018.
 */
/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class RightNavigationBarFragment extends Fragment implements View.OnClickListener {

    public static final String TAG = "RightNavigationBarFragment";

    public static final int PROFILE_POSITION = -1;
    private Context mContext;
    private Activity mActivity;

    private RecyclerView mRecycleView;
    private View mProfile;
    private CustomTextView mTflUserNameTextView, mTflUerTreeIdTextView;
    private ImageView mTflUserImageView;

    private RightNavRvAdapter rightNavRvAdapter;
    private RightNavRvAdapter.NavItemCallBack navItemCallBack;

    private ImageLoader mImageLoader;
    private SharedPrefHelper mSharedPrefHelper;

    private TFLUser mTflUser;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
        this.mActivity = getActivity();
        if (mActivity instanceof RightNavRvAdapter.NavItemCallBack) {
            navItemCallBack = (RightNavRvAdapter.NavItemCallBack) mActivity;
        }
        this.rightNavRvAdapter = new RightNavRvAdapter(navItemCallBack);
        this.mImageLoader = ImageLoaderUtil.getLoader(mContext);
        this.mSharedPrefHelper = SharedPrefHelper.getInstance();
        ensureTflUser();
    }

    private void ensureTflUser() {
        if (mTflUser == null) {
            mTflUser = mSharedPrefHelper.getLoginResponse(mContext);
        }
    }

    public void update(TFLUser tflUser) {
        this.mTflUser = tflUser;
        setData();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_right_nav_bar, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initWidgets(view);
        setData();
    }

    private void initWidgets(View view) {

        if (view != null) {

            mRecycleView = view.findViewById(R.id.rv_right_nav_items);
            mRecycleView.setLayoutManager(new LinearLayoutManager(mContext));
            mRecycleView.setAdapter(rightNavRvAdapter);

            mProfile = view.findViewById(R.id.profile_view);
            mProfile.setOnClickListener(this);

            mTflUserImageView = view.findViewById(R.id.tfl_user_image_view);
            mTflUserNameTextView = view.findViewById(R.id.tfl_user_name_text_view);
            mTflUerTreeIdTextView = view.findViewById(R.id.tfl_user_tree_id_text_view);
        }

    }

    private void setData() {
        if (mTflUserNameTextView != null) {
            String userName = "", userTreeId = "Tree ID ";
            if (mTflUser != null) {
                userName = mTflUser.firstName;
                if (!TextUtils.isEmpty(mTflUser.lastName)) {
                    userName += " " + mTflUser.lastName;
                }
                if (!TextUtils.isEmpty(mTflUser.treeId)) {
                    userTreeId += " " + mTflUser.treeId;
                }
                mImageLoader.displayImage(mTflUser.profileImage, mTflUserImageView,
                        ImageLoaderUtil.getDefaultProfileImageOption());
            }

            mTflUserNameTextView.setText(userName);
            mTflUerTreeIdTextView.setText(userTreeId);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.profile_view:
                if (navItemCallBack != null) {
                    navItemCallBack.clickItem(v, PROFILE_POSITION);
                }
                break;
        }
    }
}
