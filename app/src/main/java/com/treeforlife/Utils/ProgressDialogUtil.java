package com.treeforlife.Utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.support.annotation.NonNull;

/**
 * copy from prevoious work
 */
/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class ProgressDialogUtil {
    private static ProgressDialogUtil sProgressDialogUtil;
    private ProgressDialog mProgressDialog;

    private ProgressDialogUtil() {

    }

    public static ProgressDialogUtil getInstance() {
        if (sProgressDialogUtil == null) {
            sProgressDialogUtil = new ProgressDialogUtil();
        }
        return sProgressDialogUtil;
    }

    public void showProgressDialog(@NonNull Activity activity) {
        showProgressDialog(activity, "Please wait ...");
    }

    public void showProgressDialog(@NonNull Activity activity,
                                   @NonNull String progressMessage) {
        try {
            dismissProgressDialog();
            if (activity != null && !activity.isFinishing()) {
                mProgressDialog = new ProgressDialog(activity);
                mProgressDialog.setMessage(progressMessage);
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    public void dismissProgressDialog() {
        try {
            if (mProgressDialog != null
                    && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
