package com.treeforlife.commons;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.treeforlife.Utils.NotificationsUtil;
import com.treeforlife.Utils.SharedPreferencesUtil;
import com.treeforlife.dataobjects.TFLUser;

import static com.treeforlife.Utils.SharedPreferencesUtil.DEFAULT_INT;

/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class SharedPrefHelper {
    private static final String KEY_PREFIX = "shared_pref_tfl";
    private static final String KEY_LOGIN_RESPONSE = KEY_PREFIX + "LOGIN_RESPONSE";
    private static final String KEY_FCM_INSTANCE_ID = KEY_PREFIX + "FCM_INSTANCE_ID";
    private static final String KEY_FCM_INSTANCE_ID_TOKEN = KEY_PREFIX + "FCM_INSTANCE_TOKEN";
    private static final String KEY_FIRST_TIME_REGISTER = KEY_PREFIX + "FIRST_TIME_REGISTER";
    private static final String KEY_LOGIN_EMAIL = KEY_PREFIX + "KEY_LOGIN_EMAIL";
    private static final String KEY_PUSH_CHANNEL = KEY_PREFIX + "KEY_PUSH_CHANNEL";
    private static final String KEY_LOGIN_PASSWORD = KEY_PREFIX + "KEY_LOGIN_PASSWORD";
    private static final String KEY_NOTIFICATION_COUNT = KEY_PREFIX + "KEY_NOTIFICATION_COUNT";

    private static SharedPrefHelper sharedPrefHelper;
    private SharedPreferencesUtil sharedPreferencesUtil;


    private SharedPrefHelper() {
        sharedPreferencesUtil = SharedPreferencesUtil.getInstance();
    }

    public static SharedPrefHelper getInstance() {
        if (sharedPrefHelper == null) {
            sharedPrefHelper = new SharedPrefHelper();
        }
        return sharedPrefHelper;
    }


    public void setLoginResponse(@NonNull Context context, TFLUser loginResponse) {
        String responseString = "";
        if (loginResponse != null) {
            responseString = new Gson().toJson(loginResponse);
        }

        sharedPreferencesUtil.writeStringToSharedPreference(context, KEY_LOGIN_RESPONSE, responseString);
    }

    public TFLUser getLoginResponse(@NonNull Context context) {
        String spritzUserJsonString = sharedPreferencesUtil.readStringFromSharedPreference(
                context, KEY_LOGIN_RESPONSE);

        return new Gson().fromJson(spritzUserJsonString, TFLUser.class);
    }

    public void setFcmInstanceId(@NonNull Context context, String fcmInstanceId) {
        sharedPreferencesUtil.writeStringToSharedPreference(context, KEY_FCM_INSTANCE_ID, fcmInstanceId);
    }

    public String getFcmInstanceId(@NonNull Context context) {
        return sharedPreferencesUtil.readStringFromSharedPreference(context, KEY_FCM_INSTANCE_ID);
    }

    public void setFcmInstanceIdToken(@NonNull Context context, String fcmInstanceIdToken) {
        sharedPreferencesUtil.writeStringToSharedPreference(context, KEY_FCM_INSTANCE_ID_TOKEN, fcmInstanceIdToken);
    }

    public String getFcmInstanceIdToken(@NonNull Context context) {
        return sharedPreferencesUtil.readStringFromSharedPreference(context, KEY_FCM_INSTANCE_ID_TOKEN);
    }

    public void setFirstTimeRegister(@NonNull Context context, String registerType) {
        sharedPreferencesUtil.writeStringToSharedPreference(context, KEY_FIRST_TIME_REGISTER, registerType);
    }

    public String getFirstTimeRegister(@NonNull Context context) {
        return sharedPreferencesUtil.readStringFromSharedPreference(context, KEY_FIRST_TIME_REGISTER);
    }

    public void setLoginEmail(@NonNull Context context, String email) {
        sharedPreferencesUtil.writeStringToSharedPreference(context, KEY_LOGIN_EMAIL, email);
    }

    public void setPushChannel(@NonNull Context context, String channel) {
        sharedPreferencesUtil.writeStringToSharedPreference(context, KEY_PUSH_CHANNEL, channel);
    }

    public String getPushChannel(@NonNull Context context) {
        return sharedPreferencesUtil.readStringFromSharedPreference(context, KEY_PUSH_CHANNEL);
    }

    public String getLoginEmail(@NonNull Context context) {
        return sharedPreferencesUtil.readStringFromSharedPreference(context, KEY_LOGIN_EMAIL);
    }

    public void setLoginPassword(@NonNull Context context, String password) {
        sharedPreferencesUtil.writeStringToSharedPreference(context, KEY_LOGIN_PASSWORD, password);
    }

    public String getLoginPassword(@NonNull Context context) {
        return sharedPreferencesUtil.readStringFromSharedPreference(context, KEY_LOGIN_PASSWORD);
    }

    public void setNotificationCount(@NonNull Context context, int notificationCount) {
        sharedPreferencesUtil.writeIntToSharedPreference(context, KEY_NOTIFICATION_COUNT, notificationCount);
    }

    public void resetNotificationCount(@NonNull Context context) {
        if (!sharedPreferencesUtil.removeKeyItem(context, KEY_NOTIFICATION_COUNT)) {
            setNotificationCount(context, 0);
        }
    }

    public int getNotificationCount(@NonNull Context context) {
        return sharedPreferencesUtil.readIntFromSharedPreference(context, KEY_NOTIFICATION_COUNT);
    }

    public void increaseNotificationCount(@NonNull Context context) {
        int count = sharedPreferencesUtil.readIntFromSharedPreference(context, KEY_NOTIFICATION_COUNT);
        setNotificationCount(context, count >= 0 ? count + 1 : count == DEFAULT_INT ? 1 : 0);

    }

    public void decreaseNotificationCount(@NonNull Context context) {
        int count = sharedPreferencesUtil.readIntFromSharedPreference(context, KEY_NOTIFICATION_COUNT);
        if (count > 0) {
            setNotificationCount(context, count - 1);
        } else {
            resetNotificationCount(context);
        }
    }

    public boolean resetEmailPassword(@NonNull Context context) {
        return sharedPreferencesUtil.removeKeyItem(context, KEY_LOGIN_EMAIL)
                && sharedPreferencesUtil.removeKeyItem(context, KEY_LOGIN_PASSWORD);
    }

    public boolean resetData(@NonNull Context context) {
        return sharedPreferencesUtil.removeKeyItem(context, KEY_LOGIN_RESPONSE)
                && sharedPreferencesUtil.removeKeyItem(context, KEY_FCM_INSTANCE_ID)
                && sharedPreferencesUtil.removeKeyItem(context, KEY_FCM_INSTANCE_ID_TOKEN)
                && sharedPreferencesUtil.removeKeyItem(context, KEY_NOTIFICATION_COUNT)
                && NotificationsUtil.removeAllNotifications(context);
    }

    public boolean resetFirstTimeRegister(@NonNull Context context) {
        return sharedPreferencesUtil.removeKeyItem(context, KEY_FIRST_TIME_REGISTER);
    }

}
