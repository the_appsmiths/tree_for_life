package com.treeforlife.Utils;

import android.graphics.Bitmap;
import android.media.ExifInterface;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.io.File;

/**
 * Copied from OD BOGO USER App
 */
/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class ImageRotationListener implements ImageLoadingListener {

    @Override
    public void onLoadingStarted(String imageUri, View view) {
        Log.e("Started", "Image Loading has been started");
    }

    @Override
    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
    }

    @Override
    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
        try {
            if (loadedImage != null) {
                File file = ImageLoader.getInstance().getDiskCache().get(imageUri);
                if (file == null) {
                    return;
                }
                int rotation = getCameraPhotoOrientation(file);
                Bitmap bitmap = null;
                if (rotation != 0) {
                    bitmap = resizeBitmap(loadedImage, 1000, 1000);
                    if (bitmap != null) {
                        if (view instanceof ImageView) {
                            ((ImageView) view).setImageBitmap(bitmap);
                        }
                    }
                }
                view.setBackgroundDrawable(null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLoadingCancelled(String imageUri, View view) {
    }

    public Bitmap resizeBitmap(Bitmap input, int destWidth, int destHeight) {
        int srcWidth = input.getWidth();
        int srcHeight = input.getHeight();
        boolean needsResize = false;
        float p;
        if (srcWidth > destWidth || srcHeight > destHeight) {
            needsResize = true;
            if (srcWidth > srcHeight && srcWidth > destWidth) {
                p = (float) destWidth / (float) srcWidth;
                destHeight = (int) (srcHeight * p);
            } else {
                p = (float) destHeight / (float) srcHeight;
                destWidth = (int) (srcWidth * p);
            }
        } else {
            destWidth = srcWidth;
            destHeight = srcHeight;
        }
        if (needsResize) {
            Bitmap output = Bitmap.createScaledBitmap(input, destWidth, destHeight, true);
            return output;
        } else {
            return input;
        }
    }

    public int getCameraPhotoOrientation(File imageFile) throws Exception {
        int rotate = 0;
        try {
            if (imageFile != null && imageFile.exists()) {
                ExifInterface exif = new ExifInterface(imageFile.getAbsolutePath());
                int orientation = Integer.parseInt(exif.getAttribute(ExifInterface.TAG_ORIENTATION));
                switch (orientation) {
                    case ExifInterface.ORIENTATION_NORMAL:
                        rotate = 0;
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_270:
                        rotate = 270;
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_180:
                        rotate = 180;
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_90:
                        rotate = 90;
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rotate;
    }
}