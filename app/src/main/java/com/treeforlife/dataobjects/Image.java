package com.treeforlife.dataobjects;

import com.google.gson.annotations.SerializedName;

/**
 * Created by TheAppsmiths on 4/12/2018.
 * @author TheAppsmiths
 */

public class Image {

    @SerializedName("image_id")
    public int imageId;
    @SerializedName("image_url")
    public String imageUrl;
}
