package com.treeforlife.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.treeforlife.R;
import com.treeforlife.Utils.AndroidUtil;
import com.treeforlife.Utils.DateAndTimeUtil;
import com.treeforlife.Utils.DialogUtil;
import com.treeforlife.Utils.FontsUtil;
import com.treeforlife.commons.Constants;
import com.treeforlife.commons.ListItemCallback;
import com.treeforlife.commons.SharedPrefHelper;
import com.treeforlife.controller.TreeApisController;
import com.treeforlife.customview.CustomTextView;
import com.treeforlife.dataobjects.TFLUser;
import com.treeforlife.retrofit.ResponseId;
import com.treeforlife.retrofit.responses.TreeInfoOfYearResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * @link : https://github.com/PhilJay/MPAndroidChart
 * @link : https://github.com/PhilJay/MPAndroidChart/wiki/Getting-Started
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */

public class TreeInfoOfYearGraphicalActivity extends AppCompatActivity implements
        View.OnClickListener,
        ListItemCallback,
        TreeApisController.TreeInfoOfYearApiResultCallback {

    private static final String TAG = "TreeInfoOfYearGraphicalActivity";
    private static final int RC_SELECT_YEAR = 1001;
    private static final int START_YEAR = 2018;


    private Intent mActivityIntent;

    private Toolbar mToolbar;
    private ActionBar mActionBar;

    private CustomTextView mToolbarTitleTextView;
    private CustomTextView mSelectedYearTextView;
    private BarChart mBarChart;

    private SharedPrefHelper mSharedPrefHelper;
    private DialogUtil mDialogUtil;
    private TreeApisController mTreeApisController;

    private Handler mClickHandler;
    private Runnable mClickAction;

    private TFLUser mTflUser;
    private TreeInfoOfYearResponse.TreeInfoOfYear mTreeInfoOfYear;
    private ArrayList<String> mYearsList;
    private ArrayList<String> monthsList;

    private String mSelectedYear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tree_graphical);
        initComponents();
        initToolBar();
        initWidgets();
        loadData();

    }

    private void initComponents() {
        mClickHandler = new Handler();
        mActivityIntent = getIntent();
        mSharedPrefHelper = SharedPrefHelper.getInstance();
        mDialogUtil = DialogUtil.getInstance();
        if (mActivityIntent != null) {
            mTflUser = mActivityIntent.getParcelableExtra(Constants.KEY_TFL_USER);
        }
        ensureTFLUser();

        mTreeApisController = new TreeApisController(TAG, null, this, this,
                mTflUser);
        mTreeApisController.setYearlyValueOfTreeApiResultCallback(this);

        mYearsList = new ArrayList<>();
        for (int i = START_YEAR; i <= DateAndTimeUtil.getCurrentYear(); i++) {
            mYearsList.add(String.valueOf(i));
        }
    }

    private void ensureTFLUser() {
        if (mTflUser == null) {
            mTflUser = mSharedPrefHelper.getLoginResponse(this);
        }

    }

    private void initToolBar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        if (mToolbar != null) {
            mToolbarTitleTextView = mToolbar.findViewById(R.id.toolbar_title);
            ImageView mToolbarIconLhs = mToolbar.findViewById(R.id.toolbar_icon_back);
            mToolbarIconLhs.setOnClickListener(this);
            ImageView mToolbarIconRhs = mToolbar.findViewById(R.id.toolbar_icon_hamberg);
            mToolbarIconRhs.setVisibility(View.GONE);
        }

        setSupportActionBar(mToolbar);
        mActionBar = getSupportActionBar();

        setTitle(R.string.graphical_report);


    }

    @Override
    public void setTitle(CharSequence title) {
        setTitleToolBar(title);
    }

    private void setTitleToolBar(CharSequence title) {
        if (mToolbarTitleTextView != null) {
            mToolbarTitleTextView.setText(title);
        }

    }

    private void initWidgets() {
        mSelectedYearTextView = findViewById(R.id.selected_year_text_view);
        mSelectedYearTextView.setText(String.valueOf(DateAndTimeUtil.getCurrentYear()));

        View mSelectYearView = findViewById(R.id.select_year_view);
        mSelectYearView.setOnClickListener(this);

        mBarChart = findViewById(R.id.bar_chart);
        mBarChart.setClickable(false);
        mBarChart.setPinchZoom(false);
        mBarChart.setTouchEnabled(false);
        mBarChart.setNoDataTextColor(ContextCompat.getColor(this,
                R.color.appColor_707070_black));
        mBarChart.setNoDataTextTypeface(FontsUtil.getTypeface(this,
                FontsUtil.FontName.ROBOTO_REGULAR_WEBFONT));

        Description description = mBarChart.getDescription();
        if (description != null) {
            description.setEnabled(false);
        }
        XAxis xAxis = mBarChart.getXAxis();
        if (xAxis != null) {
            xAxis.setAxisMaximum(11);
            xAxis.setAxisMinimum(0);
            xAxis.setLabelCount(12, true);
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
            xAxis.setValueFormatter(new IndexAxisValueFormatter(new String[]{
                    "Jan",
                    "Feb",
                    "Mar",
                    "Apr",
                    "May",
                    "Jun",
                    "Jul",
                    "Aug",
                    "Sep",
                    "Oct",
                    "Nov",
                    "Dec"
            }));
        }

        YAxis rightAxis = mBarChart.getAxisRight();
        if (rightAxis != null) {
            rightAxis.setEnabled(false);
        }


    }

    private void updateYAxisMaxCount(int value) {
        if (mBarChart != null) {
            int temp = value % 50;
            int multiplier = value / 50;
            if (temp > 0) {
                multiplier++;
            }
            value = (multiplier > 2) ? multiplier * 50 : 100;

            mBarChart.setMaxVisibleValueCount(value);
            YAxis leftAxis = mBarChart.getAxisLeft();
            if (leftAxis != null) {
                leftAxis.setAxisMaximum(value);
                leftAxis.setAxisMinimum(0);
            }
        }
    }

    private void loadData() {
        resetGraphData();
        mSelectedYear = mSelectedYearTextView.getText().toString().trim();
        if (mTreeApisController != null) {
            mTreeApisController.getTreeInfoOfYear(mSelectedYear);
        }
    }

    //Written code for showing the dummy data
    /*private void setDummyData() {
        TreeInfoOfYearResponse.TreeInfoOfYear treeInfoOfYear = new TreeInfoOfYearResponse.TreeInfoOfYear();
        treeInfoOfYear.year = mSelectedYear;
        ArrayList<TreeInfoOfYearResponse.TreeInfoOfMonth> list = new ArrayList<>();
        for (int i = 0; i < 12; i++) {
            TreeInfoOfYearResponse.TreeInfoOfMonth x = new TreeInfoOfYearResponse.TreeInfoOfMonth();
            x.totalOmPointsOfMonth = (i + 1) * 8;
            x.month = i + 1;
            list.add(x);
        }
        treeInfoOfYear.monthWiseTreeDataList = list;
        onTreeInfoOfYearApiResult(true, ResponseId.GET_TREE_INFO_OF_YEAR, treeInfoOfYear);
    }*/

    private void resetGraphData() {
        if (mBarChart != null) {
            mBarChart.setData(null);
            mBarChart.invalidate();
        }

    }

    private void displayContent(int startMonthIndex) {
        if (mTreeInfoOfYear != null) {
            if (!TextUtils.isEmpty(mTreeInfoOfYear.year)
                    && mTreeInfoOfYear.year.equals(mSelectedYear)) {

                if (mTreeInfoOfYear.monthWiseTreeDataList != null
                        && mTreeInfoOfYear.monthWiseTreeDataList.size() > startMonthIndex) {

                    TreeInfoOfYearResponse.TreeInfoOfMonth treeInfoOfMonth
                            = mTreeInfoOfYear.monthWiseTreeDataList.get(startMonthIndex);

                    if (treeInfoOfMonth != null) {
                        startMonthIndex = treeInfoOfMonth.month;
                        if (startMonthIndex <= 0) {
                            startMonthIndex++;
                        }
                        List<Entry> entries = new ArrayList<Entry>();
                        List<BarEntry> barEntries = new ArrayList<BarEntry>();
                        int lastMonth = 12, maxOmPointsOfMonth = 0;
                        if (!TextUtils.isEmpty(mSelectedYear)
                                && mSelectedYear.equals(String.valueOf(DateAndTimeUtil.getCurrentYear()))) {
                            lastMonth = DateAndTimeUtil.getCurrentMonth();
                        }
                        int tempIndex = 0;
                        for (int monthIndex = startMonthIndex - 1; monthIndex < lastMonth; monthIndex++) {

                            treeInfoOfMonth = null;

                            if (mTreeInfoOfYear.monthWiseTreeDataList.size() > tempIndex) {
                                treeInfoOfMonth = mTreeInfoOfYear.monthWiseTreeDataList.get(tempIndex);
                            }

                            if (treeInfoOfMonth != null && treeInfoOfMonth.month == monthIndex + 1) {
                                entries.add(new Entry(monthIndex, treeInfoOfMonth.totalOmPointsOfMonth));
                                barEntries.add(new BarEntry(monthIndex, treeInfoOfMonth.totalOmPointsOfMonth));
                                if (treeInfoOfMonth.totalOmPointsOfMonth > maxOmPointsOfMonth) {
                                    maxOmPointsOfMonth = treeInfoOfMonth.totalOmPointsOfMonth;
                                }
                                tempIndex++;
                            } else {
                                entries.add(new Entry(monthIndex, 0));
                                barEntries.add(new BarEntry(monthIndex, 0));
                            }

                        }


                        updateYAxisMaxCount(maxOmPointsOfMonth);

                        BarDataSet dataSet = new BarDataSet(barEntries, getString(R.string.om_points));
                        BarData lineData = new BarData(dataSet);
                        mBarChart.setData(lineData);
                        mBarChart.invalidate();

                    } else {
                        displayContent(startMonthIndex + 1);
                    }


                } else {
                    AndroidUtil.showToast(this,
                            "No data found for selected year ( " + mSelectedYear + " ). ");
                }

            } else {
                mDialogUtil.showErrorDialog(this,
                        "It couldn't be verified whether the received data belongs to" +
                                " the selected year (" + mSelectedYear + " ).");
            }
        }


    }

    @Override
    public void onClick(View view) {

        if (mClickAction != null) {
            mClickHandler.removeCallbacks(mClickAction);
        }

        if (view != null) {

            switch (view.getId()) {

                case R.id.toolbar_icon_back:
                    mClickAction = new Runnable() {
                        @Override
                        public void run() {
                            onBackPressed();
                            mClickAction = null;
                        }
                    };
                    break;

                case R.id.select_year_view:
                    mClickAction = new Runnable() {
                        @Override
                        public void run() {
                            showSelectYearDialog();
                            mClickAction = null;
                        }
                    };
                    break;
            }
        }

        if (mClickAction != null && mClickHandler != null) {
            mClickHandler.postDelayed(mClickAction, 200);
        }
    }

    private void showSelectYearDialog() {
        mDialogUtil.showAlertList(this, "Select Year", mYearsList, RC_SELECT_YEAR,
                this);
    }


    @Override
    public void onItemClicked(int which, String datum, int requestCode) {
        switch (requestCode) {

            case RC_SELECT_YEAR:
                mSelectedYearTextView.setText(datum);
                loadData();
                break;
        }
    }

    @Override
    public void onTreeInfoOfYearApiResult(boolean success, ResponseId responseId,
                                          TreeInfoOfYearResponse.TreeInfoOfYear treeInfoOfYear) {
        switch (responseId) {
            case GET_TREE_INFO_OF_YEAR:
                this.mTreeInfoOfYear = treeInfoOfYear;
                displayContent(0);
                break;
        }

    }


}
