package com.treeforlife.retrofit.responses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by TheAppsmiths on 4/12/2018.
 */

public class GetWishResponse extends WebServiceResponse {
    @SerializedName("wish_form_status")
    public String wishStatus;
    @SerializedName("wish")
    public String userWish;
}
