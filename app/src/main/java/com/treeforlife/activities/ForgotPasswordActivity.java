package com.treeforlife.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.treeforlife.R;
import com.treeforlife.Utils.AndroidUtil;
import com.treeforlife.Utils.DialogUtil;
import com.treeforlife.Utils.ProgressDialogUtil;
import com.treeforlife.Utils.ValidationUtil;
import com.treeforlife.retrofit.ResponseId;
import com.treeforlife.retrofit.RetrofitManager;
import com.treeforlife.retrofit.RetrofitResponseValidator;
import com.treeforlife.retrofit.responses.WebServiceResponse;

import java.net.SocketTimeoutException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener, RetrofitResponseValidator.ValidationListener {
    public static final String TAG="ForgotPasswordActivity";
    private EditText mEmailEditText;
    private Button mSubmit;
    private String mEmail,mPhoneNumber;
    private ProgressDialogUtil mProgressDialogUtil;
    private DialogUtil mDialogUtil;
    private Toolbar mToolbar;
    private TextView mToolbarTitle,mRegisterTextView;
    private ImageView mToolbarIconRhs,mToolbarIconLhs;
    private ActionBar mActionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        initWidgets();
        initToolBar();
    }

    //method to initialize the toolbar
    private void initToolBar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        if (mToolbar != null) {
            mToolbarTitle = mToolbar.findViewById(R.id.toolbar_title);
            mToolbarIconLhs = mToolbar.findViewById(R.id.toolbar_icon_back);
            mToolbarIconLhs.setOnClickListener(this);

            mToolbarIconRhs = mToolbar.findViewById(R.id.toolbar_icon_hamberg);
            mToolbarIconRhs.setVisibility(View.GONE);

        }
        setSupportActionBar(mToolbar);
        mActionBar = getSupportActionBar();

        setToolBarTitle();
    }

    //method to set the toolbar title
    private void setToolBarTitle() {
        if (mToolbarTitle != null) {
            mToolbarTitle.setText("Forgot Password");
        }
    }
    //method to initialize the widgets
    private void initWidgets() {
        mProgressDialogUtil=ProgressDialogUtil.getInstance();
        mDialogUtil=DialogUtil.getInstance();
        mEmailEditText=(EditText)findViewById(R.id.email_edit_text);
        mSubmit=(Button)findViewById(R.id.submit_button);
        mSubmit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.submit_button:
                callFrogetPassword();
                break;
            case R.id.toolbar_icon_back:
                onBackPressed();
                break;
        }
    }

    //method to call the api of forgot password
    private void callFrogetPassword() {
        if (isValidInput() && AndroidUtil.hasInternetConnectivity(this))
        {

            mProgressDialogUtil.showProgressDialog(this);

            Call<WebServiceResponse> forgetPasswordApi = RetrofitManager.getRetrofitWebService()
                    .forgetPassword(
                            mEmail,
                            mPhoneNumber

                    );
            forgetPasswordApi.enqueue(new Callback<WebServiceResponse>() {
                @Override
                public void onResponse(Call<WebServiceResponse> call, Response<WebServiceResponse> response) {
                    new RetrofitResponseValidator(ResponseId.FORGET_PASSWORD, response, ForgotPasswordActivity.this);

                }

                @Override
                public void onFailure(Call<WebServiceResponse> call, Throwable t) {
                    String errorMessage = "";
                    if (t != null) {
                        Log.e(TAG, "onFailure: Error : " + t.getMessage());
                        if (t instanceof SocketTimeoutException) {
                            errorMessage = "Please make sure that your device has an active internet connection.";
                        }
                    }

                    mProgressDialogUtil.dismissProgressDialog();


                    if (TextUtils.isEmpty(errorMessage)) {
                        AndroidUtil.showErrorToast(ForgotPasswordActivity.this);
                    } else {
                        mDialogUtil.showErrorDialog(ForgotPasswordActivity.this, errorMessage);
                    }
                }
            });

        }
    }

    //method to check the valid input fields
    private boolean isValidInput() {
        mEmail=mEmailEditText.getText().toString().trim();
        boolean number = mEmail.matches(".*[0-9]+.*") || mEmail.matches("[+]") ;
        if (TextUtils.isEmpty(mEmail))
        {
            mEmailEditText.setError(getString(R.string.error_no_email_or_phone));
            mEmailEditText.requestFocus();
            return false;
        }
        else if (!TextUtils.isEmpty(mEmail)){
            if (!mEmail.contains("@") && number)
            {
                mPhoneNumber=mEmail;
                mEmail=null;
                if (mPhoneNumber.length() < 10 || mPhoneNumber.length() > 13)
                {
                    mEmailEditText.setError(getString(R.string.error_valid_phone));
                    mEmailEditText.requestFocus();
                    return false;
                }
            }

            else {
                mPhoneNumber=null;
                if (!ValidationUtil.isValidEmail(mEmail))
                {
                    mEmailEditText.setError(getString(R.string.error_email));
                    mEmailEditText.requestFocus();
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onValidResponse(@NonNull ResponseId responseId, @NonNull Object responseBody, @NonNull Response response) {
        switch (responseId)
        {
            case FORGET_PASSWORD:
                afterForgetPassword((WebServiceResponse)responseBody);
                break;
        }
        mProgressDialogUtil.dismissProgressDialog();
    }



    @Override
    public void onInvalidResponse(@NonNull ResponseId responseId, @NonNull String errorMessage, Response response) {

        mProgressDialogUtil.dismissProgressDialog();
        if (response == null || !response.isSuccessful()) {
            mDialogUtil.showErrorDialog(this, errorMessage);
        } else {
            AndroidUtil.showToast(this, errorMessage);
        }

    }

    private void afterForgetPassword(WebServiceResponse responseBody) {
        if (responseBody != null && responseBody.status == true)
        {
                mDialogUtil.showOkAlertDialog(this, responseBody.message, new Runnable() {
                    @Override
                    public void run() {
                        onBackPressed();
                    }
                }, false);

        }
        else {
            onBackPressed();
        }
    }
}
