package com.treeforlife.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.treeforlife.R;
import com.treeforlife.commons.Data;
import com.treeforlife.controller.GetCountriesApiController;
import com.treeforlife.customview.CustomTextView;
import com.treeforlife.dataobjects.CountryInfo;

import java.util.ArrayList;

/**
 * Created by TheAppsmiths on 4/10/2018.
 *
 * @author TheAppsmiths
 */

public class CountriesArrayAdapter extends ArrayAdapter<CountryInfo> {

    private final ArrayList<CountryInfo> mCountriesInfoList;
    private final CountriesDialCodeItemCallback mItemCallback;

    public CountriesArrayAdapter(Context context, CountriesDialCodeItemCallback itemCallback) {
        super(context, R.layout.item_country_info, Data.countryCodesList);
        this.mItemCallback = itemCallback;
        mCountriesInfoList = Data.countryCodesList;
        if(mCountriesInfoList == null){
            GetCountriesApiController.getInstance().loadData(context);
        }
    }

    @Override
    public int getCount() {
        return mCountriesInfoList != null ? mCountriesInfoList.size() : 0;
    }

    @Override
    public CountryInfo getItem(int position) {
        return (mCountriesInfoList != null && mCountriesInfoList.size() > position && position > -1)
                ? mCountriesInfoList.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        CountryCodeViewHolder mViewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_country_info, parent, false);
            mViewHolder = new CountryCodeViewHolder(convertView);
            convertView.setTag(mViewHolder);

        } else {
            mViewHolder = (CountryCodeViewHolder) convertView.getTag();
        }

        mViewHolder.bindData(position);

        return convertView;
    }

    //Holder view to show the data in adapter
    private class CountryCodeViewHolder {
        private final View mItemView;
        private final CustomTextView mCountryNameTextView, mCountryDialCodeTextView;

        CountryInfo countryInfo;

        public CountryCodeViewHolder(View itemView) {
            this.mItemView = itemView;
            mCountryNameTextView = itemView.findViewById(R.id.country_name_text_view);
            mCountryDialCodeTextView = itemView.findViewById(R.id.country_dial_code_text_view);

        }

        public void bindData(final int position) {
            countryInfo = getItem(position);

            if (countryInfo != null) {
                mCountryNameTextView.setText(countryInfo.countryName + "(" + countryInfo.countryCode + ")");
                mCountryDialCodeTextView.setText(countryInfo.dialCode);
            }

            mItemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mItemCallback != null) {
                        mItemCallback.onItemClick(position, countryInfo);
                    }
                }
            });
        }
    }

    public interface CountriesDialCodeItemCallback {
        void onItemClick(int position, CountryInfo countryInfo);
    }
}
