package com.treeforlife.dataobjects;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by TheAppsmiths on 4/5/2018.
 * updated by TheAppsmiths on 4/12/2018.
 */

public class Video implements Parcelable{
    @SerializedName("video_id")
    public String videoId;

    @SerializedName("video_description")
    public String videoDescription;

    @SerializedName("video_url")
    public String videoUrl;

    @SerializedName("is_shared")
    public String isShared;

    @SerializedName(value = "video_thumbnail",alternate = {"video_thumb"})
    public String videoThumbnail;

    protected Video(Parcel in) {
        videoId = in.readString();
        videoDescription = in.readString();
        videoUrl = in.readString();
        isShared = in.readString();
        videoThumbnail = in.readString();
    }

    public static final Creator<Video> CREATOR = new Creator<Video>() {
        @Override
        public Video createFromParcel(Parcel in) {
            return new Video(in);
        }

        @Override
        public Video[] newArray(int size) {
            return new Video[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(videoId);
        dest.writeString(videoDescription);
        dest.writeString(videoUrl);
        dest.writeString(isShared);
        dest.writeString(videoThumbnail);
    }
}
