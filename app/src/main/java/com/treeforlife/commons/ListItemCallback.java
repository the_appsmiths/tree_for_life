package com.treeforlife.commons;


public interface ListItemCallback {
    void onItemClicked(int which, String datum, int requestCode);
}
