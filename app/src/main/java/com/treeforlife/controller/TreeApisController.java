package com.treeforlife.controller;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;

import com.treeforlife.Utils.AndroidUtil;
import com.treeforlife.Utils.DialogUtil;
import com.treeforlife.Utils.ProgressDialogUtil;
import com.treeforlife.dataobjects.TFLUser;
import com.treeforlife.dataobjects.TreeInfo;
import com.treeforlife.retrofit.ResponseId;
import com.treeforlife.retrofit.RetrofitManager;
import com.treeforlife.retrofit.RetrofitResponseValidator;
import com.treeforlife.retrofit.responses.TreeInfoOfYearResponse;
import com.treeforlife.retrofit.responses.TreeInfoResponse;
import com.treeforlife.retrofit.responses.ValueOfTreeResponse;

import java.net.SocketTimeoutException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TheAppsmiths on 4/17/2018.
 *
 * @author TheAppsmiths
 */

/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class TreeApisController implements RetrofitResponseValidator.ValidationListener {
    private final String TAG;

    private final Fragment mFragment;
    private final Context mContext;
    private final Activity mActivity;
    private final TFLUser mTflUser;

    private ValueOfTreeApiResultCallback mValueOfTreeApiResultCallback;
    private TreeInfoApiResultCallback mTreeInfoApiResultCallback;
    private TreeInfoOfYearApiResultCallback mYearlyValueOfTreeApiResultCallback;

    private DialogUtil mDialogUtil;
    private ProgressDialogUtil mProgressDialogUtil;
    private boolean isGetValueOfTreeInProgress, isGetTreeInfoInProgress;

    public TreeApisController(String tag, Fragment fragment, Context context,
                              Activity activity, TFLUser tflUser) {
        this.TAG = tag;
        this.mFragment = fragment;
        this.mContext = context;
        this.mActivity = activity;
        this.mTflUser = tflUser;

        this.mDialogUtil = DialogUtil.getInstance();
        this.mProgressDialogUtil = ProgressDialogUtil.getInstance();
    }

    public void setValueOfTreeApiResultCallback(ValueOfTreeApiResultCallback callback) {
        this.mValueOfTreeApiResultCallback = callback;
    }

    public void setTreeInfoApiResultCallback(TreeInfoApiResultCallback callback) {
        this.mTreeInfoApiResultCallback = callback;
    }

    public void setYearlyValueOfTreeApiResultCallback(TreeInfoOfYearApiResultCallback callback) {
        this.mYearlyValueOfTreeApiResultCallback = callback;
    }

    public boolean getValueOfTree() {
        if (!isGetValueOfTreeInProgress && isValidUser()
                && AndroidUtil.hasInternetConnectivity(mContext)) {

            mProgressDialogUtil.showProgressDialog(mActivity);
            isGetValueOfTreeInProgress = true;

            Call<ValueOfTreeResponse> mGetTreeDetailsCall = RetrofitManager.getRetrofitWebService()
                    .getValueOfTree(mTflUser.userId, "");

            mGetTreeDetailsCall.enqueue(new Callback<ValueOfTreeResponse>() {
                @Override
                public void onResponse(Call<ValueOfTreeResponse> call, Response<ValueOfTreeResponse> response) {
                    new RetrofitResponseValidator(ResponseId.GET_VALUE_OF_TREE, response,
                            TreeApisController.this);
                }

                @Override
                public void onFailure(Call<ValueOfTreeResponse> call, Throwable t) {
                    String errorMessage = "";
                    if (t != null) {
                        Log.e(TAG, "onFailure: Error : " + t.getMessage());
                        if (t instanceof SocketTimeoutException) {
                            errorMessage = "Please make sure that your device has an active internet connection.";
                        }
                    }

                    mProgressDialogUtil.dismissProgressDialog();
                    isGetValueOfTreeInProgress = false;
                    if (mFragment != null && !mFragment.isVisible()) {
                        return;
                    }

                    if (TextUtils.isEmpty(errorMessage)) {
                        AndroidUtil.showErrorToast(mContext);
                    } else {
                        mDialogUtil.showErrorDialog(mContext, errorMessage);
                    }
                }
            });

            return true;
        }
        return false;
    }

    public boolean getTreeInfo() {
        if (!isGetTreeInfoInProgress && isValidUser()
                && AndroidUtil.hasInternetConnectivity(mContext)) {

            mProgressDialogUtil.showProgressDialog(mActivity);
            isGetTreeInfoInProgress = true;

            Call<TreeInfoResponse> mGetTreeDetailsCall = RetrofitManager.getRetrofitWebService()
                    .getTreeInfo(mTflUser.userId);

            mGetTreeDetailsCall.enqueue(new Callback<TreeInfoResponse>() {
                @Override
                public void onResponse(Call<TreeInfoResponse> call, Response<TreeInfoResponse> response) {
                    new RetrofitResponseValidator(ResponseId.GET_TREE_INFO, response,
                            TreeApisController.this);
                }

                @Override
                public void onFailure(Call<TreeInfoResponse> call, Throwable t) {
                    String errorMessage = "";
                    if (t != null) {
                        Log.e(TAG, "onFailure: Error : " + t.getMessage());
                        if (t instanceof SocketTimeoutException) {
                            errorMessage = "Please make sure that your device has an active internet connection.";
                        }
                    }

                    mProgressDialogUtil.dismissProgressDialog();
                    isGetTreeInfoInProgress = false;
                    if (mFragment != null && !mFragment.isVisible()) {
                        return;
                    }
                    if (mActivity != null && mActivity.isFinishing()) {
                        return;
                    }

                    if (TextUtils.isEmpty(errorMessage)) {
                        AndroidUtil.showErrorToast(mContext);
                    } else {
                        mDialogUtil.showErrorDialog(mContext, errorMessage);
                    }
                }
            });

            return true;
        }
        return false;
    }

    public boolean getTreeInfoOfYear(@NonNull String year) {
        if (isValidUser()
                && AndroidUtil.hasInternetConnectivity(mContext)) {

            mProgressDialogUtil.showProgressDialog(mActivity);

            Call<TreeInfoOfYearResponse> mGetTreeDetailsCall = RetrofitManager.getRetrofitWebService()
                    .getTreeInfoOfYear(mTflUser.userId, year);

            mGetTreeDetailsCall.enqueue(new Callback<TreeInfoOfYearResponse>() {
                @Override
                public void onResponse(Call<TreeInfoOfYearResponse> call, Response<TreeInfoOfYearResponse> response) {
                    new RetrofitResponseValidator(ResponseId.GET_TREE_INFO_OF_YEAR, response,
                            TreeApisController.this);
                }

                @Override
                public void onFailure(Call<TreeInfoOfYearResponse> call, Throwable t) {
                    String errorMessage = "";
                    if (t != null) {
                        Log.e(TAG, "onFailure: Error : " + t.getMessage());
                        if (t instanceof SocketTimeoutException) {
                            errorMessage = "Please make sure that your device has an active internet connection.";
                        }
                    }

                    mProgressDialogUtil.dismissProgressDialog();
                    if (mFragment != null && !mFragment.isVisible()) {
                        return;
                    }
                    if (mActivity != null && mActivity.isFinishing()) {
                        return;
                    }

                    if (TextUtils.isEmpty(errorMessage)) {
                        AndroidUtil.showErrorToast(mContext);
                    } else {
                        mDialogUtil.showErrorDialog(mContext, errorMessage);
                    }
                }
            });

            return true;
        }
        return false;
    }

    private boolean isValidUser() {
        if (mTflUser == null) {
            AndroidUtil.showToast(mContext, "Invalid User");
            return false;

        } else if (TextUtils.isEmpty(mTflUser.userId)) {
            AndroidUtil.showToast(mContext, "You need to Login !");
            return false;
        }

        return true;
    }

    @Override
    public void onValidResponse(@NonNull ResponseId responseId, @NonNull Object responseBody, @NonNull Response response) {
        mProgressDialogUtil.dismissProgressDialog();
        switch (responseId) {
            case GET_VALUE_OF_TREE:
                ValueOfTreeResponse serviceResponse = (ValueOfTreeResponse) responseBody;
                AndroidUtil.showToast(mContext, serviceResponse.message);
                if (mValueOfTreeApiResultCallback != null) {
                    mValueOfTreeApiResultCallback.onValueOfTreeApiResult(true, responseId, serviceResponse.treeInfoList);
                }
                isGetValueOfTreeInProgress = false;
                break;

            case GET_TREE_INFO:
                TreeInfoResponse treeInfoResponse = (TreeInfoResponse) responseBody;
                AndroidUtil.showToast(mContext, treeInfoResponse.message);
                if (mTreeInfoApiResultCallback != null) {
                    mTreeInfoApiResultCallback.onTreeInfoApiResult(true, responseId, treeInfoResponse.treeInfo);
                }
                isGetTreeInfoInProgress = false;
                break;

            case GET_TREE_INFO_OF_YEAR:
                TreeInfoOfYearResponse treeInfoOfYearResponse
                        = (TreeInfoOfYearResponse) responseBody;
                AndroidUtil.showToast(mContext, treeInfoOfYearResponse.message);
                if (mYearlyValueOfTreeApiResultCallback != null) {
                    mYearlyValueOfTreeApiResultCallback.onTreeInfoOfYearApiResult(true,
                            responseId,
                            treeInfoOfYearResponse.treeInfoOfYear);
                }
                break;
        }
    }

    @Override
    public void onInvalidResponse(@NonNull ResponseId responseId, @NonNull String errorMessage, Response response) {
        mProgressDialogUtil.dismissProgressDialog();
        switch (responseId) {
            case GET_VALUE_OF_TREE:
                isGetValueOfTreeInProgress = false;
                break;

            case GET_TREE_INFO:
                isGetTreeInfoInProgress = false;
                break;
        }
        if (mFragment != null && !mFragment.isVisible()) {
            return;
        }
        if (mActivity != null && mActivity.isFinishing()) {
            return;
        }

        if (TextUtils.isEmpty(errorMessage)) {
            AndroidUtil.showErrorToast(mContext);
            return;
        }

        if (response == null || !response.isSuccessful()) {
            mDialogUtil.showErrorDialog(mContext, errorMessage);
        } else {
            AndroidUtil.showToast(mContext, errorMessage);
        }
    }

    public interface ValueOfTreeApiResultCallback {
        void onValueOfTreeApiResult(boolean success, ResponseId responseId,
                                    ArrayList<TreeInfo> treeInfoList);

    }

    public interface TreeInfoApiResultCallback {

        void onTreeInfoApiResult(boolean success, ResponseId responseId, TreeInfo treeInfo);
    }

    public interface TreeInfoOfYearApiResultCallback {

        void onTreeInfoOfYearApiResult(boolean success, ResponseId responseId,
                                       TreeInfoOfYearResponse.TreeInfoOfYear treeInfoOfYear);
    }

    public interface TreeInfoApisResultCallback extends TreeInfoApiResultCallback,
            TreeInfoOfYearApiResultCallback {

    }
}
