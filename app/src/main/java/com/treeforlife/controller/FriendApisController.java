package com.treeforlife.controller;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;

import com.treeforlife.Utils.AndroidUtil;
import com.treeforlife.Utils.CometChatUtil;
import com.treeforlife.Utils.DialogUtil;
import com.treeforlife.Utils.ProgressDialogUtil;
import com.treeforlife.dataobjects.FriendTflUser;
import com.treeforlife.dataobjects.Group;
import com.treeforlife.dataobjects.SearchedTflUser;
import com.treeforlife.dataobjects.TFLUser;
import com.treeforlife.retrofit.ResponseId;
import com.treeforlife.retrofit.RetrofitManager;
import com.treeforlife.retrofit.RetrofitResponseValidator;
import com.treeforlife.retrofit.responses.GetMembersListResponse;
import com.treeforlife.retrofit.responses.WebServiceResponse;

import java.net.SocketTimeoutException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TheAppsmiths on 4/11/2018.
 *
 * @author TheAppsmiths
 */
/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class FriendApisController implements RetrofitResponseValidator.ValidationListener {
    private final String TAG;

    private final Fragment mFragment;
    private final Context mContext;
    private final Activity mActivity;
    private final TFLUser mTflUser;
    private final FriendApiResultCallback mFriendsApiResultCallback;
    private final GetFriendsListApiResultCallback mGetFriendsListApiResultCallback;
    private final ResultCallback mResultCallback;

    private DialogUtil mDialogUtil;
    private ProgressDialogUtil mProgressDialogUtil;

    private boolean isLoadingFriendsList, isLoadingFamilyMembers, isLoadingNotInFamilyMembers,
            isLoadingGroupMembers, isLoadingNotInGroupMembers;
    private int mFriendsListPageNo, mFamilyMembersPageNo, mNotInFamilyMembersPageNo,
            mGroupMembersPageNo, mNotInGroupMembersPageNo;

    public FriendApisController(String tag, Fragment fragment, Context context,
                                Activity activity, TFLUser tflUser,
                                FriendApiResultCallback callback) {
        this.TAG = tag;
        this.mFragment = fragment;
        this.mContext = context;
        this.mActivity = activity;
        this.mTflUser = tflUser;
        this.mFriendsApiResultCallback = callback;
        this.mGetFriendsListApiResultCallback = null;
        this.mResultCallback = null;

        this.mDialogUtil = DialogUtil.getInstance();
        this.mProgressDialogUtil = ProgressDialogUtil.getInstance();
    }

    public FriendApisController(String tag, Fragment fragment, Context context,
                                Activity activity, TFLUser tflUser,
                                GetFriendsListApiResultCallback callback) {
        this.TAG = tag;
        this.mFragment = fragment;
        this.mContext = context;
        this.mActivity = activity;
        this.mTflUser = tflUser;
        this.mFriendsApiResultCallback = null;
        this.mGetFriendsListApiResultCallback = callback;
        this.mResultCallback = null;

        this.mDialogUtil = DialogUtil.getInstance();
        this.mProgressDialogUtil = ProgressDialogUtil.getInstance();
    }

    public FriendApisController(String tag, Fragment fragment, Context context,
                                Activity activity, TFLUser tflUser,
                                ResultCallback callback) {
        this.TAG = tag;
        this.mFragment = fragment;
        this.mContext = context;
        this.mActivity = activity;
        this.mTflUser = tflUser;
        this.mFriendsApiResultCallback = null;
        this.mGetFriendsListApiResultCallback = null;
        this.mResultCallback = callback;

        this.mDialogUtil = DialogUtil.getInstance();
        this.mProgressDialogUtil = ProgressDialogUtil.getInstance();
    }

    public void loadMyFriends(final boolean resetData) {

        if (isValidUser()) {
            loadFriends(mTflUser.userId, resetData);
        }
    }

    public void loadFriends(@NonNull String userId, final boolean resetData) {

        if (isValidData(userId, false)
                && AndroidUtil.hasInternetConnectivity(mContext)
                && !isLoadingFriendsList) {

            isLoadingFriendsList = true;

            if (resetData) {
                mFriendsListPageNo = 0;
            }

            if (mFriendsListPageNo == 0) {
                mProgressDialogUtil.showProgressDialog(mActivity);
            }

            Call<GetMembersListResponse> getFriendsListCall
                    = RetrofitManager.getRetrofitWebService().myFreind(userId, mFriendsListPageNo);
            processListRequest(ResponseId.GET_MY_FRIENDS_LIST, getFriendsListCall);
        }
    }

    public void loadMyFamilyMembers(boolean resetData) {

        if (isValidUser()
                && AndroidUtil.hasInternetConnectivity(mContext)
                && !isLoadingFamilyMembers) {
            isLoadingFamilyMembers = true;
            if (resetData) {
                mFamilyMembersPageNo = 0;
            }
            if (mFamilyMembersPageNo == 0) {
                mProgressDialogUtil.showProgressDialog(mActivity);
            }

            Call<GetMembersListResponse> getFamilyMembersCall
                    = RetrofitManager.getRetrofitWebService()
                    .getFamilyMembers(mTflUser.userId, mFamilyMembersPageNo);
            processListRequest(ResponseId.GET_FAMILY_MEMBERS_LIST, getFamilyMembersCall);
        }
    }

    public void loadNotInMyFamilyMembers(boolean resetData) {

        if (isValidUser()
                && AndroidUtil.hasInternetConnectivity(mContext)
                && !isLoadingNotInFamilyMembers) {
            isLoadingNotInFamilyMembers = true;
            if (resetData) {
                mNotInFamilyMembersPageNo = 0;
            }
            if (mNotInFamilyMembersPageNo == 0) {
                mProgressDialogUtil.showProgressDialog(mActivity);
            }

            Call<GetMembersListResponse> notFamilyMembersCall
                    = RetrofitManager.getRetrofitWebService()
                    .getNotFamilyMembers(mTflUser.userId, mNotInFamilyMembersPageNo);
            processListRequest(ResponseId.GET_NOT_IN_FAMILY_MEMBERS_LIST, notFamilyMembersCall);

        }
    }

    public void loadMyGroupMembers(@NonNull Group group, boolean resetData) {

        if (isValidData(group)
                && AndroidUtil.hasInternetConnectivity(mContext)
                && !isLoadingGroupMembers) {
            isLoadingGroupMembers = true;
            if (resetData) {
                mGroupMembersPageNo = 0;
            }
            if (mGroupMembersPageNo == 0) {
                mProgressDialogUtil.showProgressDialog(mActivity);
            }

            Call<GetMembersListResponse> notFamilyMembersCall
                    = RetrofitManager.getRetrofitWebService()
                    .getMembers(mTflUser.userId, group.groupId, mGroupMembersPageNo);
            processListRequest(ResponseId.GET_GROUP_MEMBERS_LIST, notFamilyMembersCall);

        }
    }

    public void loadNotInMyGroupMembers(@NonNull Group group, boolean resetData) {

        if (isValidData(group)
                && AndroidUtil.hasInternetConnectivity(mContext)
                && !isLoadingNotInGroupMembers) {
            isLoadingNotInGroupMembers = true;
            if (resetData) {
                mNotInGroupMembersPageNo = 0;
            }
            if (mNotInGroupMembersPageNo == 0) {
                mProgressDialogUtil.showProgressDialog(mActivity);
            }

            Call<GetMembersListResponse> notFamilyMembersCall
                    = RetrofitManager.getRetrofitWebService()
                    .getNotInGroupMembers(mTflUser.userId, group.groupId, mNotInGroupMembersPageNo);
            processListRequest(ResponseId.GET_NOT_IN_GROUP_MEMBERS_LIST, notFamilyMembersCall);

        }
    }

    private void processListRequest(@NonNull final ResponseId responseId,
                                    @NonNull Call<GetMembersListResponse> call) {
        call.enqueue(new Callback<GetMembersListResponse>() {
            @Override
            public void onResponse(Call<GetMembersListResponse> call, Response<GetMembersListResponse> response) {
                new RetrofitResponseValidator(responseId, response, FriendApisController.this);

            }

            @Override
            public void onFailure(Call<GetMembersListResponse> call, Throwable t) {
                String errorMessage = "";
                if (t != null) {
                    Log.e(TAG, "onFailure: Error : " + t.getMessage());
                    if (t instanceof SocketTimeoutException) {
                        errorMessage = "Please make sure that your device has an active internet connection.";
                    }
                }

                mProgressDialogUtil.dismissProgressDialog();
                switch (responseId) {
                    case GET_MY_FRIENDS_LIST:
                        isLoadingFriendsList = false;
                        break;

                    case GET_FAMILY_MEMBERS_LIST:
                        isLoadingFamilyMembers = false;
                        break;

                    case GET_NOT_IN_FAMILY_MEMBERS_LIST:
                        isLoadingNotInFamilyMembers = false;
                        break;

                    case GET_GROUP_MEMBERS_LIST:
                        isLoadingGroupMembers = false;
                        break;

                    case GET_NOT_IN_GROUP_MEMBERS_LIST:
                        isLoadingNotInGroupMembers = false;
                        break;

                }

                if ((mFragment != null && !mFragment.isVisible())) {
                    switch (responseId) {
                        case GET_MY_FRIENDS_LIST:
                            if (mFriendsListPageNo != 0) {
                                return;
                            }
                            break;

                        case GET_FAMILY_MEMBERS_LIST:
                            if (mFamilyMembersPageNo != 0) {
                                return;
                            }
                            break;

                        case GET_NOT_IN_FAMILY_MEMBERS_LIST:
                            if (mNotInFamilyMembersPageNo != 0) {
                                return;
                            }
                            break;

                        case GET_GROUP_MEMBERS_LIST:
                            if (mGroupMembersPageNo != 0) {
                                return;
                            }
                            break;
                        case GET_NOT_IN_GROUP_MEMBERS_LIST:
                            if (mNotInGroupMembersPageNo != 0) {
                                return;
                            }
                            break;

                    }
                    return;
                }

                if (TextUtils.isEmpty(errorMessage)) {
                    AndroidUtil.showErrorToast(mContext);
                } else {
                    mDialogUtil.showErrorDialog(mContext, errorMessage);
                }

            }
        });
    }

    public void sendFriendRequest(@NonNull final String friendId) {
        if (isValidData(friendId, ResponseId.SEND_FRIEND_REQUEST)
                && AndroidUtil.hasInternetConnectivity(mContext)) {
            mDialogUtil.showTwoButtonsAlertDialog(mContext,
                    "Are you sure you want to send friend request to this user ?",
                    "Send Friend request",
                    "Cancel",
                    new Runnable() {
                        @Override
                        public void run() {
                            mProgressDialogUtil.showProgressDialog(mActivity);
                            Call<WebServiceResponse> mSendFriendCall =
                                    RetrofitManager.getRetrofitWebService()
                                            .sendFriendRequest(mTflUser.userId, friendId);

                            processRequest(ResponseId.SEND_FRIEND_REQUEST, mSendFriendCall);
                        }
                    });
        }

    }

    public void rejectFriendRequest(@NonNull SearchedTflUser searchedTflUser) {
        if (isValidData(searchedTflUser)) {
            rejectFriendRequest(searchedTflUser.id);

        }
    }

    public void rejectFriendRequest(@NonNull final String friendId) {
        if (isValidData(friendId, ResponseId.REJECT_FRIEND_REQUEST)
                && AndroidUtil.hasInternetConnectivity(mContext)) {
            mDialogUtil.showTwoButtonsAlertDialog(mContext,
                    "Are you sure you want to reject friend request ?",
                    "Reject",
                    "Cancel",
                    new Runnable() {
                        @Override
                        public void run() {
                            mProgressDialogUtil.showProgressDialog(mActivity);
                            Call<WebServiceResponse> mRejectFriendRequestCall =
                                    RetrofitManager.getRetrofitWebService()
                                            .rejectFriendRequest(mTflUser.userId, friendId);

                            processRequest(ResponseId.REJECT_FRIEND_REQUEST, mRejectFriendRequestCall);
                        }
                    });
        }

    }

    public void acceptFriendRequest(@NonNull SearchedTflUser searchedTflUser) {
        if (isValidData(searchedTflUser)) {
            acceptFriendRequest(searchedTflUser.id);

        }
    }

    public void acceptFriendRequest(@NonNull final String friendId) {
        if (isValidData(friendId, ResponseId.ACCEPT_FRIEND_REQUEST)
                && AndroidUtil.hasInternetConnectivity(mContext)) {
            mDialogUtil.showTwoButtonsAlertDialog(mContext,
                    "Are you sure you want to accept friend request ?",
                    "Accept",
                    "Cancel",
                    new Runnable() {
                        @Override
                        public void run() {
                            mProgressDialogUtil.showProgressDialog(mActivity);
                            Call<WebServiceResponse> mAcceptFriendCall =
                                    RetrofitManager.getRetrofitWebService()
                                            .acceptFriendRequest(mTflUser.userId, friendId);

                            processRequest(ResponseId.ACCEPT_FRIEND_REQUEST, mAcceptFriendCall);
                            CometChatUtil.getInstance().addFriends(mTflUser.userId, friendId, null);
                            CometChatUtil.getInstance().addFriends(friendId, mTflUser.userId, null);
                        }
                    });

        }

    }

    public void blockFriend(@NonNull FriendTflUser friendTflUser) {
        if (isValidData(friendTflUser)) {
            blockFriend(friendTflUser.id);

        } else {
            onFriendsApiError(ResponseId.BLOCK_FRIEND);
        }
    }

    public void blockFriend(@NonNull SearchedTflUser searchedTflUser) {
        if (isValidData(searchedTflUser)) {
            blockFriend(searchedTflUser.id);

        } else {
            onFriendsApiError(ResponseId.BLOCK_FRIEND);
        }
    }

    public void blockFriend(@NonNull final String friendId) {
        if (isValidData(friendId, ResponseId.BLOCK_FRIEND)
                && AndroidUtil.hasInternetConnectivity(mContext)) {
            mDialogUtil.showTwoButtonsAlertDialog(mContext,
                    "Are you sure you want to block this user ?",
                    "Block",
                    "Cancel",
                    new Runnable() {
                        @Override
                        public void run() {
                            mProgressDialogUtil.showProgressDialog(mActivity);
                            Call<WebServiceResponse> mBlockFriendCall =
                                    RetrofitManager.getRetrofitWebService()
                                            .blockFriend(mTflUser.userId, friendId);

                            processRequest(ResponseId.BLOCK_FRIEND, mBlockFriendCall);
                        }
                    });
        } else {
            onFriendsApiError(ResponseId.BLOCK_FRIEND);
        }
    }

    public void unblockFriend(@NonNull FriendTflUser friendTflUser) {
        if (isValidData(friendTflUser)) {
            unblockFriend(friendTflUser.id, false);

        } else {
            onFriendsApiError(ResponseId.UNBLOCK_FRIEND);
        }
    }

    public void unblockFriend(@NonNull SearchedTflUser searchedTflUser) {
        if (isValidData(searchedTflUser)) {
            unblockFriend(searchedTflUser.id, true);

        } else {
            onFriendsApiError(ResponseId.UNBLOCK_FRIEND);
        }
    }

    public void unblockFriend(@NonNull final String friendId, boolean askConfirmation) {
        if (isValidData(friendId, ResponseId.UNBLOCK_FRIEND)
                && AndroidUtil.hasInternetConnectivity(mContext)) {
            Runnable okButtonAction = new Runnable() {
                @Override
                public void run() {
                    mProgressDialogUtil.showProgressDialog(mActivity);
                    Call<WebServiceResponse> mUnblockFriendCall =
                            RetrofitManager.getRetrofitWebService()
                                    .unblockFriend(mTflUser.userId, friendId);

                    processRequest(ResponseId.UNBLOCK_FRIEND, mUnblockFriendCall);
                }
            };

            if (askConfirmation) {
                mDialogUtil.showTwoButtonsAlertDialog(mContext,
                        "Are you sure you want to unblock this user ?",
                        "Unblock",
                        "Cancel",
                        okButtonAction);
            } else {
                okButtonAction.run();
            }


        } else {
            onFriendsApiError(ResponseId.UNBLOCK_FRIEND);
        }
    }

    public void removeFriend(@NonNull FriendTflUser friendTflUser, boolean askConfirmation) {
        if (isValidData(friendTflUser)) {
            removeFriend(friendTflUser.id, askConfirmation);

        } else {
            onFriendsApiError(ResponseId.REMOVE_FRIEND);
        }
    }

    public void removeFriend(@NonNull SearchedTflUser searchedTflUser, boolean askConfirmation) {
        if (isValidData(searchedTflUser)) {
            removeFriend(searchedTflUser.id, askConfirmation);

        } else {
            onFriendsApiError(ResponseId.REMOVE_FRIEND);
        }
    }

    public void removeFriend(@NonNull final String friendId, boolean askConfirmation) {
        if (isValidData(friendId, ResponseId.REMOVE_FRIEND)
                && AndroidUtil.hasInternetConnectivity(mContext)) {
            Runnable removeAction = new Runnable() {
                @Override
                public void run() {
                    mProgressDialogUtil.showProgressDialog(mActivity);
                    Call<WebServiceResponse> mRemoveFriendCall =
                            RetrofitManager.getRetrofitWebService()
                                    .removeFriend(mTflUser.userId, friendId);

                    processRequest(ResponseId.REMOVE_FRIEND, mRemoveFriendCall);
                }
            };
            if (askConfirmation) {
                mDialogUtil.showTwoButtonsAlertDialog(mContext,
                        "Are you sure you want to remove this user from your friend list ?",
                        "Remove",
                        "Cancel",
                        removeAction,
                        new Runnable() {
                            @Override
                            public void run() {
                                onFriendsApiError(ResponseId.REMOVE_FRIEND);
                            }
                        });
            } else {
                removeAction.run();
            }


        } else {
            onFriendsApiError(ResponseId.REMOVE_FAMILY_FRIEND);
        }

    }

    public void removeFamilyFriend(@NonNull FriendTflUser friendTflUser) {
        if (isValidData(friendTflUser)) {
            removeFamilyFriend(friendTflUser.id);
        } else {
            onFriendsApiError(ResponseId.REMOVE_FAMILY_FRIEND);
        }
    }

    public void removeFamilyFriend(@NonNull String friendId) {
        if (isValidData(friendId, false) && AndroidUtil.hasInternetConnectivity(mContext)) {
            mProgressDialogUtil.showProgressDialog(mActivity);
            Call<WebServiceResponse> mAcceptFriendCall =
                    RetrofitManager.getRetrofitWebService()
                            .removeFamilyFriend(mTflUser.userId, friendId);

            processRequest(ResponseId.REMOVE_FAMILY_FRIEND, mAcceptFriendCall);

        } else {
            onFriendsApiError(ResponseId.REMOVE_FAMILY_FRIEND);
        }

    }

    public void removeGroupMember(@NonNull FriendTflUser friendTflUser, @NonNull Group group) {
        if (isValidData(friendTflUser) && isValidData(group)) {
            removeGroupMember(friendTflUser.id, group.groupId);
        } else {
            onFriendsApiError(ResponseId.REMOVE_GROUP_MEMBER);
        }
    }


    public void removeGroupMember(@NonNull String memberId, int groupId) {
        if (isValidData(memberId, false) && AndroidUtil.hasInternetConnectivity(mContext)) {
            mProgressDialogUtil.showProgressDialog(mActivity);
            Call<WebServiceResponse> mAcceptFriendCall =
                    RetrofitManager.getRetrofitWebService()
                            .removeMember(mTflUser.userId, memberId, groupId);

            processRequest(ResponseId.REMOVE_GROUP_MEMBER, mAcceptFriendCall);

        } else {
            onFriendsApiError(ResponseId.REMOVE_GROUP_MEMBER);
        }

    }

    public void addFriendToFamily(@NonNull SearchedTflUser searchedTflUser, boolean askConfirmation) {
        if (isValidData(searchedTflUser)) {
            addFriendToFamily(searchedTflUser.id, askConfirmation);

        } else {
            onFriendsApiError(ResponseId.ADD_FRIEND_TO_FAMILY);
        }
    }

    public void addFriendToFamily(@NonNull final String friendId, boolean askConfirmation) {
        if (isValidData(friendId, ResponseId.BLOCK_FRIEND)) {
            ArrayList<String> list = new ArrayList<>();
            list.add(friendId);
            addFriendsToFamily(list, askConfirmation);
        } else {
            onFriendsApiError(ResponseId.ADD_FRIEND_TO_FAMILY);
        }
    }

    public void addFriendsToFamily(@NonNull final ArrayList<String> list, boolean askConfirmation) {
        Runnable cancelAction = new Runnable() {
            @Override
            public void run() {
                if (mFriendsApiResultCallback != null) {
                    mFriendsApiResultCallback.onFriendApiResult(ResponseId.ADD_FRIEND_TO_FAMILY, false);
                }
                if (mResultCallback != null) {
                    mResultCallback.onFriendApiResult(ResponseId.ADD_FRIEND_TO_FAMILY, false);
                }
            }
        };
        if (isValidData(list)
                && AndroidUtil.hasInternetConnectivity(mContext)) {
            Runnable okAction = new Runnable() {
                @Override
                public void run() {
                    mProgressDialogUtil.showProgressDialog(mActivity);
                    Call<WebServiceResponse> mBlockFriendCall =
                            RetrofitManager.getRetrofitWebService()
                                    .addFriendsToFamily(mTflUser.userId, list);

                    processRequest(ResponseId.ADD_FRIEND_TO_FAMILY, mBlockFriendCall);
                }
            };
            if (askConfirmation) {
                mDialogUtil.showTwoButtonsAlertDialog(mContext,
                        "Are you sure you want to add this user to family?",
                        "sure",
                        "Cancel",
                        okAction,
                        cancelAction,
                        false
                );
            } else {
                okAction.run();
            }
        } else {
            cancelAction.run();
        }
    }

    public void addFriendsToGroup(@NonNull final Group group,
                                  @NonNull final ArrayList<String> list,
                                  boolean askConfirmation) {
        Runnable cancelAction = new Runnable() {
            @Override
            public void run() {
                if (mFriendsApiResultCallback != null) {
                    mFriendsApiResultCallback.onFriendApiResult(ResponseId.ADD_FRIENDS_TO_GROUP, false);
                }
                if (mResultCallback != null) {
                    mResultCallback.onFriendApiResult(ResponseId.ADD_FRIENDS_TO_GROUP, false);
                }
            }
        };
        if (isValidData(list) && isValidData(group)
                && AndroidUtil.hasInternetConnectivity(mContext)) {
            Runnable okAction = new Runnable() {
                @Override
                public void run() {
                    mProgressDialogUtil.showProgressDialog(mActivity);
                    Call<WebServiceResponse> mBlockFriendCall =
                            RetrofitManager.getRetrofitWebService()
                                    .addMembers(mTflUser.userId, list, group.groupId);

                    processRequest(ResponseId.ADD_FRIENDS_TO_GROUP, mBlockFriendCall);
                }
            };
            if (askConfirmation) {
                mDialogUtil.showTwoButtonsAlertDialog(mContext,
                        "Are you sure you want to add this user to family?",
                        "sure",
                        "Cancel",
                        okAction,
                        cancelAction,
                        false
                );
            } else {
                okAction.run();
            }
        } else {
            cancelAction.run();
        }
    }


    private void processRequest(@NonNull final ResponseId responseId, @NonNull Call<WebServiceResponse> call) {
        call.enqueue(new Callback<WebServiceResponse>() {
            @Override
            public void onResponse(Call<WebServiceResponse> call, Response<WebServiceResponse> response) {
                new RetrofitResponseValidator(responseId, response,
                        FriendApisController.this);
            }

            @Override
            public void onFailure(Call<WebServiceResponse> call, Throwable t) {
                String errorMessage = "";
                if (t != null) {
                    Log.e(TAG, "onFailure: Error : " + t.getMessage());
                    if (t instanceof SocketTimeoutException) {
                        errorMessage = "Please make sure that your device has an active internet connection.";
                    }
                }

                mProgressDialogUtil.dismissProgressDialog();

                if (mFragment != null && !mFragment.isVisible()) {
                    return;
                }

                if (TextUtils.isEmpty(errorMessage)) {
                    AndroidUtil.showErrorToast(mContext);
                } else {
                    mDialogUtil.showErrorDialog(mContext, errorMessage);
                }
            }
        });
    }

    private boolean isValidUser() {
        if (mTflUser == null) {
            AndroidUtil.showToast(mContext, "Invalid User");
            return false;

        } else if (TextUtils.isEmpty(mTflUser.userId)) {
            AndroidUtil.showToast(mContext, "You need to Login !");
            return false;
        }

        return true;
    }

    private boolean isValidData(FriendTflUser friendTflUser) {
        if (friendTflUser == null) {
            AndroidUtil.showToast(mContext, "Invalid Friend !");
            return false;

        } else if (TextUtils.isEmpty(friendTflUser.id)) {
            AndroidUtil.showToast(mContext, "Invalid Friend Id !");
            return false;
        }

        return isValidUser();

    }

    private boolean isValidData(SearchedTflUser searchedTflUser) {
        if (searchedTflUser == null) {
            AndroidUtil.showToast(mContext, "Invalid Friend !");
            return false;

        } else if (TextUtils.isEmpty(searchedTflUser.id)) {
            AndroidUtil.showToast(mContext, "Invalid Friend Id !");
            return false;
        }

        return isValidUser();

    }

    private boolean isValidData(@NonNull String friendOrGroupId, boolean isGroupId) {
        if (TextUtils.isEmpty(friendOrGroupId)) {
            String errMessage = "Invalid Friend Id !";
            if (isGroupId) {
                errMessage = "Invalid Group Id !";
            }
            AndroidUtil.showToast(mContext, errMessage);
            return false;

        }
        return isValidUser();
    }

    private boolean isValidData(@NonNull String friendId, ResponseId responseId) {
        if (TextUtils.isEmpty(friendId)) {
            AndroidUtil.showToast(mContext, "Invalid Friend Id !");
            return false;

        }
        if (isValidUser()) {
            if (friendId.equals(mTflUser.userId)) {
                String message = "";
                switch (responseId) {

                    case SEND_FRIEND_REQUEST:
                        message = "you can't send friend request to yourself.";
                        break;

                    case REJECT_FRIEND_REQUEST:
                    case ACCEPT_FRIEND_REQUEST:
                        message = "how you have received friend request from yourself ? " +
                                "as you can't send friend request to yourself. " +
                                "Anyway, you can't accept/reject your friend request.";
                        break;

                    case BLOCK_FRIEND:
                    case UNBLOCK_FRIEND:
                        message = "how you are friend of yourself ? " +
                                "as you can neither send friend request to yourself " +
                                "nor can accept/reject your friend request. " +
                                "Anyway, you can't block/unblock yourself.";
                        break;

                    case REMOVE_FRIEND:
                        message = "how you are friend of yourself ? " +
                                "as you can neither send friend request to yourself " +
                                "nor can accept/reject your friend request. " +
                                "Anyway, you can't remove yourself.";
                        break;

                }
                mDialogUtil.showErrorDialog(mContext, message);
                return false;
            }

        } else {
            return false;
        }
        return true;

    }

    private boolean isValidData(@NonNull ArrayList<String> list) {
        if (list == null || list.isEmpty()) {
            AndroidUtil.showToast(mContext, "empty ids list");
            return false;

        }
        return isValidUser();
    }

    private boolean isValidData(Group group) {
        if (group == null) {
            AndroidUtil.showToast(mContext, "Invalid Group data");
            return false;

        }
        return isValidUser();
    }

    @Override
    public void onValidResponse(@NonNull ResponseId responseId, @NonNull Object responseBody, @NonNull Response response) {
        mProgressDialogUtil.dismissProgressDialog();
        switch (responseId) {
            case BLOCK_FRIEND:
            case UNBLOCK_FRIEND:
            case SEND_FRIEND_REQUEST:
            case ACCEPT_FRIEND_REQUEST:
            case REJECT_FRIEND_REQUEST:
            case REMOVE_FRIEND:
            case REMOVE_FAMILY_FRIEND:
            case ADD_FRIEND_TO_FAMILY:
            case ADD_FRIENDS_TO_GROUP:
            case REMOVE_GROUP_MEMBER:
                WebServiceResponse serviceResponse = (WebServiceResponse) responseBody;
                AndroidUtil.showToast(mContext, serviceResponse.message);
                if (mFriendsApiResultCallback != null) {
                    mFriendsApiResultCallback.onFriendApiResult(responseId, true);
                }

                if (mResultCallback != null) {
                    mResultCallback.onFriendApiResult(responseId, true);
                }
                break;

            case GET_MY_FRIENDS_LIST:
                handleFriendsOrMembersListResponse(responseId, responseBody);
                mFriendsListPageNo++;
                isLoadingFriendsList = false;
                break;

            case GET_FAMILY_MEMBERS_LIST:
                handleFriendsOrMembersListResponse(responseId, responseBody);
                mFamilyMembersPageNo++;
                isLoadingFamilyMembers = false;
                break;

            case GET_NOT_IN_FAMILY_MEMBERS_LIST:
                handleFriendsOrMembersListResponse(responseId, responseBody);
                mNotInFamilyMembersPageNo++;
                isLoadingNotInFamilyMembers = false;
                break;

            case GET_GROUP_MEMBERS_LIST:
                handleFriendsOrMembersListResponse(responseId, responseBody);
                mFamilyMembersPageNo++;
                isLoadingGroupMembers = false;
                break;

            case GET_NOT_IN_GROUP_MEMBERS_LIST:
                handleFriendsOrMembersListResponse(responseId, responseBody);
                mNotInGroupMembersPageNo++;
                isLoadingNotInGroupMembers = false;
                break;
        }
    }

    private void handleFriendsOrMembersListResponse(@NonNull ResponseId responseId,
                                                    @NonNull Object responseBody) {
        GetMembersListResponse friendsListResponse = (GetMembersListResponse) responseBody;
        AndroidUtil.showToast(mContext, friendsListResponse.message);
        if (mGetFriendsListApiResultCallback != null) {
            mGetFriendsListApiResultCallback.onFriendsListApiResult(responseId,
                    true, friendsListResponse);
        }
        if (mResultCallback != null) {
            mResultCallback.onFriendsListApiResult(responseId, true,
                    friendsListResponse);
        }
    }

    @Override
    public void onInvalidResponse(@NonNull ResponseId responseId, @NonNull String errorMessage, Response response) {
        mProgressDialogUtil.dismissProgressDialog();
        switch (responseId) {
            case GET_MY_FRIENDS_LIST:
                isLoadingFriendsList = false;
                break;

            case GET_FAMILY_MEMBERS_LIST:
                isLoadingFamilyMembers = false;
                break;

            case GET_NOT_IN_FAMILY_MEMBERS_LIST:
                isLoadingNotInFamilyMembers = false;
                break;

            case GET_GROUP_MEMBERS_LIST:
                isLoadingGroupMembers = false;
                break;

            case GET_NOT_IN_GROUP_MEMBERS_LIST:
                isLoadingNotInGroupMembers = false;
                break;

        }

        if (mFragment != null && !mFragment.isVisible()) {
            return;
        }

        boolean showErrInDialog = false;
        switch (responseId) {
            case BLOCK_FRIEND:
            case UNBLOCK_FRIEND:
            case SEND_FRIEND_REQUEST:
            case ACCEPT_FRIEND_REQUEST:
            case REJECT_FRIEND_REQUEST:
            case REMOVE_FRIEND:
            case REMOVE_FAMILY_FRIEND:
            case ADD_FRIEND_TO_FAMILY:
            case ADD_FRIENDS_TO_GROUP:
            case REMOVE_GROUP_MEMBER:
                onFriendsApiError(responseId);
                showErrInDialog = true;
                break;

            case GET_MY_FRIENDS_LIST:
                onGetListApisError(responseId);
                isLoadingFriendsList = false;
                if (mFriendsListPageNo != 0) {
                    return;
                }
                break;

            case GET_FAMILY_MEMBERS_LIST:
                onGetListApisError(responseId);
                isLoadingFamilyMembers = false;
                if (mFamilyMembersPageNo != 0) {
                    return;
                }
                break;

            case GET_NOT_IN_FAMILY_MEMBERS_LIST:
                onGetListApisError(responseId);
                isLoadingNotInFamilyMembers = false;
                if (mNotInFamilyMembersPageNo != 0) {
                    return;
                }
                break;

            case GET_GROUP_MEMBERS_LIST:
                onGetListApisError(responseId);
                isLoadingGroupMembers = false;
                if (mNotInFamilyMembersPageNo != 0) {
                    return;
                }
                break;
            case GET_NOT_IN_GROUP_MEMBERS_LIST:
                onGetListApisError(responseId);
                isLoadingNotInGroupMembers = false;
                if (mNotInFamilyMembersPageNo != 0) {
                    return;
                }
                break;

        }
        if (showErrInDialog || response == null || !response.isSuccessful()) {
            mDialogUtil.showErrorDialog(mContext, errorMessage);
        } else {
            AndroidUtil.showToast(mContext, errorMessage);
        }
    }

    private void onFriendsApiError(ResponseId responseId) {
        if (mFriendsApiResultCallback != null) {
            mFriendsApiResultCallback.onFriendApiResult(responseId, false);
        }
        if (mResultCallback != null) {
            mResultCallback.onFriendApiResult(responseId, false);
        }
    }

    private void onGetListApisError(ResponseId responseId) {
        if (mGetFriendsListApiResultCallback != null) {
            mGetFriendsListApiResultCallback.onFriendsListApiResult(responseId,
                    false, null);
        }
        if (mResultCallback != null) {
            mResultCallback.onFriendsListApiResult(responseId, false,
                    null);
        }
    }

    public interface FriendApiResultCallback {
        void onFriendApiResult(ResponseId responseId, boolean success);
    }

    public interface GetFriendsListApiResultCallback {
        void onFriendsListApiResult(ResponseId responseId, boolean success,
                                    GetMembersListResponse response);
    }

    public interface ResultCallback extends FriendApiResultCallback, GetFriendsListApiResultCallback {

    }
}
