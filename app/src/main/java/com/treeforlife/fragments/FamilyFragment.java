package com.treeforlife.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.treeforlife.R;
import com.treeforlife.Utils.AndroidUtil;
import com.treeforlife.Utils.DialogUtil;
import com.treeforlife.Utils.FragmentUtil;
import com.treeforlife.adapters.FriendTflUsersRvAdapter;
import com.treeforlife.commons.Constants;
import com.treeforlife.commons.SharedPrefHelper;
import com.treeforlife.controller.FriendApisController;
import com.treeforlife.customview.CustomTextView;
import com.treeforlife.dataobjects.FriendTflUser;
import com.treeforlife.dataobjects.TFLUser;
import com.treeforlife.retrofit.ResponseId;
import com.treeforlife.retrofit.responses.GetMembersListResponse;

import java.util.ArrayList;

/**
 * Created by TheAppsmiths on 4/11/2018.
 * updated by TheAppsmiths on 3rd May 20018.
 */

/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class FamilyFragment extends Fragment implements View.OnClickListener,
        FriendTflUsersRvAdapter.FriendTflUsersItemCallback,
        FriendApisController.ResultCallback {


    public static final String TAG = "FamilyFragment";

    private Context mContext;
    private Activity mActivity;
    private Bundle mBundle;

    private RecyclerView mFamilyFriendsRecyclerView;
    private FriendTflUsersRvAdapter mFamilyFriendsRvAdapter;
    private CustomTextView mNoDataTextView;

    private SharedPrefHelper mSharedPrefHelper;
    private DialogUtil mDialogUtil;
    private FriendApisController mFriendApisController;

    private TFLUser mTflUser;

    private String mCaller;
    private int mDeletedItemIndex = -1, mNoOfLoadedRecords = -1, mNoOfTotalRecords;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
        this.mActivity = getActivity();

        mSharedPrefHelper = SharedPrefHelper.getInstance();
        mDialogUtil = DialogUtil.getInstance();

        extractDataFromBundle();

        mFriendApisController = new FriendApisController(TAG, this, mContext, mActivity,
                mTflUser, this);
        mFamilyFriendsRvAdapter = new FriendTflUsersRvAdapter(this);

    }

    private void extractDataFromBundle() {

        if (mBundle == null) {
            mBundle = getArguments();
        }

        if (mBundle != null) {
            mTflUser = mBundle.getParcelable(Constants.KEY_TFL_USER);
            mCaller = mBundle.getString(Constants.KEY_CALLER_COMPONENT);
        }

        ensureTflUser();
    }

    private void ensureTflUser() {
        if (mTflUser == null) {
            mTflUser = mSharedPrefHelper.getLoginResponse(mContext);
        }
    }

    public void update(Bundle bundle) {
        this.mBundle = bundle;
        extractDataFromBundle();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_family, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initWidgets(view);
        loadData(true);
        manageNoDataTextVisibility();

    }

    private void initWidgets(View view) {

        mNoDataTextView = (CustomTextView) view.findViewById(R.id.no_data_text_view);
        mFamilyFriendsRecyclerView = view.findViewById(R.id.rv_family_friends);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        mFamilyFriendsRecyclerView.setLayoutManager(layoutManager);
        mFamilyFriendsRecyclerView.setAdapter(mFamilyFriendsRvAdapter);
        setInfiniteScroller(layoutManager);
        setSwipeAndDelete();

        FloatingActionButton mAddFamilyMemberActionButton = view.findViewById(R.id.add_friend_fab);
        mAddFamilyMemberActionButton.setOnClickListener(this);
        View mAddFamilyMemberView = view.findViewById(R.id.add_family_member_view);
        mAddFamilyMemberView.setOnClickListener(this);
    }

    private void setInfiniteScroller(final LinearLayoutManager layoutManager) {
        if (mFamilyFriendsRecyclerView != null) {
            mFamilyFriendsRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    int lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition();
                    int totalItemCount = layoutManager.getItemCount();
                    if ((lastVisibleItemPosition + Constants.ITEMS_LIMIT) > totalItemCount) {
                        loadData(false);
                    }
                }
            });
        }
    }

    private void manageNoDataTextVisibility() {
        if (mNoDataTextView != null && mFamilyFriendsRvAdapter != null) {
            mNoDataTextView.setVisibility(mFamilyFriendsRvAdapter.getItemCount() > 0 ? View.GONE
                    : View.VISIBLE);
        }
    }

    private boolean isValidUser() {

        if (mTflUser == null) {

            AndroidUtil.showToast(mContext, "Invalid User");
            return false;

        } else if (TextUtils.isEmpty(mTflUser.userId)) {

            AndroidUtil.showToast(mContext, "You need to Login !");
            return false;
        }

        return true;
    }

    private void loadData(boolean resetData) {
        if (resetData) {
            mNoOfTotalRecords = 0;
            mDeletedItemIndex = mNoOfLoadedRecords = -1;

        }
        if (isVisible() && mNoOfLoadedRecords < mNoOfTotalRecords) {
            mFriendApisController.loadMyFamilyMembers(resetData);
        }
    }

    private void processDeleteItem(RecyclerView.ViewHolder viewHolder) {
        if (viewHolder != null
                && viewHolder instanceof FriendTflUsersRvAdapter.FriendsHolder) {

            final FriendTflUsersRvAdapter.FriendsHolder holder = (FriendTflUsersRvAdapter.FriendsHolder)
                    viewHolder;
            mDeletedItemIndex = holder.getAdapterPosition();
            final FriendTflUser friendTflUser = mFamilyFriendsRvAdapter.getItemData(mDeletedItemIndex);

            Runnable deleteItemAction = new Runnable() {
                @Override
                public void run() {

                    if (mFamilyFriendsRvAdapter != null) {
                        mFriendApisController.removeFamilyFriend(friendTflUser);
                    }
                }
            };

            Runnable cancelAction = new Runnable() {
                @Override
                public void run() {
                    if (mFamilyFriendsRvAdapter != null) {
                        mFamilyFriendsRvAdapter.update(mDeletedItemIndex);
                        mDeletedItemIndex = -1;
                    }
                }
            };

            String friendName = "";
            if (friendTflUser != null) {
                friendName = friendTflUser.name;
            }

            if (TextUtils.isEmpty(friendName)) {
                friendName = "this user";
            }

            mDialogUtil.showTwoButtonsAlertDialog(mContext,

                    "Are you sure you want to remove " + friendName + " from this list.",
                    "Remove",
                    "Cancel",
                    deleteItemAction,
                    cancelAction,
                    false
            );


        }

    }

    private void setSwipeAndDelete() {
        /**
         * https://www.androidhive.info/2017/09/android-recyclerview-swipe-delete-undo-using-itemtouchhelper/
         * * */
        if (mFamilyFriendsRecyclerView != null) {
            ItemTouchHelper.SimpleCallback mItemTouchSimpleCallback
                    = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {

                @Override
                public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                                      RecyclerView.ViewHolder target) {
                    return false;
                }

                @Override
                public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

                    processDeleteItem(viewHolder);
                }

                @Override
                public void onChildDrawOver(Canvas c, RecyclerView recyclerView,
                                            RecyclerView.ViewHolder viewHolder, float dX, float dY,
                                            int actionState, boolean isCurrentlyActive) {
                    if (viewHolder != null
                            && viewHolder instanceof FriendTflUsersRvAdapter.FriendsHolder) {
                        getDefaultUIUtil().onDrawOver(c, recyclerView,
                                ((FriendTflUsersRvAdapter.FriendsHolder) viewHolder).mItemForegroundView, dX, dY,
                                actionState, isCurrentlyActive);
                    }
                }

                @Override
                public int convertToAbsoluteDirection(int flags, int layoutDirection) {
                    return super.convertToAbsoluteDirection(flags, layoutDirection);
                }

                @Override
                public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                    if (viewHolder != null
                            && viewHolder instanceof FriendTflUsersRvAdapter.FriendsHolder) {
                        getDefaultUIUtil().clearView(((FriendTflUsersRvAdapter.FriendsHolder) viewHolder).mItemForegroundView);
                    }
                }

                @Override
                public void onChildDraw(Canvas c, RecyclerView recyclerView,
                                        RecyclerView.ViewHolder viewHolder, float dX, float dY,
                                        int actionState, boolean isCurrentlyActive) {

                    if (viewHolder != null
                            && viewHolder instanceof FriendTflUsersRvAdapter.FriendsHolder) {

                        getDefaultUIUtil().onDraw(c, recyclerView,
                                ((FriendTflUsersRvAdapter.FriendsHolder) viewHolder).mItemForegroundView,
                                dX, dY, actionState, isCurrentlyActive);
                    }

                }

                @Override
                public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {

                    if (viewHolder != null
                            && viewHolder instanceof FriendTflUsersRvAdapter.FriendsHolder) {

                        getDefaultUIUtil().onSelected(((FriendTflUsersRvAdapter.FriendsHolder) viewHolder)
                                .mItemForegroundView);
                    }
                }

            };


            ItemTouchHelper mItemTouchHelper = new ItemTouchHelper(mItemTouchSimpleCallback);
            mItemTouchHelper.attachToRecyclerView(mFamilyFriendsRecyclerView);
        }
    }


    @Override
    public void onClick(View view) {
        if (view != null) {

            switch (view.getId()) {
                case R.id.add_friend_fab:
                case R.id.add_family_member_view:
                    showFriendsListInDialog();
                    break;
            }
        }
    }

    private void showFriendsListInDialog() {
        if (mActivity == null || !(mActivity instanceof AppCompatActivity)) {
            return;
        }
        FragmentManager fragmentManager = ((AppCompatActivity) mActivity).getSupportFragmentManager();
        if (fragmentManager != null) {

            Bundle bundle = new Bundle();
            bundle.putParcelable(Constants.KEY_TFL_USER, mTflUser);
            bundle.putString(Constants.KEY_CALLER_COMPONENT, TAG);

            SelectFriendDialogFragment selectFriendDialogFragment = null;
            Fragment fragment = fragmentManager.findFragmentByTag(SelectFriendDialogFragment.TAG);
            if (fragment != null && fragment instanceof SelectFriendDialogFragment) {
                selectFriendDialogFragment = (SelectFriendDialogFragment) fragment;
                selectFriendDialogFragment.update(bundle);
            }
            if (selectFriendDialogFragment == null) {
                selectFriendDialogFragment = new SelectFriendDialogFragment();
                selectFriendDialogFragment.setArguments(bundle);
            }
            selectFriendDialogFragment.setTargetFragment(FamilyFragment.this, Constants.RC_SELECT_FRIEND_FRAGMENT);
            selectFriendDialogFragment.show(fragmentManager, SelectFriendDialogFragment.TAG);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if (mActivity != null) {
            mActivity.setTitle(R.string.family);
        }
    }

    @Override
    public void onFriendTflUserClick(int position, FriendTflUser friendTflUser) {
        if (friendTflUser != null) {
            if (isValidUser()) {
                Bundle bundle = new Bundle();
                bundle.putString(Constants.KEY_CALLER_COMPONENT, TAG);
                bundle.putParcelable(Constants.KEY_TFL_USER, mTflUser);
                bundle.putParcelable(Constants.KEY_FRIEND_TFL_USER, friendTflUser);
                String fragmentName = FragmentUtil.FRAGMENT_USER_PROFILE;
                if (friendTflUser.treeId.equals(mTflUser.treeId)) {
                    fragmentName = FragmentUtil.FRAGMENT_MY_PROFILE;
                }
                FragmentUtil.loadFragment((AppCompatActivity) mActivity, fragmentName, bundle,
                        true);
            }
        } else {
            AndroidUtil.showToast(mContext, "Invalid Data ");
        }
    }

    @Override
    public void onFriendApiResult(ResponseId responseId, boolean success) {
        if (isVisible() && mFamilyFriendsRvAdapter != null) {
            switch (responseId) {
                case REMOVE_FAMILY_FRIEND:
                    if (success) {
                        mFamilyFriendsRvAdapter.removeItem(mDeletedItemIndex);

                    } else {
                        mFamilyFriendsRvAdapter.update(mDeletedItemIndex);
                    }
                    break;
                case ADD_FRIEND_TO_FAMILY:
                    loadData(true);
                    break;
            }
        }
        mDeletedItemIndex = -1;
        manageNoDataTextVisibility();
    }

    @Override
    public void onFriendsListApiResult(ResponseId responseId, boolean success, GetMembersListResponse response) {
        if (isVisible() && success && response != null) {

            this.mNoOfTotalRecords = response.totalRecord;
            if (response.friendsOrFamilyMembersList != null) {

                if (mFamilyFriendsRvAdapter != null) {
                    if (mNoOfLoadedRecords == -1) {
                        mFamilyFriendsRvAdapter.update(response.friendsOrFamilyMembersList);
                    } else {
                        mFamilyFriendsRvAdapter.addToList(response.friendsOrFamilyMembersList);
                    }
                }
                if (mNoOfLoadedRecords == -1) {
                    mNoOfLoadedRecords = response.friendsOrFamilyMembersList.size();
                } else {
                    mNoOfLoadedRecords += response.friendsOrFamilyMembersList.size();
                }

            }

            manageNoDataTextVisibility();

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Constants.RC_SELECT_FRIEND_FRAGMENT:
                if (resultCode == Activity.RESULT_OK) {
                    Bundle bundle = data.getExtras();
                    if (bundle != null) {
                        ArrayList<String> mList = bundle.getStringArrayList(Constants.KEY_FRIEND_IDS_LIST);
                        if (mList != null && !mList.isEmpty()) {
                            mFriendApisController.addFriendsToFamily(mList, false);
                        } else {
                            AndroidUtil.showToast(mContext, "Invalid data : empty friends ids list");
                        }
                    }

                }
                break;
        }
    }


}
