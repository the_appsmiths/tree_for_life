package com.treeforlife.Utils;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by TheAppsmiths on 3/6/2018.
 *
 * @author TheAppsmiths
 */
/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class DateAndTimeUtil {
    private static final String TAG = "DateAndTimeUtil";

    public static String parseDate(@NonNull DateOption dateOption,
                                   @NonNull String date) {
        String day = "";
        if (dateOption != null && !TextUtils.isEmpty(date)) {

            SimpleDateFormat format1 = new SimpleDateFormat(dateOption.mInputFormat);

            try {
                Date dt1 = format1.parse(date);
                DateFormat format2 = new SimpleDateFormat(dateOption.mOutputFormat);
                day = format2.format(dt1);

            } catch (ParseException e) {
                e.printStackTrace();
                day = date;
            }
        }

        return day;
    }

    public static String parseTime(@NonNull TimeOption timeOption,
                                   @NonNull String time) {
        String result = "";
        if (timeOption != null && !TextUtils.isEmpty(time)) {


            SimpleDateFormat format1 = new SimpleDateFormat(timeOption.mInputFormat);

            try {
                Date dt1 = format1.parse(time);
                DateFormat format2 = new SimpleDateFormat(timeOption.mOutputFormat);
                result = format2.format(dt1);

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if (TextUtils.isEmpty(result)) {
            int hours = extractHourOfDay(time);
            int minutes = extractMinute(time);
            result = String.format("%02d:%02d", hours, minutes);
        }
        return result;
    }

    public static int extractHourOfDay(@NonNull String timeString) {
        int hourOfDay = 0;
        if (!TextUtils.isEmpty(timeString)) {
            DateFormat inputTimeFormat = new SimpleDateFormat("hh:mm");
            DateFormat outputFormat = new SimpleDateFormat("hh");

            int increaseHrsBy = 0;
            Pattern pattern = Pattern.compile("[pP][mM]");
            Matcher matcher = pattern.matcher(timeString);
            if (matcher.find()) {
                increaseHrsBy = 12;
                timeString = timeString.replace("PM", "").trim();
            }
            try {
                Date date = inputTimeFormat.parse(timeString);
                String hour = outputFormat.format(date);
                hourOfDay = Integer.parseInt(hour);
                if (hourOfDay == 12) {
                    if (increaseHrsBy > 0) {
                        hourOfDay = 12;
                    } else {
                        hourOfDay = 0;
                    }

                } else {
                    hourOfDay += increaseHrsBy;
                }
            } catch (ParseException | NumberFormatException e1) {
                e1.printStackTrace();
            }
        }

        return hourOfDay;
    }

    public static int extractMinute(@NonNull String timeString) {
        int minute = 0;
        if (!TextUtils.isEmpty(timeString)) {
            if (timeString.contains("PM")) {
                timeString = timeString.replace("PM", "").trim();
            }
            DateFormat dateFormat = new SimpleDateFormat("hh:mm");
            try {
                Date date = dateFormat.parse(timeString);
                DateFormat format3 = new SimpleDateFormat("mm");
                String minutes = format3.format(date);
                minute = Integer.parseInt(minutes);
            } catch (ParseException | NumberFormatException e) {
                e.printStackTrace();


            }
        }
        return minute;
    }

    public static String getCurrentTime(@NonNull TimeStructure timeStructure) {
        String time = null;
        try {
            time = new SimpleDateFormat(timeStructure.mTimeFormat).format(System.currentTimeMillis());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return time;
    }

    public static String getCurrentDate(@NonNull DateStructure dateStructure) {
        String date = null;
        try {
            date = new SimpleDateFormat(dateStructure.mDateStructure).format(System.currentTimeMillis());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    public static int getCurrentYear() {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.YEAR);
    }

    public static int getCurrentMonth() {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.MONTH)+1;
    }

    public enum DateOption {
        yyyy_DS_MM_dd_HH_COL_mm_ss_TO_dd_MMM_YYYY("yyyy-MM-dd HH:mm:ss", "dd MMM yyyy"),
        yyyy_DS_MM_dd_HH_COL_mm_ss_TO_dd_DS_MMM_YYYY("yyyy-MM-dd HH:mm:ss", "dd-MMM-yyyy"),
        yyyy_DS_MM_dd_HH_COL_mm_ss_TO_MMM_dd_COMA_YYYY("yyyy-MM-dd HH:mm:ss", "MMM dd, yyyy"),
        yyyy_DS_MM_dd_HH_COL_mm_ss_TO_dd_SLS_MM_YY("yyyy-MM-dd HH:mm:ss", "dd/MM/yy");

        public String mInputFormat, mOutputFormat;

        DateOption(@NonNull String inputFormat, @NonNull String outputFormat) {
            this.mInputFormat = inputFormat;
            this.mOutputFormat = outputFormat;
        }
    }

    public enum TimeOption {
        hh_COL_mm_a_TO_HH_COL_mm("hh:mm a", "HH:mm"),
        HH_COL_mm_ss_TO_HH_COL_mm_a("HH:mm:ss", "hh:mm a");

        public String mInputFormat, mOutputFormat;

        TimeOption(@NonNull String inputFormat, @NonNull String outputFormat) {
            this.mInputFormat = inputFormat;
            this.mOutputFormat = outputFormat;
        }
    }

    public enum DateStructure {
        dd_DS_MMM_yyyy("dd-MMM-yyyy"),
        dd_COL_MMM_yyyy("dd:MMM:yyyy"),
        yyyy("yyyy");
        public final String mDateStructure;

        DateStructure(String dateStructure) {
            this.mDateStructure = dateStructure;
        }
    }

    public enum TimeStructure {
        hh_COL_mm_a("hh:mm a"),
        hh_COL_mm("hh:mm"),
        dd_SLS_MM_yyyy_hh_COL_mm("dd/MM/yyyy hh:mm");
        public final String mTimeFormat;

        TimeStructure(String timeFormat) {
            this.mTimeFormat = timeFormat;
        }
    }

}
