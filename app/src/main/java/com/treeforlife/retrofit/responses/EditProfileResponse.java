package com.treeforlife.retrofit.responses;

import com.google.gson.annotations.SerializedName;
import com.treeforlife.dataobjects.TFLUser;

/**
 * Created by TheAppsmiths on 4/9/2018.
 *
 * @author TheAppsmiths
 */

public class EditProfileResponse extends WebServiceResponse {
    @SerializedName("data")
    public TFLUser tflUser;
}
