package com.treeforlife.dataobjects;

import com.google.gson.annotations.SerializedName;

/**
 * Created by TheAppsmiths on 3/26/2018.
 *
 * @author TheAppsmiths
 */

public class Comment {
    @SerializedName("comment_id")
    public String commentId;
    @SerializedName("comment_type")
    public String commentType;
    @SerializedName("comment_text")
    public String commentText;
    @SerializedName("comment_image")
    public String commentImageUrl;
    @SerializedName("comment_video")
    public String commentVideoUrl;
    @SerializedName("comment_video_thumb")
    public String commentVideoThumbnailUrl;
    @SerializedName("comment_by_user_id")
    public String commentatorUserId;
    @SerializedName("comment_by_user_name")
    public String commentatorName;
    @SerializedName("comment_by_user_image_url")
    public String commentatorImageUrl;
    @SerializedName("comment_on_date_time")
    public String commentDate;
    @SerializedName("om_points")
    public int commentOmPoints;

}
