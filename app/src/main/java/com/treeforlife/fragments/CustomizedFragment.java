package com.treeforlife.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.treeforlife.R;
import com.treeforlife.Utils.FragmentUtil;
import com.treeforlife.commons.Constants;
import com.treeforlife.commons.FragmentCallBack;
import com.treeforlife.customview.CustomTextView;

/**
 * Created by TheAppsmiths on 4/2/2018.
 * modified by TheAppsmiths on 03rd April 2018.
 */
/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class CustomizedFragment extends Fragment implements View.OnClickListener {
    public static final String TAG = "CustomizedFragment";

    private Activity mActivity;
    private Context mContext;
    private Bundle mBundle;
    private FragmentCallBack mCallBack;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mActivity = getActivity();
        this.mContext = context;
        this.mBundle = getArguments();
        if (mActivity instanceof FragmentCallBack) {
            this.mCallBack = (FragmentCallBack) mActivity;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_customized, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initComponent(view);
    }

    private void initComponent(View view) {
        if (view != null) {

            View mCustomizeItem1 = view.findViewById(R.id.customized_item_1);
            CustomTextView mCustomizeItem1TextView
                    = mCustomizeItem1.findViewById(R.id.customized_item_text_view);
            mCustomizeItem1TextView.setText(R.string.profile_share);
            mCustomizeItem1.setOnClickListener(this);

            View mCustomizeItem2 = view.findViewById(R.id.customized_item_2);
            CustomTextView mCustomizeItem2TextView
                    = mCustomizeItem2.findViewById(R.id.customized_item_text_view);
            mCustomizeItem2TextView.setText(R.string.videos_share);
            mCustomizeItem2.setOnClickListener(this);

            View mCustomizeItem3 = view.findViewById(R.id.customized_item_3);
            CustomTextView mCustomizeItem3TextView
                    = mCustomizeItem3.findViewById(R.id.customized_item_text_view);
            mCustomizeItem3TextView.setText(R.string.photos_share);
            mCustomizeItem3.setOnClickListener(this);

            View mCustomizeItem4 = view.findViewById(R.id.customized_item_4);
            CustomTextView mCustomizeItem4TextView
                    = mCustomizeItem4.findViewById(R.id.customized_item_text_view);
            mCustomizeItem4TextView.setText(R.string.contacts_share);
            mCustomizeItem4.setOnClickListener(this);

            View mCustomizeItem5 = view.findViewById(R.id.customized_item_5);
            CustomTextView mCustomizeItem5TextView
                    = mCustomizeItem5.findViewById(R.id.customized_item_text_view);
            mCustomizeItem5TextView.setText(R.string.blocked_contacts);
            mCustomizeItem5.setOnClickListener(this);


        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mCallBack != null) {
            Bundle bundle = new Bundle();
            bundle.putString(Constants.KEY_FRAGMENT_CALLBACK, TAG);
            mCallBack.updateBottomBar(bundle);
        }

        if (mActivity != null) {
            mActivity.setTitle(R.string.customized);
        }
    }

    @Override
    public void onClick(View view) {
        if (view != null) {
            Bundle bundle = new Bundle();
            if (mBundle != null) {
                bundle.putAll(mBundle);
            }
            String fragmentName = null;
            switch (view.getId()) {

                case R.id.customized_item_1:
                    fragmentName = FragmentUtil.FRAGMENT_PROFILE_SHARE;
                    break;

                case R.id.customized_item_2:
                    fragmentName = FragmentUtil.FRAGMENT_VIDEO_SHARE;
                    break;

                case R.id.customized_item_3:
                    fragmentName = FragmentUtil.FRAGMENT_PHOTO_SHARE;
                    break;

                case R.id.customized_item_4:
                    fragmentName = FragmentUtil.FRAGMENT_CONTACT_SHARE;
                    break;

                case R.id.customized_item_5:
                    fragmentName = FragmentUtil.FRAGMENT_BLOCKED_CONTACT;
                    break;

            }

            if (!TextUtils.isEmpty(fragmentName)) {
                FragmentUtil.loadFragment((AppCompatActivity) mActivity, fragmentName, bundle,
                        true);
            }
        }
    }
}
