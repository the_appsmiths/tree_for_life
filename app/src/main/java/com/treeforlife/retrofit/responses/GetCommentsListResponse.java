package com.treeforlife.retrofit.responses;

import com.google.gson.annotations.SerializedName;
import com.treeforlife.dataobjects.Comment;

import java.util.ArrayList;

/**
 * Created by TheAppsmiths on 4/5/2018.
 *
 * @author TheAppsmiths
 */

public class GetCommentsListResponse extends WebServiceResponse {
    @SerializedName("total_records")
    public int totalRecords;

    @SerializedName("comments")
    public ArrayList<Comment> commentsList;
}
