package com.treeforlife.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.treeforlife.R;
import com.treeforlife.Utils.AndroidUtil;
import com.treeforlife.Utils.DialogUtil;
import com.treeforlife.Utils.ImageLoaderUtil;
import com.treeforlife.Utils.ProgressDialogUtil;
import com.treeforlife.commons.Constants;
import com.treeforlife.commons.FragmentCallBack;
import com.treeforlife.dataobjects.TFLUser;
import com.treeforlife.retrofit.ResponseId;
import com.treeforlife.retrofit.RetrofitManager;
import com.treeforlife.retrofit.RetrofitResponseValidator;
import com.treeforlife.retrofit.responses.GetConverterResponse;

import java.net.SocketTimeoutException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TheAppsmiths on 3/26/2018.
 */
/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class ConverterFragment extends Fragment implements RetrofitResponseValidator.ValidationListener {

    public static final String TAG = "ConverterFragment";

    private Context mContext;
    private Activity mActivity;
    private Bundle mBundle;
    private FragmentCallBack mCallBack;

    private TextView mGoldCountTextView, mOrdinaryCountTextView, mSilverCountTextView, mCopperCountTextView,
            mHeadingTextView, mWorkingTextView;
    private ImageView mHeaderImageView;

    private BroadcastReceiver mBroadcastReceiver;

    private ProgressDialogUtil mProgressDialogUtil;
    private DialogUtil mDialogUtil;

    private TFLUser mTflUser;

    private boolean isPushNotificationReceiverRegistered;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        this.mContext = context;
        this.mActivity = getActivity();
        mBundle = getArguments();
        if (mActivity != null && mActivity instanceof FragmentCallBack) {
            this.mCallBack = (FragmentCallBack) mActivity;
        }

        mProgressDialogUtil = ProgressDialogUtil.getInstance();
        mDialogUtil = DialogUtil.getInstance();
        extractDataFromBundle();
        this.mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                afterReceivingBroadcast(context, intent);
            }
        };
        registerLocalReceiver();
    }

    private void extractDataFromBundle() {
        if (mBundle != null) {
            mTflUser = mBundle.getParcelable(Constants.KEY_TFL_USER);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_converter, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initComponent(view);
        callGetConverter();
    }


    private void initComponent(View view) {

        mOrdinaryCountTextView = view.findViewById(R.id.ordinary_count_text);
        mSilverCountTextView = view.findViewById(R.id.silver_count_text);
        mCopperCountTextView = view.findViewById(R.id.copper_count_text);
        mGoldCountTextView = view.findViewById(R.id.gold_count_text);
        mHeadingTextView = view.findViewById(R.id.heading_text_view);
        mWorkingTextView = view.findViewById(R.id.working_text);

        mHeaderImageView = view.findViewById(R.id.header_tree_image_view);

        setImageHeader();
    }

    private void setImageHeader() {
        if (mTflUser != null) {
            ImageLoaderUtil.getLoader(mContext).displayImage(mTflUser.profileImage, mHeaderImageView,
                    ImageLoaderUtil.getDefaultHeaderImageOption());
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if (mActivity != null) {
            mActivity.setTitle(R.string.converter);
        }
        if (mCallBack != null) {
            Bundle bundle = new Bundle();
            bundle.putString(Constants.KEY_FRAGMENT_CALLBACK, TAG);
            mCallBack.updateBottomBar(bundle);
        }
    }

    private void afterReceivingBroadcast(Context context, Intent intent) {
        AndroidUtil.dumpIntent(TAG, intent);
        if (intent != null) {
            String mIntentAction = intent.getAction();

            if (!TextUtils.isEmpty(mIntentAction)) {

                switch (mIntentAction) {

                    case Constants.ACTION_RECEIVED_PUSH_NOTIFICATION:
                        afterReceivingPushNotification(intent);
                        break;

                }
            }

        }

    }

    private void afterReceivingPushNotification(@NonNull Intent intent) {
        if (intent != null) {
            int notificationType = intent.getIntExtra(Constants.KEY_PUSH_NOTIFICATION_TYPE, -1);

            switch (notificationType) {

                case Constants.PUSH_NOTIFICATION_RECEIVED_OM_POINTS:
                    callGetConverter();
                    break;
            }
        }


    }

    private void registerLocalReceiver() {
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(mActivity);
        if (localBroadcastManager != null) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(Constants.ACTION_RECEIVED_PUSH_NOTIFICATION);
            localBroadcastManager.registerReceiver(mBroadcastReceiver, intentFilter);
            isPushNotificationReceiverRegistered = true;

        }
    }

    private void unRegisterLocalReceiver() {
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(mActivity);
        if (localBroadcastManager != null && isPushNotificationReceiverRegistered) {
            localBroadcastManager.unregisterReceiver(mBroadcastReceiver);
            isPushNotificationReceiverRegistered = false;

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unRegisterLocalReceiver();
    }

    private void callGetConverter() {

        if (mTflUser != null && AndroidUtil.hasInternetConnectivity(mContext)) {

            mProgressDialogUtil.showProgressDialog(mActivity);

            Call<GetConverterResponse> getAlbum = RetrofitManager.getRetrofitWebService()
                    .getConverter(mTflUser.userId);
            getAlbum.enqueue(new Callback<GetConverterResponse>() {
                @Override
                public void onResponse(Call<GetConverterResponse> call, Response<GetConverterResponse> response) {
                    new RetrofitResponseValidator(ResponseId.GET_CONVERTER, response, ConverterFragment.this);

                }

                @Override
                public void onFailure(Call<GetConverterResponse> call, Throwable t) {
                    String errorMessage = "";
                    if (t != null) {
                        Log.e(TAG, "onFailure: Error : " + t.getMessage());
                        if (t instanceof SocketTimeoutException) {
                            errorMessage = "Please make sure that your device has an active internet connection.";
                        }
                    }

                    mProgressDialogUtil.dismissProgressDialog();


                    if (TextUtils.isEmpty(errorMessage)) {
                        AndroidUtil.showErrorToast(mContext);
                    } else {
                        mDialogUtil.showErrorDialog(mContext, errorMessage);
                    }
                }
            });

        }
    }

    @Override
    public void onValidResponse(@NonNull ResponseId responseId, @NonNull Object responseBody, @NonNull Response response) {
        mProgressDialogUtil.dismissProgressDialog();
        switch (responseId) {
            case GET_CONVERTER:
                afterGetConverter((GetConverterResponse) responseBody);
                break;
        }

    }


    @Override
    public void onInvalidResponse(@NonNull ResponseId responseId, @NonNull String errorMessage, Response response) {
        mProgressDialogUtil.dismissProgressDialog();
        if (response == null || !response.isSuccessful()) {
            mDialogUtil.showErrorDialog(mActivity, errorMessage);
        } else {
            mDialogUtil.showOkAlertDialog(mActivity, errorMessage);
        }
    }

    private void afterGetConverter(GetConverterResponse responseBody) {

        if (responseBody.status == true) {

            mOrdinaryCountTextView.setText(responseBody.ordinaryPoint);
            mSilverCountTextView.setText(responseBody.silverPoint);
            mCopperCountTextView.setText(responseBody.copperPoint);
            mGoldCountTextView.setText(responseBody.goldPoint);
        }
    }
}
