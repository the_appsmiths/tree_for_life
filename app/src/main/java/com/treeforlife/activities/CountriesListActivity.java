package com.treeforlife.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import com.treeforlife.R;
import com.treeforlife.Utils.DialogUtil;
import com.treeforlife.adapters.CountriesRvAdapter;
import com.treeforlife.commons.Constants;
import com.treeforlife.commons.Data;
import com.treeforlife.commons.SharedPrefHelper;
import com.treeforlife.controller.GetCountriesApiController;
import com.treeforlife.customview.CustomEditText;
import com.treeforlife.customview.CustomTextView;
import com.treeforlife.dataobjects.CountryInfo;
import com.treeforlife.dataobjects.TFLUser;

/**
 * @author TheAppsmiths
 */
public class CountriesListActivity extends AppCompatActivity implements View.OnClickListener,
        GetCountriesApiController.ResultCallback,
        CountriesRvAdapter.CountriesItemCallback {
    private static final String TAG = "CountriesListActivity";

    private Intent mActivityIntent;

    private Toolbar mToolbar;
    private ActionBar mActionBar;

    private CustomTextView mToolbarTitle;
    private CustomTextView mNoDataTextView;
    private CustomEditText mSearchEditText;
    private RecyclerView mCountriesRecyclerView;

    private CountriesRvAdapter mCountriesRvAdapter;

    private SharedPrefHelper mSharedPrefHelper;
    private DialogUtil mDialogUtil;
    private GetCountriesApiController mGetCountriesApiController;

    private TFLUser mTflUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_countries_list);
        initComponents();
        initToolBar();
        initWidgets();
    }

    //method to initialize the components
    private void initComponents() {
        mActivityIntent = getIntent();
        mSharedPrefHelper = SharedPrefHelper.getInstance();
        mDialogUtil = DialogUtil.getInstance();
        mGetCountriesApiController = GetCountriesApiController.getInstance();
        ensureTFLUser();
        mCountriesRvAdapter = new CountriesRvAdapter(this);
        CountryInfo selectedCountryInfo = mActivityIntent.getParcelableExtra(Constants.KEY_COUNTRY_INFO);
        mCountriesRvAdapter.setSelectedCountryInfo(selectedCountryInfo);

        String selectedPhoneDialCode = mActivityIntent.getStringExtra(Constants.KEY_PHONE_DIAL_CODE);
        mCountriesRvAdapter.setSelectedCountryInfo(selectedPhoneDialCode);

    }

    //method to check the logged-in user
    private void ensureTFLUser() {
        if (mTflUser == null) {
            mTflUser = mSharedPrefHelper.getLoginResponse(this);
        }
    }

    //method to initialize the tool bar
    private void initToolBar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        if (mToolbar != null) {
            mToolbarTitle = mToolbar.findViewById(R.id.toolbar_title);
            View mToolbarIconLhs = mToolbar.findViewById(R.id.toolbar_icon_back);
            mToolbarIconLhs.setOnClickListener(this);
            View mToolbarIconRhs = mToolbar.findViewById(R.id.toolbar_icon_hamberg);
            mToolbarIconRhs.setVisibility(View.GONE);

        }
        setSupportActionBar(mToolbar);
        mActionBar = getSupportActionBar();
        setTitle(R.string.select_country);
    }

    //method to initialize the widgets
    private void initWidgets() {
        mSearchEditText = findViewById(R.id.search_edit_text);
        setSearchQueryListener();
        mNoDataTextView = (CustomTextView) findViewById(R.id.no_data_text_view);
        mCountriesRecyclerView = findViewById(R.id.rv_countries);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mCountriesRecyclerView.setLayoutManager(layoutManager);
        mCountriesRecyclerView.setAdapter(mCountriesRvAdapter);
        setData();
        manageNoDataTextVisibility();
    }

    //method to set the data to adapter
    private void setData() {
        if (Data.countryCodesList != null && !Data.countryCodesList.isEmpty()) {
            mCountriesRvAdapter.update(Data.countryCodesList);
        } else {
            if (mGetCountriesApiController.loadData(this, this)) {
                mNoDataTextView.setText(R.string.downloading_data);
            }
        }
    }

    //method for setting the query listener
    private void setSearchQueryListener() {
        if (mSearchEditText != null) {
            mSearchEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    String queryText = "";
                    if (s != null) {
                        queryText = s.toString();
                    }
                    if (mCountriesRvAdapter != null) {
                        mCountriesRvAdapter.update(queryText);
                    }
                }
            });
        }
    }

    //method to manage the visibility of no data text view
    private void manageNoDataTextVisibility() {
        if (mNoDataTextView != null && mCountriesRvAdapter != null) {
            mNoDataTextView.setVisibility(mCountriesRvAdapter.getItemCount() > 0 ? View.GONE
                    : View.VISIBLE);
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        setTitleToolBar(title);
    }

    private void setTitleToolBar(CharSequence title) {
        if (mToolbarTitle != null) {
            mToolbarTitle.setText(title);
        }

    }


    @Override
    public void onClick(View view) {
        if (view != null) {

            switch (view.getId()) {

                case R.id.toolbar_icon_back:
                    finish();
                    break;

            }
        }
    }

    @Override
    public void onItemClick(int position, CountryInfo countryInfo) {
        Intent resultIntent = new Intent();
        resultIntent.putExtras(mActivityIntent);
        resultIntent.putExtra(Constants.KEY_COUNTRY_INFO, countryInfo);
        setResult(RESULT_OK, resultIntent);
        finish();
    }

    @Override
    public void onResult(boolean success, String message) {

        if (success) {
            mCountriesRvAdapter.update(Data.countryCodesList);

        } else {
            mDialogUtil.showErrorDialog(this, message);
        }
        mNoDataTextView.setText(R.string.no_data_to_display);
        manageNoDataTextVisibility();
    }
}
