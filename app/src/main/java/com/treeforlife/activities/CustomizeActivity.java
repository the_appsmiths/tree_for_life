package com.treeforlife.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;

import com.treeforlife.R;
import com.treeforlife.commons.Constants;
import com.treeforlife.commons.SharedPrefHelper;
import com.treeforlife.controller.UpdateCustomizeApiController;
import com.treeforlife.customview.CustomTextView;
import com.treeforlife.dataobjects.SearchedTflUser;
import com.treeforlife.dataobjects.TFLUser;

public class CustomizeActivity extends AppCompatActivity implements View.OnClickListener,
        UpdateCustomizeApiController.ResultCallback {
    private static final String TAG = "CustomizeActivity";

    private Intent mActivityIntent;
    private Toolbar mToolbar;
    private ActionBar mActionBar;

    private ImageView mToolbarIconRhs, mToolbarIconLhs;
    private CustomTextView mToolbarTitle;

    private CheckBox mProfileShareCheckBox, mVideoShareCheckBox, mPhotoShareCheckBox,
            mContactShareCheckBox;

    private SharedPrefHelper mSharedPrefHelper;
    private UpdateCustomizeApiController mUpdateCustomizeApiController;

    private SearchedTflUser mSearchedTflUser;
    private TFLUser mTflUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customize);
        initComponents();
        initToolbar();
        initWidgets();
        setData();
    }

    //method to initialize the components
    private void initComponents() {
        this.mSharedPrefHelper = SharedPrefHelper.getInstance();

        this.mActivityIntent = getIntent();
        if (mActivityIntent != null) {
            mSearchedTflUser = mActivityIntent.getParcelableExtra(Constants.KEY_SEARCHED_TFL_USER);
            mTflUser = mActivityIntent.getParcelableExtra(Constants.KEY_TFL_USER);
        }
        ensureTflUser();

        this.mUpdateCustomizeApiController = new UpdateCustomizeApiController(TAG, this,
                this, mTflUser, this);
    }

    //method to check the tfl user
    private void ensureTflUser() {
        if (mTflUser == null) {
            mTflUser = mSharedPrefHelper.getLoginResponse(this);
        }
    }

    //method to initialize the toolbar
    private void initToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        if (mToolbar != null) {
            mToolbarTitle = mToolbar.findViewById(R.id.toolbar_title);
            mToolbarIconLhs = mToolbar.findViewById(R.id.toolbar_icon_back);
            mToolbarIconLhs.setVisibility(View.VISIBLE);
            mToolbarIconLhs.setOnClickListener(this);

            mToolbarIconRhs = mToolbar.findViewById(R.id.toolbar_icon_hamberg);
            mToolbarIconRhs.setVisibility(View.GONE);

        }
        setSupportActionBar(mToolbar);
        mActionBar = getSupportActionBar();

        setToolBarTitle();
    }

    //method to set the data
    private void setData() {
        if (mContactShareCheckBox != null) {
            mProfileShareCheckBox.setChecked(false);
            mVideoShareCheckBox.setChecked(false);
            mPhotoShareCheckBox.setChecked(false);
            mContactShareCheckBox.setChecked(false);

            if (mSearchedTflUser != null) {
                if (!TextUtils.isEmpty(mSearchedTflUser.myProfileShare)
                        && mSearchedTflUser.myProfileShare
                        .equals(Constants.CUSTOMIZED_STATUS_ALLOWED)) {
                    mProfileShareCheckBox.setChecked(true);
                }
                if (!TextUtils.isEmpty(mSearchedTflUser.myVideoShare)
                        && mSearchedTflUser.myVideoShare
                        .equals(Constants.CUSTOMIZED_STATUS_ALLOWED)) {
                    mVideoShareCheckBox.setChecked(true);
                }
                if (!TextUtils.isEmpty(mSearchedTflUser.myPhotoShare)
                        && mSearchedTflUser.myPhotoShare
                        .equals(Constants.CUSTOMIZED_STATUS_ALLOWED)) {
                    mPhotoShareCheckBox.setChecked(true);
                }
                if (!TextUtils.isEmpty(mSearchedTflUser.myContactShare)
                        && mSearchedTflUser.myContactShare
                        .equals(Constants.CUSTOMIZED_STATUS_ALLOWED)) {
                    mContactShareCheckBox.setChecked(true);
                }
            }

        }
    }

    //method to set the tool bar title
    private void setToolBarTitle() {
        if (mToolbarTitle != null) {
            mToolbarTitle.setText(R.string.customize_settings);
        }
    }

    //method to initialize the widgets
    private void initWidgets() {
        mProfileShareCheckBox = (CheckBox) findViewById(R.id.prodile_share_check_box);
        mVideoShareCheckBox = (CheckBox) findViewById(R.id.video_share_check_box);
        mPhotoShareCheckBox = (CheckBox) findViewById(R.id.photo_share_check_box);
        mContactShareCheckBox = (CheckBox) findViewById(R.id.contact_share_check_box);

        Button mSubmitButton = (Button) findViewById(R.id.submit_button);
        mSubmitButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view != null) {

            switch (view.getId()) {

                case R.id.submit_button:
                    mUpdateCustomizeApiController.updateCustomizeDataForFriend(mSearchedTflUser,
                            mProfileShareCheckBox.isChecked() ? Constants.CUSTOMIZED_STATUS_ALLOWED
                                    : Constants.CUSTOMIZED_STATUS_DENIED,
                            mPhotoShareCheckBox.isChecked() ? Constants.CUSTOMIZED_STATUS_ALLOWED
                                    : Constants.CUSTOMIZED_STATUS_DENIED,
                            mVideoShareCheckBox.isChecked() ? Constants.CUSTOMIZED_STATUS_ALLOWED
                                    : Constants.CUSTOMIZED_STATUS_DENIED,
                            mContactShareCheckBox.isChecked() ? Constants.CUSTOMIZED_STATUS_ALLOWED
                                    : Constants.CUSTOMIZED_STATUS_DENIED
                    );
                    break;

                case R.id.toolbar_icon_back:
                    onBackPressed();
                    break;
            }
        }
    }


    @Override
    public void onUpdateCustomizeApiResult(boolean success) {
        if (success) {
            Intent resultIntent = new Intent();
            resultIntent.putExtras(mActivityIntent);
            resultIntent.putExtra(Constants.KEY_HAS_CUSTOMIZE_UPDATED, true);
            setResult(RESULT_OK, resultIntent);
            finish();

        } else {
            setData();
        }
    }
}
