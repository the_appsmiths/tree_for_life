package com.treeforlife.fragments;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.treeforlife.R;
import com.treeforlife.Utils.AndroidUtil;
import com.treeforlife.Utils.AttachFileUtil;
import com.treeforlife.Utils.DialogUtil;
import com.treeforlife.Utils.ImageLoaderUtil;
import com.treeforlife.Utils.ProgressDialogUtil;
import com.treeforlife.adapters.FriendTflUsersRvAdapter;
import com.treeforlife.commons.Constants;
import com.treeforlife.commons.SharedPrefHelper;
import com.treeforlife.customview.CustomEditText;
import com.treeforlife.customview.CustomTextView;
import com.treeforlife.dataobjects.FriendTflUser;
import com.treeforlife.dataobjects.TFLUser;
import com.treeforlife.retrofit.ResponseId;
import com.treeforlife.retrofit.RetrofitManager;
import com.treeforlife.retrofit.RetrofitResponseValidator;
import com.treeforlife.retrofit.responses.CreatedGroupResponse;

import java.io.File;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created on 4th May 2018.
 *
 * @author TheAppsmiths
 */

/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class NewGroupDialogFragment extends DialogFragment implements View.OnClickListener,
        AttachFileUtil.AttachFileCallback,
        FriendTflUsersRvAdapter.FriendTflUsersItemCallback,
        RetrofitResponseValidator.ValidationListener {
    public static final String TAG = "NewGroupDialogFragment";

    private Context mContext;
    private Activity mActivity;
    private Bundle mBundle;
    public NewGroupDialogFragmentCallback mCallback;

    private CircleImageView mGroupImageView;
    private CustomEditText mGroupNameEditText;
    private RecyclerView mParticipantsRecyclerView;
    private CustomTextView mNoDataTextView;

    private FriendTflUsersRvAdapter mParticipantsAdapter;

    private SharedPrefHelper mSharedPrefHelper;
    private ProgressDialogUtil mProgressDialogUtil;
    private DialogUtil mDialogUtil;
    private AttachFileUtil mAttachFileUtil;
    private ImageLoader mImageLoader;

    private TFLUser mTflUser;
    private File mAttachedFile;

    private ArrayList<String> mParticipantsIdList;
    private ArrayList<FriendTflUser> mParticipantsList;
    private String mCaller, mGroupName;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
        this.mActivity = getActivity();
        this.mSharedPrefHelper = SharedPrefHelper.getInstance();
        this.mProgressDialogUtil = ProgressDialogUtil.getInstance();
        this.mDialogUtil = DialogUtil.getInstance();
        this.mAttachFileUtil = AttachFileUtil.getInstance();
        this.mImageLoader = ImageLoaderUtil.getLoader(mContext);

        setStyle(DialogFragment.STYLE_NO_FRAME, R.style.DialogTheme);
        extractDataFromBundle();

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(mContext != null ? mContext :
                mActivity != null ? mActivity : getActivity(),
                R.style.DialogTheme);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ensureComponents();
    }

    private void ensureComponents() {
        if (mContext == null) {
            mContext = getActivity();
        }
        if (mActivity == null) {
            mActivity = getActivity();
        }
        extractDataFromBundle();
    }

    private void extractDataFromBundle() {

        if (mBundle == null) {
            mBundle = getArguments();
        }

        if (mBundle != null) {
            mCaller = mBundle.getString(Constants.KEY_CALLER_COMPONENT);
            mTflUser = mBundle.getParcelable(Constants.KEY_TFL_USER);
            mParticipantsIdList = mBundle.getStringArrayList(Constants.KEY_FRIEND_IDS_LIST);
            mParticipantsList = mBundle.getParcelableArrayList(Constants.KEY_FRIEND_LIST);
        }
        ensureTflUser();

    }

    private void ensureTflUser() {
        if (mTflUser == null) {
            mTflUser = mSharedPrefHelper.getLoginResponse(mContext);
        }
    }

    public void update(Bundle bundle) {
        this.mBundle = bundle;
        extractDataFromBundle();
        ensureAdapter();
    }

    private void ensureAdapter() {
        if (mParticipantsAdapter == null) {
            mParticipantsAdapter = new FriendTflUsersRvAdapter(this);
        }
        mParticipantsAdapter.mCaller = TAG;
        mParticipantsAdapter.update(mParticipantsList);

        manageNoDataTextVisibility();
    }

    public void setCallback(NewGroupDialogFragmentCallback mCallback) {
        this.mCallback = mCallback;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_new_group_dialog, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        manageDialogSoftInputMode();
        initToolbar(view);
        initWidgets(view);
        manageNoDataTextVisibility();
    }

    private void manageDialogSoftInputMode() {
        Dialog dialog = getDialog();
        if (dialog != null) {
            Window window = dialog.getWindow();
            if (window != null) {
                window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN
                        | WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

            }
        }
    }

    private void initToolbar(View view) {
        Toolbar mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        if (mToolbar != null) {
            CustomTextView mToolbarTitle = mToolbar.findViewById(R.id.toolbar_title);
            mToolbarTitle.setText(R.string.new_group);
            View mToolbarIconLhs = mToolbar.findViewById(R.id.toolbar_icon_back);
            mToolbarIconLhs.setVisibility(View.GONE);
            View mToolbarIconRhs = mToolbar.findViewById(R.id.toolbar_icon_hamberg);
            mToolbarIconRhs.setVisibility(View.GONE);

        }
    }

    private void initWidgets(View view) {
        mGroupImageView = (CircleImageView) view.findViewById(R.id.group_image_view);
        mGroupImageView.setOnClickListener(this);

        mGroupNameEditText = (CustomEditText) view.findViewById(R.id.group_name_edit_text);

        mParticipantsRecyclerView = (RecyclerView) view.findViewById(R.id.rv_participants);
        mParticipantsRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        ensureAdapter();
        mParticipantsRecyclerView.setAdapter(mParticipantsAdapter);
        mNoDataTextView = (CustomTextView) view.findViewById(R.id.no_data_text_view);

        View mCreateGroupActionView = view.findViewById(R.id.create_group_action_view);
        mCreateGroupActionView.setOnClickListener(this);
    }

    private void manageNoDataTextVisibility() {
        if (isAdded() && mNoDataTextView != null && mParticipantsAdapter != null) {
            mNoDataTextView.setVisibility(mParticipantsAdapter.getItemCount() > 0
                    ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public void onClick(View view) {
        if (view != null) {

            switch (view.getId()) {

                case R.id.create_group_action_view:
                    callCreateGroup();
                    break;

                case R.id.group_image_view:
                    mAttachFileUtil.start(AttachFileUtil.FileType.FILE_TYPE_IMAGE,
                            mActivity, mAttachedFile != null, this);
                    break;
            }
        }
    }

    private void callCreateGroup() {
        if (isValidData() && AndroidUtil.hasInternetConnectivity(mContext)) {
            mProgressDialogUtil.showProgressDialog(mActivity);

            MultipartBody.Builder builder = new MultipartBody.Builder();
            builder.setType(MultipartBody.FORM);

            builder.addFormDataPart("user_id", mTflUser.userId);
            builder.addFormDataPart("group_name", mGroupName);

            if (mParticipantsIdList != null) {
                for (int i = 0; i < mParticipantsIdList.size(); i++) {
                    builder.addFormDataPart("friend_id[]", mParticipantsIdList.get(i));
                }
            }

            if (mAttachedFile != null) {
                File file1 = new File(mAttachedFile.getName());
                builder.addFormDataPart("group_image", file1.getName(), (RequestBody.create(MediaType.parse("image"), mAttachedFile)));
            }

            MultipartBody requestBody = builder.build();

            Call<CreatedGroupResponse> createAlbumVault = RetrofitManager.getRetrofitWebService()
                    .createGroup(requestBody);

            createAlbumVault.enqueue(new Callback<CreatedGroupResponse>() {
                @Override
                public void onResponse(Call<CreatedGroupResponse> call, Response<CreatedGroupResponse> response) {
                    new RetrofitResponseValidator(ResponseId.CREATE_GROUP, response, NewGroupDialogFragment.this);

                }

                @Override
                public void onFailure(Call<CreatedGroupResponse> call, Throwable t) {
                    String errorMessage = "";
                    if (t != null) {
                        Log.e(TAG, "onFailure: Error : " + t.getMessage());
                        if (t instanceof SocketTimeoutException) {
                            errorMessage = "Please make sure that your device has an active internet connection.";
                        }
                    }

                    mProgressDialogUtil.dismissProgressDialog();
                    if (!isVisible()) {
                        return;
                    }

                    if (TextUtils.isEmpty(errorMessage)) {
                        AndroidUtil.showErrorToast(mActivity);
                    } else {
                        mDialogUtil.showErrorDialog(mActivity, errorMessage);
                    }
                }
            });

        }
    }

    private boolean isValidUser() {

        if (mTflUser == null) {

            AndroidUtil.showToast(mContext, "Invalid User");
            return false;

        } else if (TextUtils.isEmpty(mTflUser.userId)) {

            AndroidUtil.showToast(mContext, "You need to Login !");
            return false;
        }

        return true;
    }

    private boolean isValidData() {
        mGroupName = mGroupNameEditText.getText().toString();
        if (TextUtils.isEmpty(mGroupName)) {
            mGroupNameEditText.setError(getString(R.string.empty_group_name));
            return false;

        } else if (mAttachedFile == null) {
            AndroidUtil.showToast(mContext, "Please add a cover image for this group");
            return false;

        } else if (mAttachedFile != null && !mAttachedFile.exists()) {
            AndroidUtil.showToast(mContext, "Invalid image file");
            return false;

        } else if (mParticipantsList != null && mParticipantsIdList != null
                && mParticipantsList.size() != mParticipantsIdList.size()) {

            AndroidUtil.showToast(mContext, "members & member id list size mismatched ");
            return false;
        }

        return isValidUser();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (mAttachFileUtil != null) {
            mAttachFileUtil.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (mAttachFileUtil != null) {
            mAttachFileUtil.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onNewFile(@NonNull AttachFileUtil.FileType fileType, String attachedVia, Uri fileUri, File file) {
        this.mAttachedFile = file;
        if (fileUri != null) {
            mImageLoader.displayImage(String.valueOf(fileUri), mGroupImageView,
                    ImageLoaderUtil.getDefaultImageOption());
        } else {
            mGroupImageView.setImageResource(android.R.drawable.ic_menu_camera);
        }
        if (mAttachedFile == null && fileUri != null) {
            AndroidUtil.showToast(mContext, "File path could not be fetched.");
        }

    }

    @Override
    public void onError(@NonNull String errorMessage) {
        AndroidUtil.showToast(mContext, errorMessage);
    }

    @Override
    public void onFriendTflUserClick(int position, FriendTflUser friendTflUser) {
        if (friendTflUser != null) {

        }
    }

    @Override
    public void onValidResponse(@NonNull ResponseId responseId, @NonNull Object responseBody, @NonNull Response response) {
        mProgressDialogUtil.dismissProgressDialog();
        switch (responseId) {

            case CREATE_GROUP:
                if (mCallback != null) {
                    mCallback.onResult((CreatedGroupResponse) responseBody);
                }
                dismiss();
                break;

        }
    }

    @Override
    public void onInvalidResponse(@NonNull ResponseId responseId, @NonNull String errorMessage, Response response) {
        mProgressDialogUtil.dismissProgressDialog();
        switch (responseId) {

            case CREATE_GROUP:
                if (response == null || !response.isSuccessful()) {
                    mDialogUtil.showErrorDialog(mActivity, errorMessage);
                } else {
                    mDialogUtil.showOkAlertDialog(mActivity, errorMessage);
                }
                break;

        }
    }


    public interface NewGroupDialogFragmentCallback {
        void onResult(@NonNull CreatedGroupResponse responseBody);
    }
}
