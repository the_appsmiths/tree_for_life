package com.treeforlife.Utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;

import com.treeforlife.R;
import com.treeforlife.commons.Constants;

import java.util.concurrent.atomic.AtomicInteger;


/**
 * Created by TheAppsmiths on 1/22/2018.
 *
 * @author TheAppsmiths
 * @link : https://developer.android.com/guide/topics/ui/notifiers/notifications.html
 */
/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class NotificationsUtil {
    private static final String CHANNEL_ID = "TFL Channel";
    private static final String CHANNEL_NAME = "TFL ";
    private static final String CHANNEL_DESC = "Notification";
    private final static AtomicInteger c = new AtomicInteger(0);
    public static int getID() {
        return c.incrementAndGet();
    }

    public static void showNotification(@NonNull Context context,
                                        String titleText,
                                        String messageText,
                                        Intent actionIntent) {
        if (context == null) {
            return;
        }
        Notification notification = createNotification(context, titleText, messageText,
                actionIntent);

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (notificationManager != null) {
            setNotificationChannel(notificationManager);
            notificationManager.notify(getID(), notification);
        }


    }

    private static void setNotificationChannel(NotificationManager notificationManager) {
        if (notificationManager != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                notificationManager.createNotificationChannel(
                        createNotificationChannel(CHANNEL_ID, CHANNEL_NAME, CHANNEL_DESC));
            }
        }
    }

    public static NotificationChannel createNotificationChannel(String channelId,
                                                                String channelName,
                                                                String channelDescription) {
        NotificationChannel mChannel = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mChannel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH);
            mChannel.setDescription(channelDescription);
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
        }
        return mChannel;
    }

    private static Notification createNotification(@NonNull Context context,
                                                   String titleText,
                                                   String messageText,
                                                   Intent actionIntent) {
        // The id of the channel.
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(context, CHANNEL_ID)
                        .setSmallIcon(R.mipmap.ic_app_notification);

        if (!TextUtils.isEmpty(titleText)) {
            notificationBuilder.setContentTitle(titleText /*messageText*/);
        }
        if (!TextUtils.isEmpty(messageText)) {
            notificationBuilder.setContentText(messageText);
        }

        AndroidUtil.dumpIntent("Notification intent", actionIntent);
        PendingIntent actionPendingIntent = PendingIntent.getActivity(context,
                Constants.RC_ACTIVITY_PENDING_INTENT, actionIntent,
                PendingIntent.FLAG_ONE_SHOT);
        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
        bigTextStyle.setBigContentTitle(titleText);
        bigTextStyle.bigText(messageText);
        notificationBuilder.setStyle(bigTextStyle);


        notificationBuilder.setShowWhen(true);
        notificationBuilder.setContentIntent(actionPendingIntent);
        notificationBuilder.setAutoCancel(true);
        notificationBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        notificationBuilder.setOnlyAlertOnce(true);
        notificationBuilder.setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});

        return notificationBuilder.build();
    }

    public static boolean removeAllNotifications(@NonNull Context context) {
        if (context == null) {
            return false;
        }
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            notificationManager.cancelAll();
            return true;
        }
        return false;
    }
}
