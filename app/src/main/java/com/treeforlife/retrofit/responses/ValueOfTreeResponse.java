package com.treeforlife.retrofit.responses;

import com.google.gson.annotations.SerializedName;
import com.treeforlife.dataobjects.TreeInfo;

import java.util.ArrayList;

/**
 * Created by TheAppsmiths on 4/17/2018.
 *
 * @author TheAppsmiths
 */

public class ValueOfTreeResponse extends WebServiceResponse {
    @SerializedName("data")
    public ArrayList<TreeInfo> treeInfoList;
}
