package com.treeforlife.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.treeforlife.R;
import com.treeforlife.commons.Constants;
import com.treeforlife.customview.CustomTextView;


/**
 * Created by TheAppsmiths on 3/13/2018.
 *
 * @author TheAppsmiths
 */
/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class BottomSheetFragment extends BottomSheetDialogFragment {
    private static final String TAG = "BottomSheetFragment";
    private static final String KEY_BOTTOM_SHEET_ARRAY = "bottom_sheet_array";
    private static final String KEY_BOTTOM_SHEET_REQ_CODE = "bottom_sheet_req_code";

    private Context mContext;
    private Activity mActivity;
    private Bundle mBundle;
    private BottomSheetFragmentCallback mCallback;

    private RecyclerView mBottomSheetRecyclerView;
    private BottomSheetRvAdapter mBottomSheetRvAdapter;

    private String mCaller;
    private int mBottomSheetRequestCode;
    private String[] mBottomSheetArray;


    @SuppressLint("ValidFragment")
    private BottomSheetFragment() {
    }

    public static BottomSheetFragment newInstance(String caller, int bottomSheetRequestCode,
                                                  String[] bottomSheetArray) {

        Bundle arguments = new Bundle();
        arguments.putString(Constants.KEY_CALLER_COMPONENT, caller);
        arguments.putInt(KEY_BOTTOM_SHEET_REQ_CODE, bottomSheetRequestCode);
        arguments.putStringArray(KEY_BOTTOM_SHEET_ARRAY, bottomSheetArray);

        BottomSheetFragment fragment = new BottomSheetFragment();
        fragment.setArguments(arguments);
        return fragment;
    }

    public void setBottomSheetFragmentCallback(BottomSheetFragmentCallback callback) {
        this.mCallback = callback;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
        this.mActivity = getActivity();
        this.mBundle = getArguments();
        if (mBundle != null) {
            mCaller = mBundle.getString(Constants.KEY_CALLER_COMPONENT);
        }
        if (mActivity instanceof BottomSheetFragmentCallback) {

            mCallback = (BottomSheetFragmentCallback) mActivity;

        } else if (mContext instanceof BottomSheetFragmentCallback) {

            mCallback = (BottomSheetFragmentCallback) mContext;

        } else {

            Fragment fragment = getParentFragment();
            if (fragment != null && fragment instanceof BottomSheetFragmentCallback) {
                mCallback = (BottomSheetFragmentCallback) fragment;

            }
        }

        extractDataFromBundle();
        this.mBottomSheetRvAdapter = new BottomSheetRvAdapter(mBottomSheetArray, mCallback);
    }

    private void extractDataFromBundle() {

        if (mBundle == null) {
            mBundle = getArguments();
        }

        if (mBundle != null) {
            mCaller = mBundle.getString(Constants.KEY_CALLER_COMPONENT);
            mBottomSheetRequestCode = mBundle.getInt(KEY_BOTTOM_SHEET_REQ_CODE);
            mBottomSheetArray = mBundle.getStringArray(KEY_BOTTOM_SHEET_ARRAY);

        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_bottom_sheet, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initWidgets(view);
        setRvAdapter();

    }

    private void setRvAdapter() {
        mBottomSheetRecyclerView.setAdapter(mBottomSheetRvAdapter);
    }

    private void initWidgets(View view) {
        if (view != null) {
            mBottomSheetRecyclerView = view.findViewById(R.id.rv_bottom_sheet);
            mBottomSheetRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        }

    }

    public interface BottomSheetFragmentCallback {
        void onBottomSheetItemClick(int bottomSheetRequestCode, int itemPosition, String itemText);
    }

    public class BottomSheetRvAdapter extends RecyclerView.Adapter<BottomSheetRvAdapter.ItemHolder> {
        private static final String TAG = "BottomSheetRvAdapter";

        private final String[] mBottomSheetArray;
        private final BottomSheetFragmentCallback mCallback;

        public BottomSheetRvAdapter(String[] bottomSheetArray,
                                    BottomSheetFragmentCallback callback) {
            mBottomSheetArray = bottomSheetArray;
            mCallback = callback;
        }


        @Override
        public ItemHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            return new ItemHolder(LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.contact_bottom_sheet_item, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(ItemHolder itemHolder, int position) {
            itemHolder.bindData(position);
        }

        @Override
        public int getItemCount() {
            return (mBottomSheetArray != null) ? mBottomSheetArray.length : 0;
        }


        public class ItemHolder extends RecyclerView.ViewHolder {
            private final Context mContext;

            private final View mItemView;
            private final CustomTextView mItemTextView;

            private String mItemText;

            public ItemHolder(View itemView) {
                super(itemView);
                this.mContext = itemView.getContext();
                this.mItemView = itemView;
                mItemTextView = itemView.findViewById(R.id.bottom_sheet_text_view);

            }

            private void bindData(final int position) {
                if (mBottomSheetArray != null && mBottomSheetArray.length > position) {
                    mItemText = mBottomSheetArray[position];
                }

                mItemTextView.setText(mItemText);

                if (mItemView != null) {
                    mItemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (mCallback != null) {
                                mCallback.onBottomSheetItemClick(mBottomSheetRequestCode,
                                        position, mItemText);
                            }
                        }
                    });
                }
            }
        }
    }

}
