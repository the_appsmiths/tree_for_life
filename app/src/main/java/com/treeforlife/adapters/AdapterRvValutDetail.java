package com.treeforlife.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.treeforlife.R;
import com.treeforlife.customview.CustomTextView;

/**
 * Created by TheAppsmiths on 3/19/2018.
 */

public class AdapterRvValutDetail extends RecyclerView.Adapter<AdapterRvValutDetail.MyHolder> {

    private VaultClickDetail mCallBack;

    private String[] mArrays;
    private int[] mImageList;

    public AdapterRvValutDetail(VaultClickDetail mCallBack) {
        this.mCallBack = mCallBack;
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_vault, parent, false));
    }

    @Override
    public void onBindViewHolder(MyHolder holder, int position) {

        holder.bindData(position);

    }

    public void updateList(String[] arrays, int[] mVaultImageList) {
        this.mArrays = arrays;
        this.mImageList = mVaultImageList;
    }

    public interface VaultClickDetail {
        void clickItem(int position, View view);
    }

    @Override
    public int getItemCount() {
        return (mArrays != null) ? mArrays.length : 0;
    }

    //Holder view to show the data in adapter
    public class MyHolder extends RecyclerView.ViewHolder {

        private Context mContext;
        private CustomTextView mVaultNameTextView;
        private View view, mDivider;
        private ImageView mImageForward, mCoverImage;

        public MyHolder(View itemView) {
            super(itemView);
            this.view = itemView;
            this.mContext = itemView.getContext();
            mVaultNameTextView = itemView.findViewById(R.id.vault_name_text);
            mDivider = itemView.findViewById(R.id.divider_view);
            mImageForward = itemView.findViewById(R.id.image_forward);
            mCoverImage = itemView.findViewById(R.id.cover_image);
        }

        public void bindData(int position) {
            mVaultNameTextView.setText(mArrays[position]);
            mCoverImage.setImageResource(mImageList[position]);
            mDivider.setVisibility(position == getItemCount() - 1 ? View.GONE : View.VISIBLE);

            setListener(position);

        }

        private void setListener(final int position) {

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mCallBack != null) {
                        mCallBack.clickItem(position, v);
                    }
                }
            });

        }
    }
}
