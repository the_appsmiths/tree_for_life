package com.treeforlife.dataobjects;

import com.google.gson.annotations.SerializedName;

public class TreePosition {

    @SerializedName("x_position")
    public Double xPosition;

    @SerializedName("y_position")
    public Double yPosition;

    public TreePosition(Double xPosition, Double yPosition) {
        this.xPosition = xPosition;
        this.yPosition = yPosition;
    }

    public Double getxPosition() {
        return xPosition;
    }

    public Double getyPosition() {
        return yPosition;
    }

}
