package com.treeforlife.retrofit.responses;

import com.google.gson.annotations.SerializedName;
import com.treeforlife.dataobjects.FriendTflUser;

import java.util.ArrayList;

/**
 * Created by TheAppsmiths on 4/6/2018.
 *
 */

public class GetMembersListResponse extends WebServiceResponse {

    @SerializedName(value = "members", alternate = {"data"})
    public ArrayList<FriendTflUser> friendsOrFamilyMembersList = new ArrayList<FriendTflUser>();

    @SerializedName("total_records")
    public int totalRecord;

}
