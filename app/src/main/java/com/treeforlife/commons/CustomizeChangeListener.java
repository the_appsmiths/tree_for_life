package com.treeforlife.commons;

/**
 * Created by TheAppsmiths on 5/1/2018.
 *
 * @author TheAppsmiths
 */

public interface CustomizeChangeListener {
    void onCustomizeSettingsChange();
}
