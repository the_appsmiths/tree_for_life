package com.treeforlife.commons;

import android.os.Bundle;

/**
 * Created by TheAppsmiths on 3/28/2018.
 */

public interface FragmentCallBack {

    void updateBottomBar(Bundle bundle);
}
