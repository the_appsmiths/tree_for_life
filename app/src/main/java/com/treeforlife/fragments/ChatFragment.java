package com.treeforlife.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.treeforlife.R;
import com.treeforlife.Utils.AndroidUtil;
import com.treeforlife.Utils.ProgressDialogUtil;
import com.treeforlife.commons.Constants;
import com.treeforlife.commons.FragmentCallBack;
import com.treeforlife.commons.SharedPrefHelper;
import com.treeforlife.dataobjects.TFLUser;

/**
 * Created by TheAppsmiths on 3/19/2018.
 */
/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class ChatFragment extends Fragment implements View.OnClickListener {

    public static final String TAG = "ChatFragment";
    private FragmentCallBack mCallBack;

    private Context mContext;
    private Activity mActivity;
    private Bundle mBundle;

    private Button mProceedToChatButton;


    private ProgressDialogUtil mProgressDialogUtil;
    private SharedPrefHelper mSharedPrefHelper;

    private TFLUser mTflUser;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
        this.mActivity = getActivity();
        mBundle = getArguments();
        if (mActivity instanceof FragmentCallBack) {
            this.mCallBack = (FragmentCallBack) mActivity;
        }


        this.mProgressDialogUtil = ProgressDialogUtil.getInstance();
        this.mSharedPrefHelper = SharedPrefHelper.getInstance();
        extractDataFromBundle();
    }

    private void extractDataFromBundle() {
        if (mBundle == null) {
            mBundle = getArguments();
        }

        ensureTflUser();
    }

    private void ensureTflUser() {
        if (mTflUser == null) {
            if (mBundle != null) {
                mTflUser = mBundle.getParcelable(Constants.KEY_TFL_USER);
            }
        }
        if (mTflUser == null) {
            mTflUser = mSharedPrefHelper.getLoginResponse(mContext);
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if (mActivity != null) {
            mActivity.setTitle(R.string.chat);
        }
        if (mCallBack != null) {
            Bundle bundle = new Bundle();
            bundle.putString(Constants.KEY_FRAGMENT_CALLBACK, TAG);
            mCallBack.updateBottomBar(bundle);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_chat, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        intiComponents(view);
    }

    private void intiComponents(View view) {
        if (view != null) {

            mProceedToChatButton = view.findViewById(R.id.proceed_to_chat);
            mProceedToChatButton.setVisibility(View.GONE);
            mProceedToChatButton.setEnabled(false);
            mProceedToChatButton.setOnClickListener(this);
        }


    }


    @Override
    public void onClick(View view) {
        if (view != null) {
            AndroidUtil.disableEnableView(view);
            switch (view.getId()) {

                case R.id.proceed_to_chat:
                    break;
            }

        }
    }

}
