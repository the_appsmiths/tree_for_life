package com.treeforlife.fragments;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.treeforlife.R;
import com.treeforlife.Utils.AndroidUtil;
import com.treeforlife.Utils.AttachFileUtil;
import com.treeforlife.Utils.DialogUtil;
import com.treeforlife.Utils.FragmentUtil;
import com.treeforlife.Utils.ProgressDialogUtil;
import com.treeforlife.adapters.AdapterRvVideo;
import com.treeforlife.commons.Constants;
import com.treeforlife.commons.UploadImageVideoDialog;
import com.treeforlife.dataobjects.TFLUser;
import com.treeforlife.dataobjects.Video;
import com.treeforlife.retrofit.ResponseId;
import com.treeforlife.retrofit.RetrofitManager;
import com.treeforlife.retrofit.RetrofitResponseValidator;
import com.treeforlife.retrofit.responses.GetVideoResponse;
import com.treeforlife.retrofit.responses.UploadVideoResponse;
import com.treeforlife.retrofit.responses.WebServiceResponse;

import java.io.File;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TheAppsmiths on 3/20/2018.
 */

/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class VideoFragment extends Fragment implements View.OnClickListener, AdapterRvVideo.VideoCallBack, RetrofitResponseValidator.ValidationListener, UploadImageVideoDialog.UploadDialogCallBack {

    public static final String TAG = "VideoFragment";

    private Context mContext;
    private Activity mActivity;
    private RecyclerView mRecyclerView;
    private AdapterRvVideo adapterRvVideo;
    private TextView mNoDataTextView, mVideoUploadText;
    private AttachFileUtil mAttachFileUtil;
    private View mWishView;
    private ProgressDialogUtil mProgressDialogUtil;
    private DialogUtil mDialogUtil;
    private String mVideoTitle;
    private Bundle mBundle;
    private TFLUser mTflUser;
    private ArrayList<Video> videosList = new ArrayList<Video>();
    private String mVideoId;
    private int mVideoPosition;
    private boolean isLoadingData;
    private int mPage;
    private UploadImageVideoDialog mDialog;
    private String mAfterCompressPath;
    private ProgressBar mProgressBar;
    private String outputDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
        this.mActivity = getActivity();
        this.adapterRvVideo = new AdapterRvVideo(this);
        mAttachFileUtil = AttachFileUtil.getInstance();
        mProgressDialogUtil = ProgressDialogUtil.getInstance();
        mDialogUtil = DialogUtil.getInstance();
        mDialog = new UploadImageVideoDialog(mContext);
        extractDataFromBundle(true);
    }

    private void extractDataFromBundle(boolean extractAfterGet) {

        if (extractAfterGet && mBundle == null) {
            mBundle = getArguments();
        }

        if (mBundle != null) {
            mTflUser = mBundle.getParcelable(Constants.KEY_TFL_USER);
        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_video, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initComponets(view);
        callGetVideos();
    }


    private void initComponets(View view) {

        mProgressBar = view.findViewById(R.id.progress_bar);

        mNoDataTextView = view.findViewById(R.id.no_data_text_view);
        mWishView = view.findViewById(R.id.wish_layout);
        mVideoUploadText = view.findViewById(R.id.video_upload_text);
        mVideoUploadText.setOnClickListener(this);

        mRecyclerView = view.findViewById(R.id.recycleview_video);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(layoutManager);
        setInfiniteScroller(layoutManager);
        mRecyclerView.setAdapter(adapterRvVideo);
        setNoDataTextVisibiltiy();
    }

    private void setInfiniteScroller(final LinearLayoutManager layoutManager) {
        if (layoutManager != null && mRecyclerView != null) {
            mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    int lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition();
                    int totalItemCount = layoutManager.getItemCount();
                    Log.e("TotalItemCountHome", "" + lastVisibleItemPosition + " " + Constants.ITEMS_LIMIT + " " + totalItemCount);

                    if ((lastVisibleItemPosition + Constants.ITEMS_LIMIT) > totalItemCount) {
                        Log.w(TAG, " RECYCLER ON SCROLL : loadData() called");
                        callGetVideos();
                    }
                }
            });
        }
    }

    private void setNoDataTextVisibiltiy() {
        if (mNoDataTextView != null && adapterRvVideo != null) {
            mNoDataTextView.setVisibility(adapterRvVideo.getItemCount() > 0 ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.video_upload_text:
                mDialog.UploadDialog(UploadImageVideoDialog.UploadFile.UPLOAD_VIDEO, this);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (mDialog != null) {
            mDialog.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (mDialog != null) {
            mDialog.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void PlayVideo(int position, Video mVideo) {

        if (mVideo != null) {
            Bundle bundle = new Bundle();
            bundle.putString(Constants.KEY_CALLER_COMPONENT, TAG);
            bundle.putString(Constants.KEY_VIDEO_URL, mVideo.videoUrl);
            FragmentUtil.loadFragment((AppCompatActivity) mActivity,
                    FragmentUtil.FRAGMENT_EASY_VIDEO_PLAYER, bundle,
                    true);
            // FragmentUtil.loadFragment((AppCompatActivity) mContext, FragmentUtil.FRAGMENT_VIDEO_VIEW, bundle, true);

        }

    }

    @Override
    public void RemoveItem(int position, Video mVideo) {
        if (mVideo == null) {
            return;
        }
        mVideoId = mVideo.videoId;
        mVideoPosition = position;

        Runnable OkButton = new Runnable() {
            @Override
            public void run() {
                deleteVideo(mVideoId);
            }
        };
        String videoName = mVideo.videoDescription;
        if (TextUtils.isEmpty(videoName)) {
            videoName = "this";
        } else {
            videoName = "\"" + videoName + "\"";
        }
        mDialogUtil.showOkCancelAlertDialog(mActivity, "Are you sure you want to delete " +
                videoName + " video?", OkButton);


    }

    @Override
    public void ShareVideo(int position, Video mVideo) {

        mVideoId = mVideo.videoId;
        mVideoPosition = position;

        Runnable OkButton = new Runnable() {
            @Override
            public void run() {
                shareVideo(mVideoId);
            }
        };
        mDialogUtil.showOkCancelAlertDialog(mActivity, "Are you sure you want to share this video?", OkButton);


    }


    @Override
    public void onResume() {
        super.onResume();
        if (mActivity != null) {
            mActivity.setTitle(R.string.videos);
        }
        if (mAttachFileUtil != null) {
            mAttachFileUtil.onResume();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mDialog != null) {
            mDialog.DismisDialog();
        }

    }

    private void callGetVideos() {

        if (mTflUser != null && AndroidUtil.hasInternetConnectivity(mContext)
                && !isLoadingData) {

            isLoadingData = true;

            if (mPage == 0) {
                mProgressDialogUtil.showProgressDialog(mActivity);
            }

            Call<GetVideoResponse> getAlbum = RetrofitManager.getRetrofitWebService()
                    .getVideos(mTflUser.userId, mPage);
            getAlbum.enqueue(new Callback<GetVideoResponse>() {
                @Override
                public void onResponse(Call<GetVideoResponse> call, Response<GetVideoResponse> response) {
                    new RetrofitResponseValidator(ResponseId.GET_VIDEOS, response, VideoFragment.this);

                }

                @Override
                public void onFailure(Call<GetVideoResponse> call, Throwable t) {
                    String errorMessage = "";
                    if (t != null) {
                        Log.e(TAG, "onFailure: Error : " + t.getMessage());
                        if (t instanceof SocketTimeoutException) {
                            errorMessage = "Please make sure that your device has an active internet connection.";
                        }
                    }

                    mProgressDialogUtil.dismissProgressDialog();

                    isLoadingData = false;

                    if (!isVisible() || mPage != 0) {
                        return;
                    }

                    if (TextUtils.isEmpty(errorMessage)) {
                        AndroidUtil.showErrorToast(mContext);
                    } else {
                        mDialogUtil.showErrorDialog(mContext, errorMessage);
                    }
                }
            });

        }
    }


    private void callUploadVideo(File mAttachedFile, String description) {

        if (AndroidUtil.hasInternetConnectivity(mContext)) {

            mProgressDialogUtil.showProgressDialog(mActivity);
            MultipartBody.Builder builder = new MultipartBody.Builder();
            builder.setType(MultipartBody.FORM);
            builder.addFormDataPart("user_id", mTflUser.userId);
            builder.addFormDataPart("video_description", description);
            // Single Image

            if (mAttachedFile != null) {
                File file1 = new File(mAttachedFile.getName());
                builder.addFormDataPart("video", file1.getName(), (RequestBody.create(MediaType.parse("video/*"), mAttachedFile)));

            }

            MultipartBody requestBody = builder.build();

            Call<UploadVideoResponse> createAlbumVault = RetrofitManager.getRetrofitWebService()
                    .uploadVideo(requestBody);

            createAlbumVault.enqueue(new Callback<UploadVideoResponse>() {
                @Override
                public void onResponse(Call<UploadVideoResponse> call, Response<UploadVideoResponse> response) {
                    new RetrofitResponseValidator(ResponseId.VIDEO_UPLOAD, response, VideoFragment.this);

                }

                @Override
                public void onFailure(Call<UploadVideoResponse> call, Throwable t) {
                    String errorMessage = "";
                    if (t != null) {
                        Log.e(TAG, "onFailure: Error : " + t.getMessage());
                        if (t instanceof SocketTimeoutException) {
                            errorMessage = "Please make sure that your device has an active internet connection.";
                        }
                    }

                    mProgressDialogUtil.dismissProgressDialog();


                    if (TextUtils.isEmpty(errorMessage)) {
                        AndroidUtil.showErrorToast(mActivity);
                    } else {
                        mDialogUtil.showErrorDialog(mActivity, errorMessage);
                    }
                }
            });

        }


    }


    private void deleteVideo(String mVideoId) {

        if (mTflUser != null && AndroidUtil.hasInternetConnectivity(mContext)) {
            mProgressDialogUtil.showProgressDialog(mActivity);

            Call<WebServiceResponse> getAlbum = RetrofitManager.getRetrofitWebService()
                    .deleteVideos(mTflUser.userId, mVideoId);
            getAlbum.enqueue(new Callback<WebServiceResponse>() {
                @Override
                public void onResponse(Call<WebServiceResponse> call, Response<WebServiceResponse> response) {
                    new RetrofitResponseValidator(ResponseId.DELETE_VIDEO, response, VideoFragment.this);

                }

                @Override
                public void onFailure(Call<WebServiceResponse> call, Throwable t) {
                    String errorMessage = "";
                    if (t != null) {
                        Log.e(TAG, "onFailure: Error : " + t.getMessage());
                        if (t instanceof SocketTimeoutException) {
                            errorMessage = "Please make sure that your device has an active internet connection.";
                        }
                    }

                    mProgressDialogUtil.dismissProgressDialog();


                    if (TextUtils.isEmpty(errorMessage)) {
                        AndroidUtil.showErrorToast(mContext);
                    } else {
                        mDialogUtil.showErrorDialog(mContext, errorMessage);
                    }
                }
            });

        }
    }

    private void shareVideo(String mVideoId) {

        if (mTflUser != null && AndroidUtil.hasInternetConnectivity(mContext)) {
            mProgressDialogUtil.showProgressDialog(mActivity);

            Call<WebServiceResponse> getAlbum = RetrofitManager.getRetrofitWebService()
                    .shareVideos(mTflUser.userId, mVideoId);
            getAlbum.enqueue(new Callback<WebServiceResponse>() {
                @Override
                public void onResponse(Call<WebServiceResponse> call, Response<WebServiceResponse> response) {
                    new RetrofitResponseValidator(ResponseId.SHARE_VIDEO, response, VideoFragment.this);

                }

                @Override
                public void onFailure(Call<WebServiceResponse> call, Throwable t) {
                    String errorMessage = "";
                    if (t != null) {
                        Log.e(TAG, "onFailure: Error : " + t.getMessage());
                        if (t instanceof SocketTimeoutException) {
                            errorMessage = "Please make sure that your device has an active internet connection.";
                        }
                    }

                    mProgressDialogUtil.dismissProgressDialog();


                    if (TextUtils.isEmpty(errorMessage)) {
                        AndroidUtil.showErrorToast(mContext);
                    } else {
                        mDialogUtil.showErrorDialog(mContext, errorMessage);
                    }
                }
            });

        }
    }

    @Override
    public void onValidResponse(@NonNull ResponseId responseId, @NonNull Object responseBody, @NonNull Response response) {

        switch (responseId) {
            case VIDEO_UPLOAD:
                afterVideoUpload((UploadVideoResponse) responseBody);
                isLoadingData = false;
                break;
            case GET_VIDEOS:
                afterGetVideos((GetVideoResponse) responseBody);
                break;
            case DELETE_VIDEO:
                afterDeleteVideo((WebServiceResponse) responseBody);
                break;
            case SHARE_VIDEO:
                afterShareVideo((WebServiceResponse) responseBody);
                break;
        }
        mProgressDialogUtil.dismissProgressDialog();

    }


    @Override
    public void onInvalidResponse(@NonNull ResponseId responseId, @NonNull String errorMessage, Response response) {
        mProgressDialogUtil.dismissProgressDialog();

        switch (responseId) {
            case GET_VIDEOS:
                isLoadingData = false;
                if (!isVisible() || mPage != 0) {
                    return;
                }
                setNoDataTextVisibiltiy();
                break;
        }
        if (response == null || !response.isSuccessful()) {
            mDialogUtil.showErrorDialog(mActivity, errorMessage);
        } else {
            AndroidUtil.showToast(mContext, errorMessage);
        }
    }

    private void afterGetVideos(GetVideoResponse responseBody) {

        if (responseBody != null) {
           /* videosList = responseBody.videosList;
            adapterRvVideo.updataAdapter(videosList);*/
            ArrayList<Video> list = responseBody.videosList;

            if (list != null && !list.isEmpty()) {
                videosList.addAll(list);
            }
            if (adapterRvVideo != null) {
                adapterRvVideo.updataAdapter(videosList);
            }

            setNoDataTextVisibiltiy();
            Log.e("Size and Page", "" + videosList + " " + mPage);

            if (videosList.size() <= responseBody.totalRecords) {
                mPage++;
            }
        }
    }


    private void afterVideoUpload(UploadVideoResponse responseBody) {
        if (responseBody != null && responseBody.status == true) {
            AndroidUtil.showToast(mActivity, responseBody.message);
            Video mVideo = responseBody.video;
            videosList.add(mVideo);
            adapterRvVideo.updataAdapter(videosList);
            setNoDataTextVisibiltiy();
            mPage++;
        }
    }

    private void afterDeleteVideo(WebServiceResponse responseBody) {
        if (responseBody != null && responseBody.status == true) {
            videosList.remove(mVideoPosition);
            adapterRvVideo.notifyDataSetChanged();
            AndroidUtil.showToast(mActivity, responseBody.message);
            setNoDataTextVisibiltiy();
        }
    }

    private void afterShareVideo(WebServiceResponse responseBody) {
        if (responseBody != null && responseBody.status == true) {
            AndroidUtil.showToast(mActivity, responseBody.message);
            setNoDataTextVisibiltiy();
        }
    }

    @Override
    public void onitemCall(final File mAttachedFile, final String description) {
        //Get Video Call Back and Video Upload
        if (mAttachedFile != null && !TextUtils.isEmpty(description)) {
            //  Without Zip call this Mathod
            callUploadVideo(mAttachedFile, description);
            final String destPath = outputDir + File.separator + "VID_" + new SimpleDateFormat("yyyyMMdd_HHmmss", getLocale()).format(new Date()) + ".mp4";

       /* VideoCompress.compressVideoLow(mAttachedFile.toString(),destPath, new VideoCompress.CompressListener() {
            @Override
            public void onStart() {
               mProgressDialogUtil.showVideoCompressProgressDialog(mActivity);
            }

            @Override
            public void onSuccess() {
                mProgressDialogUtil.dismissProgressDialog();
                File file = new File(destPath);
                // This Method call to upload Compress Video
                callUploadVideo(file, description);
            }

            @Override
            public void onFail() {
                mProgressDialogUtil.dismissProgressDialog();
            }

            @Override
            public void onProgress(float percent) {
                Log.e("Percent", "" + percent);
            }
        });*/
        }
    }

    private Locale getLocale() {
        Configuration config = getResources().getConfiguration();
        Locale sysLocale = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            sysLocale = getSystemLocale(config);
        } else {
            sysLocale = getSystemLocaleLegacy(config);
        }

        return sysLocale;
    }

    @SuppressWarnings("deprecation")
    public static Locale getSystemLocaleLegacy(Configuration config) {
        return config.locale;
    }

    @TargetApi(Build.VERSION_CODES.N)
    public static Locale getSystemLocale(Configuration config) {
        return config.getLocales().get(0);
    }
}
