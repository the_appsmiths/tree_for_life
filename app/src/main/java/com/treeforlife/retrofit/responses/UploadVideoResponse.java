package com.treeforlife.retrofit.responses;

import com.google.gson.annotations.SerializedName;
import com.treeforlife.dataobjects.Video;

/**
 * Created by TheAppsmiths on 4/9/2018.
 */

public class UploadVideoResponse extends WebServiceResponse {

    @SerializedName("video")
    public Video video;
}
