package com.treeforlife.retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.treeforlife.BuildConfig;
import com.treeforlife.retrofit.responses.AssignOmPointsResponse;
import com.treeforlife.retrofit.responses.CreateAlbumResponse;
import com.treeforlife.retrofit.responses.CreatedGroupResponse;
import com.treeforlife.retrofit.responses.EditProfileResponse;
import com.treeforlife.retrofit.responses.GetAlbumImagesResponse;
import com.treeforlife.retrofit.responses.GetAlbumResponse;
import com.treeforlife.retrofit.responses.GetCommentsListResponse;
import com.treeforlife.retrofit.responses.GetConverterResponse;
import com.treeforlife.retrofit.responses.GetCountriesListResponse;
import com.treeforlife.retrofit.responses.GetCustomizedDataResponse;
import com.treeforlife.retrofit.responses.GetGroupResponse;
import com.treeforlife.retrofit.responses.GetMembersListResponse;
import com.treeforlife.retrofit.responses.GetTutorialResponse;
import com.treeforlife.retrofit.responses.GetVideoResponse;
import com.treeforlife.retrofit.responses.GetWishResponse;
import com.treeforlife.retrofit.responses.LoginResponse;
import com.treeforlife.retrofit.responses.MyQRCodeResponse;
import com.treeforlife.retrofit.responses.NotificationListResponse;
import com.treeforlife.retrofit.responses.PostCommentResponse;
import com.treeforlife.retrofit.responses.RegisterResponse;
import com.treeforlife.retrofit.responses.SearchTflUserResponse;
import com.treeforlife.retrofit.responses.TreeInfoOfYearResponse;
import com.treeforlife.retrofit.responses.TreeInfoResponse;
import com.treeforlife.retrofit.responses.UploadImageResponse;
import com.treeforlife.retrofit.responses.UploadVideoResponse;
import com.treeforlife.retrofit.responses.ValueOfTreeResponse;
import com.treeforlife.retrofit.responses.WebServiceResponse;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by TheAppsmiths on 4/3/2018.
 */

//Methods to get the instances of retrofit and api calling.
public class RetrofitManager {

    private static RetrofitWebService mRetrofitWebService;
    private static CometChatWebService mCometChatWebService;

    public static RetrofitWebService getRetrofitWebService() {
        if (mRetrofitWebService == null) {
            OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();
            httpClientBuilder.readTimeout(5, TimeUnit.MINUTES)
                    .connectTimeout(5, TimeUnit.MINUTES);

            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClientBuilder.addInterceptor(interceptor);

            Gson gson = new GsonBuilder().setLenient().create();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BuildConfig.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(httpClientBuilder.build())
                    .build();

            mRetrofitWebService = retrofit.create(RetrofitWebService.class);
        }

        return mRetrofitWebService;
    }

    public static CometChatWebService getCometChatWebService() {

        if (mCometChatWebService == null) {
            OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();
            httpClientBuilder.readTimeout(5, TimeUnit.MINUTES)
                    .connectTimeout(5, TimeUnit.MINUTES);

            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClientBuilder.addInterceptor(interceptor);

            Gson gson = new GsonBuilder().setLenient().create();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://api.cometondemand.net/api/v2/")
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(httpClientBuilder.build())
                    .build();

            mCometChatWebService = retrofit.create(CometChatWebService.class);
        }

        return mCometChatWebService;

    }

    public interface RetrofitWebService {

        @FormUrlEncoded
        @POST("users/login")
        Call<LoginResponse> callLogin(
                @Field("login_type") int loginType,
                @Field("phone_number") String phoneNumber,
                @Field("email_address") String emailAddress,
                @Field("password") String password,
                @Field("device_id") String deviceId,
                @Field("device_token") String deviceToken,
                @Field("device_type") String deviceType
        );

        @FormUrlEncoded
        @POST("users/forgotpassword")
        Call<WebServiceResponse> forgetPassword(
                @Field("email") String email,
                @Field("phone_number") String phoneNumber
        );


        @POST("users/registerphone")
        Call<RegisterResponse> registerPhone(@Body RequestBody file);

        @POST("users/register")
        Call<RegisterResponse> registerEmail(@Body RequestBody file);

        @POST("apis/editprofile ")
        Call<EditProfileResponse> editProfile(@Body RequestBody file);


        @FormUrlEncoded
        @POST("apis/verifyotp")
        Call<WebServiceResponse> verifyOtp(
                @Field("user_id") String userId,
                @Field("otp") String otp
        );

        @FormUrlEncoded
        @POST("users/generateotp ")
        Call<WebServiceResponse> generateOtp(
                @Field("user_id") String userId
        );

        @FormUrlEncoded
        @POST("users/vaultlogin")
        Call<WebServiceResponse> vaultLogin(
                @Field("user_id") String userId,
                @Field("mpin") String mpin
        );

        @FormUrlEncoded
        @POST("users/setvaultpassword")
        Call<WebServiceResponse> setVaultPassword(
                @Field("user_id") String userId,
                @Field("mpin") String mpin
        );

        @FormUrlEncoded
        @POST("users/vaultforgotpassword")
        Call<WebServiceResponse> forgotVaultPassword(
                @Field("user_id") String userId
        );

        @FormUrlEncoded
        @POST("users/logout")
        Call<WebServiceResponse> logout(
                @Field("user_id") String userId,
                @Field("tree_id") String treeId,
                @Field("device_type") String deviceType,
                @Field("device_id") String deviceId

        );

        @POST("users/createcomment")
        Call<PostCommentResponse> postComment(@Body RequestBody requestBody);

        @FormUrlEncoded
        @POST("users/getalbums")
        Call<GetAlbumResponse> getVaultAlbum(
                @Field("user_id") String userId,
                @Field("page") int page
        );


        @POST("users/createalbum")
        Call<CreateAlbumResponse> createAlbum(@Body RequestBody file);


        @FormUrlEncoded
        @POST("users/deletealbum")
        Call<WebServiceResponse> deleteAlbum(
                @Field("user_id") String userId,
                @Field("album_id") int albumId
        );

        @FormUrlEncoded
        @POST("users/getalbumimages")
        Call<GetAlbumImagesResponse> getAlbumImages(
                @Field("user_id") String userId,
                @Field("album_id") int albumId,
                @Field("page") int page

        );

        @POST("users/uploadalbumimage")
        Call<UploadImageResponse> uploadImage(@Body RequestBody file);

        @FormUrlEncoded
        @POST("users/deletealbumimage")
        Call<WebServiceResponse> deleteImage(
                @Field("user_id") String userId,
                @Field("album_id") int albumId,
                @Field("image_id") int imageId
        );

        @FormUrlEncoded
        @POST("users/sharealbumimage")
        Call<WebServiceResponse> shareImage(
                @Field("user_id") String userId,
                @Field("album_id") int albumId,
                @Field("image_id") int imageId
        );

        @POST("users/uploadvideo")
        Call<UploadVideoResponse> uploadVideo(@Body RequestBody file);

        @FormUrlEncoded
        @POST("users/getvideos")
        Call<GetVideoResponse> getVideos(
                @Field("user_id") String userId,
                @Field("page") int page
        );

        @FormUrlEncoded
        @POST("users/sharevideo")
        Call<WebServiceResponse> shareVideos(
                @Field("user_id") String userId,
                @Field("video_id") String videoId
        );

        @FormUrlEncoded
        @POST("users/deletevideo")
        Call<WebServiceResponse> deleteVideos(
                @Field("user_id") String userId,
                @Field("video_id") String videoId
        );

        @FormUrlEncoded
        @POST("users/myfriend")
        Call<GetMembersListResponse> myFreind(
                @Field("user_id") String userId,
                @Field("page") int page
        );

        @FormUrlEncoded
        @POST("apis/removefriendmember")
        Call<WebServiceResponse> removeFriend(
                @Field("user_id") String userId,
                @Field("member_id") int memberId
        );

        @POST("users/creategroup")
        Call<CreatedGroupResponse> createGroup(@Body RequestBody file);

        @FormUrlEncoded
        @POST("users/getgroups")
        Call<GetGroupResponse> getGroups(
                @Field("user_id") String userId,
                @Field("page") int page
        );

        @FormUrlEncoded
        @POST("users/deletegroup")
        Call<WebServiceResponse> deleteGroups(
                @Field("user_id") String userId,
                @Field("group_id") int groupId
        );

        @FormUrlEncoded
        @POST("users/getcomments")
        Call<GetCommentsListResponse> getComments(
                        @Field("user_id") String userId,
                        @Field("fruit_type") int fruitType,
                        @Field("page") int pageNo
                );

        @FormUrlEncoded
        @POST("users/getusercomments")
        Call<GetCommentsListResponse> getMyComments(
                @Field("user_id") String userId,
                @Field("fruit_type") int fruitType,
                @Field("page") int pageNo
        );

        @FormUrlEncoded
        @POST("users/assignompoints")
        Call<AssignOmPointsResponse> assignOmPoints(
                @Field("user_id") String userId,
                @Field("fruit_type") int fruitType,
                @Field("comment_id") String commentId,
                @Field("om_points") int omPoints
        );

        @FormUrlEncoded
        @POST("users/getconverter")
        Call<GetConverterResponse> getConverter(
                @Field("user_id") String userId
        );

        @GET("users/gettutorial")
        Call<GetTutorialResponse> getTutorial();

        @FormUrlEncoded
        @POST("users/getcustomized")
        Call<GetCustomizedDataResponse> getCustomizedData(
                @Field("user_id") String userId,
                @Field("type") int customizeType,
                @Field("page") int page
        );

        @FormUrlEncoded
        @POST("users/addcustomized")
        Call<WebServiceResponse> updateCustomize(
                @Field("user_id") String userId,
                @Field("friend_id") String friendId,
                @Field("profile_share") String profileShare,
                @Field("photo_share") String photoShare,
                @Field("video_share") String videoShare,
                @Field("contacts_share") String contactShare
        );

        @FormUrlEncoded
        @POST("users/searchuser")
        Call<WebServiceResponse> getUserProfile(
                @Field("user_id") String userId,
                @Field("tree_id") String treeId
        );

        @FormUrlEncoded
        @POST("apis/blockfriend")
        Call<WebServiceResponse> blockFriend(
                @Field("user_id") String userId,
                @Field("friend_id") String treeId
        );

        @FormUrlEncoded
        @POST("apis/acceptfriendrequest")
        Call<WebServiceResponse> acceptFriendRequest(
                @Field("user_id") String userId,
                @Field("friend_id") String treeId
        );

        @FormUrlEncoded
        @POST("users/addfriend")
        Call<WebServiceResponse> sendFriendRequest(
                @Field("user_id") String userId,
                @Field("friend_id") String treeId
        );

        @FormUrlEncoded
        @POST("users/rejectfriendrequest")
        Call<WebServiceResponse> rejectFriendRequest(
                @Field("user_id") String userId,
                @Field("friend_id") String treeId
        );

        @FormUrlEncoded
        @POST("apis/removefriendmember")
        Call<WebServiceResponse> removeFriend(
                @Field("user_id") String userId,
                @Field("member_id") String treeId
        );

        @FormUrlEncoded
        @POST("users/removefamilymember")
        Call<WebServiceResponse> removeFamilyFriend(
                @Field("user_id") String userId,
                @Field("member_id") String treeId
        );

        @FormUrlEncoded
        @POST("users/addfamilymember")
        Call<GetMembersListResponse> addToFamily(
                @Field("user_id") String userId,
                @Field("friend_id[]") ArrayList<String> friendList
        );

        @FormUrlEncoded
        @POST("users/addfamilymember")
        Call<WebServiceResponse> addFriendsToFamily(
                @Field("user_id") String userId,
                @Field("friend_id[]") ArrayList<String> friendList
        );

        @FormUrlEncoded
        @POST("users/myfamily")
        Call<GetMembersListResponse> getFamilyMembers(
                @Field("user_id") String userId,
                @Field("page") int page);

        @FormUrlEncoded
        @POST("users/myfamilylist")
        Call<GetMembersListResponse> getNotFamilyMembers(
                @Field("user_id") String userId,
                @Field("page") int page
        );

        @FormUrlEncoded
        @POST("users/removegroupmember")
        Call<WebServiceResponse> removeMember(
                @Field("user_id") String userId,
                @Field("member_id") String memberId,
                @Field("group_id") int groupId);

        @FormUrlEncoded
        @POST("users/addgroupmember")
        Call<WebServiceResponse> addMembers(
                @Field("user_id") String userId,
                @Field("friend_id[]") ArrayList<String> friendList,
                @Field("group_id") int groupId);

        @FormUrlEncoded
        @POST("users/groupmembers")
        Call<GetMembersListResponse> getMembers(
                @Field("user_id") String userId,
                @Field("group_id") int groupId,
                @Field("page") int page);

        @FormUrlEncoded
        @POST("users/myfriendlist")
        Call<GetMembersListResponse> getNotInGroupMembers(
                @Field("user_id") String userId,
                @Field("group_id") int groupId,
                @Field("page") int Page
        );

        @FormUrlEncoded
        @POST("apis/unblockfriend")
        Call<WebServiceResponse> unblockFriend(
                @Field("user_id") String userId,
                @Field("friend_id") String treeId
        );

        @GET("users/getcountries")
        Call<GetCountriesListResponse> getCountriesList();

        @FormUrlEncoded
        @POST("users/myqrcode")
        Call<MyQRCodeResponse> getQRCode(
                @Field("user_id") String userId);

        @FormUrlEncoded
        @POST("users/getwishstatus")
        Call<GetWishResponse> getWishStatus(
                @Field("user_id") String userId
        );

        @FormUrlEncoded
        @POST("users/wishform")
        Call<GetWishResponse> wishform(
                @Field("user_id") String userId,
                @Field("wish") String wish);

        @FormUrlEncoded
        @POST("users/searchuser")
        Call<SearchTflUserResponse> searchUser(
                @Field("user_id") String userId,
                @Field("email") String email,
                @Field("tree_id") String treeId,
                @Field("phone_number") String phoneNumber
        );

        @FormUrlEncoded
        @POST("users/valueoftree")
        Call<ValueOfTreeResponse> getValueOfTree(
                @Field("user_id") String userId,
                @Field("year") String year
        );

        @FormUrlEncoded
        @POST("users/gettreevalue")
        Call<TreeInfoResponse> getTreeInfo(
                @Field("user_id") String userId
        );

        @FormUrlEncoded
        @POST("users/getyearlyvalueoftree")
        Call<TreeInfoOfYearResponse> getTreeInfoOfYear(
                @Field("user_id") String userId,
                @Field("year") String year
        );


        @FormUrlEncoded
        @POST("users/notificationlist")
        Call<NotificationListResponse> notificationList(
                @Field("user_id") String userId,
                @Field("page") int page
        );

        @FormUrlEncoded
        @POST("users/changevaultpassword")
        Call<WebServiceResponse> changeVaultPassword(
                @Field("user_id") String userId,
                @Field("old_password") String oldPassword,
                @Field("new_password") String newPassword,
                @Field("con_password") String newConfirmPassword
        );

        @FormUrlEncoded
        @POST("users/changepassword")
        Call<WebServiceResponse> changeLoginPassword(
                @Field("user_id") String userId,
                @Field("old_password") String oldPassword,
                @Field("new_password") String newPassword,
                @Field("con_password") String newConfirmPassword
        );


    }

    public interface CometChatWebService {

        @Headers("api-key: 50998x31625bd39f4d3bcd2b1da267809b680a")
        @FormUrlEncoded
        @POST("updateUser")
        Call<JSONObject> updateUser(
                @Field("UID") String uid,
                @Field("name") String name,
                @Field("profileURL") String profileImageUrl,
                @Field("avatarURL") String avtarImageUrl
        );

        @Headers("api-key: 50998x31625bd39f4d3bcd2b1da267809b680a")
        @FormUrlEncoded
        @POST("addFriends")
        Call<JSONObject> addFriends(
                @Field("UID") String uid,
                @Field("friendsUID") String friendsUID,
                @Field("clearExisting") boolean clearExisting
        );

        @Headers("api-key: 50998x31625bd39f4d3bcd2b1da267809b680a")
        @FormUrlEncoded
        @POST("deleteFriends")
        Call<JSONObject> deleteFriends(
                @Field("UID") String uid,
                @Field("friendsUID") String friendsUID
        );


    }

}
