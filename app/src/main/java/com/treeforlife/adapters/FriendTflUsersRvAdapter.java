package com.treeforlife.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.treeforlife.R;
import com.treeforlife.Utils.ImageLoaderUtil;
import com.treeforlife.customview.CustomTextView;
import com.treeforlife.dataobjects.FriendTflUser;
import com.treeforlife.fragments.NewGroupDialogFragment;

import java.util.ArrayList;

/**
 * Created by TheAppsmiths on 4/2/2018.
 * updated by TheAppsmiths on 11th April 2018.
 */

public class FriendTflUsersRvAdapter extends RecyclerView.Adapter<FriendTflUsersRvAdapter.FriendsHolder> {
    private ArrayList<FriendTflUser> mList;
    private final FriendTflUsersItemCallback mCallback;
    public String mCaller;

    public FriendTflUsersRvAdapter(FriendTflUsersItemCallback callback) {
        mCallback = callback;
    }

    @Override
    public FriendsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new FriendsHolder(LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_share_friend, parent, false));
    }

    @Override
    public void onBindViewHolder(FriendsHolder holder, int position) {

        holder.bindData(position);
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    private void ensureDataList() {
        if (mList == null) {
            mList = new ArrayList<FriendTflUser>();
        }
    }

    //method to update the data at particular position
    public void update(int position) {
        if (getItemCount() > position) {
            notifyItemChanged(position);
        }
    }

    //method to remove the item from particular position
    public void removeItem(int position) {
        ensureDataList();
        if (mList.size() > position && position >= 0) {
            mList.remove(position);
            notifyDataSetChanged();
        }
    }

    //method to update the array list
    public void update(ArrayList<FriendTflUser> friendTflUsers) {
        ensureDataList();
        this.mList.clear();
        if (friendTflUsers != null && !friendTflUsers.isEmpty()) {
            this.mList.addAll(friendTflUsers);
        }
        notifyDataSetChanged();
    }

    //method to add the list
    public void addToList(ArrayList<FriendTflUser> list) {
        if (list != null && !list.isEmpty()) {
            if (mList == null || mList.isEmpty()) {
                update(list);
            } else {
                int startPosition = mList.size();
                this.mList.addAll(list);
                notifyItemRangeInserted(startPosition, mList.size());
                notifyItemRangeChanged(startPosition - 1, startPosition);
            }
        }

    }

    //method to restore the item
    public void restoreItem(FriendTflUser friendTflUser, int position) {
        if (friendTflUser != null) {
            ensureDataList();
            if (mList.size() > position && position > -1) {
                this.mList.add(position, friendTflUser);
                notifyItemInserted(position);
            }
        }
    }

    //method to get the particular item
    public FriendTflUser getItemData(int mDeletedItemIndex) {
        FriendTflUser friendTflUser = null;
        if (mList != null && mList.size() > mDeletedItemIndex && mDeletedItemIndex > -1) {
            friendTflUser = mList.get(mDeletedItemIndex);
        }
        return friendTflUser;
    }

    //Holder view to show the data in adapter
    public class FriendsHolder extends RecyclerView.ViewHolder {
        public final View mItemView, mItemBackgroundView, mItemForegroundView;
        private final View mDeleteView, mViewProfileView;
        private final CustomTextView mNameTextView;
        private final ImageView mProfileImageView;

        private final Context mContext;
        private final ImageLoader mImageLoader;

        private FriendTflUser mFriendTflUser;

        public FriendsHolder(View itemView) {
            super(itemView);
            this.mItemView = itemView;
            mItemBackgroundView = itemView.findViewById(R.id.item_background_view);
            mItemForegroundView = itemView.findViewById(R.id.item_foreground_view);
            mViewProfileView = itemView.findViewById(R.id.view_profile_container);
            mDeleteView = itemView.findViewById(R.id.delete_view);

            mNameTextView = itemView.findViewById(R.id.name_text_view);
            mProfileImageView = itemView.findViewById(R.id.item_image_view);
            mItemForegroundView.setVisibility(View.VISIBLE);

            mContext = itemView.getContext();
            mImageLoader = ImageLoaderUtil.getLoader(mContext);
        }

        public void bindData(final int position) {

            if (mList != null && mList.size() > position && position > -1) {
                mFriendTflUser = mList.get(position);
            }
            if (mFriendTflUser != null) {
                mNameTextView.setText(mFriendTflUser.name);
                mImageLoader.displayImage(String.valueOf(mFriendTflUser.imageUrl), mProfileImageView,
                        ImageLoaderUtil.getDefaultProfileImageOption());
            }
            if (!TextUtils.isEmpty(mCaller) && mCaller.equals(NewGroupDialogFragment.TAG)) {
                mViewProfileView.setVisibility(View.GONE);
            } else {
                mViewProfileView.setVisibility(View.VISIBLE);
            }

            mItemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mCallback != null) {
                        mCallback.onFriendTflUserClick(position, mFriendTflUser);
                    }
                }
            });
        }

        //non used method
        public int getDeleteViewWidth() {
            if (mDeleteView != null) {
                return mDeleteView.getWidth();
            }
            return 130;
        }
    }

    public interface FriendTflUsersItemCallback {
        void onFriendTflUserClick(int position, FriendTflUser friendTflUser);
    }
}
