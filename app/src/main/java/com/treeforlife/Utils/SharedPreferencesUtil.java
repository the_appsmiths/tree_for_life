package com.treeforlife.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.treeforlife.BuildConfig;
import com.treeforlife.R;

/**
 * Created by TheAppsmiths on 10/17/2017.
 *
 * @author TheAppsmiths
 */

public final class SharedPreferencesUtil {
    public static final int DEFAULT_INT = -1000;
    public static final long DEFAULT_LONG = -1001;
    public static final float DEFAULT_FLOAT = -1.0f;
    private static String SHARED_PREF_NAME =
            BuildConfig.APPLICATION_ID + "_" + BuildConfig.VERSION_NAME;
    private static SharedPreferencesUtil sharedPreferencesUtil;

    /* ----------------- START OF PART#1 CODE : To Save data in SharedPreference --------------- */
    // Ref : https://developer.android.com/training/basics/data-storage/shared-preferences.html
    private SharedPreferences sharedPreferences;

    private SharedPreferencesUtil() {
    }

    public static SharedPreferencesUtil getInstance() {
        if (sharedPreferencesUtil == null) {
            sharedPreferencesUtil = new SharedPreferencesUtil();
        }
        return sharedPreferencesUtil;
    }

    /**
     * <p><br>To retrieve a default shared preference file that belongs to the activity.
     * Use this file to store
     * private primitive data in key-value pairs. </p>
     *
     * @param activity Activity Name
     * @return an instance of sharedPreference for that Activity
     * <p>
     * <p><br> this method {@link #getLocalSharedPreferences(Activity)}</p>
     * has been written By TheAppsmiths  on 02 Dec. 2016
     */
    public SharedPreferences getLocalSharedPreferences(@NonNull Activity activity) {
        return activity.getPreferences(Context.MODE_PRIVATE);
    }


    /**
     * <p><br>Unlike {@link #getLocalSharedPreferences(Activity)}, <br>
     * This method is used to retrieve a common shared preference file,
     * that is identified by app name. <br>
     * This file belongs to app & can be accessed from all the contexts of app.
     * Use this file to store tore private primitive data in key-value pairs. </p>
     *
     * @param context Activity Name
     * @return an instance of sharedPreference for app.
     * <p>
     * <p><br> this method {@link #getSharedPreferences(Context)} </p>
     * Defined By TheAppsmiths  on 02 Dec. 2016
     */
    private SharedPreferences getSharedPreferences(@NonNull Context context) {
        if (TextUtils.isEmpty(SHARED_PREF_NAME)) {
            SHARED_PREF_NAME = "cached_data_" + context.getString(R.string.app_name);
        }
        return context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
    }


    /**
     * <p> To initialize a common shared preference file that is identified by app name. <br>
     * This file belongs to app & can be accessed from all the contexts of app.
     * Use this file to store tore private primitive data
     * (booleans, floats, ints, longs, and strings) in key-value pairs.
     * </p>
     *
     * @param context {@link NonNull} context of App or any component of App.
     *                <p>
     *                <p><br> this method {@link #initSharedPreferences(Context)} </p>
     *                Defined By TheAppsmiths  on 02 Dec. 2016
     */
    public void initSharedPreferences(@NonNull Context context) {
        sharedPreferences = getSharedPreferences(context);
    }

    /**
     * <p> To ensure that sharedPreferences has been initialized & it's not null.
     *
     * @param context {@link NonNull} context of App or any component of App.
     *                <p>
     *                <p><br> this method {@link #ensureSharedPreferences(Context)} </p>
     *                Defined By TheAppsmiths  on 02 Dec. 2016
     */
    private void ensureSharedPreferences(Context context) {
        if (sharedPreferences == null) {
            if (context != null) {
                initSharedPreferences(context);
            } else {
                throw new RuntimeException("sharedPreferences has not been initialized & " +
                        "passed context is null");
            }
        }
    }

    /**
     * helper methods to write data into common shared preferences file
     * <p> <ul>
     * <li> {@link #writeStringToSharedPreference(Context, String, String)} </li>
     * <li> {@link #writeBooleanToSharedPreference(Context, String, boolean)} </li>
     * <li> {@link #writeIntToSharedPreference(Context, String, int)} </li>
     * <li> {@link #writeLongToSharedPreference(Context, String, long)} </li>
     * <li> {@link #writeFloatToSharedPreference(Context, String, float)} </li>
     * <p>
     * </ul> </p>
     */
    public void writeStringToSharedPreference(Context context, String key, String value) {
        ensureSharedPreferences(context);
        sharedPreferences.edit().putString(key, value).apply();
    }

    public void writeBooleanToSharedPreference(Context context, String key, boolean value) {
        ensureSharedPreferences(context);
        sharedPreferences.edit().putBoolean(key, value).apply();
    }

    public void writeIntToSharedPreference(Context context, String key, int value) {
        ensureSharedPreferences(context);
        sharedPreferences.edit().putInt(key, value).apply();
    }

    public void writeLongToSharedPreference(Context context, String key, long value) {
        ensureSharedPreferences(context);
        sharedPreferences.edit().putLong(key, value).apply();
    }

    public void writeFloatToSharedPreference(Context context, String key, float value) {
        ensureSharedPreferences(context);
        sharedPreferences.edit().putFloat(key, value).apply();
    }

    /**
     * helper methods to read data from a common shared preferences file
     * <p> <ul>
     * <li> {@link #readStringFromSharedPreference(Context, String)}  </li>
     * <li> {@link #readStringFromSharedPreference(Context, String, String)}  </li>
     * <li> {@link #readBooleanFromSharedPreference(Context, String)}  </li>
     * <li> {@link #readBooleanFromSharedPreference(Context, String, boolean)}  </li>
     * <li> {@link #readIntFromSharedPreference(Context, String)} </li>
     * <li> {@link #readIntFromSharedPreference(Context, String, int)}  </li>
     * <li> {@link #readLongFromSharedPreference(Context, String)}  </li>
     * <li> {@link #readLongFromSharedPreference(Context, String, long)}  </li>
     * <li> {@link #readFloatFromSharedPreference(Context, String)} </li>
     * <li> {@link #readFloatFromSharedPreference(Context, String, float)}  </li>
     * <p>
     * </ul> </p>
     * <p>
     * <p> These methods have been written By TheAppsmiths  on 02 Dec. 2016 </p>
     */
    public String readStringFromSharedPreference(Context context, String key) {
        return readStringFromSharedPreference(context, key, "");
    }

    public String readStringFromSharedPreference(Context context, String key, String defaultValue) {
        ensureSharedPreferences(context);
        return sharedPreferences.getString(key, defaultValue);
    }

    public boolean readBooleanFromSharedPreference(Context context, String key) {
        return readBooleanFromSharedPreference(context, key, false);
    }

    public boolean readBooleanFromSharedPreference(Context context, String key, boolean defaultValue) {
        ensureSharedPreferences(context);
        return sharedPreferences.getBoolean(key, defaultValue);
    }

    public int readIntFromSharedPreference(Context context, String key) {
        return readIntFromSharedPreference(context, key, DEFAULT_INT);
    }

    public int readIntFromSharedPreference(Context context, String key, int defaultValue) {
        ensureSharedPreferences(context);
        return sharedPreferences.getInt(key, defaultValue);
    }

    public long readLongFromSharedPreference(Context context, String key) {
        return readLongFromSharedPreference(context, key, DEFAULT_LONG);
    }

    public long readLongFromSharedPreference(Context context, String key, long defaultValue) {
        ensureSharedPreferences(context);
        return sharedPreferences.getLong(key, defaultValue);
    }

    public float readFloatFromSharedPreference(Context context, String key) {
        return readFloatFromSharedPreference(context, key, DEFAULT_FLOAT);
    }

    public float readFloatFromSharedPreference(Context context, String key, float defaultValue) {
        ensureSharedPreferences(context);
        return sharedPreferences.getFloat(key, defaultValue);
    }

    public boolean removeKeyItem(Context context, String key) {
        ensureSharedPreferences(context);
        return sharedPreferences.edit().remove(key).commit();
    }


    /* ----------------- END   OF PART#1 CODE : To Save data in SharedPreference --------------- */
}