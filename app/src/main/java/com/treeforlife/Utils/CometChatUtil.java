package com.treeforlife.Utils;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.inscripts.interfaces.Callbacks;
import com.treeforlife.BuildConfig;
import com.treeforlife.commons.CometChatResponseListener;
import com.treeforlife.retrofit.ResponseId;
import com.treeforlife.retrofit.RetrofitManager;

import org.json.JSONObject;

import cometchat.inscripts.com.cometchatcore.coresdk.CometChat;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created on 6/9/2018.
 *
 * @author TheAppsmiths
 * @link : https://developer.cometchat.com/docs/android-quick-start
 * @link : https://docs.cometchat.com/android-sdk/quick-start/
 */
/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class CometChatUtil {
    private static final String TAG = "CometChatUtil";

    private static CometChatUtil mCometChatUtil;
    public CometChat cometChat;

    private CometChatUtil() {
    }

    public static CometChatUtil getInstance() {
        if (mCometChatUtil == null) {
            mCometChatUtil = new CometChatUtil();
        }
        return mCometChatUtil;
    }

    private boolean ensureCometChat(@NonNull Context activity) {
        String errMessage = "Comet chat has not been initialized !";

        if (cometChat == null) {
            AndroidUtil.showToast(activity, errMessage, true);
        }
        return cometChat != null;
    }

    /**
     * Initializes the Chat SDK.Initialization binds the SDK to your app and
     * syncs the various basic parameters required for the CometChat SDK to function.
     */
    public void initializeCometChat(@NonNull Context context, final CometChatResponseListener listener) {
        String licenseKey = BuildConfig.COMET_CHAT_LICENSE_KEY;
        String apiKey = BuildConfig.COMET_CHAT_API_KEY;

        if (!TextUtils.isEmpty(licenseKey)) {

            if (!TextUtils.isEmpty(apiKey)) {
                final CometChat cometChat = CometChat.getInstance(context);
                cometChat.initializeCometChat("", licenseKey, apiKey, true, new Callbacks() {
                    @Override
                    public void successCallback(JSONObject jsonObject) {
                        CometChatUtil.this.cometChat = cometChat;
                        Log.d(TAG, "Initialize Success : " + jsonObject.toString());
                        if (listener != null) {
                            listener.onCometChatResponse(ResponseId.COMET_CHAT_INITIALIZED, true, jsonObject);
                        }
                    }

                    @Override
                    public void failCallback(JSONObject jsonObject) {
                        Log.d(TAG, "Initialize Fail : " + jsonObject.toString());
                        if (listener != null) {
                            listener.onCometChatResponse(ResponseId.COMET_CHAT_INITIALIZED, true, jsonObject);
                        }
                    }
                });
            } else {
                Log.e(TAG, "initializeCometChat: API Key is null or empty");
            }
        } else {
            Log.e(TAG, "initializeCometChat: License Key is null or empty");
        }
    }


    public void createCometChatUser(@NonNull Activity activity,
                                    @NonNull String uid,
                                    @NonNull String name,
                                    String profilePicUrl,
                                    final CometChatResponseListener listener) {
        if (ensureCometChat(activity)) {
            cometChat.createUser(activity, uid, name, profilePicUrl, profilePicUrl, "",
                    new Callbacks() {
                        @Override
                        public void successCallback(JSONObject jsonObject) {
                            if (listener != null) {
                                listener.onCometChatResponse(ResponseId.COMET_CHAT_CREATE_USER,
                                        true,
                                        jsonObject);
                            }
                        }

                        @Override
                        public void failCallback(JSONObject jsonObject) {
                            if (listener != null) {
                                listener.onCometChatResponse(ResponseId.COMET_CHAT_CREATE_USER,
                                        false,
                                        jsonObject);
                            }
                        }
                    });
        } else {
            if (listener != null) {
                listener.onCometChatResponse(ResponseId.COMET_CHAT_CREATE_USER,
                        true, null);
            }
        }
    }

    public void loginCometChatUser(@NonNull Context context,
                                   @NonNull String uid,
                                   final CometChatResponseListener listener) {
        if (ensureCometChat(context) && context != null) {
            cometChat.loginWithUID(context, uid,
                    new Callbacks() {
                        @Override
                        public void successCallback(JSONObject jsonObject) {
                            if (listener != null) {
                                listener.onCometChatResponse(ResponseId.COMET_CHAT_LOGIN_USER,
                                        true,
                                        jsonObject);
                            }
                        }

                        @Override
                        public void failCallback(JSONObject jsonObject) {
                            if (listener != null) {
                                listener.onCometChatResponse(ResponseId.COMET_CHAT_LOGIN_USER,
                                        false,
                                        jsonObject);
                            }
                        }
                    });
        } else {
            if (listener != null) {
                listener.onCometChatResponse(ResponseId.COMET_CHAT_LOGIN_USER,
                        true, null);
            }
        }
    }


    public void updateCometChatUser(@NonNull String uid,
                                    @NonNull String name,
                                    String profilePicUrl,
                                    final CometChatResponseListener listener) {
        Call<JSONObject> updateUserCall = RetrofitManager.getCometChatWebService()
                .updateUser(uid, name, profilePicUrl, profilePicUrl);
        updateUserCall.enqueue(new Callback<JSONObject>() {
            @Override
            public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                new ApiResponseValidator(ResponseId.COMET_CHAT_UPDATE_USER, response, listener).validate();
            }

            @Override
            public void onFailure(Call<JSONObject> call, Throwable t) {
                if (listener != null) {
                    listener.onCometChatResponse(ResponseId.COMET_CHAT_UPDATE_USER,
                            true, null);
                }
            }
        });
    }

    public void addFriends(@NonNull String uid,
                           @NonNull String friendsUid,
                           final CometChatResponseListener listener) {
        Call<JSONObject> updateUserCall = RetrofitManager.getCometChatWebService()
                .addFriends(uid, friendsUid, false);
        updateUserCall.enqueue(new Callback<JSONObject>() {
            @Override
            public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                new ApiResponseValidator(ResponseId.COMET_CHAT_ADD_FRIEND, response, listener).validate();
            }

            @Override
            public void onFailure(Call<JSONObject> call, Throwable t) {
                if (listener != null) {
                    listener.onCometChatResponse(ResponseId.COMET_CHAT_ADD_FRIEND,
                            true, null);
                }
            }
        });
    }

    public void removeFriends(@NonNull String uid,
                              @NonNull String friendsUid,
                              final CometChatResponseListener listener) {
        Call<JSONObject> updateUserCall = RetrofitManager.getCometChatWebService()
                .deleteFriends(uid, friendsUid);
        updateUserCall.enqueue(new Callback<JSONObject>() {
            @Override
            public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                new ApiResponseValidator(ResponseId.COMET_CHAT_REMOVE_FRIEND, response, listener).validate();
            }

            @Override
            public void onFailure(Call<JSONObject> call, Throwable t) {
                if (listener != null) {
                    listener.onCometChatResponse(ResponseId.COMET_CHAT_REMOVE_FRIEND,
                            true, null);
                }
            }
        });
    }


    public void blockCometChatUser(@NonNull Activity activity,
                                   @NonNull String uid,
                                   final CometChatResponseListener listener) {
        if (ensureCometChat(activity) && activity != null) {
            cometChat.blockUser(uid,
                    new Callbacks() {
                        @Override
                        public void successCallback(JSONObject jsonObject) {
                            if (listener != null) {
                                listener.onCometChatResponse(ResponseId.COMET_CHAT_BLOCK_USER,
                                        true,
                                        jsonObject);
                            }
                        }

                        @Override
                        public void failCallback(JSONObject jsonObject) {
                            if (listener != null) {
                                listener.onCometChatResponse(ResponseId.COMET_CHAT_BLOCK_USER,
                                        false,
                                        jsonObject);
                            }
                        }
                    });
        } else {
            if (listener != null) {
                listener.onCometChatResponse(ResponseId.COMET_CHAT_BLOCK_USER,
                        true, null);
            }
        }
    }

    public void unblockCometChatUser(@NonNull Activity activity,
                                     @NonNull String uid,
                                     final CometChatResponseListener listener) {
        if (ensureCometChat(activity)) {
            cometChat.unblockUser(uid,
                    new Callbacks() {
                        @Override
                        public void successCallback(JSONObject jsonObject) {
                            if (listener != null) {
                                listener.onCometChatResponse(ResponseId.COMET_CHAT_UNBLOCK_USER,
                                        true,
                                        jsonObject);
                            }
                        }

                        @Override
                        public void failCallback(JSONObject jsonObject) {
                            if (listener != null) {
                                listener.onCometChatResponse(ResponseId.COMET_CHAT_UNBLOCK_USER,
                                        false,
                                        jsonObject);
                            }
                        }
                    });
        } else {
            if (listener != null) {
                listener.onCometChatResponse(ResponseId.COMET_CHAT_UNBLOCK_USER,
                        true, null);
            }
        }
    }

    public void logoutCometChatUser(@NonNull Activity activity,
                                    final CometChatResponseListener listener) {
        if (ensureCometChat(activity)) {
            cometChat.logout(
                    new Callbacks() {
                        @Override
                        public void successCallback(JSONObject jsonObject) {
                            if (listener != null) {
                                listener.onCometChatResponse(ResponseId.COMET_CHAT_LOGOUT_USER,
                                        true,
                                        jsonObject);
                            }
                        }

                        @Override
                        public void failCallback(JSONObject jsonObject) {
                            if (listener != null) {
                                listener.onCometChatResponse(ResponseId.COMET_CHAT_LOGOUT_USER,
                                        false,
                                        jsonObject);
                            }
                        }
                    });
        } else {
            if (listener != null) {
                listener.onCometChatResponse(ResponseId.COMET_CHAT_LOGOUT_USER,
                        true, null);
            }
        }
    }

    public static class ApiResponseValidator {
        private final ResponseId responseId;
        private final Response response;
        private final CometChatResponseListener listener;

        public ApiResponseValidator(ResponseId responseId, Response response,
                                    CometChatResponseListener listener) {
            this.responseId = responseId;
            this.response = response;
            this.listener = listener;
        }

        public void validate() {
            JSONObject jsonObject = null;
            if (response != null) {
                Object body = response.body();
                Log.w(TAG, responseId + " BODY : " + body);
                if (body != null && body instanceof JSONObject) {

                    jsonObject = (JSONObject) body;
                    if (jsonObject.has("success")) {
                        JSONObject object = jsonObject.optJSONObject("success");
                        if (object != null && "2000".equals(object.optString("status"))) {
                            if (listener != null) {
                                listener.onCometChatResponse(responseId, true, jsonObject);
                            }
                            return;
                        }
                    }
                }
            }

            if (listener != null) {
                listener.onCometChatResponse(responseId, false, jsonObject);
            }
        }
    }

}
