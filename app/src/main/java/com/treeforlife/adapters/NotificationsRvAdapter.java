package com.treeforlife.adapters;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.treeforlife.R;
import com.treeforlife.Utils.DateAndTimeUtil;
import com.treeforlife.Utils.ImageLoaderUtil;
import com.treeforlife.commons.Constants;
import com.treeforlife.customview.CustomTextView;
import com.treeforlife.dataobjects.Notification;

import java.util.ArrayList;

/**
 * Created by TheAppsmiths on 3/20/2018.
 * updated by TheAppsmiths on 26th April 2018.
 */

public class NotificationsRvAdapter extends RecyclerView.Adapter<NotificationsRvAdapter.NotificationHolder> {

    private NotificationItemCallBack mCallBack;

    private ArrayList<Notification> mNotificationsList;

    public NotificationsRvAdapter(NotificationItemCallBack mCallBack) {
        this.mCallBack = mCallBack;
    }

    @Override
    public NotificationHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NotificationHolder(LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_notification, parent, false));
    }

    @Override
    public void onBindViewHolder(NotificationHolder holder, int position) {

        holder.bindData(position);
    }

    @Override
    public int getItemCount() {
        return (mNotificationsList != null) ? mNotificationsList.size() : 0;
    }

    private void ensureNotificationsList() {
        if (mNotificationsList == null) {
            mNotificationsList = new ArrayList<Notification>();
        }
    }


    public void update(ArrayList<Notification> notificationsList) {
        ensureNotificationsList();
        this.mNotificationsList.clear();
        if (notificationsList != null && !notificationsList.isEmpty()) {
            this.mNotificationsList.addAll(notificationsList);
        }
        notifyDataSetChanged();
    }

    public void addNotificationList(ArrayList<Notification> notificationsList) {
        if (notificationsList != null && !notificationsList.isEmpty()) {
            if (mNotificationsList == null || mNotificationsList.isEmpty()) {
                update(notificationsList);
            } else {
                int startPosition = mNotificationsList.size();
                this.mNotificationsList.addAll(notificationsList);
                notifyItemRangeInserted(startPosition, mNotificationsList.size());
                notifyItemRangeChanged(startPosition - 1, startPosition);
            }
        }

    }

    //Holder view to show the data in adapter
    public class NotificationHolder extends RecyclerView.ViewHolder {

        private final View mItemView, mBottomLineView;
        private final ImageView mNotificationImageView;
        private final CustomTextView mNotificationDateTextView, mNotificationMessageTextView;

        private final ImageLoader mImageLoader;
        private Notification mNotification;

        public NotificationHolder(View itemView) {
            super(itemView);
            mItemView = itemView;
            mNotificationImageView = (ImageView) itemView.findViewById(R.id.notification_image_view);
            mNotificationDateTextView = (CustomTextView) itemView.findViewById(R.id.notification_date_text_view);
            mNotificationMessageTextView = (CustomTextView) itemView.findViewById(R.id.notification_message_text_view);
            mBottomLineView = itemView.findViewById(R.id.bottom_line_view);

            mImageLoader = ImageLoaderUtil.getLoader(itemView.getContext());

        }

        public void bindData(int position) {

            if (mNotificationsList != null && mNotificationsList.size() > position) {
                mNotification = mNotificationsList.get(position);

                mNotificationMessageTextView.setText(mNotification.notificationTitle);
                mNotificationDateTextView.setText(DateAndTimeUtil.parseDate(
                        DateAndTimeUtil.DateOption.yyyy_DS_MM_dd_HH_COL_mm_ss_TO_dd_MMM_YYYY,
                        mNotification.notificationDate));

                DisplayImageOptions displayImageOptions = ImageLoaderUtil.getDefaultImageOption();

                if (!TextUtils.isEmpty(mNotification.notificationType)) {

                    switch (mNotification.notificationType) {

                        case Constants.NOTIFICATION_FRIEND_REQ_RECEIVED:
                        case Constants.NOTIFICATION_FRIEND_REQ_ACCEPTED:
                        case Constants.NOTIFICATION_NEW_IMAGE:
                        case Constants.NOTIFICATION_NEW_VIDEO:
                        case Constants.NOTIFICATION_RECEIVED_OM_POINTS:
                        case Constants.NOTIFICATION_USER_BLOCKED:
                            displayImageOptions = ImageLoaderUtil.getDefaultProfileImageOption();
                            break;

                        case Constants.NOTIFICATION_TREE_STATE_CHANGED:
                            displayImageOptions = ImageLoaderUtil.getDefaultHeaderImageOption();
                            break;

                    }
                }

                mImageLoader.displayImage(mNotification.notificationImageUrl,
                        mNotificationImageView, displayImageOptions);
                mBottomLineView.setVisibility(position == getItemCount() - 1 ? View.GONE : View.VISIBLE);


            }

            setListeners(position);


        }

        private void setListeners(final int position) {
            mItemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (mCallBack != null) {
                        mCallBack.onItemClick(position, mNotification);
                    }
                }
            });
        }
    }

    public interface NotificationItemCallBack {
        void onItemClick(int position, Notification notification);
    }
}
