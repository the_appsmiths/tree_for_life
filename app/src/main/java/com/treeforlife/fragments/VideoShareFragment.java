package com.treeforlife.fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.treeforlife.R;
import com.treeforlife.Utils.DialogUtil;
import com.treeforlife.Utils.FragmentUtil;
import com.treeforlife.adapters.FriendTflUsersRvAdapter;
import com.treeforlife.commons.Constants;
import com.treeforlife.commons.CustomizeChangeListener;
import com.treeforlife.commons.SharedPrefHelper;
import com.treeforlife.controller.GetCustomizeApiController;
import com.treeforlife.controller.UpdateCustomizeApiController;
import com.treeforlife.customview.CustomTextView;
import com.treeforlife.dataobjects.FriendTflUser;
import com.treeforlife.dataobjects.TFLUser;

import java.util.ArrayList;

/**
 * Created by TheAppsmiths on 4/2/2018.
 * updated by TheAppsmiths on 11th April 2018.
 */
/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class VideoShareFragment extends Fragment implements
        FriendTflUsersRvAdapter.FriendTflUsersItemCallback,
        GetCustomizeApiController.ResultCallback,
        UpdateCustomizeApiController.ResultCallback,
        CustomizeChangeListener {

    private static final String TAG = "VideoShareFragment";

    private Activity mActivity;
    private Context mContext;
    private Bundle mBundle;

    private View mParentLayout;
    private CustomTextView mNoDataTextView;
    private RecyclerView mRecyclerView;
    private FriendTflUsersRvAdapter mAdapter;

    private TFLUser mTflUser;
    private DialogUtil mDialogUtil;
    private SharedPrefHelper mSharedPrefHelper;
    private GetCustomizeApiController mGetCustomizeApiController;
    private UpdateCustomizeApiController mUpdateCustomizeApiController;

    private int mNoOfLoadedRecords = -1, mNoOfTotalRecords;
    private int mDeletedItemIndex = -1;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mActivity = getActivity();
        this.mContext = context;
        this.mBundle = getArguments();

        this.mAdapter = new FriendTflUsersRvAdapter(this);
        this.mSharedPrefHelper = SharedPrefHelper.getInstance();
        this.mDialogUtil = DialogUtil.getInstance();

        extractDataFromBundle();
        ensureTflUser();
        this.mGetCustomizeApiController = new GetCustomizeApiController(TAG, this, mContext,
                mActivity, mTflUser, Constants.CUSTOMIZED_TYPE_VIDEO_SHARE, this);

        this.mUpdateCustomizeApiController = new UpdateCustomizeApiController(TAG, this, mContext,
                mActivity, mTflUser, Constants.CUSTOMIZED_TYPE_VIDEO_SHARE, this);

    }

    private void extractDataFromBundle() {
        if (mBundle == null) {
            mBundle = getArguments();
        }

        if (mBundle != null) {
            mTflUser = mBundle.getParcelable(Constants.KEY_TFL_USER);

        }
    }

    private void ensureTflUser() {
        if (mTflUser == null) {
            mTflUser = mSharedPrefHelper.getLoginResponse(mContext);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_video_share, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initComponents(view);
        loadData(true);
    }

    private void initComponents(View view) {

        mParentLayout = view.findViewById(R.id.profile_parent_layout);
        mNoDataTextView = (CustomTextView) view.findViewById(R.id.no_data_text_view);
        mRecyclerView = view.findViewById(R.id.recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);
        setInfiniteScroller(layoutManager);
        setSwipeAndDelete();
        manageNoDataTextVisibility();
    }

    private void setInfiniteScroller(final LinearLayoutManager layoutManager) {
        if (mRecyclerView != null) {
            mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    int lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition();
                    int totalItemCount = layoutManager.getItemCount();
                    if ((lastVisibleItemPosition + Constants.ITEMS_LIMIT) > totalItemCount) {
                        loadData(false);
                    }
                }
            });
        }
    }

    private void manageNoDataTextVisibility() {
        if (isVisible() && mNoDataTextView != null && mAdapter != null) {
            mNoDataTextView.setVisibility(mAdapter.getItemCount() > 0 ? View.GONE
                    : View.VISIBLE);
        }
    }

    private void processDeleteItem(RecyclerView.ViewHolder viewHolder) {
        if (viewHolder != null
                && viewHolder instanceof FriendTflUsersRvAdapter.FriendsHolder) {

            final FriendTflUsersRvAdapter.FriendsHolder holder = (FriendTflUsersRvAdapter.FriendsHolder)
                    viewHolder;
            mDeletedItemIndex = holder.getAdapterPosition();
            final FriendTflUser friendTflUser = mAdapter.getItemData(mDeletedItemIndex);

            Runnable deleteItemAction = new Runnable() {
                @Override
                public void run() {

                    if (mUpdateCustomizeApiController != null) {
                        mUpdateCustomizeApiController.removeFriendFromCustomizeList(friendTflUser);
                    }
                }
            };

            Runnable cancelAction = new Runnable() {
                @Override
                public void run() {
                    if (mAdapter != null) {
                        mAdapter.update(mDeletedItemIndex);
                        mDeletedItemIndex = -1;
                    }
                }
            };

            String friendName = "";
            if (friendTflUser != null) {
                friendName = friendTflUser.name;
            }

            if (TextUtils.isEmpty(friendName)) {
                friendName = "this user";
            }

            mDialogUtil.showTwoButtonsAlertDialog(mContext,

                    "Are you sure you want to remove " + friendName + " from this list.",
                    "Remove",
                    "Cancel",
                    deleteItemAction,
                    cancelAction,
                    false
            );


        }

    }

    private void setSwipeAndDelete() {
        /**
         * https://www.androidhive.info/2017/09/android-recyclerview-swipe-delete-undo-using-itemtouchhelper/
         * * */
        if (mRecyclerView != null) {
            ItemTouchHelper.SimpleCallback mItemTouchSimpleCallback
                    = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {

                @Override
                public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                                      RecyclerView.ViewHolder target) {
                    return false;
                }

                @Override
                public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

                    processDeleteItem(viewHolder);
                }

                @Override
                public void onChildDrawOver(Canvas c, RecyclerView recyclerView,
                                            RecyclerView.ViewHolder viewHolder, float dX, float dY,
                                            int actionState, boolean isCurrentlyActive) {
                    if (viewHolder != null
                            && viewHolder instanceof FriendTflUsersRvAdapter.FriendsHolder) {
                        getDefaultUIUtil().onDrawOver(c, recyclerView,
                                ((FriendTflUsersRvAdapter.FriendsHolder) viewHolder).mItemForegroundView, dX, dY,
                                actionState, isCurrentlyActive);
                    }
                }

                @Override
                public int convertToAbsoluteDirection(int flags, int layoutDirection) {
                    return super.convertToAbsoluteDirection(flags, layoutDirection);
                }

                @Override
                public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                    if (viewHolder != null
                            && viewHolder instanceof FriendTflUsersRvAdapter.FriendsHolder) {
                        getDefaultUIUtil().clearView(((FriendTflUsersRvAdapter.FriendsHolder) viewHolder).mItemForegroundView);
                    }
                }

                @Override
                public void onChildDraw(Canvas c, RecyclerView recyclerView,
                                        RecyclerView.ViewHolder viewHolder, float dX, float dY,
                                        int actionState, boolean isCurrentlyActive) {

                    if (viewHolder != null
                            && viewHolder instanceof FriendTflUsersRvAdapter.FriendsHolder) {

                        getDefaultUIUtil().onDraw(c, recyclerView,
                                ((FriendTflUsersRvAdapter.FriendsHolder) viewHolder).mItemForegroundView,
                                dX, dY, actionState, isCurrentlyActive);
                    }

                }

                @Override
                public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {

                    if (viewHolder != null
                            && viewHolder instanceof FriendTflUsersRvAdapter.FriendsHolder) {

                        getDefaultUIUtil().onSelected(((FriendTflUsersRvAdapter.FriendsHolder) viewHolder)
                                .mItemForegroundView);
                    }
                }

            };


            ItemTouchHelper mItemTouchHelper = new ItemTouchHelper(mItemTouchSimpleCallback);
            mItemTouchHelper.attachToRecyclerView(mRecyclerView);
        }
    }

    private void loadData(boolean resetData) {
        if (resetData) {
            mNoOfLoadedRecords = -1;
            mNoOfTotalRecords = 0;
        }
        if (mNoOfLoadedRecords < mNoOfTotalRecords) {
            mGetCustomizeApiController.loadData(resetData);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mActivity != null) {
            mActivity.setTitle(R.string.videos_share);
        }
    }

    @Override
    public void onGetCustomizeApiResult(int noOfTotalRecords, ArrayList<FriendTflUser> customizeDataList) {
        this.mNoOfTotalRecords = noOfTotalRecords;
        if (customizeDataList != null) {

            if (mAdapter != null) {
                if (mNoOfLoadedRecords == -1) {
                    mAdapter.update(customizeDataList);
                } else {
                    mAdapter.addToList(customizeDataList);
                }
            }
            if (mNoOfLoadedRecords == -1) {
                mNoOfLoadedRecords = customizeDataList.size();
            } else {
                mNoOfLoadedRecords += customizeDataList.size();
            }
        }
        manageNoDataTextVisibility();
    }

    @Override
    public void onUpdateCustomizeApiResult(boolean success) {
        if (isVisible() && mAdapter != null) {
            if (success) {
                mAdapter.removeItem(mDeletedItemIndex);
                manageNoDataTextVisibility();

            } else {
                mAdapter.update(mDeletedItemIndex);
            }
        }
        mDeletedItemIndex = -1;
    }

    @Override
    public void onFriendTflUserClick(int position, FriendTflUser friendTflUser) {
        if (friendTflUser != null) {
            Bundle bundle = new Bundle();
            bundle.putString(Constants.KEY_CALLER_COMPONENT, TAG);
            bundle.putParcelable(Constants.KEY_TFL_USER, mTflUser);
            bundle.putParcelable(Constants.KEY_FRIEND_TFL_USER, friendTflUser);
            FragmentUtil.loadFragment((AppCompatActivity) mActivity, FragmentUtil.FRAGMENT_USER_PROFILE,
                    bundle, true);
        }
    }

    @Override
    public void onCustomizeSettingsChange() {
        loadData(true);
    }
}
