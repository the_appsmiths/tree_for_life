package com.treeforlife.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.treeforlife.R;
import com.treeforlife.Utils.ImageLoaderUtil;
import com.treeforlife.dataobjects.Video;

import java.util.ArrayList;

/**
 * Created by TheAppsmiths on 3/20/2018.
 */

public class AdapterRvVideo extends RecyclerView.Adapter<AdapterRvVideo.MyHolder> {

    private ArrayList<Video> mVideosList;

    private VideoCallBack mCallBack;


    public AdapterRvVideo(VideoCallBack mCallBack) {
        this.mCallBack=mCallBack;

    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyHolder(LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_video,parent,false));
    }

    @Override
    public void onBindViewHolder(MyHolder holder, int position) {

        holder.bindData(position);

    }

    @Override
    public int getItemCount() {
        return (mVideosList != null) ? mVideosList.size() : 0;
    }

    public interface VideoCallBack {
        void PlayVideo(int position , Video video);
        void RemoveItem(int position, Video video);
        void ShareVideo(int position, Video video);
    }

    public void updataAdapter(ArrayList<Video> mArrays) {
        this.mVideosList =mArrays;
        notifyDataSetChanged();
    }

    //Holder view to show the data in adapter
    public class MyHolder extends RecyclerView.ViewHolder {

        private TextView mVideoText;
        private Context mContext;
        private ImageView mPlayVideoImage,mRemoveImage,mImageBg,mShareImage;
        private Video mVideo;
        private final ImageLoader mImageLoader;
        public MyHolder(View itemView) {
            super(itemView);
            this.mContext=itemView.getContext();
            mVideoText=itemView.findViewById(R.id.video_text);
            mPlayVideoImage=itemView.findViewById( R.id.play_video_image);
            mRemoveImage = itemView.findViewById(R.id.remove_album_image_view);
            mImageBg=itemView.findViewById(R.id.image_view_item);
            mShareImage = itemView.findViewById(R.id.share_album_image_view);
            mShareImage.setVisibility(View.GONE);
            mImageLoader = ImageLoaderUtil.getLoader(mContext);

        }

        public void bindData(int position) {
            if (mVideosList != null && mVideosList.size() > position)
            {
                mVideo =mVideosList.get(position);
                mVideoText.setText(mVideo.videoDescription);
                mImageLoader.displayImage(mVideo.videoThumbnail,mImageBg,
                        ImageLoaderUtil.getDefaultImageOption());
            }

            setListener(position);
        }

        private void setListener(final int position) {

            mPlayVideoImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mCallBack != null)
                    {
                        mCallBack.PlayVideo(position, mVideo);
                    }

                }
            });

            mRemoveImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if ( mCallBack != null)
                    {
                        mCallBack.RemoveItem(position, mVideo);
                    }

                }
            });

            mShareImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (mCallBack != null)
                    {
                        mCallBack.ShareVideo(position, mVideo);
                    }
                }
            });

        }
    }
}
