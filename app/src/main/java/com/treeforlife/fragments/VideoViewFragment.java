package com.treeforlife.fragments;

import android.app.Activity;
import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.VideoView;

import com.treeforlife.R;
import com.treeforlife.Utils.AndroidUtil;
import com.treeforlife.commons.Constants;
import com.treeforlife.dataobjects.Video;

/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class VideoViewFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = "VideoViewFragment";

    private Context mContext;
    private Activity mActivity;
    private Bundle mBundle;
    private VideoView mVideoView;
    private ProgressBar mProgressBar;
    private ImageView mPlayPauseImageView;

    private String mVideoUrl;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
        this.mActivity = getActivity();
        this.mBundle = getArguments();

        extractDataFromBundle();
    }

    private void extractDataFromBundle() {

        if (mBundle == null) {
            mBundle = getArguments();
        }

        if (mBundle != null) {
            mVideoUrl = mBundle.getString(Constants.KEY_VIDEO_URL);

        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_video_view, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initWidgets(view);
    }

    private void initWidgets(View view) {
        if (view != null) {
            mVideoView = view.findViewById(R.id.video_view);
            mProgressBar = view.findViewById(R.id.progress_bar);
            mPlayPauseImageView = view.findViewById(R.id.play_video_image);
            mPlayPauseImageView.setOnClickListener(this);
            setUpVideoView();
            playVideo();
            videoBuffering();
            onCompleteVideo();
        }

    }

    private void onCompleteVideo() {
        mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mPlayPauseImageView.setVisibility(View.VISIBLE);
            }
        });
    }

    private void videoBuffering() {

        mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mProgressBar.setVisibility(View.GONE);
                mVideoView.start();
            }
        });
        mVideoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                mProgressBar.setVisibility(View.GONE);
                return false;
            }
        });
    }

    private void setUpVideoView() {
        if (!isVisible()) {
            return;
        }
        if (TextUtils.isEmpty(mVideoUrl)) {
            extractDataFromBundle();
        }

    }

    public void playVideo() {

        if (TextUtils.isEmpty(mVideoUrl)) {
            extractDataFromBundle();
        }
        if (mVideoView != null) {
            mPlayPauseImageView.setVisibility(View.GONE);
            MediaController mediaController = new MediaController(mContext);
            mediaController.setAnchorView(mVideoView);

            //specify the location of media file
            Uri uri = Uri.parse(mVideoUrl);

            //Setting MediaController and URI, then starting the videoView
            mVideoView.setMediaController(mediaController);
            mVideoView.setVideoURI(uri);
            mVideoView.requestFocus();
        } else {
            AndroidUtil.showToast(mContext, "Empty Video Url");
        }
    }


    @Override
    public void onClick(View view) {
        if (view != null) {

            switch (view.getId()) {
                case R.id.play_video_image:
                    mProgressBar.setVisibility(View.VISIBLE);
                    playVideo();
                    break;

            }
        }
    }


}
