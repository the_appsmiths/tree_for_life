package com.treeforlife.Utils;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.InvocationTargetException;

/**
 * Created by TheAppsmiths on 12/20/2017.
 *
 *@author TheAppsmiths
 */
/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class DeviceInfoUtil {
    private static final String TAG = "DeviceInfoUtil";

    public static String getPhoneUserName(@NonNull Activity activity) {
        String phoneUserName = "";

        try {
            phoneUserName = Settings.System.getString(activity.getContentResolver(), "device_name");
            Log.d(TAG, "device_name : " + phoneUserName);
            if (TextUtils.isEmpty(phoneUserName)) {
                phoneUserName = Settings.Secure.getString(activity.getContentResolver(), "device_name");
                Log.d(TAG, "Secure device_name : " + phoneUserName);
            }
            if (TextUtils.isEmpty(phoneUserName)) {
                phoneUserName = Settings.Secure.getString(activity.getContentResolver(),
                        "lock_screen_owner_info");
                Log.d(TAG, "lock_screen_owner_info : " + phoneUserName);
            }

            if (activity != null) {
                AccountManager accountManager = /*AccountManager.get(activity); */
                        ((AccountManager) activity.getSystemService(Context.ACCOUNT_SERVICE));
                if (accountManager != null) {
                    Account[] accounts = accountManager.getAccountsByType("com.google");
                    if (accounts != null && accounts.length > 0) {
                        phoneUserName = accounts[0].name;

                        if (!TextUtils.isEmpty(phoneUserName)) {
                            if (phoneUserName.contains("@")) {
                                phoneUserName = phoneUserName.substring(0,
                                        phoneUserName.lastIndexOf("@"));
                            }
                        }

                        Log.d(TAG, "account Name : " + phoneUserName);

                        if (!TextUtils.isEmpty(phoneUserName)) {
                            phoneUserName = phoneUserName.replaceAll("[^a-zA-Z ]", " ")
                                    .replaceAll(" {1,}", " ").replaceAll("^ {0,}", "");
                            String[] name = phoneUserName.split(" ");
                            if (name.length > 0) {
                                if (name[0].length() >= 3)
                                    phoneUserName = name[0];
                            }

                        }
                    }
                    if (TextUtils.isEmpty(phoneUserName)) {
                        phoneUserName = Build.MODEL + getAndroidId(activity);
                    }
                    if (!TextUtils.isEmpty(phoneUserName)) {
                        phoneUserName = phoneUserName.replaceAll("[^a-zA-Z ]", "");
                    }

                    if (phoneUserName.length() > 15) {
                        phoneUserName = phoneUserName.substring(0, 15);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.d(TAG, "getPhoneUserName : RESULT = " + phoneUserName);
        return phoneUserName;
    }

    public static String getAndroidId(@NonNull Context context) {
        String androidId = "";
        if (context != null) {
            androidId = Settings.Secure.getString(context.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            Log.d(TAG, "getAndroidId: androidId =" + androidId);
        }
        return androidId;
    }

    public static String getTelephonyDeviceIdOrIMEINo(@NonNull Context context) {
        String result = "";
        TelephonyManager telephonyManager = null;
        if (context != null) {
            telephonyManager =
                    (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        }
        if (telephonyManager != null) {
            try {
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                    result = telephonyManager.getDeviceId();
                    if (TextUtils.isEmpty(result)) {
                        try {
                            result = callStringMethodViaReflection(telephonyManager, "getImei");
                        } catch (NoSuchMethodException e) {
                            e.printStackTrace();
                        } catch (InvocationTargetException e) {
                            e.printStackTrace();
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    }
                }

            } catch (Exception | Error e) {
                e.printStackTrace();
            }

            if (TextUtils.isEmpty(result)) {
                result = getAndroidId(context);
            }
        }
        return result;
    }

    private static String callStringMethodViaReflection(Object object, String methodName) throws
            NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        if (object == null || TextUtils.isEmpty(methodName)) {
            throw new RuntimeException("Illegal value for Param : object is " + object +
                    ", methodName is " + methodName);
        }
        return (String) object.getClass().getDeclaredMethod(methodName).invoke(object);
    }

    private static JSONObject getDeviceInfo() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("OS", "ANDROID");
            jsonObject.put("MANUFACTURER", Build.MANUFACTURER);
            jsonObject.put("MODEL", Build.MODEL);
            jsonObject.put("PRODUCT", Build.PRODUCT);
            jsonObject.put("DEVICE", Build.DEVICE);
            jsonObject.put("ID", Build.ID);
            jsonObject.put("FINGERPRINT", Build.FINGERPRINT);
            jsonObject.put("DISPLAY/build_number", Build.DISPLAY);
            jsonObject.put("HOST", Build.HOST);
            jsonObject.put("HARDWARE", Build.HARDWARE);
            jsonObject.put("USER", Build.USER);
            jsonObject.put("TYPE", Build.TYPE);
            jsonObject.put("TIME", Build.TIME);
            jsonObject.put("SERIAL", Build.SERIAL);
            jsonObject.put("TAGS", Build.TAGS);

            jsonObject.put("VERSION.CODENAME", Build.VERSION.CODENAME);
            jsonObject.put("VERSION.INCREMENTAL", Build.VERSION.INCREMENTAL);
            jsonObject.put("VERSION.RELEASE/android_version", Build.VERSION.RELEASE);
            jsonObject.put("VERSION.SDK_INT", Build.VERSION.SDK_INT);
            try {
                jsonObject.put("VERSION.SDK", Build.VERSION.SDK);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                jsonObject.put("VERSION.BASE_OS", Build.VERSION.BASE_OS);
                jsonObject.put("VERSION.SECURITY_PATCH", Build.VERSION.SECURITY_PATCH);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

}
