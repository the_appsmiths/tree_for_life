package com.treeforlife.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.treeforlife.R;
import com.treeforlife.Utils.AndroidUtil;
import com.treeforlife.Utils.DialogUtil;
import com.treeforlife.Utils.ImageLoaderUtil;
import com.treeforlife.activities.TreeInfoOfYearGraphicalActivity;
import com.treeforlife.commons.Constants;
import com.treeforlife.commons.FragmentCallBack;
import com.treeforlife.commons.SharedPrefHelper;
import com.treeforlife.controller.TreeApisController;
import com.treeforlife.customview.CustomTextView;
import com.treeforlife.dataobjects.TFLUser;
import com.treeforlife.dataobjects.TreeInfo;
import com.treeforlife.retrofit.ResponseId;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by TheAppsmiths on 3/19/2018.
 */

public class TreeFragment extends Fragment implements View.OnClickListener,
        TreeApisController.TreeInfoApiResultCallback {

    public static final String TAG = "TreeFragment";

    private FragmentCallBack mCallBack;

    private Context mContext;
    private Activity mActivity;
    private Bundle mBundle;

    private CustomTextView mCountryNameTextView, mTreeIdTextView;
    private ImageView mTreeImageView, mHeaderImageView;
    private FrameLayout mTreeFrameLayout;
    private LinearLayout mTreeImageContainerLayout;
    private ImageView[] mOldFruitsImageView, mNewFruitsImageViews;

    private BroadcastReceiver mBroadcastReceiver;

    private DialogUtil mDialogUtil;
    private TreeApisController mTreeApisController;
    private SharedPrefHelper mSharedPrefHelper;

    private TFLUser mTflUser;
    private TreeInfo mTreeInfo;

    private final int NO_OF_FRUIT_TYPE = 4, NO_OF_FRUIT_LAYERS = 6;
    private int FRUIT_HEIGHT = 50, FRUIT_WIDTH = 50;
    private float PERCENT_WIDTH_FOR_FRUIT = 0.1f;

    private String[] mFruitsValues;// = {"A", "A", "A", "M", "O", "P", "O", "M", "A", "M", "A", "A", "A", "M", "O", "P","A", "A", "A", "M", "O", "P", "O", "M", "A", "M", "A", "A", "A", "M", "O", "P","A", "A", "A", "M", "O", "P", "O", "M", "A", "M", "A", "A", "A", "M", "O", "P","A", "A", "A", "M", "O", "P", "O", "M", "A", "M", "A", "A", "A", "M", "O", "P","A", "A", "A", "M", "O", "P", "O", "M", "A", "M", "A", "A", "A", "M", "O", "P","A", "A", "A", "M", "O", "P", "O", "M", "A", "M", "A", "A", "A", "M", "O", "P","A", "A", "A", "M", "O", "P", "O", "M", "A", "M", "A", "A", "A", "M", "O", "P", "O", "M", "A", "M", "A", "A", "A", "M", "O", "P", "O", "M", "A", "M"};
    private String mFruitPositions = "[{x:0.25325520833333331, y:0.404296875}, {x:0.314453125, y:0.63020833333333337}, {x:0.69791666666666663, y:0.3984375},{x:0.31119791666666669, y:0.29752604166666669},{x:0.76627604166666663, y:0.56640625},{x:0.11263020833333333, y:0.46028645833333331},{x:0.10872395833333333, y:0.61328125},{x:0.68359375, y:0.29947916666666669},{x:0.21158854166666666, y:0.26236979166666669},{x:0.419921875, y:0.31315104166666669},{x:0.771484375, y:0.20768229166666666},{x:0.4609375, y:0.23502604166666666},{x:0.10026041666666667, y:0.51041666666666663},{x:0.093098958333333329, y:0.388671875},{x:0.158203125, y:0.416015625},{x:0.59440104166666663, y:0.47005208333333331},{x:0.546875, y:0.12434895833333333},{x:0.236328125, y:0.19596354166666666},{x:0.78776041666666663, y:0.50651041666666663},{x:0.49283854166666669, y:0.1171875},{x:0.5390625, y:0.51692708333333337},{x:0.626953125, y:0.4140625},{x:0.85221354166666663, y:0.20442708333333334},{x:0.748046875, y:0.376953125},{x:0.8515625, y:0.61653645833333337},{x:0.59505208333333337, y:0.19596354166666666},{x:0.36002604166666669, y:0.65755208333333337},{x:0.87760416666666663, y:0.52994791666666663},{x:0.40299479166666669, y:0.26692708333333331},{x:0.66015625, y:0.3671875},{x:0.42838541666666669, y:0.39908854166666669},{x:0.17317708333333334, y:0.21158854166666666},{x:0.212890625, y:0.60026041666666663},{x:0.18359375, y:0.31315104166666669},{x:0.47721354166666669, y:0.068359375},{x:0.51041666666666663, y:0.18619791666666666},{x:0.533203125, y:0.291015625},{x:0.32486979166666669, y:0.20442708333333334},{x:0.37174479166666669, y:0.14973958333333334},{x:0.68359375, y:0.21028645833333334},{x:0.61588541666666663, y:0.26236979166666669},{x:0.869140625, y:0.44140625},{x:0.54427083333333337, y:0.048828125},{x:0.55533854166666663, y:0.23111979166666666},{x:0.67903645833333337, y:0.090494791666666671},{x:0.42057291666666669, y:0.12890625},{x:0.41927083333333331, y:0.18880208333333334},{x:0.685546875, y:0.546875},{x:0.912109375, y:0.5859375},{x:0.75455729166666663, y:0.3125},{x:0.154296875, y:0.60416666666666663},{x:0.41080729166666669, y:0.486328125},{x:0.62825520833333337, y:0.626953125},{x:0.88606770833333337, y:0.38802083333333331},{x:0.93424479166666663, y:0.41080729166666669},{x:0.49479166666666669, y:0.26106770833333331},{x:0.061197916666666664, y:0.50325520833333337},{x:0.065755208333333329, y:0.58658854166666663},{x:0.88216145833333337, y:0.26302083333333331},{x:0.13606770833333334, y:0.34114583333333331},{x:0.57291666666666663, y:0.67708333333333337},{x:0.25716145833333331, y:0.12825520833333334},{x:0.79947916666666663, y:0.32747395833333331},{x:0.37825520833333331, y:0.37369791666666669},{x:0.36197916666666669, y:0.30208333333333331},{x:0.27473958333333331, y:0.232421875},{x:0.25651041666666669, y:0.52734375},{x:0.71354166666666663, y:0.47721354166666669},{x:0.396484375, y:0.59244791666666663},{x:0.77083333333333337, y:0.4609375},{x:0.56510416666666663, y:0.38671875},{x:0.92578125, y:0.34765625},{x:0.59830729166666663, y:0.125},{x:0.12825520833333334, y:0.29231770833333331},{x:0.15494791666666666, y:0.5078125},{x:0.82942708333333337, y:0.30078125},{x:0.81184895833333337, y:0.537109375},{x:0.671875, y:0.50846354166666663},{x:0.36653645833333331, y:0.54036458333333337},{x:0.83203125, y:0.48763020833333331},{x:0.384765625, y:0.087239583333333329},{x:0.63216145833333337, y:0.0703125},{x:0.7578125, y:0.59049479166666663},{x:0.599609375, y:0.56966145833333337},{x:0.27018229166666669, y:0.61067708333333337},{x:0.69986979166666663, y:0.60286458333333337},{x:0.34700520833333331, y:0.44010416666666669},{x:0.26432291666666669, y:0.31966145833333331},{x:0.61002604166666663, y:0.328125},{x:0.21419270833333334, y:0.50455729166666663},{x:0.29947916666666669, y:0.53059895833333337},{x:0.78580729166666663, y:0.13606770833333334},{x:0.29752604166666669, y:0.1796875},{x:0.63736979166666663, y:0.15690104166666666},{x:0.32942708333333331, y:0.090494791666666671},{x:0.35872395833333331, y:0.23697916666666666},{x:0.31640625, y:0.39973958333333331},{x:0.53385416666666663, y:0.44075520833333331},{x:0.75455729166666663, y:0.25455729166666669},{x:0.71809895833333337, y:0.13671875}]";
    private int mTreeHeight, mTreeWidth, mTotalNoOfFruits,
            mNoOfApples, mNoOfMangoes, mNoOfOranges, mNoOfPears,
            mRemainingApples, mRemainingMangoes, mRemainingOranges, mRemainingPears;
    private int[] mLayersNoOfFruits = new int[NO_OF_FRUIT_LAYERS];
    private boolean isPushNotificationReceiverRegistered;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
        this.mActivity = getActivity();

        if (mActivity instanceof FragmentCallBack) {
            this.mCallBack = (FragmentCallBack) mActivity;
        }


        mDialogUtil = DialogUtil.getInstance();
        this.mSharedPrefHelper = SharedPrefHelper.getInstance();
        extractDataFromBundle();
        ensureTflUser();

        mTreeApisController = new TreeApisController(TAG, this, mContext, mActivity,
                mTflUser);
        mTreeApisController.setTreeInfoApiResultCallback(this);

        this.mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                afterReceivingBroadcast(context, intent);
            }
        };
        registerLocalReceiver();
    }

    /**
     * method to initialize or set the required fields from bundle.
     */
    private void extractDataFromBundle() {
        if (mBundle == null) {
            mBundle = getArguments();
        }

        if (mBundle != null) {
            mTflUser = mBundle.getParcelable(Constants.KEY_TFL_USER);

        }
    }

    /**
     * method to initialize field {@link #mTflUser}, if it is null.
     */
    private void ensureTflUser() {
        if (mTflUser == null) {
            mTflUser = mSharedPrefHelper.getLoginResponse(mContext);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tree, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        intiComponents(view);
        mTreeApisController.getTreeInfo();
    }

    /**
     * method to load & display user profile image.
     * in {@link #mHeaderImageView}
     */
    private void setImageHeader() {

        if (mTflUser != null) {
            ImageLoaderUtil.getLoader(mContext).displayImage(mTflUser.profileImage, mHeaderImageView,
                    ImageLoaderUtil.getDefaultHeaderImageOption());
        }

    }

    /**
     * method to get width & height of {@link #mTreeImageView}
     * and stores in {@link #mTreeWidth} && {@link #mTreeHeight}
     */
    private void setTreeObserver() {
        if (mTreeImageView != null) {
            final ViewTreeObserver viewTreeObserver = mTreeImageView.getViewTreeObserver();
            if (viewTreeObserver != null) {
                viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {

                        mTreeHeight = mTreeImageView.getMeasuredHeight();
                        mTreeWidth = mTreeImageView.getMeasuredWidth();
                        if (mTreeWidth > 0 && mTreeHeight >= 0) {
                            viewTreeObserver.removeOnGlobalLayoutListener(this);
                            addFruitsToTree();
                        }
                    }
                });
            }
        }
    }

    /**
     * method to initialize the views from inflated xml layout
     *
     * @param view inflated view (xml layout)
     */
    private void intiComponents(View view) {

        View mShowGraphicalReport = view.findViewById(R.id.show_graphical_report_view);
        mShowGraphicalReport.setOnClickListener(this);

        mCountryNameTextView = view.findViewById(R.id.country_name_text_view);
        mTreeIdTextView = view.findViewById(R.id.tree_id_text_view);

        mTreeImageContainerLayout = view.findViewById(R.id.tree_image_container_layout);
        mTreeFrameLayout = view.findViewById(R.id.tree_frame_layout);
        mTreeImageView = view.findViewById(R.id.tree_image_view);
        mHeaderImageView = view.findViewById(R.id.commentor_image_view);
        setImageHeader();
    }

    /**
     * method to add fruit views to tree
     */
    private void addFruitsToTree() {
        if (mTreeFrameLayout != null && mTreeImageView != null
                && mTreeHeight > 0 && mTreeWidth > 0) {
            JSONArray jsonArray1 = null;
            try {
                if (TextUtils.isEmpty(mFruitPositions)) {
                    return;
                }

                jsonArray1 = new JSONArray(mFruitPositions);

                for (int i = 0; i < mFruitsValues.length; i++) {
                    ImageView fruitImage = new ImageView(mContext);
                    fruitImage.setId(View.generateViewId());

                    FRUIT_HEIGHT = FRUIT_WIDTH = (int) (mTreeWidth * PERCENT_WIDTH_FOR_FRUIT);


                    FrameLayout.LayoutParams fruitImageLayoutParams
                            = new FrameLayout.LayoutParams(FRUIT_WIDTH,
                            FRUIT_HEIGHT);


                    double x = ((Double) (jsonArray1.getJSONObject(i)).get("x") * mTreeWidth);
                    double y = ((Double) (jsonArray1.getJSONObject(i)).get("y") * mTreeHeight);


                    fruitImageLayoutParams.leftMargin = ((int) x - (FRUIT_WIDTH / 2));
                    fruitImageLayoutParams.topMargin = ((int) y - (FRUIT_HEIGHT / 2));

                    fruitImage.setLayoutParams(fruitImageLayoutParams);
                    fruitImage.setImageResource(getFruitImage(mFruitsValues[i]));

                    mTreeFrameLayout.addView(fruitImage);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * method to get image of fruit
     *
     * @param fruitName name of the fruit for which image is required.
     * @return required image resource id.
     */
    private int getFruitImage(String fruitName) {
        switch (fruitName) {
            case "A":
                return R.mipmap.ic_apple;
            case "M":
                return R.mipmap.ic_mango;

            case "O":
                return R.mipmap.ic_orange;

            case "P":
                return R.mipmap.ic_pear;
            default:
                return 0;
        }
    }


    @Override
    public void onClick(View v) {
        if (v != null) {
            switch (v.getId()) {
                case R.id.show_graphical_report_view:
                    startActivity(new Intent(mContext, TreeInfoOfYearGraphicalActivity.class));
                    break;
            }
        }

    }


    @Override
    public void onResume() {
        super.onResume();
        if (mActivity != null) {
            mActivity.setTitle(R.string.tree);
        }
        if (mCallBack != null) {
            Bundle bundle = new Bundle();
            bundle.putString(Constants.KEY_FRAGMENT_CALLBACK, TAG);
            mCallBack.updateBottomBar(bundle);
        }
    }

    /**
     * method to set/display content data
     */
    private void displayContent() {
        if (mTreeInfo != null) {

            mNoOfApples = mTreeInfo.noOfApples;
            mNoOfMangoes = mTreeInfo.noOfMangoes;
            mNoOfOranges = mTreeInfo.noOfOranges;
            mNoOfPears = mTreeInfo.noOfPears;
            mFruitsValues = mTreeInfo.fruit_series;
            mTotalNoOfFruits = mNoOfApples + mNoOfMangoes + mNoOfOranges + mNoOfPears;

            if (mCountryNameTextView != null) {

                mCountryNameTextView.setText(mTreeInfo.treeCountryName);
                mTreeIdTextView.setText(mTreeInfo.treeId);
                if (Constants.BUCKET_STATUS_FULL.equals(mTreeInfo.isEmptyBucket)) {
                    mTreeImageView.setImageResource(R.mipmap.img_tree_full_bucket);
                } else {
                    mTreeImageView.setImageResource(R.mipmap.img_tree_empty_bucket);
                }

                if (!TextUtils.isEmpty(mTreeInfo.treeType)) {

                    mTreeImageContainerLayout.setGravity(Gravity.CENTER);
                    switch (mTreeInfo.treeType) {

                        case Constants.TREE_TYPE_SEED:
                            mTreeImageContainerLayout.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL);
                            mTreeImageView.setImageResource(R.mipmap.img_seed);
                            break;

                        case Constants.TREE_TYPE_PLANT:
                            mTreeImageContainerLayout.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL);
                            mTreeImageView.setImageResource(R.mipmap.img_plant);
                            break;

                        case Constants.TREE_TYPE_HALF_TREE:
                            mTreeImageView.setImageResource(R.mipmap.img_half_tree);
                            break;

                        case Constants.TREE_TYPE_FULL_TREE:
                            if (Constants.BUCKET_STATUS_FULL.equals(mTreeInfo.isEmptyBucket)) {
                                mTreeImageView.setImageResource(R.mipmap.img_tree_full_bucket);
                            } else {
                                mTreeImageView.setImageResource(R.mipmap.img_tree_empty_bucket);
                                if (mTotalNoOfFruits > 0) {
                                    setTreeObserver();
                                }
                            }

                            break;
                    }
                }
            }

        }
    }


    @Override
    public void onTreeInfoApiResult(boolean success, ResponseId responseId, TreeInfo treeInfo) {
        switch (responseId) {

            case GET_TREE_INFO:
                mTreeInfo = treeInfo;
                displayContent();
                break;
        }
    }

    /**
     * method to perform required operation after receiving a broadcast.
     *
     * @param context ref received in onReceive() of {@link #mBroadcastReceiver}
     * @param intent  contains action_name & data received in onReceive()
     *                of {@link #mBroadcastReceiver}
     */
    private void afterReceivingBroadcast(Context context, Intent intent) {
        AndroidUtil.dumpIntent(TAG, intent);
        if (intent != null) {
            String mIntentAction = intent.getAction();

            if (!TextUtils.isEmpty(mIntentAction)) {

                switch (mIntentAction) {

                    case Constants.ACTION_RECEIVED_PUSH_NOTIFICATION:
                        afterReceivingPushNotification(intent);
                        break;

                }
            }

        }

    }

    /**
     * method to perform required operation after receiving a push notification.
     *
     * @param intent intent
     */
    private void afterReceivingPushNotification(@NonNull Intent intent) {
        if (intent != null) {
            int notificationType = intent.getIntExtra(Constants.KEY_PUSH_NOTIFICATION_TYPE, -1);

            switch (notificationType) {

                case Constants.PUSH_NOTIFICATION_TREE_STATE_CHANGED:
                    if (mTreeApisController != null) {
                        mTreeApisController.getTreeInfo();
                    }
                    break;

            }
        }
    }

    /**
     * method to register {@link #mBroadcastReceiver}.
     */
    private void registerLocalReceiver() {
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(mActivity);
        if (localBroadcastManager != null) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(Constants.ACTION_RECEIVED_PUSH_NOTIFICATION);
            localBroadcastManager.registerReceiver(mBroadcastReceiver, intentFilter);
            isPushNotificationReceiverRegistered = true;
        }
    }

    /**
     * method to unregister {@link #mBroadcastReceiver}.
     */
    private void unRegisterLocalReceiver() {
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(mActivity);
        if (localBroadcastManager != null && isPushNotificationReceiverRegistered) {
            localBroadcastManager.unregisterReceiver(mBroadcastReceiver);
            isPushNotificationReceiverRegistered = false;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unRegisterLocalReceiver();
    }
}
