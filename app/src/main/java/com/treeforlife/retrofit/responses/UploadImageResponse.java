package com.treeforlife.retrofit.responses;

import com.google.gson.annotations.SerializedName;
import com.treeforlife.dataobjects.AlbumImage;

/**
 * Created by TheAppsmiths on 4/5/2018.
 */

public class UploadImageResponse extends WebServiceResponse {

    @SerializedName("image")
    public AlbumImage albumImage;
}
