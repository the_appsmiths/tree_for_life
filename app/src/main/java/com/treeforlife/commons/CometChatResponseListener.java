package com.treeforlife.commons;

import com.treeforlife.retrofit.ResponseId;

import org.json.JSONObject;

/**
 * Created on 6/9/2018.
 *
 * @author TheAppsmiths
 */

public interface CometChatResponseListener {
    void onCometChatResponse(ResponseId responseId, boolean isSuccess, JSONObject response);
}
