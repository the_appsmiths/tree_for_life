package com.treeforlife.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.treeforlife.R;
import com.treeforlife.Utils.AndroidUtil;
import com.treeforlife.Utils.AttachFileUtil;
import com.treeforlife.Utils.DialogUtil;
import com.treeforlife.Utils.FragmentUtil;
import com.treeforlife.Utils.ImageLoaderUtil;
import com.treeforlife.Utils.ProgressDialogUtil;
import com.treeforlife.adapters.CommentsRvAdapter;
import com.treeforlife.commons.CommonAppMethods;
import com.treeforlife.commons.Constants;
import com.treeforlife.commons.FragmentCallBack;
import com.treeforlife.commons.ListItemCallback;
import com.treeforlife.commons.SharedPrefHelper;
import com.treeforlife.customview.CustomEditText;
import com.treeforlife.customview.CustomTextView;
import com.treeforlife.dataobjects.Comment;
import com.treeforlife.dataobjects.TFLUser;
import com.treeforlife.retrofit.ResponseId;
import com.treeforlife.retrofit.RetrofitManager;
import com.treeforlife.retrofit.RetrofitResponseValidator;
import com.treeforlife.retrofit.responses.AssignOmPointsResponse;
import com.treeforlife.retrofit.responses.GetCommentsListResponse;
import com.treeforlife.retrofit.responses.PostCommentResponse;

import java.io.File;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author TheAppsmiths
 */

/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class MyCommentsFragment extends Fragment implements View.OnClickListener,
        CommentsRvAdapter.CommentItemCallback, AttachFileUtil.AttachFileCallback,
        RetrofitResponseValidator.ValidationListener, ListItemCallback {
    public static final String TAG = "CommentsFragment";

    private Context mContext;
    private Activity mActivity;
    private Bundle mBundle;
    private FragmentCallBack mCallBack;
    private CustomTextView mSelectedYearTextView;

    private SwipeRefreshLayout mCommentsRefreshLayout;
    private RecyclerView mRvComments;
    private CustomTextView mNoDataTextView;
    private CustomEditText mCommentEditText;
    private RelativeLayout mPostCommentContainer;
    private ImageView mAttachFileImageView;
    private ImageView mPostCommentImageView;

    private CommentsRvAdapter mCommentsRvAdapter;

    private ImageLoader mImageLoader;
    private AttachFileUtil mAttachFileUtil;
    private SharedPrefHelper mSharedPrefHelper;
    private DialogUtil mDialogUtil;
    private ProgressDialogUtil mProgressDialogUtil;
    private static final int RC_SELECT_COMMENT = 1001;
    private static final String START_COMMENT_TYPE = "All Comments";
    private TFLUser mTflUser;
    private Uri mAttachedFileUri;
    private ArrayList<String> mCommentTypeList;
    private File mAttachedFile;
    private AttachFileUtil.FileType mAttachedFileType;

    private String mCommentText;
    private int mFruitType = -1, mCommentsPageNo, mAssignedOmPointsPosition = -1,
            mNoOfLoadedRecords = -1, mTotalNoOfRecords = 0;
    private boolean isLoadingComments;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
        this.mActivity = getActivity();
        this.mBundle = getArguments();

        if (mActivity != null && mActivity instanceof FragmentCallBack) {
            this.mCallBack = (FragmentCallBack) mActivity;
        }
        mCommentsRvAdapter = new CommentsRvAdapter(this);

        this.mImageLoader = ImageLoaderUtil.getLoader(mContext);
        this.mAttachFileUtil = AttachFileUtil.getInstance();
        this.mSharedPrefHelper = SharedPrefHelper.getInstance();
        this.mDialogUtil = DialogUtil.getInstance();
        this.mProgressDialogUtil = ProgressDialogUtil.getInstance();
        mCommentTypeList = new ArrayList<>();
        mCommentTypeList.add("All Comments");
        mCommentTypeList.add("Truth");
        mCommentTypeList.add("Trust");
        mCommentTypeList.add("Loyalty");
        mCommentTypeList.add("Tolerance");
        extractDataFromBundle();
        ensureTflUser();

    }

    private void extractDataFromBundle() {
        if (mBundle == null) {
            mBundle = getArguments();
        }

        if (mBundle != null) {
            mTflUser = mBundle.getParcelable(Constants.KEY_TFL_USER);
            mFruitType = mBundle.getInt(Constants.KEY_FRUIT_TYPE);
        }
    }

    private void ensureTflUser() {
        if (mTflUser == null) {
            mTflUser = mSharedPrefHelper.getLoginResponse(mContext);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my_comments, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initWidgets(view);
        manageNoDataTextVisibility();
        loadComments();
    }

    private void initWidgets(@NonNull View view) {
        if (view != null) {
            mSelectedYearTextView = view.findViewById(R.id.selected_year_text_view);
            view.findViewById(R.id.select_year_view).setOnClickListener(this);
            mSelectedYearTextView.setText("All Comments");
            mCommentsRefreshLayout = view.findViewById(R.id.comments_refresh_layout);
            mRvComments = (RecyclerView) view.findViewById(R.id.rv_comments);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
            linearLayoutManager.setReverseLayout(true);
            linearLayoutManager.setStackFromEnd(true);
            mRvComments.setLayoutManager(linearLayoutManager);
            setInfiniteScroller(linearLayoutManager);
            mRvComments.setAdapter(mCommentsRvAdapter);

            mNoDataTextView = (CustomTextView) view.findViewById(R.id.no_data_text_view);

            mPostCommentContainer = view.findViewById(R.id.post_comment_container);
            mCommentEditText = (CustomEditText) view.findViewById(R.id.comment_edit_text);
            mAttachFileImageView = (ImageView) view.findViewById(R.id.attach_file_image_view);
            mAttachFileImageView.setOnClickListener(this);
            mPostCommentImageView = view.findViewById(R.id.post_comment_image_view);
            mPostCommentImageView.setOnClickListener(this);


        }

    }

    private void showCommentTypeDialog() {
        mDialogUtil.showAlertList(mActivity, "Select comment type", mCommentTypeList, RC_SELECT_COMMENT,
                this);
    }

    @Override
    public void onItemClicked(int which, String datum, int requestCode) {
        switch (requestCode) {

            case RC_SELECT_COMMENT:
                mSelectedYearTextView.setText(datum);
                String titleText = "";
                switch (datum) {

                    case "Truth":
                        titleText = "TRUTH";
                        mFruitType = Constants.FRUIT_TYPE_APPLE;
                        break;

                    case "Trust":
                        titleText = "TRUST";
                        mFruitType = Constants.FRUIT_TYPE_ORANGE;
                        break;

                    case "Loyalty":
                        titleText = "LOYALTY";
                        mFruitType = Constants.FRUIT_TYPE_PEAR;
                        break;

                    case "Tolerance":
                        titleText = "TOLERANCE";
                        mFruitType = Constants.FRUIT_TYPE_MANGO;
                        break;

                }

                if (TextUtils.isEmpty(titleText)) {
                    titleText = "ALL COMMENTS";
                    mFruitType = Constants.FRUIT_TYPE_ALL;
                }
                mCommentsPageNo = 0;
                mAssignedOmPointsPosition = -1;
                mNoOfLoadedRecords = -1;
                mTotalNoOfRecords = 0;
                mCommentsRvAdapter.update(new ArrayList<Comment>());
                mActivity.setTitle(titleText);
                loadComments();
                break;
        }
    }

    private void setInfiniteScroller(final LinearLayoutManager layoutManager) {
        if (mRvComments != null) {
            mRvComments.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                    switch (newState) {
                        case RecyclerView.SCROLL_STATE_IDLE:
                            if (mPostCommentContainer != null) {
                                int firstVisibleIndex
                                        = layoutManager.findFirstCompletelyVisibleItemPosition();
                                if (firstVisibleIndex == 0 && mFruitType != Constants.FRUIT_TYPE_ALL) {
                                    mPostCommentContainer.setVisibility(View.VISIBLE);
                                    mRvComments.smoothScrollToPosition(0);
                                } else if (firstVisibleIndex == -1 && mFruitType != Constants.FRUIT_TYPE_ALL) {
                                    mPostCommentContainer.setVisibility(View.VISIBLE);
                                } else {
                                    mPostCommentContainer.setVisibility(View.GONE);
                                }
                            }

                    }
                }

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    int lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition();
                    int totalItemCount = layoutManager.getItemCount();
                    if ((lastVisibleItemPosition + Constants.ITEMS_LIMIT) > totalItemCount) {
                        loadComments();
                    }
                }
            });
        }

        if (mCommentsRefreshLayout != null) {
            mCommentsRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    loadComments();
                }
            });
        }
    }

    private void manageNoDataTextVisibility() {
        if (mNoDataTextView != null && mCommentsRvAdapter != null) {
            mNoDataTextView.setVisibility(mCommentsRvAdapter.getItemCount() > 0 ? View.GONE
                    : View.VISIBLE);
        }
    }

    private void loadComments() {

        if (mFruitType == Constants.FRUIT_TYPE_ALL) {
            mPostCommentContainer.setVisibility(View.GONE);
        } else {
            mPostCommentContainer.setVisibility(View.VISIBLE);
        }
        if (isValidUser() && AndroidUtil.hasInternetConnectivity(mContext)
                && mTotalNoOfRecords > mNoOfLoadedRecords) {

            if (isLoadingComments) {
                return;
            }

            Log.d(TAG, "loadComments() called");
            isLoadingComments = true;
            if (mCommentsPageNo == 0) {
                mProgressDialogUtil.showProgressDialog(mActivity);
            } else {
                mCommentsRefreshLayout.setRefreshing(true);
            }

            Call<GetCommentsListResponse> mGetCommentsCall
                    = RetrofitManager.getRetrofitWebService()
                    .getMyComments(mTflUser.userId, mFruitType, mCommentsPageNo);

            mGetCommentsCall.enqueue(new Callback<GetCommentsListResponse>() {
                @Override
                public void onResponse(Call<GetCommentsListResponse> call, Response<GetCommentsListResponse> response) {
                    new RetrofitResponseValidator(ResponseId.GET_COMMENTS_LIST, response, MyCommentsFragment.this);
                }

                @Override
                public void onFailure(Call<GetCommentsListResponse> call, Throwable t) {
                    String errorMessage = "";
                    if (t != null) {
                        Log.e(TAG, "onFailure: Error : " + t.getMessage());
                        if (t instanceof SocketTimeoutException) {
                            errorMessage = "Please make sure that your device has an active internet connection.";
                        }
                    }

                    mProgressDialogUtil.dismissProgressDialog();
                    isLoadingComments = false;

                    if (!isVisible() || mCommentsPageNo != 0) {
                        return;
                    }

                    if (TextUtils.isEmpty(errorMessage)) {
                        AndroidUtil.showErrorToast(mContext);
                    } else {
                        mDialogUtil.showErrorDialog(mContext, errorMessage);
                        mCommentsRefreshLayout.setRefreshing(false);
                    }
                }
            });

        } else {
            mCommentsRefreshLayout.setRefreshing(false);
        }
    }

    private boolean isValidUser() {

        if (mTflUser == null) {

            AndroidUtil.showToast(mContext, "Invalid User");
            return false;

        } else if (TextUtils.isEmpty(mTflUser.userId)) {

            AndroidUtil.showToast(mContext, "You need to Login !");
            return false;
        }

        return true;
    }

    @Override
    public void onClick(View view) {
        if (view != null) {
            switch (view.getId()) {

                case R.id.attach_file_image_view:
                    attachFile();
                    break;

                case R.id.post_comment_image_view:
                    managePostComment();
                    break;
                case R.id.select_year_view:
                    showCommentTypeDialog();
                    break;
            }
        }
    }

    private void attachFile() {
        mAttachFileUtil.start(AttachFileUtil.FileType.FILE_TYPE_IMAGE_AND_VIDEO, mActivity,
                mAttachedFile != null,
                this);
    }

    private void managePostComment() {
        if (isValidUser() && AndroidUtil.hasInternetConnectivity(mContext)) {
            mPostCommentImageView.setEnabled(false);
            mProgressDialogUtil.showProgressDialog(mActivity);

            switch (getCommentType()) {

                case 1:
                    callPostComment(1);
                    break;

                case 2:
                    callPostComment(2);
                    break;

                case 3:
                    if (mAttachedFile != null) {
                        callPostComment(3);
                    } else {
                        AndroidUtil.showToast(mContext, "No video is attached.");
                    }

                    break;
            }

        }
    }

    private void callPostComment(int commentType) {
        if (isValidUser() && AndroidUtil.hasInternetConnectivity(mContext)) {
            if (commentType == 1 && !isValidCommentText()) {
                enablePostComment();
                return;

            } else if (commentType == 2 && mAttachedFile == null) {
                enablePostComment();
                AndroidUtil.showToast(mContext, "No image is attached.");
                return;

            } else if (commentType == 3 && mAttachedFile == null) {
                enablePostComment();
                AndroidUtil.showToast(mContext, "No video is attached.");
                return;
            }

            MultipartBody.Builder builder = new MultipartBody.Builder();
            builder.setType(MultipartBody.FORM);


            builder.addFormDataPart("user_id", mTflUser.userId);
            builder.addFormDataPart("fruit_type", String.valueOf(mFruitType));
            builder.addFormDataPart("comment_type", String.valueOf(commentType));

            mCommentText = mCommentEditText.getText().toString().trim();
            builder.addFormDataPart("comment_text", CommonAppMethods.encodeAllCharsToUnicode(mCommentText));


            if (mAttachedFile != null) {
                File mFile = new File(mAttachedFile.getName());
                switch (commentType) {

                    case 2:
                        builder.addFormDataPart("file",
                                mFile.getName(),
                                (RequestBody.create(MediaType.parse("image"),
                                        mAttachedFile)));
                        break;

                    case 3:
                        builder.addFormDataPart("videofile",
                                mFile.getName(),
                                (RequestBody.create(MediaType.parse("video"),
                                        mAttachedFile)));
                        break;

                }
            }
            MultipartBody requestBody = builder.build();

            Call<PostCommentResponse> mPostCommentCall = RetrofitManager.getRetrofitWebService()
                    .postComment(requestBody);

            mPostCommentCall.enqueue(new Callback<PostCommentResponse>() {
                @Override
                public void onResponse(Call<PostCommentResponse> call, Response<PostCommentResponse> response) {
                    new RetrofitResponseValidator(ResponseId.POST_COMMENT, response, MyCommentsFragment.this);
                }

                @Override
                public void onFailure(Call<PostCommentResponse> call, Throwable t) {
                    String errorMessage = "";
                    if (t != null) {
                        Log.e(TAG, "onFailure: Error : " + t.getMessage());
                        if (t instanceof SocketTimeoutException) {
                            errorMessage = "Please make sure that your device has an active internet connection.";
                        }
                    }

                    mProgressDialogUtil.dismissProgressDialog();
                    mPostCommentImageView.setEnabled(true);

                    if (!isVisible()) {
                        return;
                    }

                    if (TextUtils.isEmpty(errorMessage)) {
                        AndroidUtil.showErrorToast(mContext);
                    } else {
                        mDialogUtil.showErrorDialog(mContext, errorMessage);
                    }
                }
            });

        }
    }

    private void enablePostComment() {
        mProgressDialogUtil.dismissProgressDialog();
        mPostCommentImageView.setEnabled(true);
    }

    private void clearCommentData() {
        mAttachedFile = null;
        mAttachedFileUri = null;
        if (mCommentEditText != null) {
            mCommentEditText.setText(null);
        }

        if (mAttachFileImageView != null) {
            mAttachFileImageView.setImageResource(R.drawable.img_attach_file_inactive);
        }
    }

    private boolean isValidCommentText() {
        mCommentText = mCommentEditText.getText().toString();

        if (TextUtils.isEmpty(mCommentText)) {
            Toast.makeText(mContext, R.string.error_empty_comment, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @Override
    public void onCommentItemClick(int position, Comment comment) {
        if (comment != null) {
            if (!TextUtils.isEmpty(comment.commentType)) {
                Bundle bundle = new Bundle();
                bundle.putString(Constants.KEY_CALLER_COMPONENT, TAG);

                switch (comment.commentType) {

                    case Constants.COMMENT_TYPE_IMAGE:
                        bundle.putString(Constants.KEY_IMAGE_URL, comment.commentImageUrl);
                        FragmentManager fragmentManager = getChildFragmentManager();

                        if (fragmentManager != null) {
                            ImageViewDialogFragment mImageViewDialogFragment;
                            Fragment fragment =
                                    fragmentManager.findFragmentByTag(ImageViewDialogFragment.TAG);
                            if (fragment != null && fragment instanceof ImageViewDialogFragment) {
                                mImageViewDialogFragment = (ImageViewDialogFragment) fragment;
                                mImageViewDialogFragment.update(bundle);
                            } else {
                                mImageViewDialogFragment = new ImageViewDialogFragment();
                                mImageViewDialogFragment.setArguments(bundle);
                            }

                            mImageViewDialogFragment.show(fragmentManager, ImageViewDialogFragment.TAG);
                        }
                        break;

                    case Constants.COMMENT_TYPE_VIDEO:
                        bundle.putString(Constants.KEY_VIDEO_URL, comment.commentVideoUrl);
                        FragmentUtil.loadFragment((AppCompatActivity) mActivity,
                                FragmentUtil.FRAGMENT_EASY_VIDEO_PLAYER, bundle,
                                true);
                        break;
                }
            }


        }

    }

    @Override
    public void onAssignOmPointClick(int position, Comment comment, int omPoints) {
        boolean mayProceed = true;
        if (omPoints < 1) {
            AndroidUtil.showToast(mContext, "Less than 1 OM point can't be assigned.");
            mayProceed = false;

        } else if (comment == null) {
            AndroidUtil.showToast(mContext, "Invalid Comment.");
            mayProceed = false;

        }

        if (mayProceed && isValidUser() && AndroidUtil.hasInternetConnectivity(mContext)) {
            mProgressDialogUtil.showProgressDialog(mActivity);
            this.mAssignedOmPointsPosition = position;
            Call<AssignOmPointsResponse> mAssignOmPointsCall =
                    RetrofitManager.getRetrofitWebService().assignOmPoints(mTflUser.userId,
                            mFruitType, comment.commentId, omPoints);

            mAssignOmPointsCall.enqueue(new Callback<AssignOmPointsResponse>() {
                @Override
                public void onResponse(Call<AssignOmPointsResponse> call, Response<AssignOmPointsResponse> response) {
                    new RetrofitResponseValidator(ResponseId.ASSIGN_OM_POINTS, response, MyCommentsFragment.this);
                }

                @Override
                public void onFailure(Call<AssignOmPointsResponse> call, Throwable t) {

                }
            });
        } else {
            if (mCommentsRvAdapter != null) {
                mCommentsRvAdapter.update(position, comment);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAttachFileUtil != null) {
            mAttachFileUtil.onResume();
        }
        if (mCallBack != null) {
            Bundle bundle = new Bundle();
            bundle.putString(Constants.KEY_FRAGMENT_CALLBACK, TAG);
            mCallBack.updateBottomBar(bundle);
        }
        String titleText = "";
        if (mActivity != null) {
            switch (mFruitType) {

                case Constants.FRUIT_TYPE_APPLE:
                    titleText = "TRUTH";
                    break;

                case Constants.FRUIT_TYPE_ORANGE:
                    titleText = "TRUST";
                    break;

                case Constants.FRUIT_TYPE_PEAR:
                    titleText = "LOYALTY";
                    break;

                case Constants.FRUIT_TYPE_MANGO:
                    titleText = "TOLERANCE";
                    break;

            }

            if (TextUtils.isEmpty(titleText)) {
                titleText = "ALL COMMENTS";
            }
            mActivity.setTitle(titleText);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (mAttachFileUtil != null) {
            mAttachFileUtil.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (mAttachFileUtil != null) {
            mAttachFileUtil.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onNewFile(@NonNull AttachFileUtil.FileType fileType, String attachedVia, Uri fileUri, File file) {
        Log.d(TAG, "onNewFile() called with: fileType = [" + fileType + "], attachedVia = ["
                + attachedVia + "], fileUri = [" + fileUri + "], file = [" + file + "]");

        this.mAttachedFileUri = fileUri;
        this.mAttachedFile = file;
        this.mAttachedFileType = fileType;
        if (mAttachedFile != null) {
            mAttachFileImageView.setImageResource(R.drawable.img_attach_file_active);
        } else {
            mAttachFileImageView.setImageResource(R.drawable.img_attach_file_inactive);
        }
        if (mAttachedFile == null && fileUri != null) {
            AndroidUtil.showToast(mContext, "File path could not be fetched.");
        }


    }

    @Override
    public void onError(@NonNull String errorMessage) {
        Log.e(TAG, "Attach File: errorMessage = [" + errorMessage + "]");
        AndroidUtil.showToast(mContext, errorMessage);

    }

    @Override
    public void onValidResponse(@NonNull ResponseId responseId, @NonNull Object responseBody, @NonNull Response response) {
        switch (responseId) {

            case GET_COMMENTS_LIST:
                if (isVisible()) {
                    afterLoadingComments((GetCommentsListResponse) responseBody);
                }
                mCommentsRefreshLayout.setRefreshing(false);
                isLoadingComments = false;
                break;

            case POST_COMMENT:
                if (isVisible()) {
                    afterPostingComment((PostCommentResponse) responseBody);
                }
                break;

            case ASSIGN_OM_POINTS:
                if (isVisible()) {
                    afterAssigningOmPoints((AssignOmPointsResponse) responseBody);
                }
                break;
        }

        mProgressDialogUtil.dismissProgressDialog();
    }

    private void afterLoadingComments(GetCommentsListResponse response) {
        this.mTotalNoOfRecords = response.totalRecords;
        if (mCommentsPageNo == 0) {
            AndroidUtil.showToast(mContext, response.message);
        }

        if (response.commentsList != null
                && !response.commentsList.isEmpty()) {
            if (mCommentsPageNo == 0) {
                mNoOfLoadedRecords = response.commentsList.size();

            } else {
                mNoOfLoadedRecords += response.commentsList.size();
            }
            if (mCommentsRvAdapter != null) {
                mCommentsRvAdapter.addCommentList(response.commentsList);
                if (mCommentsPageNo == 0 && mCommentsRvAdapter.getItemCount() > 0) {
                    mRvComments.smoothScrollToPosition(0);
                }
            }
            manageNoDataTextVisibility();
            mCommentsPageNo++;
        }
    }

    private void afterPostingComment(PostCommentResponse response) {
        if (response.comment != null) {

            if (mCommentsRvAdapter != null) {
                mCommentsRvAdapter.addComment(response.comment);
                mNoOfLoadedRecords = mNoOfLoadedRecords == -1 ? 1 : mNoOfLoadedRecords + 1;
                mTotalNoOfRecords++;
                if (mCommentsRvAdapter.getItemCount() > 0) {
                    mRvComments.smoothScrollToPosition(0);
                }
            }
            manageNoDataTextVisibility();
            clearCommentData();
            mPostCommentImageView.setEnabled(true);
        }
        String message = response.message;
        if (TextUtils.isEmpty(message)) {
            message = "Your Comment has been posted successfully.";
        }
        AndroidUtil.showToast(mContext, message);
    }

    private void afterAssigningOmPoints(AssignOmPointsResponse response) {
        if (response.comment != null) {

            if (mCommentsRvAdapter != null) {
                mCommentsRvAdapter.update(mAssignedOmPointsPosition, response.comment);
            }
        }
        AndroidUtil.showToast(mContext, response.message);
        mAssignedOmPointsPosition = -1;
    }

    @Override
    public void onInvalidResponse(@NonNull ResponseId responseId, @NonNull String errorMessage, Response response) {
        boolean showErrInDialog = false;
        switch (responseId) {

            case GET_COMMENTS_LIST:
                mCommentsRefreshLayout.setRefreshing(false);
                isLoadingComments = false;
                break;

            case POST_COMMENT:
                mPostCommentImageView.setEnabled(true);
                showErrInDialog = true;
                break;

            case ASSIGN_OM_POINTS:
                showErrInDialog = true;
                if (mCommentsRvAdapter != null) {
                    mCommentsRvAdapter.update(mAssignedOmPointsPosition);
                    mAssignedOmPointsPosition = -1;
                }
                break;
        }

        if (response == null || !response.isSuccessful()) {
            mDialogUtil.showErrorDialog(mActivity, errorMessage);
        } else {
            if (showErrInDialog) {
                mDialogUtil.showOkAlertDialog(mActivity, errorMessage);
            } else {
                AndroidUtil.showToast(mContext, errorMessage);
            }
        }

        mProgressDialogUtil.dismissProgressDialog();
    }

    public int getCommentType() {
        int commentType = 1;
        if (mAttachedFile != null) {
            if (mAttachedFileType != null) {

                switch (mAttachedFileType) {

                    case FILE_TYPE_IMAGE:
                        commentType = 2;
                        break;

                    case FILE_TYPE_VIDEO:
                        commentType = 3;
                        break;

                }
            }
        }
        return commentType;
    }
}
