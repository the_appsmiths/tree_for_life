package com.treeforlife.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.treeforlife.R;
import com.treeforlife.Utils.AndroidUtil;
import com.treeforlife.Utils.DialogUtil;
import com.treeforlife.Utils.ProgressDialogUtil;
import com.treeforlife.adapters.TutorialsPagerAdapter;
import com.treeforlife.commons.Constants;
import com.treeforlife.commons.SharedPrefHelper;
import com.treeforlife.customview.CustomButton;
import com.treeforlife.customview.CustomTextView;
import com.treeforlife.dataobjects.TFLUser;
import com.treeforlife.retrofit.ResponseId;
import com.treeforlife.retrofit.RetrofitManager;
import com.treeforlife.retrofit.RetrofitResponseValidator;
import com.treeforlife.retrofit.responses.GetTutorialResponse;

import java.net.SocketTimeoutException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TheAppSmiths.
 * (TheAppSmiths is the Mobile App Development division of Adreno Technologies India Pvt. Ltd.)
 * *
 * Created on 25 June 2018 - 4:18 PM.
 *
 * @author Android Developer [AD143].
 **/

/**
 * All methods are of self-explanatory nature by their name.
 * No need to write the documentation for a particular method.
 */
public class TutorialActivity extends AppCompatActivity implements View.OnClickListener,
        RetrofitResponseValidator.ValidationListener {
    private static final String TAG = "TutorialActivity";

    private Intent mActivityIntent;
    private Toolbar mToolbar;
    private ActionBar mActionBar;

    private CustomTextView mToolbarTitle;
    private CustomButton mGetStartedButton;
    private ViewPager mTutorialViewPager;
    private LinearLayout mBottomDotsContainerLayout;
    private ArrayList<TextView> mBottomDotsTextViews;
    private View mHeaderView;

    private TutorialsPagerAdapter mTutorialsPagerAdapter;

    private SharedPrefHelper mSharedPrefHelper;
    private ProgressDialogUtil mProgressDialogUtil;
    private DialogUtil mDialogUtil;

    private TFLUser mTflUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);
        initComponents();
        initToolBar();
        initWidgets();
        loadTutorialList();
    }

    private void initComponents() {
        mActivityIntent = getIntent();
        mSharedPrefHelper = SharedPrefHelper.getInstance();
        mProgressDialogUtil = ProgressDialogUtil.getInstance();
        mDialogUtil = DialogUtil.getInstance();
        mTutorialsPagerAdapter = new TutorialsPagerAdapter(getSupportFragmentManager());

        ensureTflUser();

    }

    private void ensureTflUser() {
        if (mActivityIntent != null) {
            mTflUser = mActivityIntent.getParcelableExtra(Constants.KEY_TFL_USER);
        }

        if (mTflUser == null) {
            mTflUser = mSharedPrefHelper.getLoginResponse(this);
        }
    }

    //method to initialize the toolbar
    private void initToolBar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        if (mToolbar != null) {
            mToolbarTitle = mToolbar.findViewById(R.id.toolbar_title);
            View mToolbarIconLhs = mToolbar.findViewById(R.id.toolbar_icon_back);
            mToolbarIconLhs.setVisibility(View.GONE);

            View mToolbarIconRhs = mToolbar.findViewById(R.id.toolbar_icon_hamberg);
            mToolbarIconRhs.setVisibility(View.GONE);
        }

        setSupportActionBar(mToolbar);
        mActionBar = getSupportActionBar();
        setTitle(R.string.tutorial);
    }

    private void initWidgets() {
        mHeaderView = (ImageView) findViewById(R.id.header_view);

        mGetStartedButton = (CustomButton) findViewById(R.id.get_started_button);
        mGetStartedButton.setOnClickListener(this);

        mBottomDotsContainerLayout = (LinearLayout) findViewById(R.id.bottom_dots_container_layout);

        mTutorialViewPager = (ViewPager) findViewById(R.id.tutorial_view_pager);
        mTutorialViewPager.setAdapter(mTutorialsPagerAdapter);

        setListeners();

    }

    private void setListeners() {
        if (mTutorialViewPager != null) {
            mTutorialViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    updateBottomDotsTextColor(position);
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        setTitleToolBar(title);
    }


    //method to set the title of toolbar
    private void setTitleToolBar(CharSequence title) {
        if (mToolbarTitle != null) {
            mToolbarTitle.setText(title);
        }
    }


    private void loadTutorialList() {

        if (AndroidUtil.hasInternetConnectivity(this)) {
            mProgressDialogUtil.showProgressDialog(this);

            Call<GetTutorialResponse> getTutorialCall = RetrofitManager.getRetrofitWebService()
                    .getTutorial();
            getTutorialCall.enqueue(new Callback<GetTutorialResponse>() {
                @Override
                public void onResponse(Call<GetTutorialResponse> call, Response<GetTutorialResponse> response) {
                    new RetrofitResponseValidator(ResponseId.GET_TUTORIAL, response, TutorialActivity.this);
                }

                @Override
                public void onFailure(Call<GetTutorialResponse> call, Throwable t) {
                    String errorMessage = "";
                    if (t != null) {
                        Log.e(TAG, "onFailure: Error : " + t.getMessage());
                        if (t instanceof SocketTimeoutException) {
                            errorMessage = "Please make sure that your device has an active internet connection.";
                        }
                    }

                    mProgressDialogUtil.dismissProgressDialog();


                    if (TextUtils.isEmpty(errorMessage)) {
                        AndroidUtil.showErrorToast(TutorialActivity.this);
                    } else {
                        mDialogUtil.showErrorDialog(TutorialActivity.this, errorMessage);
                    }
                }
            });
        }
    }

    @Override
    protected void onDestroy() {
        mSharedPrefHelper.resetFirstTimeRegister(this);
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        if (view != null) {
            switch (view.getId()) {

                case R.id.get_started_button:

                    Intent nextStepIntent;
                    if (!hasUserWishData()) {
                        nextStepIntent = new Intent(this, WishFormActivity.class);
                        nextStepIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        nextStepIntent.putExtra(Constants.KEY_TFL_USER, mTflUser);

                    } else {
                        nextStepIntent = new Intent(this, HomeActivity.class);
                        nextStepIntent.putExtra(Constants.KEY_TFL_USER, mTflUser);
                        nextStepIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                    }
                    startActivity(nextStepIntent);
                    finish();
                    break;
            }
        }
    }

    private boolean hasUserWishData() {
        return mTflUser != null
                && !TextUtils.isEmpty(mTflUser.wishStatus)
                && !TextUtils.isEmpty(mTflUser.wishStatus);
    }

    @Override
    public void onValidResponse(@NonNull ResponseId responseId, @NonNull Object responseBody, @NonNull Response response) {
        switch (responseId) {

            case GET_TUTORIAL:
                GetTutorialResponse tutorialResponse = (GetTutorialResponse) responseBody;
                if (mTutorialsPagerAdapter != null) {
                    mTutorialsPagerAdapter.updateTutorialsList(tutorialResponse.tutorialList);
                    addDotsToBottomLayout();
                    updateBottomDotsTextColor(0);
                }
                mProgressDialogUtil.dismissProgressDialog();

                break;
        }


    }

    private void addDotsToBottomLayout() {
        if (mTutorialsPagerAdapter != null && mBottomDotsContainerLayout != null) {
            mBottomDotsContainerLayout.removeAllViews();
            ensureBottomDotsTextViews();
            int bottomDotSize;
            for (int i = 0; i < mTutorialsPagerAdapter.getCount(); i++) {
                TextView textView = new TextView(this);
                textView.setText(Html.fromHtml("&#8226;"));
                if (getResources().getBoolean(R.bool.isTab)) {
                    bottomDotSize = 60;
                } else {
                    bottomDotSize = 40;
                }
                textView.setTextSize(bottomDotSize);
                textView.setPadding(5, 5, 5, 5);
                mBottomDotsTextViews.add(textView);
                mBottomDotsContainerLayout.addView(textView);
            }
        }
    }

    private void ensureBottomDotsTextViews() {
        if (mBottomDotsTextViews == null) {
            mBottomDotsTextViews = new ArrayList<>();
        }
        mBottomDotsTextViews.clear();
    }

    private void updateBottomDotsTextColor(int position) {
        if (mBottomDotsTextViews != null) {
            for (int i = 0; i < mBottomDotsTextViews.size(); i++) {
                TextView textView = mBottomDotsTextViews.get(i);
                if (textView != null) {
                    textView.setTextColor(ContextCompat.getColor(this,
                            i == position ? R.color.appColor_6b4427_brown
                                    : R.color.appColor_959595_gray));
                }
            }
        }
    }

    @Override
    public void onInvalidResponse(@NonNull ResponseId responseId, @NonNull String errorMessage, Response response) {
        if (response == null || !response.isSuccessful()) {
            mDialogUtil.showErrorDialog(this, errorMessage);
        } else {
            mDialogUtil.showOkAlertDialog(this, errorMessage);
        }
        mProgressDialogUtil.dismissProgressDialog();
    }
}
