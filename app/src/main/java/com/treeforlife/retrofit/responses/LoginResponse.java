package com.treeforlife.retrofit.responses;

import com.google.gson.annotations.SerializedName;
import com.treeforlife.dataobjects.TFLUser;

/**
 * Created by TheAppsmiths on 4/3/2018.
 */

public class LoginResponse extends WebServiceResponse {

    @SerializedName("data")
    public TFLUser mTFLUser;
    @SerializedName("user_id")
    public String userId;
    @SerializedName("register_by")
    public String registerBy;

}
