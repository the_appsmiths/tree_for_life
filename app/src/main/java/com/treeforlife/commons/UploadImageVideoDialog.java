package com.treeforlife.commons;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.treeforlife.R;
import com.treeforlife.Utils.AttachFileUtil;
import com.treeforlife.Utils.ImageLoaderUtil;
import com.treeforlife.customview.CustomTextView;

import java.io.File;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by TheAppsmiths on 4/11/2018.
 */

/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class UploadImageVideoDialog implements View.OnClickListener, AttachFileUtil.AttachFileCallback {

    public static final String TAG = "UploadImageVideoDialog";
    private Dialog dialog;
    private EditText mAlbumNameEdit;
    private Button mSubmitButton;
    private ImageView mUploadAlbumImage;
    private CircleImageView mChooseAlbum;
    private CustomTextView mValidationErrTextView;
    private Activity mActivity;
    private Context mContext;
    private AttachFileUtil mAttachFileUtil;
    private Uri mAttachedFileUri;
    private File mAttachedFile;
    private ImageLoader mImageLoader;
    private UploadDialogCallBack mCallBack;
    private WishFormCallBack mWishCallBack;
    private String mWish;
    private boolean isUploadFile;
    private VideoView mVideoView;
    private TextView mHeadingDialog;
    private UploadFile mUploadFile;
    private String emptyTextErrMsg;

    public UploadImageVideoDialog(Context context) {
        this.mContext = context;
        this.mAttachFileUtil = AttachFileUtil.getInstance();
        mImageLoader = ImageLoaderUtil.getLoader(mContext);
        dialog = new Dialog(mContext);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
    }

    public void UploadDialog(UploadFile uploadFile, UploadDialogCallBack mCallBack) {
        this.mAttachedFile = null;
        this.mUploadFile = uploadFile;
        this.mCallBack = mCallBack;
        dialog.setContentView(R.layout.dialog_custom_album);
        mVideoView = (VideoView) dialog.findViewById(R.id.video_view);
        mVideoView.setOnClickListener(this);
        mChooseAlbum = (CircleImageView) dialog.findViewById(R.id.img_video_picker_view);
        mChooseAlbum.setOnClickListener(this);
        mUploadAlbumImage = (ImageView) dialog.findViewById(R.id.image_view);
        mUploadAlbumImage.setOnClickListener(this);
        mAlbumNameEdit = (EditText) dialog.findViewById(R.id.title_name_edit_text);
        mSubmitButton = (Button) dialog.findViewById(R.id.submit_button);
        mSubmitButton.setOnClickListener(this);

        mHeadingDialog = (TextView) dialog.findViewById(R.id.heading_text_view);
        mValidationErrTextView = dialog.findViewById(R.id.validation_err_text_view);
        mValidationErrTextView.setVisibility(View.GONE);

        switch (uploadFile) {
            case UPLOAD_IMAGE:
                emptyTextErrMsg = "Image name is required.";
                //mChooseAlbum.setText("Choose Image");
                mHeadingDialog.setText("New Image");
                mAlbumNameEdit.setHint("Enter Image Name");
                mSubmitButton.setText("Add Image");
                isUploadFile = true;
                break;

            case UPLOAD_VIDEO:
                emptyTextErrMsg = "Video name is required.";
                mHeadingDialog.setText("Upload Video");
                // mChooseAlbum.setText("Choose Video");
                mAlbumNameEdit.setHint("Enter Video Name");
                mSubmitButton.setText("Upload");
                isUploadFile = false;
                break;

            case UPLOAD_ALBUM:
                emptyTextErrMsg = "Album name is required.";
                mHeadingDialog.setText("New Album");
                mAlbumNameEdit.setHint("Enter Album Name");
                mSubmitButton.setText("Create Album");
                isUploadFile = true;
                break;
        }

        dialog.show();
    }

    public void DismisDialog() {
        dialog.dismiss();
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.submit_button:
                submitData();

                break;
            case R.id.img_video_picker_view:
            case R.id.image_view:
            case R.id.video_view:
                if (isUploadFile) {
                    ChooseImage();
                } else if (!isUploadFile) {
                    chooseVideo();
                }

                break;
        }
    }

    private void ChooseImage() {
        mAttachFileUtil.start(AttachFileUtil.FileType.FILE_TYPE_IMAGE, (AppCompatActivity) mContext,
                mAttachedFile != null, this);
    }

    private void chooseVideo() {
        mAttachFileUtil.start(AttachFileUtil.FileType.FILE_TYPE_VIDEO, (AppCompatActivity) mContext,
                mAttachedFile != null, this);

    }


    private void submitData() {
        boolean isValidData = true;
        String errorMessage = emptyTextErrMsg;

        mValidationErrTextView.setVisibility(View.GONE);

        if (TextUtils.isEmpty(mAlbumNameEdit.getText().toString())) {
            isValidData = false;
        } else if (mAttachedFile == null) {
            errorMessage = "Please attach a file.";
            if (mUploadFile != null) {
                switch (mUploadFile) {
                    case UPLOAD_IMAGE:
                        errorMessage = "Please attach an image.";
                        break;

                    case UPLOAD_VIDEO:
                        errorMessage = "Please attach a video.";
                        break;

                    case UPLOAD_ALBUM:
                        errorMessage = "Please attach an album image.";
                        break;
                }

            }

            isValidData = false;
        }

        if (isValidData) {
            if (mCallBack != null) {
                mCallBack.onitemCall(mAttachedFile, mAlbumNameEdit.getText().toString());
            }
            dialog.dismiss();
        } else {
            mValidationErrTextView.setVisibility(View.VISIBLE);
            mValidationErrTextView.setText(errorMessage);
        }


    }

    @Override
    public void onNewFile(@NonNull AttachFileUtil.FileType fileType, String attachedVia, Uri fileUri, File file) {

        Log.d(TAG, "onNewFile() called with: fileType = [" + fileType + "], attachedVia = ["
                + attachedVia + "], fileUri = [" + fileUri + "], file = [" + file + "]");

        this.mAttachedFileUri = fileUri;
        this.mAttachedFile = file;


        if (isUploadFile) {
            mUploadAlbumImage.setVisibility(View.VISIBLE);
            if (mAttachedFileUri == null) {
                mUploadAlbumImage.setImageBitmap(null);
            } else {
                mImageLoader.displayImage(String.valueOf(mAttachedFileUri), mUploadAlbumImage);
            }

        } else {
            mVideoView.setVisibility(View.VISIBLE);
            MediaController mediaController = new MediaController(mContext);
            mediaController.setAnchorView(mVideoView);
            Uri video = null;
            if (mAttachedFile != null) {
                video = Uri.parse(mAttachedFile.toString());
            }
            mVideoView.setMediaController(mediaController);
            mVideoView.setVideoURI(video);
            mVideoView.start();
        }


    }


    @Override
    public void onError(@NonNull String errorMessage) {

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (resultCode) {
            case Activity.RESULT_OK:
                if (mAttachFileUtil != null) {
                    mAttachFileUtil.onActivityResult(requestCode, resultCode, data);
                }
                break;

            case Activity.RESULT_CANCELED:
                break;

        }

    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (mAttachFileUtil != null) {
            mAttachFileUtil.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

    }

    public interface UploadDialogCallBack {
        void onitemCall(File mAttachedFile, String description);

    }

    public interface WishFormCallBack {
        void wishformCall(String wish);
    }

    public enum UploadFile {
        UPLOAD_IMAGE,
        UPLOAD_VIDEO,
        UPLOAD_ALBUM,
    }

}

