package com.treeforlife.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.treeforlife.R;
import com.treeforlife.Utils.AndroidUtil;
import com.treeforlife.Utils.DialogUtil;
import com.treeforlife.Utils.ProgressDialogUtil;
import com.treeforlife.commons.Constants;
import com.treeforlife.commons.SharedPrefHelper;
import com.treeforlife.customview.CustomTextView;
import com.treeforlife.dataobjects.TFLUser;
import com.treeforlife.retrofit.ResponseId;
import com.treeforlife.retrofit.RetrofitManager;
import com.treeforlife.retrofit.RetrofitResponseValidator;
import com.treeforlife.retrofit.responses.GetWishResponse;

import java.net.SocketTimeoutException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class WishFormActivity extends AppCompatActivity implements View.OnClickListener, RetrofitResponseValidator.ValidationListener {

    private static final String TAG = "WishFormActivity";
    private EditText mWishFormEdit;
    private Button mWishSubmit;
    private String mWishForm;
    private TFLUser mTflUser;
    private ProgressDialogUtil mProgressDialogUtil;
    private DialogUtil mDialogUtil;
    private Intent mActivityIntent;
    private SharedPrefHelper mSharedPrefHelper;
    private TextView mTextCounter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wish_form);
        initWidgets();
        initComponents();
        initToolbar();
    }

    private void initToolbar() {

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        if (mToolbar != null) {
            CustomTextView mToolbarTitle = mToolbar.findViewById(R.id.toolbar_title);
            mToolbarTitle.setText(R.string.wish_form);
            View mToolbarIconLhs = mToolbar.findViewById(R.id.toolbar_icon_back);
            mToolbarIconLhs.setVisibility(View.GONE);
            View mToolbarIconRhs = mToolbar.findViewById(R.id.toolbar_icon_hamberg);
            mToolbarIconRhs.setVisibility(View.GONE);

        }
    }

    private void initWidgets() {
        mTextCounter = (TextView) findViewById(R.id.text_counter);
        mWishFormEdit = (EditText) findViewById(R.id.wish_form_edit);
        mWishFormEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mTextCounter.setText(30 - s.toString().length() + "/30");
            }
        });

        mWishSubmit = (Button) findViewById(R.id.submit_button);
        mWishSubmit.setOnClickListener(this);
    }

    private void initComponents() {
        mActivityIntent = getIntent();
        mProgressDialogUtil = ProgressDialogUtil.getInstance();
        mDialogUtil = DialogUtil.getInstance();
        mSharedPrefHelper = SharedPrefHelper.getInstance();

        if (mActivityIntent != null) {
            mTflUser = mActivityIntent.getParcelableExtra(Constants.KEY_TFL_USER);

        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.submit_button:
                submitWish();
                break;
        }
    }

    private void submitWish() {


        if (isValidate() && mTflUser != null && AndroidUtil.hasInternetConnectivity(this)) {
            mProgressDialogUtil.showProgressDialog(this);

            Call<GetWishResponse> getAlbum = RetrofitManager.getRetrofitWebService()
                    .wishform(mTflUser.userId, mWishForm);
            getAlbum.enqueue(new Callback<GetWishResponse>() {
                @Override
                public void onResponse(Call<GetWishResponse> call, Response<GetWishResponse> response) {
                    new RetrofitResponseValidator(ResponseId.UPLOAD_MY_WISH, response, WishFormActivity.this);
                }

                @Override
                public void onFailure(Call<GetWishResponse> call, Throwable t) {
                    String errorMessage = "";
                    if (t != null) {
                        Log.e(TAG, "onFailure: Error : " + t.getMessage());
                        if (t instanceof SocketTimeoutException) {
                            errorMessage = "Please make sure that your device has an active internet connection.";
                        }
                    }

                    mProgressDialogUtil.dismissProgressDialog();


                    if (TextUtils.isEmpty(errorMessage)) {
                        AndroidUtil.showErrorToast(WishFormActivity.this);
                    } else {
                        mDialogUtil.showErrorDialog(WishFormActivity.this, errorMessage);
                    }
                }
            });

        }

    }

    private boolean isValidate() {
        mWishForm = mWishFormEdit.getText().toString().trim();
        if (TextUtils.isEmpty(mWishForm)) {
            mWishFormEdit.setError(getString(R.string.enter_wish));
            mWishFormEdit.requestFocus();
            return false;
        }

        return true;
    }

    @Override
    public void onValidResponse(@NonNull ResponseId responseId, @NonNull Object responseBody, @NonNull Response response) {
        mProgressDialogUtil.dismissProgressDialog();
        switch (responseId) {
            case UPLOAD_MY_WISH:
                afterUploadWish((GetWishResponse) responseBody);
                break;
        }

    }


    @Override
    public void onInvalidResponse(@NonNull ResponseId responseId, @NonNull String errorMessage, Response response) {
        mProgressDialogUtil.dismissProgressDialog();

        if (response == null || !response.isSuccessful()) {
            mDialogUtil.showErrorDialog(this, errorMessage);
        } else {
            mDialogUtil.showOkAlertDialog(this, errorMessage);
        }

    }

    private void afterUploadWish(GetWishResponse responseBody) {
        if (responseBody.status) {
            mTflUser.myWish = responseBody.userWish;
            mTflUser.wishStatus = responseBody.wishStatus;
            mSharedPrefHelper.setLoginResponse(this, mTflUser);

            //Redirect to Home Screen Activity
            Intent intent = new Intent(this, HomeActivity.class);
            intent.putExtra(Constants.KEY_TFL_USER, mTflUser);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            AndroidUtil.showToast(this, responseBody.message);
            ActivityCompat.finishAffinity(this);
            startActivity(intent);
        }
    }
}
