package com.treeforlife.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.treeforlife.R;
import com.treeforlife.Utils.AndroidUtil;
import com.treeforlife.Utils.DialogUtil;
import com.treeforlife.Utils.FragmentUtil;
import com.treeforlife.Utils.ImageLoaderUtil;
import com.treeforlife.activities.CustomizeActivity;
import com.treeforlife.adapters.UserImagesRvAdapter;
import com.treeforlife.adapters.UserVideosRvAdapter;
import com.treeforlife.commons.CommonAppMethods;
import com.treeforlife.commons.Constants;
import com.treeforlife.commons.CustomizeChangeListener;
import com.treeforlife.commons.ListItemCallback;
import com.treeforlife.commons.SharedPrefHelper;
import com.treeforlife.controller.FriendApisController;
import com.treeforlife.controller.SearchUserApiController;
import com.treeforlife.customview.CustomTextView;
import com.treeforlife.dataobjects.FriendTflUser;
import com.treeforlife.dataobjects.Image;
import com.treeforlife.dataobjects.Notification;
import com.treeforlife.dataobjects.SearchedTflUser;
import com.treeforlife.dataobjects.TFLUser;
import com.treeforlife.dataobjects.Video;
import com.treeforlife.retrofit.ResponseId;

/**
 * Created by TheAppsmiths on 3/23/2018.
 * updated by TheAppsmiths on 12th April 2018.
 */

/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class UserProfileFragment extends Fragment implements View.OnClickListener,
        ListItemCallback,
        CustomizeChangeListener,
        FriendApisController.FriendApiResultCallback,
        SearchUserApiController.ResultCallback,
        UserImagesRvAdapter.ImageItemCallback,
        UserVideosRvAdapter.VideoItemCallBack {

    public static final String TAG = "UserProfileFragment";

    private Activity mActivity;
    private Context mContext;
    private Bundle mBundle;

    private CustomTextView mUserNameTextView, mUserEmailTextView, mUserPhoneTextView,
            mEmailTextView, mStreetTextView, mCityTextView, mStateTextView, mCountryTextView, mPhoneTextView,
            mBiographyTextView, mFriendshipStatusTextView, mUsersImagesToggleTextView,
            mUsersVideosToggleTextView, mNoDataTextView, mViewContactsTextView;
    private ImageView mUserProfileImageView;
    private RecyclerView mRecyclerView;
    private View mFocusedToggledView, mManageCustomizeView, mEmailPhoneView, mContactsView;
    private UserImagesRvAdapter mUserImagesRvAdapter;
    private UserVideosRvAdapter mUserVideosRvAdapter;

    private BroadcastReceiver mBroadcastReceiver;

    private SharedPrefHelper mSharedPrefHelper;
    private ImageLoader mImageLoader;
    private SearchUserApiController mSearchUserApiController;
    private FriendApisController mFriendApisController;
    private DialogUtil mDialogUtil;
    private TFLUser mTflUser;
    private FriendTflUser mFriendTflUser;
    private SearchedTflUser mSearchedTflUser;
    private Notification mNotification;

    private String mCaller, mTitleText, mUserTreeId;

    private final int RC_ON_FRIEND = 220;
    private final int RC_ON_FRIEND_REQ_RECEIVED = 221;
    private final int RC_ON_BLOCK = 222;

    private boolean isPushNotificationReceiverRegistered;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
        this.mActivity = getActivity();
        this.mBundle = getArguments();
        this.mUserImagesRvAdapter = new UserImagesRvAdapter(this);
        this.mUserVideosRvAdapter = new UserVideosRvAdapter(UserVideosRvAdapter.LayoutType.GRID, this);

        this.mImageLoader = ImageLoaderUtil.getLoader(mContext);
        this.mSharedPrefHelper = SharedPrefHelper.getInstance();
        this.mDialogUtil = DialogUtil.getInstance();

        extractDataFromBundle(true);
        ensureTflUser();

        this.mSearchUserApiController = new SearchUserApiController(TAG, this, mContext,
                mActivity, mTflUser, this);
        this.mFriendApisController = new FriendApisController(TAG, this, mContext,
                mActivity, mTflUser, this);
        mTitleText = "User's Profile";

        this.mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                afterReceivingBroadcast(context, intent);
            }
        };
        registerLocalReceiver();
    }

    private void extractDataFromBundle(boolean extractAfterGet) {
        resetUserProfileData();

        if (extractAfterGet) {
            mBundle = getArguments();
        }

        if (mBundle != null) {
            mCaller = mBundle.getString(Constants.KEY_CALLER_COMPONENT);
            mTflUser = mBundle.getParcelable(Constants.KEY_TFL_USER);
            mFriendTflUser = mBundle.getParcelable(Constants.KEY_FRIEND_TFL_USER);
            mSearchedTflUser = mBundle.getParcelable(Constants.KEY_SEARCHED_TFL_USER);
            mNotification = mBundle.getParcelable(Constants.KEY_NOTIFICATION);
            mUserTreeId = mBundle.getString(Constants.KEY_TREE_ID);
        }
    }

    private void resetUserProfileData() {
        mCaller = null;
        mTflUser = null;
        mFriendTflUser = null;
        mSearchedTflUser = null;
        mNotification = null;
        mUserTreeId = null;

        if (mUserImagesRvAdapter != null) {
            mUserImagesRvAdapter.clearImagesList();
        }
        if (mUserVideosRvAdapter != null) {
            mUserVideosRvAdapter.clearVideosList();
        }
    }

    private void ensureTflUser() {
        if (mTflUser == null) {
            mTflUser = mSharedPrefHelper.getLoginResponse(mContext);
        }
    }

    public void update(Bundle bundle) {
        mBundle = bundle;
        extractDataFromBundle(false);
        displayContent();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_user_profile, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initComponent(view);
        if (!TextUtils.isEmpty(mCaller)
                && mCaller.equals(AddFriendFragment.TAG)) {
            displayContent();
        } else {
            loadData();
        }
    }

    private void loadData() {
        String treeIdOrEmail = findUserTreeOrEmailId();


        if (!mSearchUserApiController.searchUser(treeIdOrEmail)) {
            displayContent();
        }
    }

    private String findUserTreeOrEmailId() {
        String treeIdOrEmail = "";
        if (mFriendTflUser != null) {
            treeIdOrEmail = mFriendTflUser.treeId;
            if (TextUtils.isEmpty(treeIdOrEmail)) {
                treeIdOrEmail = mFriendTflUser.email;
            }
            if (TextUtils.isEmpty(treeIdOrEmail)) {
                treeIdOrEmail = mFriendTflUser.phoneNumber;
            }
        } else if (mNotification != null) {
            treeIdOrEmail = mNotification.treeId;

        } else if (mSearchedTflUser != null) {
            treeIdOrEmail = mSearchedTflUser.treeId;

        } else {
            treeIdOrEmail = mUserTreeId;
        }
        return treeIdOrEmail;
    }

    private void initComponent(View view) {

        mUserProfileImageView = view.findViewById(R.id.user_profile_image);
        mUserProfileImageView.setOnClickListener(this);

        mFriendshipStatusTextView = view.findViewById(R.id.friendship_text_view);
        mFriendshipStatusTextView.setOnClickListener(this);

        mManageCustomizeView = view.findViewById(R.id.manage_customize_view);
        mManageCustomizeView.setOnClickListener(this);

        mUserNameTextView = view.findViewById(R.id.user_name);
        mEmailPhoneView = view.findViewById(R.id.email_phone_container);
        mUserEmailTextView = view.findViewById(R.id.profile_email_text_view);
        mUserPhoneTextView = view.findViewById(R.id.profile_phone_text_view);

        mEmailTextView = view.findViewById(R.id.email_text);
        mCityTextView = view.findViewById(R.id.city_text);
        mStreetTextView = view.findViewById(R.id.street_text);
        mStateTextView = view.findViewById(R.id.state_text);
        mCountryTextView = view.findViewById(R.id.country_text);
        mPhoneTextView = view.findViewById(R.id.phone_text);
        mBiographyTextView = view.findViewById(R.id.biography_text_view);

        mContactsView = view.findViewById(R.id.contacts_view);
        mContactsView.setOnClickListener(this);
        mViewContactsTextView = view.findViewById(R.id.view_contacts_text_view);

        mUsersImagesToggleTextView = view.findViewById(R.id.image_text_view);
        mUsersImagesToggleTextView.setOnClickListener(this);
        mUsersVideosToggleTextView = view.findViewById(R.id.video_text_view);
        mUsersVideosToggleTextView.setOnClickListener(this);

        mNoDataTextView = view.findViewById(R.id.no_data_text_view);
        mRecyclerView = view.findViewById(R.id.recycleview_user);
        GridLayoutManager layoutManager = new GridLayoutManager(mContext, 3);
        mRecyclerView.setLayoutManager(layoutManager);


    }

    private void displayContent() {
        if (isVisible() && mUserNameTextView != null) {
            String profilePicUrl = "", email = "", phone = "", userProfileName = "", street = "",
                    city = "", state = "", country = "", biography = "", userName = "";

            if (mSearchedTflUser != null) {

                profilePicUrl = mSearchedTflUser.profileImageUrl;
                if (Constants.CUSTOMIZED_STATUS_ALLOWED
                        .equals(mSearchedTflUser.friendProfileShare)
                        || mSearchedTflUser.blockedByUserId.equals(mTflUser.userId)) {
                    email = mSearchedTflUser.email;
                    phone = mSearchedTflUser.phone;
                    biography = mSearchedTflUser.biography;

                    mEmailPhoneView.setVisibility(View.VISIBLE);
                } else {
                    email = phone = "Not shared. ";
                    biography = "Profile isn't shared with you.";
                    mEmailPhoneView.setVisibility(View.GONE);
                }

                userName = userProfileName = mSearchedTflUser.firstName;
                if (!TextUtils.isEmpty(mSearchedTflUser.lastName)) {
                    userName = userProfileName += " " + mSearchedTflUser.lastName;
                }

                street = mSearchedTflUser.street;

                city = mSearchedTflUser.city;
                state = mSearchedTflUser.state;
                country = mSearchedTflUser.addressCountry;


                if (TextUtils.isEmpty(city)) {
                    city = getString(R.string.n_a);
                }
                if (TextUtils.isEmpty(state)) {
                    state = getString(R.string.n_a);
                }
                if (TextUtils.isEmpty(country)) {
                    country = getString(R.string.n_a);
                }

                if (Constants.CUSTOMIZED_STATUS_ALLOWED.equals(mSearchedTflUser.friendPhotoShare)
                        || mSearchedTflUser.blockedByUserId.equals(mTflUser.userId)) {
                    mUserImagesRvAdapter.update(mSearchedTflUser.imagesList);
                }
                if (Constants.CUSTOMIZED_STATUS_ALLOWED.equals(mSearchedTflUser.friendVideoShare)
                        || mSearchedTflUser.blockedByUserId.equals(mTflUser.userId)) {
                    mUserVideosRvAdapter.update(mSearchedTflUser.videosList);
                }


            } else if (mFriendTflUser != null) {
                profilePicUrl = mFriendTflUser.imageUrl;
                email = mFriendTflUser.email;
                userProfileName = mFriendTflUser.name;

            }

            if (TextUtils.isEmpty(profilePicUrl)) {
                mUserProfileImageView.setImageResource(R.drawable.dummy_profile_image);

            } else {
                mImageLoader.displayImage(profilePicUrl, mUserProfileImageView,
                        ImageLoaderUtil.getDefaultProfileImageOption());
            }

            if (TextUtils.isEmpty(email)) {
                email = "No email ";
            }
            mUserEmailTextView.setText(email);

            if (TextUtils.isEmpty(phone)) {
                phone = "No phone number ";
            }
            mUserPhoneTextView.setText(phone);

            if (TextUtils.isEmpty(userProfileName)) {
                userProfileName = "No Name";
            }
            mUserNameTextView.setText(userProfileName);

            mEmailTextView.setText(email);
            mPhoneTextView.setText(phone);
            mCityTextView.setText(city);
            mStateTextView.setText(state);
            mCountryTextView.setText(country);
            if (TextUtils.isEmpty(street)) {
                street = getString(R.string.n_a);
            }
            mStreetTextView.setText(street);

            if (TextUtils.isEmpty(userName)) {
                userName = "User";
            }

            mViewContactsTextView.setText(String.format(getString(R.string.view_contacts),
                    userName + "'s"));
            mContactsView.setVisibility(hasContactsAndPermission(false) ? View.VISIBLE : View.GONE);

            if (TextUtils.isEmpty(biography)) {
                biography = "No Biography";
            }
            mBiographyTextView.setText(CommonAppMethods.decodeText(biography));
            updateFriendShipStatus();
            populateUserImages();
            manageTitleText();

        }
    }

    private boolean hasContactsAndPermission(boolean showErrMessage) {
        boolean result = true;
        String errorMessage = "";

        if (mSearchedTflUser == null) {
            result = false;
            errorMessage = "Invalid Data";

        } else if (TextUtils.isEmpty(mSearchedTflUser.friendContactShare)
                || !mSearchedTflUser.friendContactShare.equals(Constants.CUSTOMIZED_STATUS_ALLOWED)) {
            result = false;
            errorMessage = "Contacts are not shared with you.";

        } else if (mSearchedTflUser.noOfFriends <= 0) {
            result = false;
            errorMessage = "There is no contact of this user";

        } else if (mSearchedTflUser.friendsList == null || mSearchedTflUser.friendsList.isEmpty()) {
            result = false;
            errorMessage = "Contacts list is empty.";

        }

        if (showErrMessage && !result) {
            AndroidUtil.showToast(mContext, errorMessage);
        }
        return result;
    }

    private void populateUserImages() {
        mRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 3));
        mRecyclerView.setAdapter(mUserImagesRvAdapter);
        updateBg(true);
        mFocusedToggledView = mUsersImagesToggleTextView;
    }

    private void populateUserVideos() {
        mRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 2));
        mRecyclerView.setAdapter(mUserVideosRvAdapter);
        updateBg(false);
        mFocusedToggledView = mUsersVideosToggleTextView;
    }

    private void updateBg(boolean isPopulatingImages) {
        int mSelectedColor = ContextCompat.getColor(mContext, R.color.appColor_008640_green);
        int mUnSelectedColor = ContextCompat.getColor(mContext, R.color.appColor_white);
        int mUnSelectedTextColor = ContextCompat.getColor(mContext, R.color.appColor_6b4427_brown);

        if (mUsersImagesToggleTextView != null) {
            mUsersImagesToggleTextView.setTextColor(isPopulatingImages ? mUnSelectedColor : mUnSelectedTextColor);

            Drawable drawable = mUsersImagesToggleTextView.getBackground();
            if (drawable != null && drawable instanceof StateListDrawable) {
                StateListDrawable stateListDrawable = (StateListDrawable) drawable;
                drawable = stateListDrawable.getCurrent();
                if (drawable instanceof GradientDrawable) {
                    GradientDrawable gradientDrawable = (GradientDrawable) drawable;
                    gradientDrawable.setColor(isPopulatingImages ? mSelectedColor : mUnSelectedColor);
                }
            }
        }

        if (mUsersVideosToggleTextView != null) {
            mUsersVideosToggleTextView.setTextColor(!isPopulatingImages ? mUnSelectedColor : mUnSelectedTextColor);

            Drawable drawable = mUsersVideosToggleTextView.getBackground();
            if (drawable != null && drawable instanceof StateListDrawable) {
                StateListDrawable stateListDrawable = (StateListDrawable) drawable;
                drawable = stateListDrawable.getCurrent();
                if (drawable instanceof GradientDrawable) {
                    GradientDrawable gradientDrawable = (GradientDrawable) drawable;
                    gradientDrawable.setColor(!isPopulatingImages ? mSelectedColor : mUnSelectedColor);
                }
            }
        }
        updateNoDataTextVisibility(isPopulatingImages);
    }

    private void updateNoDataTextVisibility(boolean isPopulatingImages) {
        if (mNoDataTextView != null) {

            int count = 0;
            if (isPopulatingImages && mUserImagesRvAdapter != null) {
                count = mUserImagesRvAdapter.getItemCount();

            } else if (!isPopulatingImages && mUserVideosRvAdapter != null) {
                count = mUserVideosRvAdapter.getItemCount();

            }
            mNoDataTextView.setText(String.format(getString(R.string.no_xx_to_display),
                    isPopulatingImages ? "image" : "video"));
            if (mSearchedTflUser != null) {
                if (isPopulatingImages) {
                    if (Constants.CUSTOMIZED_STATUS_DENIED.equals(mSearchedTflUser.friendPhotoShare)) {
                        mNoDataTextView.setText(R.string.photo_sharing_denied);
                    }
                } else {
                    if (Constants.CUSTOMIZED_STATUS_DENIED.equals(mSearchedTflUser.friendVideoShare)) {
                        mNoDataTextView.setText(R.string.video_sharing_not_allowed);
                    }
                }
            }

            mNoDataTextView.setVisibility(count == 0 ? View.VISIBLE : View.GONE);


            if (count > 0) {
                mRecyclerView.smoothScrollToPosition(count);
            }
        }
    }

    //logic to update the status of friend's status
    private void updateFriendShipStatus() {
        mManageCustomizeView.setVisibility(View.VISIBLE);
        mTitleText = "Friend's Profile";
        String friendshipNextStateText = "";
        if (mSearchedTflUser != null) {
            friendshipNextStateText = mSearchedTflUser.friendShipStatus;
            mFriendshipStatusTextView.setEnabled(true);
            if (!TextUtils.isEmpty(friendshipNextStateText)) {
                switch (friendshipNextStateText) {

                    case Constants.FRIENDSHIP_STATUS_NOT_FRIEND:
                    case Constants.FRIENDSHIP_STATUS_FRIEND_REQUEST_REJECTED:
                    case Constants.FRIENDSHIP_STATUS_UN_FRIEND:
                        mManageCustomizeView.setVisibility(View.GONE);
                        friendshipNextStateText = "Add Friend";
                        mTitleText = "User's Profile";
                        break;

                    case Constants.FRIENDSHIP_STATUS_FRIEND:
                        friendshipNextStateText = "Friend";
                        break;

                    case Constants.FRIENDSHIP_STATUS_BLOCK:
                        if (!TextUtils.isEmpty(mSearchedTflUser.blockedByUserId)
                                && mTflUser != null
                                && mSearchedTflUser.blockedByUserId.equals(mTflUser.userId)) {

                            friendshipNextStateText = "Blocked by you";
                        } else {
                            friendshipNextStateText = "You're Blocked";
                        }

                        break;

                    case Constants.FRIENDSHIP_STATUS_FRIEND_REQUEST_SENT:
                        mTitleText = "User's Profile";
                        friendshipNextStateText = "Friend request sent";
                        mFriendshipStatusTextView.setEnabled(false);
                        break;

                    case Constants.FRIENDSHIP_STATUS_FRIEND_REQUEST_RECEIVED:
                        mTitleText = "User's Profile";
                        mManageCustomizeView.setVisibility(View.GONE);
                        friendshipNextStateText = "Process request";
                        break;
                }
            }
        }
        mFriendshipStatusTextView.setText(friendshipNextStateText);
    }

    @Override
    public void onResume() {
        super.onResume();
        manageTitleText();
    }

    private void manageTitleText() {
        if (mActivity != null) {
            mActivity.setTitle(mTitleText);
        }
    }


    private void afterReceivingBroadcast(Context context, Intent intent) {
        AndroidUtil.dumpIntent(TAG, intent);
        if (intent != null) {
            String mIntentAction = intent.getAction();

            if (!TextUtils.isEmpty(mIntentAction)) {

                switch (mIntentAction) {

                    case Constants.ACTION_RECEIVED_PUSH_NOTIFICATION:
                        afterReceivingPushNotification(intent);
                        break;

                }
            }

        }

    }

    private void afterReceivingPushNotification(@NonNull Intent intent) {
        int notificationType = intent.getIntExtra(Constants.KEY_PUSH_NOTIFICATION_TYPE, -1);

        switch (notificationType) {

            case Constants.PUSH_NOTIFICATION_FRIEND_REQ_ACCEPTED:
            case Constants.PUSH_NOTIFICATION_FRIEND_REQ_RECEIVED:
            case Constants.PUSH_NOTIFICATION_NEW_IMAGE:
            case Constants.PUSH_NOTIFICATION_NEW_VIDEO:
                String treeId = intent.getStringExtra(Constants.KEY_TREE_ID);
                if (!TextUtils.isEmpty(treeId)
                        && treeId.equals(findUserTreeOrEmailId())) {
                    loadData();
                }
                break;

            case Constants.PUSH_NOTIFICATION_USER_BLOCKED:

                FragmentUtil.loadFragment((AppCompatActivity) mContext, FragmentUtil.FRAGMENT_HOME, true);
                break;
        }
    }


    private void registerLocalReceiver() {
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(mActivity);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.ACTION_RECEIVED_PUSH_NOTIFICATION);
        localBroadcastManager.registerReceiver(mBroadcastReceiver, intentFilter);
        isPushNotificationReceiverRegistered = true;

    }

    private void unRegisterLocalReceiver() {
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(mActivity);
        if (isPushNotificationReceiverRegistered) {
            localBroadcastManager.unregisterReceiver(mBroadcastReceiver);
            isPushNotificationReceiverRegistered = false;

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unRegisterLocalReceiver();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.user_profile_image:
                showProfileImage();
                break;

            case R.id.image_text_view:
                if (mFocusedToggledView == null
                        || mFocusedToggledView.getId() != R.id.image_text_view) {
                    populateUserImages();
                }
                mFocusedToggledView = view;

                break;

            case R.id.video_text_view:
                if (mFocusedToggledView == null
                        || mFocusedToggledView.getId() != R.id.video_text_view) {
                    populateUserVideos();
                }
                mFocusedToggledView = view;
                break;

            case R.id.friendship_text_view:
                manageFriendReqCall();
                break;

            case R.id.manage_customize_view:
                goToCustomizeSettingsPage();
                break;

            case R.id.contacts_view:
                if (hasContactsAndPermission(true)) {
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.KEY_CALLER_COMPONENT, TAG);
                    bundle.putParcelable(Constants.KEY_TFL_USER, mTflUser);
                    bundle.putParcelable(Constants.KEY_SEARCHED_TFL_USER, mSearchedTflUser);
                    FragmentUtil.loadFragment((AppCompatActivity) mActivity,
                            FragmentUtil.FRAGMENT_FRIENDS, bundle, true);
                }
                break;
        }
    }

    //logic to show the image of profile
    private void showProfileImage() {
        String imageUriOrUrl = "";


        if (mSearchedTflUser != null) {
            imageUriOrUrl = mSearchedTflUser.profileImageUrl;
        } else if (mFriendTflUser != null) {
            imageUriOrUrl = mFriendTflUser.imageUrl;
        }


        if (!TextUtils.isEmpty(imageUriOrUrl)) {
            FragmentManager fragmentManager = getChildFragmentManager();

            Bundle bundle = new Bundle();
            bundle.putString(Constants.KEY_CALLER_COMPONENT, TAG);
            bundle.putString(Constants.KEY_IMAGE_URL, imageUriOrUrl);

            ImageViewDialogFragment mImageViewDialogFragment;
            Fragment fragment =
                    fragmentManager.findFragmentByTag(ImageViewDialogFragment.TAG);
            if (fragment != null && fragment instanceof ImageViewDialogFragment) {
                mImageViewDialogFragment = (ImageViewDialogFragment) fragment;
                mImageViewDialogFragment.update(bundle);
            } else {
                mImageViewDialogFragment = new ImageViewDialogFragment();
                mImageViewDialogFragment.setArguments(bundle);
            }

            mImageViewDialogFragment.show(fragmentManager, ImageViewDialogFragment.TAG);
        } else {
            AndroidUtil.showToast(mContext, getString(R.string.no_profile_image));
        }
    }

    private void goToCustomizeSettingsPage() {
        if (mSearchedTflUser != null) {
            Intent intent = new Intent(mContext, CustomizeActivity.class);
            intent.putExtra(Constants.KEY_CALLER_COMPONENT, TAG);
            intent.putExtra(Constants.KEY_TFL_USER, mTflUser);
            intent.putExtra(Constants.KEY_SEARCHED_TFL_USER, mSearchedTflUser);
            mActivity.startActivityForResult(intent, Constants.RC_START_ACTIVITY_CUSTOMIZE);
        } else {
            AndroidUtil.showToast(mContext, "Invalid Data");
        }
    }

    //logic to manage the friendship request
    private void manageFriendReqCall() {
        if (mSearchedTflUser != null && mFriendApisController != null) {

            if (!TextUtils.isEmpty(mSearchedTflUser.friendShipStatus)) {
                switch (mSearchedTflUser.friendShipStatus) {

                    case Constants.FRIENDSHIP_STATUS_NOT_FRIEND:
                    case Constants.FRIENDSHIP_STATUS_FRIEND_REQUEST_REJECTED:
                    case Constants.FRIENDSHIP_STATUS_UN_FRIEND:

                        mFriendApisController.sendFriendRequest(mSearchedTflUser.id);
                        break;

                    case Constants.FRIENDSHIP_STATUS_FRIEND:
                        DialogUtil.getInstance().showAlertList(mContext,
                                "Select",
                                new String[]{"Block Friend", "Remove Friend"},
                                RC_ON_FRIEND,
                                this);
                        break;

                    case Constants.FRIENDSHIP_STATUS_BLOCK:
                        String[] actions = new String[]{"Remove Friend"};
                        if (mSearchedTflUser != null && mTflUser != null &&
                                !TextUtils.isEmpty(mSearchedTflUser.blockedByUserId)
                                && mSearchedTflUser.blockedByUserId.equals(mTflUser.userId)) {

                            actions = new String[]{"Remove Friend", "Unblock Friend"};
                        }
                        DialogUtil.getInstance().showAlertList(mContext,
                                "Select",
                                actions,
                                RC_ON_BLOCK,
                                this);
                        break;

                    case Constants.FRIENDSHIP_STATUS_FRIEND_REQUEST_RECEIVED:
                        DialogUtil.getInstance().showAlertList(mContext,
                                "Select",
                                new String[]{"Accept request", "Reject request"},
                                RC_ON_FRIEND_REQ_RECEIVED,
                                this);
                        break;
                }
            }
        }
    }

    @Override
    public void onSearchUserApiResult(SearchedTflUser searchedTflUser) {
        this.mSearchedTflUser = searchedTflUser;
        displayContent();

    }


    @Override
    public void onImageClick(int position, Image image) {
        if (image != null && !TextUtils.isEmpty(image.imageUrl)) {

            FragmentManager fragmentManager = getChildFragmentManager();

            Bundle bundle = new Bundle();
            bundle.putString(Constants.KEY_CALLER_COMPONENT, TAG);
            bundle.putString(Constants.KEY_IMAGE_URL, image.imageUrl);

            ImageViewDialogFragment mImageViewDialogFragment;
            Fragment fragment =
                    fragmentManager.findFragmentByTag(ImageViewDialogFragment.TAG);
            if (fragment != null && fragment instanceof ImageViewDialogFragment) {
                mImageViewDialogFragment = (ImageViewDialogFragment) fragment;
                mImageViewDialogFragment.update(bundle);
            } else {
                mImageViewDialogFragment = new ImageViewDialogFragment();
                mImageViewDialogFragment.setArguments(bundle);
            }

            mImageViewDialogFragment.show(fragmentManager, ImageViewDialogFragment.TAG);
        } else {
            AndroidUtil.showToast(mContext, "Invalid image data/empty image url");
        }
    }

    @Override
    public void videoItemClick(int position, Video video) {
        if (video != null) {
            Bundle bundle = new Bundle();
            bundle.putString(Constants.KEY_CALLER_COMPONENT, TAG);
            bundle.putString(Constants.KEY_VIDEO_URL, video.videoUrl);
            FragmentUtil.loadFragment((AppCompatActivity) mActivity,
                    FragmentUtil.FRAGMENT_EASY_VIDEO_PLAYER, bundle,
                    true);
        }
    }

    @Override
    public void onFriendApiResult(ResponseId responseId, boolean success) {
        if (isVisible()) {

            switch (responseId) {
                case ADD_FRIEND_TO_FAMILY:
                    goToCustomizeSettingsPage();
                    return;
            }

            if (success) {
                String friendShipStatus = "";
                switch (responseId) {

                    case SEND_FRIEND_REQUEST:
                        friendShipStatus = Constants.FRIENDSHIP_STATUS_FRIEND_REQUEST_SENT;
                        goToCustomizeSettingsPage();
                        break;

                    case ACCEPT_FRIEND_REQUEST:
                        addToFamily();

                    case UNBLOCK_FRIEND:
                        friendShipStatus = Constants.FRIENDSHIP_STATUS_FRIEND;
                        break;

                    case REJECT_FRIEND_REQUEST:
                        friendShipStatus = Constants.FRIENDSHIP_STATUS_FRIEND_REQUEST_REJECTED;
                        break;

                    case REMOVE_FRIEND:
                        friendShipStatus = Constants.FRIENDSHIP_STATUS_UN_FRIEND;
                        break;

                    case BLOCK_FRIEND:
                        friendShipStatus = Constants.FRIENDSHIP_STATUS_BLOCK;
                        if (mSearchedTflUser != null && mTflUser != null) {
                            mSearchedTflUser.blockedByUserId = mTflUser.userId;
                        }
                        break;
                }

                if (!TextUtils.isEmpty(friendShipStatus)) {
                    if (mSearchedTflUser != null) {
                        mSearchedTflUser.friendShipStatus = friendShipStatus;
                    }
                    updateFriendShipStatus();
                }
            }
        }
    }

    private void addToFamily() {
        if (isVisible()) {
            Runnable addToFamilyAction = new Runnable() {
                @Override
                public void run() {
                    mFriendApisController.addFriendToFamily(mSearchedTflUser, false);
                }
            };
            Runnable cancelAction = new Runnable() {
                @Override
                public void run() {
                    goToCustomizeSettingsPage();
                }
            };

            mDialogUtil.showTwoButtonsAlertDialog(mContext,
                    "Add to Family ?",
                    "Would you like to add this friend to your family ?",
                    "Yes",
                    "No",
                    addToFamilyAction,
                    cancelAction,
                    false);
        }
    }


    @Override
    public void onItemClicked(int which, String datum, int requestCode) {
        switch (requestCode) {

            case RC_ON_FRIEND:
                switch (which) {
                    case 0:
                        mFriendApisController.blockFriend(mSearchedTflUser);
                        break;

                    case 1:
                        mFriendApisController.removeFriend(mSearchedTflUser, true);
                        break;
                }
                break;

            case RC_ON_BLOCK:
                switch (which) {
                    case 0:
                        mFriendApisController.removeFriend(mSearchedTflUser, true);
                        break;

                    case 1:
                        mFriendApisController.unblockFriend(mSearchedTflUser);
                        break;
                }
                break;

            case RC_ON_FRIEND_REQ_RECEIVED:
                switch (which) {
                    case 0:
                        mFriendApisController.acceptFriendRequest(mSearchedTflUser);
                        break;

                    case 1:
                        mFriendApisController.rejectFriendRequest(mSearchedTflUser);
                        break;
                }
                break;
        }
    }

    @Override
    public void onCustomizeSettingsChange() {
        loadData();
    }
}
