package com.treeforlife.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.treeforlife.R;
import com.treeforlife.activities.RegisterActivity;
import com.treeforlife.commons.Constants;
import com.treeforlife.customview.CustomTextView;

/**
 * Created by TheAppsmiths on 4/3/2018.
 */
/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class RegisterOptionDialogFragment extends DialogFragment implements View.OnClickListener {

    public static final String TAG = "RegisterOptionDialogFragment";

    private Context mContext;
    private Activity mActivity;
    private Bundle mBundle;
    private TextView mLoginTextButon;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mActivity = getActivity();
        this.mContext = context;
        this.mBundle = getArguments();

        setStyle(android.support.v4.app.DialogFragment.STYLE_NO_FRAME, R.style.DialogTheme);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ensureComponents();
    }

    private void ensureComponents() {
        if (mContext == null) {
            mContext = getActivity();
        }
        if (mActivity == null) {
            mActivity = getActivity();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_dialog_regsiter_option, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initComponents(view);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(mContext != null ? mContext :
                mActivity != null ? mActivity : getActivity(),
                R.style.DialogTheme);
    }

    private void initComponents(View view) {
        if (view != null) {
            Toolbar mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
            if (mToolbar != null) {
                CustomTextView mToolbarTitle = mToolbar.findViewById(R.id.toolbar_title);
                mToolbarTitle.setText("Choose option to register");
                View mToolbarIconLhs = mToolbar.findViewById(R.id.toolbar_icon_back);
                mToolbarIconLhs.setVisibility(View.GONE);
                View mToolbarIconRhs = mToolbar.findViewById(R.id.toolbar_icon_hamberg);
                mToolbarIconRhs.setVisibility(View.GONE);

            }
            Button mRegisterPhone = view.findViewById(R.id.register_phone_btn);
            mRegisterPhone.setOnClickListener(this);
            Button mRegisterEmail = view.findViewById(R.id.register_email_btn);
            mRegisterEmail.setOnClickListener(this);
            mLoginTextButon = view.findViewById(R.id.login_text_view);
            mLoginTextButon.setOnClickListener(this);
            decorateLoginTextView();

        }

    }

    private void decorateLoginTextView() {
        if (mLoginTextButon != null) {
            String text = mLoginTextButon.getText().toString();
            String subText = getString(R.string.login_here);
            if (!TextUtils.isEmpty(text) &&
                    !TextUtils.isEmpty(subText) &&
                    text.contains(subText)) {

                SpannableString spannableString = new SpannableString(text);
                spannableString.setSpan(
                        new ForegroundColorSpan(ContextCompat.getColor(mContext, R.color.appColor_774c2b_brown)),
                        text.indexOf(subText),
                        text.lastIndexOf(subText) + subText.length(),
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                mLoginTextButon.setText(spannableString);
            }
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.register_phone_btn:
                Intent in = new Intent(mContext, RegisterActivity.class);
                in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                in.putExtra(Constants.KEY_REGISTER_BY, Constants.REGISTER_BY_PHONE);
                startActivity(in);
                break;

            case R.id.register_email_btn:
                Intent in1 = new Intent(mContext, RegisterActivity.class);
                in1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                in1.putExtra(Constants.KEY_REGISTER_BY, Constants.REGISTER_BY_EMAIL);
                startActivity(in1);
                break;

            case R.id.login_text_view:
                dismiss();
                break;

        }

    }
}
