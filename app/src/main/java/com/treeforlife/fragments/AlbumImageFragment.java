package com.treeforlife.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.treeforlife.R;
import com.treeforlife.Utils.AndroidUtil;
import com.treeforlife.Utils.AttachFileUtil;
import com.treeforlife.Utils.DialogUtil;
import com.treeforlife.Utils.ProgressDialogUtil;
import com.treeforlife.adapters.AlbumImagesRvAdapter;
import com.treeforlife.commons.Constants;
import com.treeforlife.commons.ListItemCallback;
import com.treeforlife.commons.UploadImageVideoDialog;
import com.treeforlife.dataobjects.Album;
import com.treeforlife.dataobjects.AlbumImage;
import com.treeforlife.dataobjects.TFLUser;
import com.treeforlife.retrofit.ResponseId;
import com.treeforlife.retrofit.RetrofitManager;
import com.treeforlife.retrofit.RetrofitResponseValidator;
import com.treeforlife.retrofit.responses.GetAlbumImagesResponse;
import com.treeforlife.retrofit.responses.UploadImageResponse;
import com.treeforlife.retrofit.responses.WebServiceResponse;

import java.io.File;
import java.net.SocketTimeoutException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TheAppsmiths on 3/20/2018.
 */

public class AlbumImageFragment extends Fragment implements AlbumImagesRvAdapter.AlbumImageCallBack,
        View.OnClickListener, ListItemCallback, RetrofitResponseValidator.ValidationListener,
        UploadImageVideoDialog.UploadDialogCallBack {

    public static final String TAG = "AlbumImageFragment";

    private Context mContext;
    private Activity mActivity;
    private Bundle mBundle;

    private RecyclerView mRecyclerView;
    private AlbumImagesRvAdapter mAlbumImagesRvAdapter;
    private TextView mNoDataTextView, mAddImageTextView;
    private View mWishView;

    private ProgressDialogUtil mProgressDialogUtil;
    private DialogUtil mDialogUtil;
    private AttachFileUtil mAttachFileUtil;
    private UploadImageVideoDialog mDialog;

    private TFLUser mTflUser;
    private Album mAlbum;
    private AlbumImage mAlbumImageToDelete;

    private String mCaller;
    private int mPage, mAlbumImagePositionToDelete;
    private boolean isLoadingData;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
        this.mActivity = getActivity();
        this.mBundle = getArguments();

        this.mAlbumImagesRvAdapter = new AlbumImagesRvAdapter(this);

        this.mDialogUtil = DialogUtil.getInstance();
        this.mProgressDialogUtil = ProgressDialogUtil.getInstance();
        this.mAttachFileUtil = AttachFileUtil.getInstance();
        this.mDialog = new UploadImageVideoDialog(mContext);
        extractDataFromBundle();
    }

    /**
     * extract data from bundle
     */
    private void extractDataFromBundle() {

        if (mBundle == null) {
            mBundle = getArguments();
        }

        if (mBundle != null) {
            mCaller = mBundle.getString(Constants.KEY_CALLER_COMPONENT);
            mTflUser = mBundle.getParcelable(Constants.KEY_TFL_USER);
            mAlbum = mBundle.getParcelable(Constants.KEY_ALBUM);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_album_images, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initWidgets(view);
        getAlbumImages();
    }


    /**
     * initialize the widget
     *
     * @param view
     */
    private void initWidgets(View view) {

        int mSpanCount;
        if (getResources().getBoolean(R.bool.isTab)) {
            System.out.println("tablet");
            mSpanCount = 3;
        } else {
            System.out.println("mobile");
            mSpanCount = 2;
        }

        mAddImageTextView = view.findViewById(R.id.add_image_text_view);
        mAddImageTextView.setOnClickListener(this);

        mNoDataTextView = view.findViewById(R.id.no_data_text_view);

        mWishView = view.findViewById(R.id.wish_layout);

        mRecyclerView = view.findViewById(R.id.album_images_recycler_view);
        GridLayoutManager layoutManager = new GridLayoutManager(mContext, mSpanCount);
        mRecyclerView.setLayoutManager(layoutManager);
        setInfiniteScroller(layoutManager);
        mRecyclerView.setAdapter(mAlbumImagesRvAdapter);

        setNoDataTextVisibility();

    }

    /**
     * set infinite scroll for more feature
     *
     * @param layoutManager
     */
    private void setInfiniteScroller(final GridLayoutManager layoutManager) {
        if (layoutManager != null && mRecyclerView != null) {
            mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    int lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition();
                    int totalItemCount = layoutManager.getItemCount();
                    Log.e("TotalItemCountHome", "" + lastVisibleItemPosition + " " + Constants.ITEMS_LIMIT + " " + totalItemCount);

                    if ((lastVisibleItemPosition + Constants.ITEMS_LIMIT) > totalItemCount) {
                        Log.w(TAG, " RECYCLER ON SCROLL : loadData() called");
                        getAlbumImages();
                    }
                }
            });
        }
    }

    /**
     * visibility of not data text
     */
    private void setNoDataTextVisibility() {
        if (mNoDataTextView != null && mAlbumImagesRvAdapter != null) {
            mNoDataTextView.setVisibility(mAlbumImagesRvAdapter.getItemCount() > 0 ? View.GONE : View.VISIBLE);
        }
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.add_image_text_view:
                mDialog.UploadDialog(UploadImageVideoDialog.UploadFile.UPLOAD_IMAGE, this);
                break;
        }
    }

    /**
     * method to upload the album
     *
     * @param mAttachedFile
     * @param description
     */
    private void uploadAlbumImage(File mAttachedFile, String description) {

        if (isValidUser() && isValidAlbum() && AndroidUtil.hasInternetConnectivity(mContext)) {
            mProgressDialogUtil.showProgressDialog(mActivity);
            MultipartBody.Builder builder = new MultipartBody.Builder();
            builder.setType(MultipartBody.FORM);
            builder.addFormDataPart("user_id", mTflUser.userId);
            builder.addFormDataPart("image_desc", description);
            builder.addFormDataPart("album_id", String.valueOf(mAlbum.albumId));
            // Single Image

            if (mAttachedFile != null) {
                File file1 = new File(mAttachedFile.getName());
                builder.addFormDataPart("image", file1.getName(), (RequestBody.create(MediaType.parse("image"), mAttachedFile)));

            }

            MultipartBody requestBody = builder.build();

            Call<UploadImageResponse> uploadImage = RetrofitManager.getRetrofitWebService()
                    .uploadImage(requestBody);

            uploadImage.enqueue(new Callback<UploadImageResponse>() {
                @Override
                public void onResponse(Call<UploadImageResponse> call, Response<UploadImageResponse> response) {
                    new RetrofitResponseValidator(ResponseId.UPLOAD_ALBUM_IMAGE, response, AlbumImageFragment.this);

                }

                @Override
                public void onFailure(Call<UploadImageResponse> call, Throwable t) {
                    String errorMessage = "";
                    if (t != null) {
                        Log.e(TAG, "onFailure: Error : " + t.getMessage());
                        if (t instanceof SocketTimeoutException) {
                            errorMessage = "Please make sure that your device has an active internet connection.";
                        }
                    }

                    mProgressDialogUtil.dismissProgressDialog();


                    if (TextUtils.isEmpty(errorMessage)) {
                        AndroidUtil.showErrorToast(mActivity);
                    } else {
                        mDialogUtil.showErrorDialog(mActivity, errorMessage);
                    }
                }
            });

        } else {
            AndroidUtil.showToast(mActivity, "Please Choose Image");
        }


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (mDialog != null) {
            mDialog.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (mDialog != null) {
            mDialog.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    @Override
    public void onItemClicked(int which, String datum, int requestCode) {

    }

    private boolean isValidUser() {
        if (mTflUser == null) {
            AndroidUtil.showToast(mContext, "Invalid User");
            return false;

        } else if (TextUtils.isEmpty(mTflUser.userId)) {
            AndroidUtil.showToast(mContext, "You need to Login !");
            return false;
        }

        return true;
    }

    private boolean isValidAlbum() {
        if (mAlbum == null) {
            AndroidUtil.showToast(mContext, "Invalid Album");
            return false;

        }

        return true;
    }

    /**
     * method to get the list of albums
     */
    private void getAlbumImages() {

        if (isValidUser() && isValidAlbum() && AndroidUtil.hasInternetConnectivity(mContext)
                && !isLoadingData) {
            isLoadingData = true;
            if (mPage == 0) {
                mProgressDialogUtil.showProgressDialog(mActivity);
            }

            Call<GetAlbumImagesResponse> getAlbumImagesCall = RetrofitManager.getRetrofitWebService()
                    .getAlbumImages(mTflUser.userId, mAlbum.albumId, mPage);
            getAlbumImagesCall.enqueue(new Callback<GetAlbumImagesResponse>() {
                @Override
                public void onResponse(Call<GetAlbumImagesResponse> call, Response<GetAlbumImagesResponse> response) {
                    new RetrofitResponseValidator(ResponseId.GET_ALBUM_IMAGES, response, AlbumImageFragment.this);

                }

                @Override
                public void onFailure(Call<GetAlbumImagesResponse> call, Throwable t) {
                    String errorMessage = "";
                    if (t != null) {
                        Log.e(TAG, "onFailure: Error : " + t.getMessage());
                        if (t instanceof SocketTimeoutException) {
                            errorMessage = "Please make sure that your device has an active internet connection.";
                        }
                    }

                    mProgressDialogUtil.dismissProgressDialog();
                    isLoadingData = false;
                    if (!isVisible() || mPage != 0) {
                        return;
                    }

                    if (TextUtils.isEmpty(errorMessage)) {
                        AndroidUtil.showErrorToast(mContext);
                    } else {
                        mDialogUtil.showErrorDialog(mContext, errorMessage);
                    }
                }
            });

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mActivity != null) {
            mActivity.setTitle(R.string.images);
        }
        if (mAttachFileUtil != null) {
            mAttachFileUtil.onResume();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mDialog != null) {
            mDialog.DismisDialog();
        }
    }

    @Override
    public void onitemCall(File mAttachedFile, String description) {
        if (mAttachedFile != null && !TextUtils.isEmpty(description)) {
            uploadAlbumImage(mAttachedFile, description);
        }
    }

    @Override
    public void removeAlbumImage(int position, AlbumImage albumImage) {
        if (albumImage == null) {
            return;
        }

        mAlbumImagePositionToDelete = position;
        mAlbumImageToDelete = albumImage;

        Runnable OkButton = new Runnable() {
            @Override
            public void run() {
                callDeleteAlbumImage();
            }
        };
        String imageName = albumImage.imageDescription;
        if (TextUtils.isEmpty(imageName)) {
            imageName = "this";
        } else {
            imageName = "\"" + imageName + "\"";
        }
        mDialogUtil.showOkCancelAlertDialog(mActivity, "Are you sure you want to delete "
                + imageName + " image?", OkButton);


    }

    @Override
    public void shareAlbumImage(int position, final AlbumImage albumImage) {

        Runnable OkButton = new Runnable() {
            @Override
            public void run() {
                callShareAlbumImage(albumImage);
            }
        };
        mDialogUtil.showOkCancelAlertDialog(mActivity, "Are you sure you want to share this image?", OkButton);


    }

    @Override
    public void albumImageClick(int position, AlbumImage albumImage) {
        if (albumImage != null && !TextUtils.isEmpty(albumImage.imageUrl)) {

            FragmentManager fragmentManager = getChildFragmentManager();

            if (fragmentManager != null) {
                Bundle bundle = new Bundle();
                bundle.putString(Constants.KEY_CALLER_COMPONENT, TAG);
                bundle.putString(Constants.KEY_IMAGE_URL, albumImage.imageUrl);

                ImageViewDialogFragment mImageViewDialogFragment;
                Fragment fragment =
                        fragmentManager.findFragmentByTag(ImageViewDialogFragment.TAG);
                if (fragment != null && fragment instanceof ImageViewDialogFragment) {
                    mImageViewDialogFragment = (ImageViewDialogFragment) fragment;
                    mImageViewDialogFragment.update(bundle);
                } else {
                    mImageViewDialogFragment = new ImageViewDialogFragment();
                    mImageViewDialogFragment.setArguments(bundle);
                }

                mImageViewDialogFragment.show(fragmentManager, ImageViewDialogFragment.TAG);
            }
        } else {
            AndroidUtil.showToast(mContext, "Invalid album image data/empty image url");
        }
    }

    /**
     * method call to delete album image
     */
    private void callDeleteAlbumImage() {
        if (mAlbumImageToDelete != null) {

            if (isValidUser() && isValidAlbum() && AndroidUtil.hasInternetConnectivity(mContext)) {
                mProgressDialogUtil.showProgressDialog(mActivity);

                Call<WebServiceResponse> getAlbum = RetrofitManager.getRetrofitWebService()
                        .deleteImage(mTflUser.userId, mAlbum.albumId, mAlbumImageToDelete.imageId);
                getAlbum.enqueue(new Callback<WebServiceResponse>() {
                    @Override
                    public void onResponse(Call<WebServiceResponse> call, Response<WebServiceResponse> response) {
                        new RetrofitResponseValidator(ResponseId.DELETE_ALBUM_IMAGE, response, AlbumImageFragment.this);

                    }

                    @Override
                    public void onFailure(Call<WebServiceResponse> call, Throwable t) {
                        String errorMessage = "";
                        if (t != null) {
                            Log.e(TAG, "onFailure: Error : " + t.getMessage());
                            if (t instanceof SocketTimeoutException) {
                                errorMessage = "Please make sure that your device has an active internet connection.";
                            }
                        }

                        mProgressDialogUtil.dismissProgressDialog();


                        if (TextUtils.isEmpty(errorMessage)) {
                            AndroidUtil.showErrorToast(mContext);
                        } else {
                            mDialogUtil.showErrorDialog(mContext, errorMessage);
                        }

                        mAlbumImageToDelete = null;
                        mAlbumImagePositionToDelete = -1;

                    }
                });

            }
        } else {
            AndroidUtil.showToast(mContext, "Invalid Album Image");
        }

    }

    /**
     * share album image api
     *
     * @param albumImage
     */
    private void callShareAlbumImage(AlbumImage albumImage) {
        if (albumImage != null) {
            if (isValidUser() && isValidAlbum() && AndroidUtil.hasInternetConnectivity(mContext)) {
                mProgressDialogUtil.showProgressDialog(mActivity);

                Call<WebServiceResponse> getAlbum = RetrofitManager.getRetrofitWebService()
                        .shareImage(mTflUser.userId, mAlbum.albumId, albumImage.imageId);
                getAlbum.enqueue(new Callback<WebServiceResponse>() {
                    @Override
                    public void onResponse(Call<WebServiceResponse> call, Response<WebServiceResponse> response) {
                        new RetrofitResponseValidator(ResponseId.SHARE_ALBUM_IMAGE, response, AlbumImageFragment.this);

                    }

                    @Override
                    public void onFailure(Call<WebServiceResponse> call, Throwable t) {
                        String errorMessage = "";
                        if (t != null) {
                            Log.e(TAG, "onFailure: Error : " + t.getMessage());
                            if (t instanceof SocketTimeoutException) {
                                errorMessage = "Please make sure that your device has an active internet connection.";
                            }
                        }

                        mProgressDialogUtil.dismissProgressDialog();


                        if (TextUtils.isEmpty(errorMessage)) {
                            AndroidUtil.showErrorToast(mContext);
                        } else {
                            mDialogUtil.showErrorDialog(mContext, errorMessage);
                        }
                    }
                });

            }
        } else {
            AndroidUtil.showToast(mContext, "Invalid Album Image");
        }

    }


    @Override
    public void onValidResponse(@NonNull ResponseId responseId, @NonNull Object responseBody, @NonNull Response response) {
        switch (responseId) {
            case GET_ALBUM_IMAGES:
                afterGettingAlbumImages((GetAlbumImagesResponse) responseBody);
                isLoadingData = false;
                break;

            case UPLOAD_ALBUM_IMAGE:
                afterUploadingAlbumImage((UploadImageResponse) responseBody);
                break;

            case DELETE_ALBUM_IMAGE:
                afterDeletingAlbumImage((WebServiceResponse) responseBody);
                mAlbumImageToDelete = null;
                mAlbumImagePositionToDelete = -1;
                break;

            case SHARE_ALBUM_IMAGE:
                WebServiceResponse serviceResponse = (WebServiceResponse) responseBody;
                AndroidUtil.showToast(mActivity, serviceResponse.message);
                break;
        }
        mProgressDialogUtil.dismissProgressDialog();

    }


    @Override
    public void onInvalidResponse(@NonNull ResponseId responseId, @NonNull String errorMessage, Response response) {
        boolean showErrInDialog = false;
        mProgressDialogUtil.dismissProgressDialog();
        switch (responseId) {
            case GET_ALBUM_IMAGES:
                isLoadingData = false;
                if (!isVisible() || mPage != 0) {
                    return;
                }
                setNoDataTextVisibility();
                break;

            case DELETE_ALBUM_IMAGE:
                mAlbumImageToDelete = null;
                mAlbumImagePositionToDelete = -1;
                showErrInDialog = true;
                break;
        }

        if (response == null || !response.isSuccessful()) {
            mDialogUtil.showErrorDialog(mActivity, errorMessage);
        } else {
            if (showErrInDialog) {
                mDialogUtil.showOkAlertDialog(mActivity, errorMessage);
            } else {
                AndroidUtil.showToast(mContext, errorMessage);
            }
        }

    }

    /**
     * method after success result of album image
     *
     * @param response
     */
    private void afterGettingAlbumImages(GetAlbumImagesResponse response) {
        if (response != null) {

            if (mAlbumImagesRvAdapter != null) {
                if (mPage == 0) {
                    mAlbumImagesRvAdapter.update(response.imagesList);
                } else {
                    mAlbumImagesRvAdapter.addToList(response.imagesList);
                }
            }
            mPage++;
        }

        setNoDataTextVisibility();

    }

    /**
     * method after image upload
     *
     * @param response
     */
    private void afterUploadingAlbumImage(UploadImageResponse response) {
        if (response != null) {
            if (mAlbumImagesRvAdapter != null) {
                mAlbumImagesRvAdapter.addImage(response.albumImage);
            }
            AndroidUtil.showToast(mActivity, response.message);
            setNoDataTextVisibility();
            mPage++;

        }
    }

    /**
     * method after delete album image
     *
     * @param response
     */
    private void afterDeletingAlbumImage(WebServiceResponse response) {
        if (response != null) {
            if (response.status) {
                if (mAlbumImagesRvAdapter != null) {
                    mAlbumImagesRvAdapter.removeItem(mAlbumImagePositionToDelete);
                }

            }
            setNoDataTextVisibility();
            AndroidUtil.showToast(mActivity, response.message);
        }
    }
}
