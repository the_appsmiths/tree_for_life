package com.treeforlife.retrofit;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.treeforlife.BuildConfig;
import com.treeforlife.Utils.HttpErrorUtil;
import com.treeforlife.retrofit.responses.WebServiceResponse;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by TheAppsmiths on 4/3/2018.
 * <p>
 * copy from previous work
 */

public class RetrofitResponseValidator {

    private static final String TAG = "ResponseValidator";
    private final ResponseId mResponseId;
    private final Response response;
    private final ValidationListener listener;


    public RetrofitResponseValidator(@NonNull ResponseId responseId, Response response,
                                     @NonNull ValidationListener listener) {
        this.mResponseId = responseId;
        this.response = response;
        this.listener = listener;
        validate();
    }

    private void validate() {
        boolean isValidResponse = false;
        String errorMessage = "Error in Response : ";

        if (response != null) {
            Object temp = response.body();
            if (temp != null) {

                if (temp instanceof WebServiceResponse) {

                    if (((WebServiceResponse) temp).status) {
                        isValidResponse = true;

                    } else {

                        errorMessage = ((WebServiceResponse) temp).message;
                        if (TextUtils.isEmpty(errorMessage)) {
                            errorMessage = " SERVER COULD NOT PROCESS THE REQUEST";
                        }
                    }

                } else {
                    errorMessage = " Unexpected Response Format" + temp;
                }


                if (listener != null) {
                    if (isValidResponse) {
                        listener.onValidResponse(mResponseId, temp, response);
                        return;
                    } else {
                        Log.e(TAG, "validate: Invalid response ");
                    }
                } else {
                    Log.e(TAG, "validate: listener is null");
                }

            } else {
                String errorInfo = HttpErrorUtil.getHttpCodeInfo(response.code());
                errorMessage += errorInfo;

                ResponseBody errorBody = response.errorBody();
                if (errorBody != null) {
                    try {
                        errorMessage += errorBody.string();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                Log.e(TAG, "validate: Error " + errorMessage);
                errorMessage = "Oops! Something Bad Happened!\n" +
                        "We apologize for any inconvenience, but an unexpected error occurred at server end.\n\n" +
                        "It’s not you, it’s us. This is our fault.\n\n" +
                        "Please refresh & try again after some time.\n";
                if (BuildConfig.DEBUG) {
                    errorMessage += errorInfo;
                }

            }

        } else {
            errorMessage += " Response <T> is null";
        }

        if (!isValidResponse) {

            if (listener != null) {
                listener.onInvalidResponse(mResponseId, errorMessage, response);
            } else {
                Log.e(TAG, "validate: Invalid Response & listener is null");
            }
        }
    }

    public static WebServiceResponse extractOutWebServiceResponse(Response response) {
        WebServiceResponse result = null;
        if (response != null) {
            Object temp = response.body();
            if (temp != null && temp instanceof WebServiceResponse) {
                result = (WebServiceResponse) temp;
            }
        }

        return result;
    }

    public interface ValidationListener {
        void onValidResponse(@NonNull ResponseId responseId, @NonNull Object responseBody, @NonNull Response response);

        void onInvalidResponse(@NonNull ResponseId responseId, @NonNull String errorMessage,
                               Response response);
    }
}
