package com.treeforlife.dataobjects;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by TheAppsmiths on 4/11/2018.
 *
 * @author TheAppsmiths
 */

public class FriendTflUser implements Parcelable {
    @SerializedName(value = "friend_id", alternate = {"member_id","id"})
    public String id;
    @SerializedName(value = "friend_name", alternate = {"member_name","name"})
    public String name;
    @SerializedName("email")
    public String email;
    @SerializedName("phone_number")
    public String phoneNumber;
    @SerializedName("profile_share")
    public String profileShare;
    @SerializedName("video_share")
    public String videoShare;
    @SerializedName("photo_share")
    public String photoShare;
    @SerializedName("contacts_share")
    public String contactShare;
    @SerializedName(value = "tree_id", alternate = {"member_tree_id","friend_tree_id"})
    public String treeId;
    @SerializedName(value = "friend_image", alternate = {"member_profile_pic"})
    public String imageUrl;

    protected FriendTflUser(Parcel in) {
        id = in.readString();
        name = in.readString();
        email = in.readString();
        phoneNumber = in.readString();
        profileShare = in.readString();
        videoShare = in.readString();
        photoShare = in.readString();
        contactShare = in.readString();
        treeId = in.readString();
        imageUrl = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(email);
        dest.writeString(phoneNumber);
        dest.writeString(profileShare);
        dest.writeString(videoShare);
        dest.writeString(photoShare);
        dest.writeString(contactShare);
        dest.writeString(treeId);
        dest.writeString(imageUrl);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FriendTflUser> CREATOR = new Creator<FriendTflUser>() {
        @Override
        public FriendTflUser createFromParcel(Parcel in) {
            return new FriendTflUser(in);
        }

        @Override
        public FriendTflUser[] newArray(int size) {
            return new FriendTflUser[size];
        }
    };
}
