package com.treeforlife.dataobjects;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by TheAppsmiths on 4/9/2018.
 */

public class Group implements Parcelable {

    @SerializedName("group_id")
    public int groupId;
    @SerializedName("group_title")
    public String groupTitle;
    @SerializedName("group_image")
    public String groupImage;

    protected Group(Parcel in) {
        groupId = in.readInt();
        groupTitle = in.readString();
        groupImage = in.readString();
    }

    public static final Creator<Group> CREATOR = new Creator<Group>() {
        @Override
        public Group createFromParcel(Parcel in) {
            return new Group(in);
        }

        @Override
        public Group[] newArray(int size) {
            return new Group[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(groupId);
        dest.writeString(groupTitle);
        dest.writeString(groupImage);
    }
}
