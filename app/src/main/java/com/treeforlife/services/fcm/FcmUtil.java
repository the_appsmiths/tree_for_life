package com.treeforlife.services.fcm;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.treeforlife.commons.SharedPrefHelper;

/**
 * Created by TheAppsmiths on 2/16/2018.
 *
 * @author TheAppsmiths
 */

public class FcmUtil {
    private static final String TAG = "FcmUtil";

    //method to set the id and token of fcm
    public static void setFcmIdAndToken(@NonNull Context context) {
        if (context != null) {

            String fcmInstanceId = getFcmInstanceId();
            String fcmInstanceIdToken = getFcmInstanceIdToken();

            SharedPrefHelper mSharedPrefHelper = SharedPrefHelper.getInstance();
            mSharedPrefHelper.setFcmInstanceId(context, fcmInstanceId);
            mSharedPrefHelper.setFcmInstanceIdToken(context, fcmInstanceIdToken);
        } else {
            Log.e(TAG, "setFcmIdAndToken: ERROR : Param context = null. ");
        }
    }

    //get fcm id from the fcm
    public static String getFcmInstanceId() {
        String fcmInstanceId = null;
        FirebaseInstanceId firebaseInstanceId = FirebaseInstanceId.getInstance();
        if (firebaseInstanceId != null) {
            fcmInstanceId = firebaseInstanceId.getId();
        } else {
            Log.e(TAG, "getFcmInstanceId: UNEXPECTED ERROR : fcmInstanceId = null. ");
        }
        Log.w(TAG, ": fcmInstanceId = " + fcmInstanceId);
        return fcmInstanceId;
    }

    //get fcm instance id token from fcm
    public static String getFcmInstanceIdToken() {
        String fcmInstanceIdToken = null;
        FirebaseInstanceId firebaseInstanceId = FirebaseInstanceId.getInstance();
        if (firebaseInstanceId != null) {
            fcmInstanceIdToken = firebaseInstanceId.getToken();
        } else {
            Log.e(TAG, "getFcmInstanceIdToken: UNEXPECTED ERROR : fcmInstanceId = null. ");
        }
        Log.w(TAG, ": fcmInstanceIdToken = " + fcmInstanceIdToken);
        return fcmInstanceIdToken;
    }
}
