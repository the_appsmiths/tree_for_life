package com.treeforlife.Utils;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by ANDROID-1 on 4/20/2017.
 *
 * @author TheAppsmiths
 */
/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class PermissionsManager {
    public static final int REQUEST_PERMISSIONS_CAMERA_WRITE_EXTERNAL_STORAGE = 7111;
    public static final int REQUEST_PERMISSION_READ_EXTERNAL_STORAGE = 7007;
    private static final int REQUEST_ACTIVITY_OPEN_APP_INFO = 7999;
    private static final int REQUEST_ACTIVITY_OPEN_WRITE_SETTINGS = 7998;
    private static final int REQUEST_ACTIVITY_OPEN_LOCATION_SETTINGS = 7997;
    private static final int REQUEST_PERMISSION_ACTION_LOCATION_SOURCE_SETTINGS = 7996;
    private static final int REQUEST_PERMISSIONS_APP = 7000;
    private static final int REQUEST_PERMISSION_WRITE_SETTINGS = 7001;
    private static final int REQUEST_PERMISSION_ACCESS_COARSE_LOCATION = 7002;
    private static final int REQUEST_PERMISSION_ACCESS_FINE_LOCATION = 7003;
    private static final int REQUEST_PERMISSION_READ_CONTACTS = 7004;
    private static final int REQUEST_PERMISSION_RECEIVE_SMS = 7005;
    public static final int REQUEST_PERMISSION_CAMERA = 7006;
    private static final int REQUEST_PERMISSION_CALL_PHONE = 7008;

    private static final String[] requiredPermissionsArray = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    private static PermissionsManager permissionsManager;
    private final DialogUtil mDialogUtil;

    private String TAG = "PermissionsManager";
    private String ILLEGAL_PARAM_MESSAGE = "Illegal vale of parameter : ";
    private String UNSUPPORTED_OPR_MESSAGE = "Unsupported operation for : ";

    private HashMap<String, Integer> permissionCallCountMap;
    private int limit = 3;


    private PermissionsManager() {
        permissionCallCountMap = new HashMap<>();
        permissionCallCountMap.put("ALL_APP_PERMISSIONS", 0);
        for (String permissionName : requiredPermissionsArray) {
            permissionCallCountMap.put(permissionName, 0);
        }
        this.mDialogUtil = DialogUtil.getInstance();
    }

    public static PermissionsManager getInstance() {
        if (permissionsManager == null) {
            permissionsManager = new PermissionsManager();
        }
        return permissionsManager;
    }

    public void resetPermissionCallCount(String permissionName) {
        if (permissionCallCountMap != null) {
            permissionCallCountMap.put(permissionName, 0);
        }
    }

    private boolean isValidPermissionCode(int permissionCode) {
        switch (permissionCode) {
            case REQUEST_PERMISSION_WRITE_SETTINGS:
            case REQUEST_PERMISSION_ACCESS_COARSE_LOCATION:
            case REQUEST_PERMISSION_ACCESS_FINE_LOCATION:
            case REQUEST_PERMISSION_READ_CONTACTS:
            case REQUEST_PERMISSION_RECEIVE_SMS:
            case REQUEST_PERMISSION_CAMERA:
            case REQUEST_PERMISSION_READ_EXTERNAL_STORAGE:
            case REQUEST_PERMISSION_ACTION_LOCATION_SOURCE_SETTINGS:
            case REQUEST_PERMISSION_CALL_PHONE:
                return true;

            default:
                return false;
        }

    }

    public int getRequestCodeOfPermission(String permissionName) throws RuntimeException {
        if (!TextUtils.isEmpty(permissionName)) {
            switch (permissionName) {

                case Manifest.permission.WRITE_EXTERNAL_STORAGE:
                    return REQUEST_PERMISSIONS_CAMERA_WRITE_EXTERNAL_STORAGE;

                case Manifest.permission.WRITE_SETTINGS:
                    return REQUEST_PERMISSION_WRITE_SETTINGS;

                case Manifest.permission.ACCESS_COARSE_LOCATION:
                    return REQUEST_PERMISSION_ACCESS_COARSE_LOCATION;

                case Manifest.permission.ACCESS_FINE_LOCATION:
                    return REQUEST_PERMISSION_ACCESS_FINE_LOCATION;

                case Manifest.permission.RECEIVE_SMS:
                    return REQUEST_PERMISSION_RECEIVE_SMS;

                case Manifest.permission.READ_CONTACTS:
                    return REQUEST_PERMISSION_READ_CONTACTS;

                case Manifest.permission.CAMERA:
                    return REQUEST_PERMISSION_CAMERA;

                case Manifest.permission.READ_EXTERNAL_STORAGE:
                    return REQUEST_PERMISSION_READ_EXTERNAL_STORAGE;

                case Manifest.permission.CALL_PHONE:
                    return REQUEST_PERMISSION_CALL_PHONE;

                case Settings.ACTION_LOCATION_SOURCE_SETTINGS:
                    return REQUEST_PERMISSION_ACTION_LOCATION_SOURCE_SETTINGS;


                default:
                    throw new RuntimeException(UNSUPPORTED_OPR_MESSAGE + " permissionName = " + permissionName);
            }
        }

        throw new RuntimeException(ILLEGAL_PARAM_MESSAGE + " permissionName is Empty : " + permissionName);
    }

    public int getRequestCodeOfPermission(@NonNull String[] permissionNamesArray) throws RuntimeException {
        if (permissionNamesArray != null) {
            if (permissionNamesArray.length > 0) {
                if (permissionNamesArray.length == 1) {
                    return getRequestCodeOfPermission(permissionNamesArray[0]);
                } else {
                    return REQUEST_PERMISSIONS_APP;
                }
            }
        }

        throw new RuntimeException(ILLEGAL_PARAM_MESSAGE + " permissionNamesArray is Empty : " + Arrays.toString(permissionNamesArray));

    }

    private String getPermissionName(int permissionCode) throws RuntimeException {
        switch (permissionCode) {
            case REQUEST_PERMISSION_WRITE_SETTINGS:
                return Manifest.permission.WRITE_SETTINGS;

            case REQUEST_PERMISSION_ACCESS_COARSE_LOCATION:
                return Manifest.permission.ACCESS_COARSE_LOCATION;

            case REQUEST_PERMISSION_ACCESS_FINE_LOCATION:
                return Manifest.permission.ACCESS_FINE_LOCATION;

            case REQUEST_PERMISSION_READ_CONTACTS:
                return Manifest.permission.READ_CONTACTS;

            case REQUEST_PERMISSION_RECEIVE_SMS:
                return Manifest.permission.RECEIVE_SMS;

            case REQUEST_PERMISSION_CAMERA:
                return Manifest.permission.CAMERA;

            case REQUEST_PERMISSION_READ_EXTERNAL_STORAGE:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    return Manifest.permission.READ_EXTERNAL_STORAGE;
                } else {
                    return "android.permission.READ_EXTERNAL_STORAGE";
                }

            case REQUEST_PERMISSION_CALL_PHONE:
                return Manifest.permission.CALL_PHONE;

            case REQUEST_PERMISSION_ACTION_LOCATION_SOURCE_SETTINGS:
                return Settings.ACTION_LOCATION_SOURCE_SETTINGS;
        }

        throw new RuntimeException(ILLEGAL_PARAM_MESSAGE + " permissionCode = " + permissionCode);
    }


    public boolean isPermissionAlreadyGranted(@NonNull Activity activity, String permissionName
    ) throws RuntimeException {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (TextUtils.isEmpty(permissionName)) {
                throw new RuntimeException(ILLEGAL_PARAM_MESSAGE + " PermissionName is Empty : " + permissionName);
            }

            switch (permissionName) {
                case Manifest.permission.WRITE_SETTINGS:
                    return Settings.System.canWrite(activity);

                case Settings.ACTION_LOCATION_SOURCE_SETTINGS:
                    return ((LocationManager) activity.getSystemService(LOCATION_SERVICE))
                            .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                default:
                    return ContextCompat.checkSelfPermission(activity, permissionName)
                            == PackageManager.PERMISSION_GRANTED;

            }

        }
        return true;
    }


    @TargetApi(Build.VERSION_CODES.M)
    public boolean requestWriteSettingsPermission(@NonNull final Activity activity) throws RuntimeException {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (activity == null) {
                throw new RuntimeException(ILLEGAL_PARAM_MESSAGE + " activity is null");
            }
            if (!Settings.System.canWrite(activity)) {
                String message = "Write Settings Permission would be Required";
                mDialogUtil.showOkAlertDialog(activity, message,
                        new Runnable() {
                            @Override
                            public void run() {
                                Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
                                intent.setData(Uri.parse("package:" + activity.getPackageName()));
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                openExternalActivity(activity, intent, REQUEST_ACTIVITY_OPEN_WRITE_SETTINGS);
                            }
                        }, true).show();

                return true;
            }
        }

        return false;
    }

    public boolean requestLocationSettingsPermission(@NonNull final Activity activity) throws RuntimeException {
        return requestLocationSettingsPermission(activity, false);
    }

    public boolean requestLocationSettingsPermission(@NonNull final Activity activity, boolean showDialog) throws RuntimeException {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (activity == null) {
                throw new RuntimeException(ILLEGAL_PARAM_MESSAGE + " activity is null");
            }
            if (showDialog) {
                String message = "Location Settings Permission would be Required";
                mDialogUtil.showOkAlertDialog(activity, message,
                        new Runnable() {
                            @Override
                            public void run() {
                                openSettingsLocationPage(activity);
                            }
                        }, true).show();

            } else {
                openSettingsLocationPage(activity);
            }


            return true;
        }

        return false;
    }

    public void openSettingsLocationPage(Activity activity) {
        if (activity != null) {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            openExternalActivity(activity, intent, REQUEST_ACTIVITY_OPEN_LOCATION_SETTINGS);
        }
    }

    public boolean hasCameraAndExternalStorageWritePermission(@NonNull Activity activity) {
        String[] requiredPermissionsArray = new String[]
                {
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                };

        return requestPermissions(
                REQUEST_PERMISSIONS_CAMERA_WRITE_EXTERNAL_STORAGE,
                activity, requiredPermissionsArray);
    }

    public boolean requestPermission(@NonNull Activity activity,
                                     @NonNull String permissionName) throws RuntimeException {
        return requestPermission(activity, getRequestCodeOfPermission(permissionName), permissionName, true);
    }


    public boolean requestPermission(@NonNull Activity activity,
                                     @NonNull String permissionName,
                                     boolean showMessageDialog) throws RuntimeException {
        return requestPermission(activity, getRequestCodeOfPermission(permissionName),
                permissionName, showMessageDialog);
    }

    private boolean requestPermission(@NonNull Activity activity,
                                      int permissionCode,
                                      @NonNull String permissionName,
                                      boolean showMessageDialog) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (activity == null) {
                throw new RuntimeException(ILLEGAL_PARAM_MESSAGE + " activity is null");
            }

            if (permissionCode <= 0 && TextUtils.isEmpty(permissionName)) {
                throw new RuntimeException(ILLEGAL_PARAM_MESSAGE + " permissionCode = "
                        + permissionCode + ",  permissionName is Empty : " + permissionName);
            }

            if (TextUtils.isEmpty(permissionName)) {
                permissionName = getPermissionName(permissionCode);
            }

            if (permissionCode <= 0) {
                permissionCode = getRequestCodeOfPermission(permissionName);
            }

            if (!isPermissionAlreadyGranted(activity, permissionName)) {
                switch (permissionCode) {
                    case REQUEST_PERMISSION_WRITE_SETTINGS:
                        return requestWriteSettingsPermission(activity);

                    case REQUEST_PERMISSION_ACTION_LOCATION_SOURCE_SETTINGS:
                        return requestLocationSettingsPermission(activity, showMessageDialog);

                    default:
                        if (showMessageDialog) {
                            String message = getMessageForRequestPermissionRationale(activity, permissionName);
                            if (!TextUtils.isEmpty(message)) {
                                final Activity activity1 = activity;
                                final String permissionName1 = permissionName;
                                final int requestPermissionCode1 = permissionCode;
                                Runnable okButtonListener;
                                okButtonListener = new Runnable() {
                                    @Override
                                    public void run() {
                                        requestPermission(activity1, permissionName1, requestPermissionCode1);
                                    }
                                };

                                mDialogUtil.showOkAlertDialog(activity, message, okButtonListener, true);

                            } else {
                                activity.requestPermissions(new String[]{permissionName}, permissionCode);
                            }
                        } else {
                            requestPermission(activity, permissionName, permissionCode);
                        }
                        updateCallCountOfPermission(permissionName);
                        break;
                }
                return true;
            }
        }
        return false;
    }

    private void requestPermission(@NonNull Activity activity, @NonNull String permissionName,
                                   int permissionCode) {
        if (permissionCallCountMap.get(permissionName) <= limit) {
            ActivityCompat.requestPermissions(activity, new String[]{permissionName},
                    permissionCode);
        } else {
            openAppInfoPage(activity);
        }
    }

    public boolean requestAppPermissions(@NonNull Activity activity) {
        return requestPermissions(getRequestCodeOfPermission(requiredPermissionsArray),
                activity, requiredPermissionsArray);
    }

    public boolean requestPermissions(final int requestCode,
                                      @NonNull final Activity activity,
                                      @NonNull String[] permissionsArray) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (permissionsArray == null || permissionsArray.length == 0) {
                throw new RuntimeException(ILLEGAL_PARAM_MESSAGE + " permissionsArray is Empty : " + permissionsArray);
            }

            if (permissionsArray.length == 1) {
                return requestPermission(activity, permissionsArray[0]);
            }

            ArrayList<String> deniedPermissionsList = getListOfDeniedPermissions(permissionsArray, activity);
            if (deniedPermissionsList != null && !deniedPermissionsList.isEmpty()) {

                int noOfDeniedPermissions = deniedPermissionsList.size();
                if (noOfDeniedPermissions == 1) {
                    return requestPermission(activity, deniedPermissionsList.get(0));

                } else {
                    String[] requestPermissionsArray = new String[noOfDeniedPermissions];
                    deniedPermissionsList.toArray(requestPermissionsArray);
                    Log.d(TAG, "requestPermissions:  = " + Arrays.toString(requestPermissionsArray));
                    String message = getMessageForRequestPermissionRationale(deniedPermissionsList, activity);
                    if (!TextUtils.isEmpty(message)) {
                        // will be called for first time or when user has clicked Never Ask Again
                        final String[] reqPermissionsArray = requestPermissionsArray;
                        int callCountOfAppPermissions = permissionCallCountMap.get("ALL_APP_PERMISSIONS");
                        if (callCountOfAppPermissions <= limit) {
                            mDialogUtil.showAlertDialog(activity, "", message, "OK", "Cancel",
                                    "Manage",
                                    new Runnable() {
                                        @RequiresApi(api = Build.VERSION_CODES.M)
                                        @Override
                                        public void run() {
                                            ActivityCompat.requestPermissions(activity,
                                                    reqPermissionsArray,
                                                    requestCode);
                                        }
                                    },

                                    null,

                                    new Runnable() {
                                        @Override
                                        public void run() {
                                            openAppInfoPage(activity);
                                        }
                                    }, true
                            );

                        } else {
                            mDialogUtil.showOkAlertDialog(activity,
                                    message,
                                    new Runnable() {
                                        @Override
                                        public void run() {
                                            openAppInfoPage(activity);
                                        }
                                    }, true
                            );
                        }
                    } else {
                        ActivityCompat.requestPermissions(activity, requestPermissionsArray, requestCode);

                    }
                }
                return true;
            }
        }

        return false;
    }

    public void openAppInfoPage(@NonNull Activity activity) {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
        intent.setData(uri);
        openExternalActivity(activity, intent, REQUEST_ACTIVITY_OPEN_APP_INFO);
    }

    private void openExternalActivity(@NonNull Activity activity,
                                      @NonNull Intent intent,
                                      int reqCode) {
        if (activity != null
                && intent.resolveActivity(activity.getPackageManager()) != null) {
            activity.startActivityForResult(intent, reqCode);
        } else {
            Log.d(TAG, "openExternalActivity: Failed");
        }
    }

    private void updateCallCountOfPermission(@NonNull String permissionName) {
        Integer previousCallCount = permissionCallCountMap.get(permissionName);
        int previousCount = 0;
        if (previousCallCount != null) {
            previousCount = previousCallCount;
        }
        permissionCallCountMap.put(permissionName, previousCount + 1);

        int i = 0;
        int[] allPermissionsCount = new int[requiredPermissionsArray.length];
        for (String permissionKey : requiredPermissionsArray) {
            allPermissionsCount[i] = permissionCallCountMap.get(permissionKey);
            i++;
        }

        int min = allPermissionsCount[0];
        for (int j = 0; j < allPermissionsCount.length; j++) {
            if (min > allPermissionsCount[j]) {
                min = allPermissionsCount[j];
            }
        }

        if (permissionCallCountMap.get("ALL_APP_PERMISSIONS") < min) {
            permissionCallCountMap.put("ALL_APP_PERMISSIONS", min);
        }

    }


    private ArrayList<String> getListOfDeniedPermissions(@NonNull String[] permissionsArray,
                                                         @NonNull Activity activity) {
        if (permissionsArray == null || permissionsArray.length == 0) {
            throw new RuntimeException(ILLEGAL_PARAM_MESSAGE + " permissionsArray is Empty : " + permissionsArray);
        }

        if (activity == null) {
            throw new RuntimeException(ILLEGAL_PARAM_MESSAGE + " activity is null");
        }

        ArrayList<String> deniedPermissionsArrayList = new ArrayList<String>();
        int noOfDeniedPermissions = 0;
        for (String permissionName : permissionsArray) {
            if (!isPermissionAlreadyGranted(activity, permissionName)) {
                deniedPermissionsArrayList.add(permissionName);
                Log.d(TAG, "NOT GRANTED/ TO BE REQUESTED : PERMISSIONS#" + noOfDeniedPermissions
                        + " : " + permissionName);
                noOfDeniedPermissions++;
            }
        }
        return deniedPermissionsArrayList;
    }

    private boolean shouldShowCustomMessage(@NonNull Activity activity,
                                            @NonNull String permissionName) throws RuntimeException {
        if (activity == null) {
            throw new RuntimeException(ILLEGAL_PARAM_MESSAGE + " activity is null");
        }
        if (TextUtils.isEmpty(permissionName)) {
            throw new RuntimeException(ILLEGAL_PARAM_MESSAGE + " permissionName is EMPTY : " + permissionName);
        }
        boolean result;
        result = ActivityCompat.shouldShowRequestPermissionRationale(activity, permissionName);
        Integer previousCallCount = permissionCallCountMap.get(permissionName);
        if (previousCallCount != null) {
            result = result && previousCallCount > limit;
        }
        return result;
    }

    private String getMessageForRequestPermissionRationale(@NonNull Activity activity,
                                                           @NonNull String permissionName) throws RuntimeException {
        String message = null;
        if (activity == null) {
            throw new RuntimeException(ILLEGAL_PARAM_MESSAGE + " activity is null");
        }

        if (TextUtils.isEmpty(permissionName)) {
            throw new RuntimeException(ILLEGAL_PARAM_MESSAGE + " permissionName is EMPTY : " + permissionName);
        }
        if (shouldShowCustomMessage(activity, permissionName)) {
               /*
                1 - False
                2 - On DENY - TRUE
                3 - NEVER ASK AGAIN ->DENY -> FALSE -> No UI
                */
            message = "you need to grant access to \n" + permissionName + " permission.";
        }
        if (!TextUtils.isEmpty(message)) {
            message = message.replaceAll("android.permission.", "").replaceAll("_", " ");
        }
        return message;
    }

    private String getMessageForRequestPermissionRationale(@NonNull ArrayList<String> permissionsList,
                                                           @NonNull Activity activity) throws RuntimeException {
        if (permissionsList == null || permissionsList.isEmpty()) {
            throw new RuntimeException(ILLEGAL_PARAM_MESSAGE + " permissionsList is Empty : " + permissionsList);
        }

        if (activity == null) {
            throw new RuntimeException(ILLEGAL_PARAM_MESSAGE + " activity is null");
        }

        int counter = 0;
        String message = "";
        for (String permissionName : permissionsList) {
            if (shouldShowCustomMessage(activity, permissionName)) {
               /*
                1 - False
                2 - On DENY - TRUE
                3 - NEVER ASK AGAIN ->DENY -> FALSE -> No UI
                */
                if (counter == 0) {
                    message = "you need to grant access to \n" + permissionName;
                } else {
                    message += ", \n" + permissionName;
                }
                counter++;
            }
            updateCallCountOfPermission(permissionName);
        }

        if (counter == 1) {
            message += " permission.";
        } else if (counter > 1) {
            message += " permissions.";
        }
        message = message.replaceAll("android.permission.", "").replaceAll("_", " ");
        updateCallCountOfPermission("ALL_APP_PERMISSIONS");
        return message;
    }

    public void parsePermissionsResult(int requestCode, String[] permissions, int[] grantResults,
                                       Activity mActivity, PermissionsResultCallback callback) {

        if (grantResults.length > 0 && permissions.length > 0 &&
                grantResults.length == permissions.length) {
            ArrayList<String> grantedPermissionsList = new ArrayList<>();
            ArrayList<String> deniedPermissionsList = new ArrayList<>();
            ArrayList<String> deniedAndNeverAskPermissionsList = new ArrayList<>();
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    grantedPermissionsList.add(permissions[i]);
                } else {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(mActivity, permissions[i])) {
                        deniedPermissionsList.add(permissions[i]);
                        resetPermissionCallCount(permissions[i]);
                    } else {
                        deniedAndNeverAskPermissionsList.add(permissions[i]);
                    }
                }
            }
            if (callback != null) {
                if (!deniedPermissionsList.isEmpty()) {
                    int noOfDeniedPermission = deniedPermissionsList.size();
                    String[] deniedPermissionsArray = new String[noOfDeniedPermission];
                    deniedPermissionsList.toArray(deniedPermissionsArray);
                    callback.deniedPermissions(requestCode, deniedPermissionsArray);

                }
                if (!deniedAndNeverAskPermissionsList.isEmpty()) {
                    callback.deniedAndNeverAskPermissions(requestCode,
                            deniedAndNeverAskPermissionsList.toArray(
                                    new String[deniedAndNeverAskPermissionsList.size()]
                            )
                    );
                }
                if (deniedPermissionsList.isEmpty() && !grantedPermissionsList.isEmpty()) {
                    callback.grantedPermissions(requestCode,
                            grantedPermissionsList.toArray(
                                    new String[grantedPermissionsList.size()]
                            )
                    );
                }

            }
        } else {
            Log.e(TAG, "onGetCustomizeApiResult: " + ILLEGAL_PARAM_MESSAGE);
        }

    }

    public StringBuilder getMessageForNeverAskPermissions(@NonNull String[] deniedAndNeverAskPermissions) {
        StringBuilder messageBuilder = new StringBuilder();
        if (deniedAndNeverAskPermissions != null) {
            messageBuilder.append("Necessary permission(s) not granted to this app. " +
                    "Please Go To AppInfo ->  App permissions :" +
                    "\nGrant the following permission(s) :\n");

            for (String grantedPermission : deniedAndNeverAskPermissions) {
                if (!TextUtils.isEmpty(grantedPermission)) {

                    switch (grantedPermission) {
                        case Manifest.permission.ACCESS_FINE_LOCATION:
                            messageBuilder.append("\n\tLocation ");
                            break;

                        case Manifest.permission.CALL_PHONE:
                            messageBuilder.append("\n\tTelephone ");
                            break;

                        case Manifest.permission.CAMERA:
                            messageBuilder.append("\n\tCamera ");
                            break;

                        case Manifest.permission.READ_EXTERNAL_STORAGE:
                        case Manifest.permission.WRITE_EXTERNAL_STORAGE:
                            messageBuilder.append("\n\tStorage");
                            break;
                    }
                }
            }
        }
        return messageBuilder;
    }

    public interface PermissionsResultCallback {
        void deniedPermissions(int requestCode, @NonNull String[] deniedPermissions);

        void grantedPermissions(int requestCode, @NonNull String[] grantedPermissions);

        void deniedAndNeverAskPermissions(int requestCode, @NonNull String[] deniedAndNeverAskPermissions);
    }
}
