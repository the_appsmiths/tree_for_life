package com.treeforlife.retrofit.responses;

import com.google.gson.annotations.SerializedName;
import com.treeforlife.dataobjects.Group;

/**
 * Created by TheAppsmiths on 4/9/2018.
 */

public class CreatedGroupResponse extends WebServiceResponse{
    @SerializedName("groups")
    public Group group;
}
