package com.treeforlife.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.treeforlife.R;
import com.treeforlife.Utils.AndroidUtil;
import com.treeforlife.Utils.DialogUtil;
import com.treeforlife.Utils.FragmentUtil;
import com.treeforlife.Utils.ProgressDialogUtil;
import com.treeforlife.adapters.NotificationsRvAdapter;
import com.treeforlife.commons.Constants;
import com.treeforlife.commons.FragmentCallBack;
import com.treeforlife.dataobjects.Notification;
import com.treeforlife.dataobjects.TFLUser;
import com.treeforlife.retrofit.ResponseId;
import com.treeforlife.retrofit.RetrofitManager;
import com.treeforlife.retrofit.RetrofitResponseValidator;
import com.treeforlife.retrofit.responses.NotificationListResponse;

import java.net.SocketTimeoutException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TheAppsmiths on 3/19/2018.
 *
 */
/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class NotificationFragment extends Fragment implements
        RetrofitResponseValidator.ValidationListener,
        NotificationsRvAdapter.NotificationItemCallBack {

    public static final String TAG = "NotificationFragment";
    private Context mContext;
    private Activity mActivity;
    private Bundle mBundle;
    private FragmentCallBack mCallBack;

    private RecyclerView mRecyclerView;
    private NotificationsRvAdapter mNotificationsRvAdapter;
    private TextView mNoDataText;

    private ProgressDialogUtil mProgressDialogUtil;
    private DialogUtil mDialogUtil;

    private TFLUser mTflUser;

    private boolean isLoadingData;
    private int mPage;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
        this.mActivity = getActivity();
        this.mNotificationsRvAdapter = new NotificationsRvAdapter(this);
        this.mProgressDialogUtil = ProgressDialogUtil.getInstance();
        this.mDialogUtil = DialogUtil.getInstance();
        if (mActivity instanceof FragmentCallBack) {
            this.mCallBack = (FragmentCallBack) mActivity;
        }
        extractDataFromBundle();
    }

    private void extractDataFromBundle() {

        if (mBundle == null) {
            mBundle = getArguments();
        }

        if (mBundle != null) {
            mTflUser = mBundle.getParcelable(Constants.KEY_TFL_USER);
        }

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_notification, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        intiComponents(view);
        callNotificationList();
    }


    @Override
    public void onResume() {
        super.onResume();
        if (mActivity != null) {
            mActivity.setTitle(R.string.notifications);
        }
        if (mCallBack != null) {
            Bundle bundle = new Bundle();
            bundle.putString(Constants.KEY_FRAGMENT_CALLBACK, TAG);
            mCallBack.updateBottomBar(bundle);
        }
    }

    private void intiComponents(View view) {
        mNoDataText = view.findViewById(R.id.no_data_text_view);

        mRecyclerView = view.findViewById(R.id.rv_notification);
        LinearLayoutManager linearLayout = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(linearLayout);
        setInfiniteScroll(linearLayout);
        mRecyclerView.setAdapter(mNotificationsRvAdapter);

        manageNoDataTextVisibility();

    }

    private void setInfiniteScroll(final LinearLayoutManager linearLayout) {
        if (mRecyclerView != null) {
            mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    int lastVisibleItemPosition = linearLayout.findLastVisibleItemPosition();
                    int totalItemCount = linearLayout.getItemCount();

                    if ((lastVisibleItemPosition + Constants.ITEMS_LIMIT) > totalItemCount) {
                        Log.w(TAG, " RECYCLER ON SCROLL : loadData() called");
                        callNotificationList();
                    }
                }
            });
        }
    }

    private void manageNoDataTextVisibility() {
        if (mNoDataText != null && mNotificationsRvAdapter != null) {
            mNoDataText.setVisibility(mNotificationsRvAdapter.getItemCount() > 0 ? View.GONE : View.VISIBLE);
        }
    }

    private void callNotificationList() {

        if (mTflUser != null && AndroidUtil.hasInternetConnectivity(mContext) && !isLoadingData) {
            isLoadingData = true;
            if (mPage == 0) {
                mProgressDialogUtil.showProgressDialog(mActivity);
            }

            Call<NotificationListResponse> getAlbum = RetrofitManager.getRetrofitWebService()
                    .notificationList(mTflUser.userId, mPage);
            getAlbum.enqueue(new Callback<NotificationListResponse>() {
                @Override
                public void onResponse(Call<NotificationListResponse> call, Response<NotificationListResponse> response) {
                    new RetrofitResponseValidator(ResponseId.NOTIFICATION_LIST, response, NotificationFragment.this);

                }

                @Override
                public void onFailure(Call<NotificationListResponse> call, Throwable t) {
                    String errorMessage = "";
                    if (t != null) {
                        Log.e(TAG, "onFailure: Error : " + t.getMessage());
                        if (t instanceof SocketTimeoutException) {
                            errorMessage = "Please make sure that your device has an active internet connection.";
                        }
                    }

                    mProgressDialogUtil.dismissProgressDialog();
                    isLoadingData = false;
                    if (!isVisible() || mPage != 0) {
                        return;
                    }


                    if (TextUtils.isEmpty(errorMessage)) {
                        AndroidUtil.showErrorToast(mContext);
                    } else {
                        mDialogUtil.showErrorDialog(mContext, errorMessage);
                    }
                }
            });

        }
    }

    @Override
    public void onValidResponse(@NonNull ResponseId responseId, @NonNull Object responseBody, @NonNull Response response) {
        switch (responseId) {
            case NOTIFICATION_LIST:
                afterGetNotification((NotificationListResponse) responseBody);
                isLoadingData = false;
                break;
        }

        mProgressDialogUtil.dismissProgressDialog();
    }

    @Override
    public void onInvalidResponse(@NonNull ResponseId responseId, @NonNull String errorMessage, Response response) {
        mProgressDialogUtil.dismissProgressDialog();
        switch (responseId) {
            case NOTIFICATION_LIST:
                isLoadingData = false;
                if (!isVisible() || mPage != 0) {
                    return;
                }
                manageNoDataTextVisibility();
                break;
        }
        if (response == null || !response.isSuccessful()) {
            mDialogUtil.showErrorDialog(mContext, errorMessage);
        } else {
            AndroidUtil.showToast(mContext, errorMessage);
        }
    }

    private void afterGetNotification(NotificationListResponse responseBody) {

        ArrayList<Notification> list = responseBody.notificationList;

        if (list != null && !list.isEmpty()) {
            if (mNotificationsRvAdapter != null) {
                mNotificationsRvAdapter.addNotificationList(list);
            }
        }

        manageNoDataTextVisibility();
        mPage++;
    }

    @Override
    public void onItemClick(int position, Notification notification) {

        if (notification != null) {

            if (!TextUtils.isEmpty(notification.notificationType)) {

                switch (notification.notificationType) {

                    case Constants.NOTIFICATION_FRIEND_REQ_RECEIVED:
                    case Constants.NOTIFICATION_FRIEND_REQ_ACCEPTED:
                    case Constants.NOTIFICATION_NEW_IMAGE:
                    case Constants.NOTIFICATION_NEW_VIDEO:
                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.KEY_CALLER_COMPONENT, TAG);
                        bundle.putParcelable(Constants.KEY_TFL_USER, mTflUser);
                        bundle.putParcelable(Constants.KEY_NOTIFICATION, notification);
                        FragmentUtil.loadFragment((AppCompatActivity) mActivity,
                                FragmentUtil.FRAGMENT_USER_PROFILE, bundle, true);

                        break;

                    case Constants.NOTIFICATION_RECEIVED_OM_POINTS:
                        bundle = new Bundle();
                        bundle.putParcelable(Constants.KEY_TFL_USER, mTflUser);
                        FragmentUtil.loadFragment((AppCompatActivity) mContext,
                                FragmentUtil.FRAGMENT_CONVERTER, bundle, true);
                        break;

                    case Constants.NOTIFICATION_TREE_STATE_CHANGED:
                        bundle = new Bundle();
                        bundle.putParcelable(Constants.KEY_TFL_USER, mTflUser);
                        FragmentUtil.loadFragment((AppCompatActivity) mContext,
                                FragmentUtil.FRAGMENT_TREE, bundle, true);
                        break;

                    default:
                        Log.e(TAG, "unhandled: NOTIFICATION TYPE = " + notification.notificationType);
                        break;


                }
            }
        }


    }
}
