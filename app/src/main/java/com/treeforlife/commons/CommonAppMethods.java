package com.treeforlife.commons;

import android.text.TextUtils;

import org.apache.commons.lang.StringEscapeUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by TheAppSmiths.
 * (TheAppSmiths is the Mobile App Development division of Adreno Technologies India Pvt. Ltd.)
 * *
 * Created on 26 June 2018 - 2:07 PM.
 *
 * @author Android Developer [AD143].
 **/


public class CommonAppMethods {
    public static String removeStartingSpaces(String string) {
        if (TextUtils.isEmpty(string)) {
            return string;
        }
        String patternString = "^ *";
        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(string);
        if (matcher.find()) {
            string = matcher.replaceFirst("");
        }
        return string;
    }

    public static String encodeAllCharsToUnicode(String text) {
        StringBuilder unicodeString = new StringBuilder();
        if (!TextUtils.isEmpty(text)) {
            for (int i = 0; i < text.length(); i++) {
                unicodeString.append(encodeCharToUnicode(text.charAt(i)));
            }
        }
        return unicodeString.toString();
    }

    public static String encodeCharToUnicode(char ch) {
        if (ch < 0x10) {
            return "\\u000" + Integer.toHexString(ch);
        } else if (ch < 0x100) {
            return "\\u00" + Integer.toHexString(ch);
        } else if (ch < 0x1000) {
            return "\\u0" + Integer.toHexString(ch);
        }
        return "\\u" + Integer.toHexString(ch);
    }

    public static String decodeText(String text) {
        return StringEscapeUtils.unescapeJava(text);
    }
}
