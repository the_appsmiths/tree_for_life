package com.treeforlife.dataobjects;

import com.google.gson.annotations.SerializedName;

/**
 * Created by TheAppsmiths on 4/17/2018.
 *
 * @author TheAppsmiths
 */

public class TreeInfo {
    @SerializedName("tree_id")
    public String treeId;
    @SerializedName("tree_country")
    public String treeCountryName;
    @SerializedName("tree_type")
    public String treeType;
    @SerializedName("is_empty_bucket")
    public String isEmptyBucket;
    @SerializedName("apple_fruits")
    public int noOfApples;
    @SerializedName("mango_fruits")
    public int noOfMangoes;
    @SerializedName("pear_fruits")
    public int noOfPears;
    @SerializedName("orange_fruits")
    public int noOfOranges;

    @SerializedName("fruit_series")
    public String[] fruit_series;
}
