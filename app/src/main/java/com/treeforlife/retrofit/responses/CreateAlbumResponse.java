package com.treeforlife.retrofit.responses;

import com.google.gson.annotations.SerializedName;
import com.treeforlife.dataobjects.Album;

/**
 * Created by TheAppsmiths on 4/5/2018.
 */

public class CreateAlbumResponse extends WebServiceResponse{
    @SerializedName("album")
    public Album mCreateAlbum;

}
