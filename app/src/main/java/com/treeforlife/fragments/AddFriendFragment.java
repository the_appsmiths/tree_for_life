package com.treeforlife.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.treeforlife.R;
import com.treeforlife.Utils.AndroidUtil;
import com.treeforlife.Utils.FragmentUtil;
import com.treeforlife.Utils.ImageLoaderUtil;
import com.treeforlife.Utils.ValidationUtil;
import com.treeforlife.activities.ScanQrCodeActivity;
import com.treeforlife.commons.Constants;
import com.treeforlife.commons.FragmentCallBack;
import com.treeforlife.commons.SharedPrefHelper;
import com.treeforlife.controller.SearchUserApiController;
import com.treeforlife.dataobjects.SearchedTflUser;
import com.treeforlife.dataobjects.TFLUser;

/**
 * Created by TheAppsmiths on 3/20/2018.
 * updated by TheAppsmiths on 12th April 2018
 */

public class AddFriendFragment extends Fragment implements View.OnClickListener,
        SearchUserApiController.ResultCallback {

    public static final String TAG = "AddFriendFragment";

    private Context mContext;
    private Activity mActivity;
    private Bundle mBundle;
    private FragmentCallBack mCallBack;

    private EditText mFriendEmailEditText, mFriendTreeIdEditText;

    private SearchUserApiController mSearchUserApiController;
    private SharedPrefHelper mSharedPrefHelper;

    private TFLUser mTflUser;
    private String mPhoneNumber;
    private String mEmail;

    private final int RC_START_ACTIVITY_SCAN_QR_CODE = 1111;

    private ImageView mHeaderImageView;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
        this.mActivity = getActivity();
        this.mBundle = getArguments();

        if (mActivity != null && mActivity instanceof FragmentCallBack) {
            this.mCallBack = (FragmentCallBack) mActivity;
        }

        this.mSharedPrefHelper = SharedPrefHelper.getInstance();
        extractDataFromBundle();
        ensureTflUser();

        this.mSearchUserApiController = new SearchUserApiController(TAG, this, mContext,
                mActivity, mTflUser, this);

    }

    /**
     * get extra data from bundle
     */
    private void extractDataFromBundle() {
        if (mBundle == null) {
            mBundle = getArguments();
        }

        if (mBundle != null) {
            mTflUser = mBundle.getParcelable(Constants.KEY_TFL_USER);

        }
    }

    /**
     * ensure the user availability
     */
    private void ensureTflUser() {
        if (mTflUser == null) {
            mTflUser = mSharedPrefHelper.getLoginResponse(mContext);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_addfriend, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initComponents(view);
    }

    /**
     * init components
     *
     * @param view
     */
    private void initComponents(View view) {

        mFriendEmailEditText = view.findViewById(R.id.friend_email_edit_text);
        mFriendTreeIdEditText = view.findViewById(R.id.tree_id_edit_text);

        View mSearchView = view.findViewById(R.id.search_text_view);
        mSearchView.setOnClickListener(this);

        Button mScanButton = view.findViewById(R.id.scan_button);
        mScanButton.setOnClickListener(this);

        mHeaderImageView = view.findViewById(R.id.header_tree_image_view);
        setImageHeader();
    }

    /**
     * set image header
     */
    private void setImageHeader() {

        if (mTflUser != null) {
            ImageLoaderUtil.getLoader(mContext).displayImage(mTflUser.profileImage, mHeaderImageView,
                    ImageLoaderUtil.getDefaultHeaderImageOption());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mActivity != null) {
            mActivity.setTitle(R.string.add_freind);
        }
        if (mCallBack != null) {
            Bundle bundle = new Bundle();
            bundle.putString(Constants.KEY_FRAGMENT_CALLBACK, TAG);
            mCallBack.updateBottomBar(bundle);
        }
    }

    @Override
    public void onClick(View view) {
        if (view != null) {

            switch (view.getId()) {

                case R.id.search_text_view:
                    String email = mFriendEmailEditText.getText().toString();
                    mEmail = email;
                    if (!TextUtils.isEmpty(email) && isValidInput()) {
                        mSearchUserApiController.searchUser(mEmail,
                                mFriendTreeIdEditText.getText().toString(), mPhoneNumber, true);
                    } else if (!TextUtils.isEmpty(mFriendTreeIdEditText.getText().toString())) {
                        mPhoneNumber = "";
                        mSearchUserApiController.searchUser(mEmail,
                                mFriendTreeIdEditText.getText().toString(), mPhoneNumber, true);
                    } else {
                        AndroidUtil.showToast(mActivity, "Please Enter Atleast on Field");
                    }

                    break;


                case R.id.scan_button:
                    Intent intent = new Intent(mContext, ScanQrCodeActivity.class);
                    mActivity.startActivityForResult(intent, RC_START_ACTIVITY_SCAN_QR_CODE);
                    break;
            }
        }

    }

    private boolean isValidInput() {
        mEmail = mFriendEmailEditText.getText().toString();

        boolean number = mEmail.matches(".*[0-9]+.*") || mEmail.matches("[+]");
        if (!TextUtils.isEmpty(mEmail)) {

            if (!mEmail.contains("@") && number) {
                mPhoneNumber = mEmail;
                mEmail = null;
                if (!ValidationUtil.isValidPhoneNumber(mPhoneNumber, true, false)) {
                    mFriendEmailEditText.setError(getString(R.string.error_valid_phone));
                    mFriendEmailEditText.requestFocus();
                    return false;
                }
            } else {
                mPhoneNumber = null;
                if (!ValidationUtil.isValidEmail(mEmail)) {
                    mFriendEmailEditText.setError(getString(R.string.error_email));
                    mFriendEmailEditText.requestFocus();
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (resultCode) {

            case Activity.RESULT_OK:

                switch (requestCode) {

                    case RC_START_ACTIVITY_SCAN_QR_CODE:
                        String searchBy = "";
                        if (data != null) {
                            searchBy = data.getStringExtra(Constants.KEY_TREE_ID);
                            if (TextUtils.isEmpty(searchBy)) {
                                searchBy = data.getStringExtra(Constants.KEY_EMAIL_ID);
                            }
                        }


                        if (!TextUtils.isEmpty(searchBy)) {
                            mSearchUserApiController.searchUser(searchBy, false);
                        } else {
                            AndroidUtil.showToast(mContext, "Neither email_id nor tree_id was found in the scanned QR Code.");
                        }
                        break;
                }
        }
    }

    @Override
    public void onSearchUserApiResult(SearchedTflUser searchedTflUser) {
        if (searchedTflUser != null) {
            Bundle bundle = new Bundle();
            bundle.putString(Constants.KEY_CALLER_COMPONENT, TAG);
            bundle.putParcelable(Constants.KEY_TFL_USER, mTflUser);
            bundle.putParcelable(Constants.KEY_SEARCHED_TFL_USER, searchedTflUser);
            FragmentUtil.loadFragment((AppCompatActivity) mActivity, FragmentUtil.FRAGMENT_USER_PROFILE,
                    bundle, true);
        }
    }
}
