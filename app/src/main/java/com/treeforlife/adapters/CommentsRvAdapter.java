package com.treeforlife.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.treeforlife.R;
import com.treeforlife.Utils.DateAndTimeUtil;
import com.treeforlife.Utils.DialogUtil;
import com.treeforlife.Utils.ImageLoaderUtil;
import com.treeforlife.Utils.ImageRotationListener;
import com.treeforlife.commons.CommonAppMethods;
import com.treeforlife.commons.Constants;
import com.treeforlife.commons.SharedPrefHelper;
import com.treeforlife.customview.CustomTextView;
import com.treeforlife.dataobjects.Comment;
import com.treeforlife.dataobjects.TFLUser;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by TheAppsmiths on 3/26/2018.
 *
 * @author TheAppsmiths
 */

public class CommentsRvAdapter extends RecyclerView.Adapter<CommentsRvAdapter.CommentHolder> {
    private static final String TAG = "CommentsRvAdapter";

    private final CommentItemCallback mItemCallback;
    private ArrayList<Comment> mCommentsList;


    public CommentsRvAdapter(CommentItemCallback mItemCallback) {
        this.mItemCallback = mItemCallback;
    }

    @Override
    public CommentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CommentHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_comment, parent, false));
    }

    @Override
    public void onBindViewHolder(CommentHolder holder, int position) {
        holder.bindData(position);
    }

    @Override
    public int getItemCount() {
        return mCommentsList != null ? mCommentsList.size() : 0;
    }

    private void ensureCommentsList() {
        if (mCommentsList == null) {
            mCommentsList = new ArrayList<Comment>();
        }
    }

    //method to refresh the update
    public void update(int position) {
        if (getItemCount() > position) {
            notifyItemChanged(position);
        }
    }

    //method to update the data to a position
    public void update(int position, Comment comment) {
        if (mCommentsList != null && mCommentsList.size() > position) {

            mCommentsList.set(position, comment);
            notifyItemChanged(position);
        }
    }

    //method to update the list
    public void update(ArrayList<Comment> commentsList) {
        ensureCommentsList();
        this.mCommentsList.clear();
        if (commentsList != null && !commentsList.isEmpty()) {
            this.mCommentsList.addAll(commentsList);
        }
        notifyDataSetChanged();
    }

    //method to add the comment in list
    public void addComment(Comment comment) {
        if (comment != null) {
            ensureCommentsList();
            this.mCommentsList.add(0, comment);
            notifyItemInserted(0);
        }
    }

    //method to add the comment list
    public void addCommentList(ArrayList<Comment> commentsList) {
        if (commentsList != null && !commentsList.isEmpty()) {
            if (mCommentsList == null || mCommentsList.isEmpty()) {
                update(commentsList);
            } else {
                int startPosition = mCommentsList.size();
                this.mCommentsList.addAll(commentsList);
                notifyItemRangeInserted(startPosition, mCommentsList.size());
                notifyItemRangeChanged(startPosition - 1, startPosition);
            }
        }

    }

    //Holder view to show the data in adapter
    public class CommentHolder extends RecyclerView.ViewHolder {
        private final View mItemView, mImageContainer;
        private final CircleImageView mCommenterImageView;
        private final CustomTextView mCommenterNameTextView, mCommentDateTextView, mCommentTextView;
        private final ImageView mCommentImageView, mPlayImageView;
        private final RatingBar mCommentRatingBar;
        private final View mRatingBarLayout;

        private final Context mContext;
        private final ImageLoader mImageLoader;
        private final DialogUtil mDialogUtil;

        private Comment mComment;
        private float mRating;
        private TFLUser mTflUser;


        public CommentHolder(View itemView) {
            super(itemView);

            mItemView = itemView;
            mCommenterImageView = (CircleImageView) itemView.findViewById(R.id.commenter_image_view);

            mCommenterNameTextView = (CustomTextView) itemView.findViewById(R.id.commenter_name_text_view);
            mCommentDateTextView = (CustomTextView) itemView.findViewById(R.id.comment_date_text_view);
            mCommentTextView = (CustomTextView) itemView.findViewById(R.id.comment_text_view);

            mImageContainer = itemView.findViewById(R.id.image_container);

            mCommentImageView = (ImageView) itemView.findViewById(R.id.comment_image_view);
            mPlayImageView = (ImageView) itemView.findViewById(R.id.play_image_view);

            mCommentRatingBar = (RatingBar) itemView.findViewById(R.id.comment_rating_bar);
            mRatingBarLayout = itemView.findViewById(R.id.rating_bar_layout);

            mContext = itemView.getContext();
            mImageLoader = ImageLoaderUtil.getLoader(mContext);

            if (SharedPrefHelper.getInstance() != null) {
                this.mTflUser = SharedPrefHelper.getInstance().getLoginResponse(mContext);
            }

            this.mDialogUtil = DialogUtil.getInstance();

        }


        public void bindData(final int position) {
            if (mCommentsList != null && mCommentsList.size() > position) {
                mComment = mCommentsList.get(position);

                if (mComment != null) {

                    mImageLoader.displayImage(mComment.commentatorImageUrl, mCommenterImageView,
                            ImageLoaderUtil.getDefaultProfileImageOption(), new ImageRotationListener());

                    mCommenterNameTextView.setText(mComment.commentatorName);
                    mCommentDateTextView.setText(DateAndTimeUtil.parseDate(
                            DateAndTimeUtil.DateOption.yyyy_DS_MM_dd_HH_COL_mm_ss_TO_dd_MMM_YYYY,
                            mComment.commentDate));

                    mCommentTextView.setText(CommonAppMethods.decodeText(mComment.commentText));

                    if (TextUtils.isEmpty(mComment.commentVideoUrl)) {
                        mPlayImageView.setVisibility(View.GONE);
                    } else {
                        mPlayImageView.setVisibility(View.VISIBLE);
                    }

                    String url = mComment.commentImageUrl;

                    if (TextUtils.isEmpty(url)) {
                        url = mComment.commentVideoThumbnailUrl;
                    }
                    if (!TextUtils.isEmpty(url)) {
                        mImageLoader.displayImage(url, mCommentImageView,
                                ImageLoaderUtil.getDefaultImageOption());

                    }
                    if (!TextUtils.isEmpty(mComment.commentType) &&
                            (mComment.commentType.equals(Constants.COMMENT_TYPE_IMAGE)
                                    || mComment.commentType.equals(Constants.COMMENT_TYPE_VIDEO))
                            ) {
                        mImageContainer.setVisibility(View.VISIBLE);
                    } else {
                        mImageContainer.setVisibility(View.GONE);
                    }

                    mCommentRatingBar.setRating(mComment.commentOmPoints);
                    mCommentRatingBar.setIsIndicator(mComment.commentOmPoints > 0
                            || (mTflUser != null &&
                            !TextUtils.isEmpty(mTflUser.userId) &&
                            mTflUser.userId.equals(mComment.commentatorUserId)));

                    mRatingBarLayout.setVisibility(mTflUser != null &&
                            !TextUtils.isEmpty(mTflUser.userId) &&
                            mTflUser.userId.equals(mComment.commentatorUserId) ? View.GONE
                            : View.VISIBLE);

                    setListeners(position);
                }
            }

        }

        private void setListeners(final int position) {
            if (mComment != null) {

                if (mComment.commentOmPoints <= 0) {
                    mCommentRatingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                        @Override
                        public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                            if (mComment.commentOmPoints <= 0) {
                                mRating = rating;
                                Resources resources = mContext.getResources();
                                if (resources != null) {
                                    String assignOmPointText = resources.getQuantityString(
                                            R.plurals.assignNumberOfOmPoints,
                                            (int) rating,
                                            (int) rating);

                                    if (rating >= 1) {
                                        String dialogText = resources.getQuantityString(
                                                R.plurals.are_u_sure_to_assign_om_point,
                                                (int) rating,
                                                (int) rating);
                                        mDialogUtil.showTwoButtonsAlertDialog(mContext,
                                                dialogText,
                                                "Sure",
                                                "Cancel",
                                                new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        if (mItemCallback != null) {
                                                            mItemCallback.onAssignOmPointClick(position, mComment, (int) mRating);
                                                            mCommentRatingBar.setIsIndicator(true);
                                                        }
                                                    }
                                                },
                                                new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        mCommentRatingBar.setRating(0);
                                                    }
                                                }
                                        );
                                    }
                                }
                            }
                        }
                    });
                }

                mItemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mItemCallback != null) {
                            mItemCallback.onCommentItemClick(position, mComment);
                        }
                    }
                });
            }
        }
    }

    public interface CommentItemCallback {
        void onCommentItemClick(int position, Comment comment);

        void onAssignOmPointClick(int position, Comment comment, int omPoints);
    }
}
