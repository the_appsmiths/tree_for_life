package com.treeforlife.Utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.util.PatternsCompat;
import android.text.TextUtils;

import java.util.Locale;

/**
 * copy from Previous work
 */

/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class ValidationUtil {

    public static ValidationResult validateName(String name, boolean isFirstName) {
        boolean isValidInput = true;
        String errMsg = "";
        if (TextUtils.isEmpty(name)) {
            isValidInput = false;
            errMsg = isFirstName ? "First name" : "Last Name";
            errMsg += " can't be blank.";

        } else if (name.matches(".*\\d+.*")) {
            isValidInput = false;
            errMsg = isFirstName ? "First name" : "Last Name";
            errMsg += " can't have a digit.";
        }

        return new ValidationResult(isValidInput, errMsg);
    }

    public static boolean isValidName(@NonNull String name) {
        return !TextUtils.isEmpty(name) && name.length() >= 3 && !name.matches(".*\\d+.*");

    }

    public static boolean isValidEmail(@NonNull String email) {
        if (TextUtils.isEmpty(email)) {
            return false;
        }

        return PatternsCompat.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isValidPassword(@NonNull String password) {
        if (TextUtils.isEmpty(password) || password.length() < 10) {
            return false;
        }

        return password.matches(".*\\d+.*")
                && password.matches(".*[a-z]+.*")
                && password.matches(".*[A-Z]+.*");
    }


    public static boolean isValidPhoneNumber(String mPhone, boolean withCountryCode,
                                             boolean onlyDigits) {
        int minLength = 6;
        int maxLength;
        if (withCountryCode) {
            maxLength = 18;
        } else {
            maxLength = 15;
        }
        if (!TextUtils.isEmpty(mPhone)) {
            return onlyDigits ?
                    mPhone.matches(String.format(Locale.getDefault(),
                            "^[+]{0,1}[0-9]{%d,%d}$", minLength, maxLength)) :
                    mPhone.length() >= minLength && mPhone.length() <= maxLength;
        }
        return false;
    }

    public static boolean isValidAddress(@NonNull Context context, String mAddress) {
        return !TextUtils.isEmpty(mAddress);
    }


    public static class ValidationResult {
        final public boolean isValidInput;
        public final String errorMessage;

        public ValidationResult(boolean isValidInput, String errorMessage) {
            this.isValidInput = isValidInput;
            this.errorMessage = errorMessage;
        }
    }
}
