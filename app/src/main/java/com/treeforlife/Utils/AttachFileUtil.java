package com.treeforlife.Utils;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.util.Log;

import com.treeforlife.BuildConfig;
import com.treeforlife.commons.Constants;
import com.treeforlife.commons.ListItemCallback;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by TheAppsmiths on 3/28/2018.
 *
 * @author TheAppsmiths
 */

public class AttachFileUtil implements ListItemCallback,
        PermissionsManager.PermissionsResultCallback {
    private static final String TAG = "AttachFileUtil";
    private static final String MAX_FILE_SIZE_EXCEED_ERROR =
            "Attached file size is too large. Max "
                    + Constants.ALLOWED_ATTACHED_FILE_SIZE_IN_MB + " MB is allowed.";


    private static AttachFileUtil sAttachFileUtil;

    private final DialogUtil mDialogUtil;
    private final PermissionsManager mPermissionsManager;
    private Activity mActivity;
    private AttachFileCallback mCallback;
    private Uri mAttachedFileUri;
    private File mAttachedFile;
    private AlertDialog neverAskedPermissionsInfoDialog;

    private final String[] mRemoveOrSelectFileTypeOptions = new String[]
            {
                    "Attach a Photo",
                    "Attach a Video",
                    "Remove attachment"

            };

    private final String[] mSelectFileTypeOptions = new String[]
            {
                    "Attach a Photo",
                    "Attach a Video",

            };

    private final String[] mCameraGalleryOptions = new String[]
            {
                    "Camera",
                    "Gallery"

            };

    private final String[] mRemoveOrSelectPhotoViaOptions = new String[]
            {
                    "Camera",
                    "Gallery",
                    "Remove Photo"

            };

    private final String[] mRemoveOrSelectVideoViaOptions = new String[]
            {
                    "Camera",
                    "Gallery",
                    //"Remove Video"

            };

    private final int RC_SELECT_FILE_TYPE_ALERT = 601;
    private final int RC_SELECT_PHOTO_VIA_ALERT = 602;
    private final int RC_SELECT_VIDEO_VIA_ALERT = 603;
    private final int RC_SELECT_REMOVE_PHOTO_VIA_ALERT = 604;
    private final int RC_SELECT_REMOVE_VIDEO_VIA_ALERT = 605;

    private int mAttachFileType = -1, mAttachFileVia = -1;
    private boolean hasGoneToAppInfo;

    private AttachFileUtil() {
        mDialogUtil = DialogUtil.getInstance();
        mPermissionsManager = PermissionsManager.getInstance();
    }

    public static AttachFileUtil getInstance() {
        if (sAttachFileUtil == null) {
            sAttachFileUtil = new AttachFileUtil();
        }

        return sAttachFileUtil;
    }

    /**
     * @param attachFileType fileType to attach
     * @param activity       reference
     * @param showRemove     flag true if menu option remove attachment to be shown.
     * @param callback       to notify the status.
     */
    public void start(@NonNull FileType attachFileType, @NonNull Activity activity,
                      boolean showRemove,
                      @NonNull AttachFileCallback callback) {
        this.mActivity = activity;
        this.mCallback = callback;
        switch (attachFileType) {

            case FILE_TYPE_IMAGE:
                showOptionsToSelectOrRemovePhoto(showRemove);
                break;

            case FILE_TYPE_VIDEO:
                showOptionsToSelectOrRemoveVideo(showRemove);
                break;

            case FILE_TYPE_IMAGE_AND_VIDEO:
                showOptionsToSelectFileType(showRemove);
                break;
        }

    }

    /**
     * method to show dialog with required options.
     *
     * @param showRemove flag true if remove menu option is to be shown.
     */
    private void showOptionsToSelectFileType(boolean showRemove) {
        mDialogUtil.showAlertList(mActivity, "Select Option ",
                showRemove ? mRemoveOrSelectFileTypeOptions : mSelectFileTypeOptions,
                RC_SELECT_FILE_TYPE_ALERT, this);
    }

    /**
     * method to show dialog with required options to attach a photo.
     */
    private void showOptionsToAttachPhoto() {
        mDialogUtil.showAlertList(mActivity, "Select Option ", mCameraGalleryOptions,
                RC_SELECT_PHOTO_VIA_ALERT, this);
    }

    /**
     * method to show dialog with required options to attach a video.
     */
    private void showOptionsToAttachVideo() {
        mDialogUtil.showAlertList(mActivity, "Select ", mCameraGalleryOptions,
                RC_SELECT_VIDEO_VIA_ALERT, this);
    }

    /**
     * method is self-explanatory by its name.
     *
     * @param showRemove flag true if remove menu option is to be shown.
     */
    private void showOptionsToSelectOrRemovePhoto(boolean showRemove) {
        mDialogUtil.showAlertList(mActivity, "Select Option ",
                showRemove ? mRemoveOrSelectPhotoViaOptions : mCameraGalleryOptions,
                RC_SELECT_REMOVE_PHOTO_VIA_ALERT, this);
    }

    /**
     * method is self-explanatory by its name.
     *
     * @param showRemove flag true if remove menu option is to be shown.
     */
    private void showOptionsToSelectOrRemoveVideo(boolean showRemove) {
        mDialogUtil.showAlertList(mActivity, "Select ",
                showRemove ? mRemoveOrSelectVideoViaOptions : mCameraGalleryOptions,
                RC_SELECT_REMOVE_VIDEO_VIA_ALERT, this);
    }

    @Override
    public void onItemClicked(int which, String datum, int requestCode) {
        switch (requestCode) {

            case RC_SELECT_FILE_TYPE_ALERT:
                switch (which) {

                    case 0:
                        showOptionsToAttachPhoto();
                        break;

                    case 1:
                        showOptionsToAttachVideo();
                        break;

                    case 2:
                        removeFile();
                        break;

                }
                break;

            case RC_SELECT_PHOTO_VIA_ALERT:
            case RC_SELECT_VIDEO_VIA_ALERT:
                mAttachFileType = requestCode;
                mAttachFileVia = which;
                proceedToAttachFile();
                break;

            case RC_SELECT_REMOVE_PHOTO_VIA_ALERT:
            case RC_SELECT_REMOVE_VIDEO_VIA_ALERT:
                switch (which) {
                    case 0:
                    case 1:
                        mAttachFileType = requestCode;
                        mAttachFileVia = which;
                        proceedToAttachFile();
                        break;

                    case 2:
                        removeFile();
                        break;
                }
                break;

        }
    }

    /**
     * method to perform remove a file/attachment operation.
     */
    private void removeFile() {
        mAttachFileType = -1;
        mAttachFileVia = -1;
        mAttachedFile = null;
        mAttachedFileUri = null;
        if (mCallback != null) {
            mCallback.onNewFile(getFileType(), "Removed",
                    mAttachedFileUri, mAttachedFile);
        }
    }

    /**
     * to be called from onRequestPermissionsResult() of {@link #mActivity}
     *
     * @param requestCode  requestCode
     * @param permissions  array of permissions.
     * @param grantResults array of result for respective permission in above array.
     */
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        mPermissionsManager.parsePermissionsResult(requestCode, permissions, grantResults, mActivity, this);

    }

    @Override
    public void deniedPermissions(int requestCode, @NonNull String[] deniedPermissions) {
        Log.d(TAG, "deniedPermissions = [" + Arrays.toString(deniedPermissions) + "]");
        AndroidUtil.showToast(mActivity, "Required permission(s) was/were denied !");
        // mPermissionsManager.requestPermissions(requestCode, mActivity, deniedPermissions);

    }

    @Override
    public void grantedPermissions(int requestCode, @NonNull String[] grantedPermissions) {
        Log.d(TAG, "grantedPermissions = [" + Arrays.toString(grantedPermissions) + "]");
        switch (requestCode) {
            case PermissionsManager.REQUEST_PERMISSION_CAMERA:
            case PermissionsManager.REQUEST_PERMISSION_READ_EXTERNAL_STORAGE:
            case PermissionsManager.REQUEST_PERMISSIONS_CAMERA_WRITE_EXTERNAL_STORAGE:
                proceedToAttachFile();
                break;
        }

    }

    @Override
    public void deniedAndNeverAskPermissions(int requestCode, @NonNull String[] deniedAndNeverAskPermissions) {
        Log.d(TAG, "deniedAndNeverAskPermissions = [" + Arrays.toString(deniedAndNeverAskPermissions) + "]");
        if (neverAskedPermissionsInfoDialog == null
                || !neverAskedPermissionsInfoDialog.isShowing()) {
            StringBuilder message = mPermissionsManager.getMessageForNeverAskPermissions(deniedAndNeverAskPermissions);

            if (!TextUtils.isEmpty(message)) {
                neverAskedPermissionsInfoDialog = mDialogUtil.showOkAlertDialog(mActivity,
                        message.toString(),
                        new Runnable() {
                            @Override
                            public void run() {
                                mPermissionsManager.openAppInfoPage(mActivity);
                                hasGoneToAppInfo = true;


                            }
                        }, true
                );
            }
        }

    }

    /**
     * method to be called from onResume() of {@link #mActivity}
     * to ensure the processing if use comes back to {@link #mActivity} page
     * after granting the required permission at App info/detail page.
     */
    public void onResume() {
        if (hasGoneToAppInfo
                && mPermissionsManager.isPermissionAlreadyGranted(mActivity,
                Manifest.permission.CAMERA)
                && mPermissionsManager.isPermissionAlreadyGranted(mActivity,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            hasGoneToAppInfo = false;
            proceedToAttachFile();

        }
    }

    /**
     * method to perform required operation to attach a file.
     */
    private void proceedToAttachFile() {
        switch (mAttachFileType) {

            case RC_SELECT_PHOTO_VIA_ALERT:
            case RC_SELECT_REMOVE_PHOTO_VIA_ALERT:
                proceedToAttachPhoto();
                break;

            case RC_SELECT_VIDEO_VIA_ALERT:
            case RC_SELECT_REMOVE_VIDEO_VIA_ALERT:
                proceedToAttachVideo();
                break;
        }
    }

    /**
     * method to perform required operation to attach a photo/image.
     */
    private void proceedToAttachPhoto() {
        if (mActivity != null) {
            boolean hasPermission;
            switch (mAttachFileVia) {
                case 0:
                    hasPermission =
                            !mPermissionsManager.hasCameraAndExternalStorageWritePermission(mActivity);

                    if (hasPermission) {

                        String fileName = "camera_image_" + getCurrentDate();


                        File storageDir = /*mActivity.getFilesDir()*/
                                mActivity.getExternalFilesDir(Environment.DIRECTORY_PICTURES);


                        try {
                            mAttachedFile = File.createTempFile(
                                    fileName,   //prefix
                                    ".jpg",         // suffix
                                    storageDir       //directory
                            );

                            // mImageFile = new File(storageDir, fileName+".jpg");

                            mAttachedFileUri = FileProvider.getUriForFile(mActivity,
                                    BuildConfig.APPLICATION_ID + ".provider",
                                    mAttachedFile);

                            Intent cameraIntent = AndroidUtil.IntentHelper.getIntentToOpenCamera(mAttachedFileUri);
                            Intent chooser = AndroidUtil.IntentHelper.createChooserIntent(mActivity, cameraIntent);
                            List<ResolveInfo> resInfoList = mActivity.getPackageManager().queryIntentActivities(cameraIntent, PackageManager.MATCH_DEFAULT_ONLY);
                            for (ResolveInfo resolveInfo : resInfoList) {
                                String packageName = resolveInfo.activityInfo.packageName;
                                mActivity.grantUriPermission(packageName, mAttachedFileUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            }
                            mActivity.startActivityForResult(chooser, Constants.RC_START_IMPLICIT_ACTIVITY_CAMERA);

                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }

                    break;

                case 1:
                    hasPermission = !mPermissionsManager.requestPermission(mActivity,
                            Manifest.permission.READ_EXTERNAL_STORAGE);

                    if (hasPermission) {
                        Intent galleryIntent = AndroidUtil.IntentHelper.getIntentToOpenGalleryImages();
                        Intent chooser = AndroidUtil.IntentHelper.createChooserIntent(mActivity, galleryIntent);
                        mActivity.startActivityForResult(chooser, Constants.RC_START_IMPLICIT_ACTIVITY_GALLERY);

                    }
                    break;
            }
        }
    }

    /**
     * @return today's date in 'yyyy_MMM_dd_hh_mm_ss_a_' format.
     */
    private String getCurrentDate() {
        String date = "";
        try {
            date = new SimpleDateFormat("yyyy_MMM_dd_hh_mm_ss_a_")
                    .format(new Date(System.currentTimeMillis()));
        } catch (Exception e) {
            e.printStackTrace();
            date += System.currentTimeMillis();
        }

        return date;
    }

    /**
     * method to perform required operation to attach a video.
     */
    private void proceedToAttachVideo() {
        if (mActivity != null) {
            boolean hasPermission;
            switch (mAttachFileVia) {
                case 0:
                    hasPermission =
                            !mPermissionsManager.hasCameraAndExternalStorageWritePermission(mActivity);

                    if (hasPermission) {

                        String fileName = "camera_video_" + getCurrentDate();


                        File storageDir = /*mActivity.getFilesDir()*/
                                mActivity.getExternalFilesDir(Environment.DIRECTORY_MOVIES);


                        try {
                            mAttachedFile = File.createTempFile(
                                    fileName,   //prefix
                                    ".mp4",         // suffix
                                    storageDir       //directory
                            );

                            // mImageFile = new File(storageDir, fileName+".jpg");

                            mAttachedFileUri = FileProvider.getUriForFile(mActivity,
                                    BuildConfig.APPLICATION_ID + ".provider",
                                    mAttachedFile);

                            Intent cameraIntent = AndroidUtil.IntentHelper.getIntentToOpenCameraRecorder(mAttachedFileUri);
                            Intent chooser = AndroidUtil.IntentHelper.createChooserIntent(mActivity, cameraIntent);
                            List<ResolveInfo> resInfoList = mActivity.getPackageManager().queryIntentActivities(cameraIntent, PackageManager.MATCH_DEFAULT_ONLY);
                            for (ResolveInfo resolveInfo : resInfoList) {
                                String packageName = resolveInfo.activityInfo.packageName;
                                mActivity.grantUriPermission(packageName, mAttachedFileUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            }
                            mActivity.startActivityForResult(chooser, Constants.RC_START_IMPLICIT_ACTIVITY_CAMERA);

                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }

                    break;

                case 1:
                    hasPermission = !mPermissionsManager.requestPermission(mActivity,
                            Manifest.permission.READ_EXTERNAL_STORAGE);

                    if (hasPermission) {
                        Intent galleryIntent = AndroidUtil.IntentHelper.getIntentToOpenGalleryVideos();
                        Intent chooser = AndroidUtil.IntentHelper.createChooserIntent(mActivity, galleryIntent);
                        mActivity.startActivityForResult(chooser, Constants.RC_START_IMPLICIT_ACTIVITY_GALLERY);

                    }
                    break;
            }
        }
    }

    /**
     * to be called from onActivityResult() method of {@link #mActivity}
     *
     * @param requestCode requestCode passed via #startActivityForResult().
     * @param resultCode  resultCode
     * @param data        output result data.
     */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        AndroidUtil.dumpIntent(TAG, data);

        switch (resultCode) {
            case Activity.RESULT_OK:
                switch (requestCode) {

                    case Constants.RC_START_IMPLICIT_ACTIVITY_GALLERY:
                        processAlbumResult(data);
                        break;

                    case Constants.RC_START_IMPLICIT_ACTIVITY_CAMERA:
                        processCameraResult(data);
                        break;
                }
                break;

            case Activity.RESULT_CANCELED:
                break;

        }

    }

    /**
     * method to process the received result from Gallery.
     *
     * @param data result received in onActivityResult()
     */
    private void processAlbumResult(Intent data) {
        String errorMessage = null;
        if (data != null) {
            mAttachedFileUri = data.getData();
            if (mAttachedFileUri != null) {
                UriUtil.dumpUri(TAG + " : Album Result", mAttachedFileUri);
                String filepath = UriUtil.getFilePath(mActivity, mAttachedFileUri);
                if (!TextUtils.isEmpty(filepath)) {
                    mAttachedFile = new File(filepath);
                    if (isValidFileSize()) {
                        if (mCallback != null) {
                            mCallback.onNewFile(getFileType(), mCameraGalleryOptions[mAttachFileVia],
                                    mAttachedFileUri, mAttachedFile);
                        }

                    } else {
                        errorMessage = MAX_FILE_SIZE_EXCEED_ERROR;
                    }


                }
            } else {
                errorMessage = "There is no uri of selected image data.";
            }
        } else {
            errorMessage = "There is no image data from album.";
        }

        if (!TextUtils.isEmpty(errorMessage)
                & mCallback != null) {
            mCallback.onError(errorMessage);
        }

    }

    /**
     * @return true if file size is not more than
     * {@link Constants#ALLOWED_ATTACHED_FILE_SIZE_IN_MB}
     */
    private boolean isValidFileSize() {
        return mAttachedFile != null
                && mAttachedFile.length()
                <= Constants.ALLOWED_ATTACHED_FILE_SIZE_IN_MB * 1024 * 1024;
    }

    /**
     * @return FileType on the basis of {@link #mAttachFileType}
     */
    private FileType getFileType() {
        FileType fileType = FileType.FILE_TYPE_NONE;
        switch (mAttachFileType) {

            case RC_SELECT_PHOTO_VIA_ALERT:
            case RC_SELECT_REMOVE_PHOTO_VIA_ALERT:
                fileType = FileType.FILE_TYPE_IMAGE;
                break;

            case RC_SELECT_VIDEO_VIA_ALERT:
            case RC_SELECT_REMOVE_VIDEO_VIA_ALERT:
                fileType = FileType.FILE_TYPE_VIDEO;
                break;
        }
        return fileType;
    }

    /**
     * method to process the received result of Camera.
     *
     * @param data result intent received in onActivityResult()
     */
    private void processCameraResult(Intent data) {
        if (mCallback != null) {
            if (mAttachedFileUri == null && mAttachedFile == null) {
                mCallback.onError("could not capture the camera file.");
            } else {
                if (isValidFileSize()) {
                    mCallback.onNewFile(getFileType(), mCameraGalleryOptions[mAttachFileVia],
                            mAttachedFileUri, mAttachedFile);
                } else {
                    mCallback.onError(MAX_FILE_SIZE_EXCEED_ERROR);
                }
            }
        }
    }

    public enum FileType {
        FILE_TYPE_IMAGE,
        FILE_TYPE_VIDEO,
        FILE_TYPE_IMAGE_AND_VIDEO,
        FILE_TYPE_NONE;
    }

    public interface AttachFileCallback {

        void onNewFile(@NonNull FileType fileType, String attachedVia, Uri fileUri, File file);

        void onError(@NonNull String errorMessage);
    }
}
