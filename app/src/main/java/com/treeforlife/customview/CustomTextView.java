package com.treeforlife.customview;

import android.content.Context;
import android.util.AttributeSet;

import com.treeforlife.Utils.FontsUtil;

/**
 * Created by TheAppsmiths on 3/20/2018.
 */

public class CustomTextView extends android.support.v7.widget.AppCompatTextView{

    private static final String TAG = "CustomTextView";
    private static final String APP_SCHEMA = "http://schemas.android.com/apk/res-auto";
    private static final int DEFAULT_INT_VALUE = -1;
    private Context mContext;

    public CustomTextView(Context context) {
        this(context, null);
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        this(context, attrs, android.R.attr.textViewStyle);
    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        applyFont(attrs);
    }
    //method to apply the font
    private void applyFont(AttributeSet attrs) {
        {
            if (attrs != null && mContext != null) {
                int fontIndex = attrs.getAttributeIntValue(APP_SCHEMA, "fontName", DEFAULT_INT_VALUE);
                if (fontIndex != DEFAULT_INT_VALUE) {
                    setTypeface(FontsUtil.getTypeface(mContext, fontIndex));
                }
            }
        }
    }
    //method to apply the font name
    public void setFontName(FontsUtil.FontName fontName) {
        setTypeface(FontsUtil.getTypeface(mContext, fontName));
    }

}
