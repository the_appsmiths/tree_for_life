package com.treeforlife.retrofit.responses;

import com.google.gson.annotations.SerializedName;
import com.treeforlife.dataobjects.Tutorial;

import java.util.ArrayList;

/**
 * Created by TheAppsmiths on 4/10/2018.
 */

public class GetTutorialResponse extends WebServiceResponse {

    @SerializedName("tutorial")

    public ArrayList<Tutorial> tutorialList;

}
