package com.treeforlife.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.treeforlife.R;
import com.treeforlife.Utils.ImageLoaderUtil;
import com.treeforlife.Utils.ImageRotationListener;
import com.treeforlife.dataobjects.Group;

import java.util.ArrayList;

/**
 * Created by TheAppsmiths on 4/9/2018.
 */

public class AdapterRvGroup extends RecyclerView.Adapter<AdapterRvGroup.MyHolder> {

    private GroupCallBack callBack;
    private ArrayList<Group> mGroupList;

    public AdapterRvGroup(GroupCallBack CallBack) {
        this.callBack = CallBack;
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_vault_album, parent, false));
    }

    @Override
    public void onBindViewHolder(MyHolder holder, int position) {
        holder.bindHoler(position);

    }

    @Override
    public int getItemCount() {
        return (mGroupList != null) ? mGroupList.size() : 0;
    }

    public void updateAdapter(ArrayList<Group> mArrays) {
        this.mGroupList = mArrays;
        notifyDataSetChanged();

    }


    public interface GroupCallBack {
        void clickItem(int position, Group view);

        void removeItem(int position, Group view);
    }

    //Holder view to show the data in adapter
    public class MyHolder extends RecyclerView.ViewHolder {
        private TextView mAlbumText;
        private View view;
        private Context mContext;
        private ImageView mRemoveImage, mAlbumImage;
        private Group mGroup;

        private ImageLoader mImageLoader;

        public MyHolder(View itemView) {
            super(itemView);
            this.view = itemView;
            this.mContext = itemView.getContext();
            mAlbumText = itemView.findViewById(R.id.album_text);
            mRemoveImage = itemView.findViewById(R.id.remove_item_image);
            mAlbumImage = itemView.findViewById(R.id.image_album);

            mImageLoader = ImageLoaderUtil.getLoader(mContext);
        }

        public void bindHoler(int position) {
            if (mGroupList != null && mGroupList.size() > position) {
                mGroup = mGroupList.get(position);
                mAlbumText.setText(mGroup.groupTitle);
                mImageLoader.displayImage(mGroup.groupImage, mAlbumImage,
                        ImageLoaderUtil.getDefaultImageOption(), new ImageRotationListener());

            }
            setLisener(position);
        }

        private void setLisener(final int position) {
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (callBack != null) {
                        callBack.clickItem(position, mGroup);
                    }
                }
            });

            mRemoveImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callBack.removeItem(position, mGroup);
                }
            });

        }
    }
}
