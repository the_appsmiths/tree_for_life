package com.treeforlife.dataobjects;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by TheAppsmiths on 4/24/2018.
 */

public class Notification implements Parcelable {

    @SerializedName("notification_type")
    public String notificationType;

    @SerializedName("notification_title")
    public String notificationTitle;

    @SerializedName("notification_date")
    public String notificationDate;

    @SerializedName("notification_image")
    public String notificationImageUrl;

    @SerializedName("notification_tree_id")
    public String treeId;

    protected Notification(Parcel in) {
        notificationType = in.readString();
        notificationTitle = in.readString();
        notificationDate = in.readString();
        notificationImageUrl = in.readString();
        treeId = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(notificationType);
        dest.writeString(notificationTitle);
        dest.writeString(notificationDate);
        dest.writeString(notificationImageUrl);
        dest.writeString(treeId);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Notification> CREATOR = new Creator<Notification>() {
        @Override
        public Notification createFromParcel(Parcel in) {
            return new Notification(in);
        }

        @Override
        public Notification[] newArray(int size) {
            return new Notification[size];
        }
    };
}
