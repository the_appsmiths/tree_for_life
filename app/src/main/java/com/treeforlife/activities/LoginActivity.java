package com.treeforlife.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.messaging.FirebaseMessaging;
import com.inscripts.interfaces.SubscribeCallbacks;
import com.treeforlife.R;
import com.treeforlife.Utils.AndroidUtil;
import com.treeforlife.Utils.CometChatUtil;
import com.treeforlife.Utils.DeviceInfoUtil;
import com.treeforlife.Utils.DialogUtil;
import com.treeforlife.Utils.FontsUtil;
import com.treeforlife.Utils.ProgressDialogUtil;
import com.treeforlife.Utils.ValidationUtil;
import com.treeforlife.commons.CometChatResponseListener;
import com.treeforlife.commons.Constants;
import com.treeforlife.commons.SharedPrefHelper;
import com.treeforlife.fragments.RegisterOptionDialogFragment;
import com.treeforlife.retrofit.ResponseId;
import com.treeforlife.retrofit.RetrofitManager;
import com.treeforlife.retrofit.RetrofitResponseValidator;
import com.treeforlife.retrofit.responses.LoginResponse;
import com.treeforlife.services.fcm.FcmUtil;

import org.json.JSONObject;

import java.net.SocketTimeoutException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener,
        RetrofitResponseValidator.ValidationListener, CometChatResponseListener {

    public static final String TAG = "LoginActivity";

    private Intent mActivityIntent;
    private Toolbar mToolbar;
    private ActionBar mActionBar;

    private ImageView mToolbarIconRhs, mToolbarIconLhs;
    private EditText mEditTextEmail, mEditTextPassword;
    private TextView mToolbarTitle, mRegisterTextView;
    private Button mLogin;

    private ProgressDialogUtil mProgressDialogUtil;
    private DialogUtil mDialogUtil;
    private CometChatUtil mCometChatUtil;
    private SharedPrefHelper mSharedPrefHelper;
    private LoginResponse mLoginResponse;

    private String mEmail, mPassword, mPhoneNumber, mFcmToken;
    private int mLoginType;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initComponents();
        initWidgets();
        initToolBar();
    }

    //method to initialize the components
    private void initComponents() {
        mActivityIntent = getIntent();
        mProgressDialogUtil = ProgressDialogUtil.getInstance();
        mDialogUtil = DialogUtil.getInstance();
        mCometChatUtil = CometChatUtil.getInstance();
        mSharedPrefHelper = SharedPrefHelper.getInstance();
        ensureFcmToken();
    }

    //method to ensure the comet chat
    private boolean ensureCometChat() {
        if (mCometChatUtil == null) {
            mCometChatUtil = CometChatUtil.getInstance();
        }
        if (mCometChatUtil.cometChat == null) {
            mCometChatUtil.initializeCometChat(this, this);
        }
        return mCometChatUtil.cometChat != null;
    }

    //method to ensure the fcm token
    private void ensureFcmToken() {
        if (TextUtils.isEmpty(mFcmToken)) {
            this.mFcmToken = mSharedPrefHelper.getFcmInstanceIdToken(this);
        }
        if (TextUtils.isEmpty(mFcmToken)) {
            mFcmToken = FcmUtil.getFcmInstanceIdToken();
            Log.e(TAG, "initComponents: FCM TOKEN was empty and has been initialized again.");
        }
        Log.w(TAG, "ensureFcmToken: mFcmToken = " + mFcmToken);
    }

    //method to initialize the widgets
    private void initWidgets() {
        mEditTextEmail = (EditText) findViewById(R.id.email_textview);
        mEditTextEmail.setTypeface(FontsUtil.getTypeface(this, FontsUtil.FontName.TITILLIUM_WEB_REGULAR));
        mEditTextPassword = (EditText) findViewById(R.id.password_textview);
        mLogin = (Button) findViewById(R.id.login_button);
        mLogin.setOnClickListener(this);

        TextView mForgetPasswordView = findViewById(R.id.forget_password_text);
        mForgetPasswordView.setOnClickListener(this);

        mRegisterTextView = (TextView) findViewById(R.id.register_text_view);
        mRegisterTextView.setOnClickListener(this);
        decorateRegisterTextView();

        String text = mActivityIntent.getStringExtra(Constants.KEY_EMAIL);
        if (TextUtils.isEmpty(text)) {
            text = mActivityIntent.getStringExtra(Constants.KEY_PHONE);
        }
        mEditTextEmail.setText(text);
        if (!TextUtils.isEmpty(text)) {
            mEditTextEmail.setSelection(text.length());
        }
    }

    ////method to decorate the register text view
    private void decorateRegisterTextView() {
        if (mRegisterTextView != null) {
            String text = mRegisterTextView.getText().toString();
            String subText = getString(R.string.register_here);
            if (!TextUtils.isEmpty(text) &&
                    !TextUtils.isEmpty(subText) &&
                    text.contains(subText)) {

                SpannableString spannableString = new SpannableString(text);
                spannableString.setSpan(
                        new ForegroundColorSpan(ContextCompat.getColor(this, R.color.appColor_774c2b_brown)),
                        text.indexOf(subText),
                        text.lastIndexOf(subText) + subText.length(),
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                mRegisterTextView.setText(spannableString);
            }
        }
    }

    //method to initialize the tool bar
    private void initToolBar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        if (mToolbar != null) {
            mToolbarTitle = mToolbar.findViewById(R.id.toolbar_title);
            mToolbarIconLhs = mToolbar.findViewById(R.id.toolbar_icon_back);
            mToolbarIconLhs.setVisibility(View.GONE);
            mToolbarIconRhs = mToolbar.findViewById(R.id.toolbar_icon_hamberg);
            mToolbarIconRhs.setVisibility(View.GONE);

        }
        setSupportActionBar(mToolbar);
        mActionBar = getSupportActionBar();
        setToolBarTitle();


        if (!TextUtils.isEmpty(mSharedPrefHelper.getLoginEmail(this))
                && !TextUtils.isEmpty(mSharedPrefHelper.getLoginPassword(this))) {
            mEditTextEmail.setText(mSharedPrefHelper.getLoginEmail(this));
            mEditTextPassword.setText(mSharedPrefHelper.getLoginPassword(this));
            mLogin.post(new Runnable() {
                @Override
                public void run() {
                    mLogin.performClick();
                }
            });
        }
    }

    //method to set the toolbar title
    private void setToolBarTitle() {
        if (mToolbarTitle != null) {
            mToolbarTitle.setText(R.string.log_in);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == null) {
            return;
        }
        AndroidUtil.hideKeyPad(this, v);
        switch (v.getId()) {
            case R.id.login_button:
                callLogin();
                break;

            case R.id.register_text_view:
                mEditTextEmail.setError(null);
                mEditTextPassword.setError(null);
                goToRegister();
                break;

            case R.id.forget_password_text:
                Intent intent = new Intent(this, ForgotPasswordActivity.class);
                intent.putExtra(Constants.KEY_CALLER_COMPONENT, TAG);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                break;
        }
    }

    //method to move the user to register screen
    private void goToRegister() {
        RegisterOptionDialogFragment mDialog = new RegisterOptionDialogFragment();
        mDialog.show(getFragmentManager(), RegisterOptionDialogFragment.TAG);
    }

    //method to call api for login
    private void callLogin() {
        if (isValidInput() && AndroidUtil.hasInternetConnectivity(this)) {

            mProgressDialogUtil.showProgressDialog(this);
            ensureFcmToken();
            if (TextUtils.isEmpty(mFcmToken)) {
                mFcmToken = "no_fcm_token";
            }
            Call<LoginResponse> loginApi = RetrofitManager.getRetrofitWebService()
                    .callLogin(
                            mLoginType,
                            mPhoneNumber,
                            mEmail,
                            mPassword,
                            DeviceInfoUtil.getTelephonyDeviceIdOrIMEINo(this),
                            mFcmToken,
                            Constants.DEVICE_TYPE_ANDROID

                    );
            loginApi.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    new RetrofitResponseValidator(ResponseId.LOGIN, response, LoginActivity.this);

                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    String errorMessage = "";
                    if (t != null) {
                        Log.e(TAG, "onFailure: Error : " + t.getMessage());
                        if (t instanceof SocketTimeoutException) {
                            errorMessage = "Please make sure that your device has an active internet connection.";
                        }
                    }

                    mProgressDialogUtil.dismissProgressDialog();


                    if (TextUtils.isEmpty(errorMessage)) {
                        AndroidUtil.showErrorToast(LoginActivity.this);
                    } else {
                        mDialogUtil.showErrorDialog(LoginActivity.this, errorMessage);
                    }
                }
            });

        }
    }

    //method to check the validation
    private boolean isValidInput() {
        mEmail = mEditTextEmail.getText().toString().trim();
        mPassword = mEditTextPassword.getText().toString();
        boolean number = mEmail.matches(".*[0-9]+.*") || mEmail.matches("[+]");
        if (TextUtils.isEmpty(mEmail)) {
            mEditTextEmail.setError(getString(R.string.error_no_email_or_phone));
            mEditTextEmail.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(mPassword) && !ValidationUtil.isValidPassword(mPassword)) {
            mEditTextPassword.setError(getString(R.string.error_no_password));
            mEditTextPassword.requestFocus();
            return false;
        } else if (!TextUtils.isEmpty(mEmail)) {

            if (!mEmail.contains("@") && number) {
                mPhoneNumber = mEmail;
                mLoginType = 2;
                mEmail = null;
                if (!ValidationUtil.isValidPhoneNumber(mPhoneNumber, true, false)) {
                    mEditTextEmail.setError(getString(R.string.error_valid_phone));
                    mEditTextEmail.requestFocus();
                    return false;
                }
            } else {
                mLoginType = 1;
                mPhoneNumber = null;
                if (!ValidationUtil.isValidEmail(mEmail)) {
                    mEditTextEmail.setError(getString(R.string.error_email));
                    mEditTextEmail.requestFocus();
                    return false;
                }
            }
        }
        return true;
    }


    @Override
    public void onValidResponse(@NonNull ResponseId responseId, @NonNull Object responseBody, @NonNull Response response) {
        switch (responseId) {
            case LOGIN:
                mLoginResponse = (LoginResponse) responseBody;
                mSharedPrefHelper.resetEmailPassword(LoginActivity.this);
                //afterLogin();
                loginToCometChat();
                break;
        }
    }

    //method to login into comet chat
    private void loginToCometChat() {
        if (ensureCometChat()) {
            if (mLoginResponse != null && mLoginResponse.mTFLUser != null
                    && !TextUtils.isEmpty(mLoginResponse.mTFLUser.userId)) {
                mCometChatUtil.loginCometChatUser(getApplicationContext(), mLoginResponse.mTFLUser.userId, this);
            } else {
                AndroidUtil.showToast(this, "Login Failed !!");
                mProgressDialogUtil.dismissProgressDialog();
            }
        }
    }


    @Override
    public void onInvalidResponse(@NonNull ResponseId responseId, @NonNull String errorMessage, Response response) {
        boolean showErrorInDialog = true;
        mProgressDialogUtil.dismissProgressDialog();
        switch (responseId) {
            case LOGIN:
                LoginResponse serviceResponse = parseLoginFailedResponse(response);
                if (serviceResponse != null
                        && !TextUtils.isEmpty(serviceResponse.errorCode)) {

                    switch (serviceResponse.errorCode) {

                        case Constants.ERR_CODE_ACCOUNT_NOT_VERIFIED:
                            if (!TextUtils.isEmpty(serviceResponse.registerBy)
                                    && serviceResponse.registerBy
                                    .equals(Constants.REGISTER_BY_PHONE)) {
                                showErrorInDialog = false;
                                if (TextUtils.isEmpty(mEmail)) {
                                    mSharedPrefHelper.setLoginEmail(this, mPhoneNumber);

                                } else {
                                    mSharedPrefHelper.setLoginEmail(this, mEmail);

                                }
                                mSharedPrefHelper.setLoginPassword(this, mPassword);
                                Intent intent = new Intent(this, OtpActivity.class);
                                intent.putExtra(Constants.KEY_USER_ID_REGISTER, serviceResponse.userId);
                                startActivity(intent);
                                finishAffinity();
                            }
                            break;
                    }

                }
                break;
        }

        if (response == null || !response.isSuccessful()) {
            mDialogUtil.showErrorDialog(this, errorMessage);
        } else {
            if (showErrorInDialog) {
                mDialogUtil.showOkAlertDialog(this, errorMessage);
            } else {
                AndroidUtil.showToast(this, errorMessage);
            }
        }
    }

    @Override
    public void onCometChatResponse(ResponseId responseId, boolean isSuccess, JSONObject response) {
        Log.w(TAG, "onCometChatResponse() called with: responseId = [" + responseId + "], " +
                "isSuccess = [" + isSuccess + "], response = [" + response + "]");

        if (!isFinishing()) {

            switch (responseId) {
                case COMET_CHAT_INITIALIZED:
                    loginToCometChat();
                    break;

                case COMET_CHAT_LOGIN_USER:
                    if (isSuccess) {
                        mCometChatUtil.cometChat.subscribe(false, new SubscribeCallbacks() {
                            /*
                             * @ref : https://docs.cometchat.com/android-sdk/reference/subscribe-to-cometchat/
                             * */
                            @Override
                            public void gotOnlineList(JSONObject jsonObject) {
                                Log.w(TAG, "gotOnlineList() called with: jsonObject = [" + jsonObject + "]");
                            }

                            @Override
                            public void gotBotList(JSONObject jsonObject) {
                                Log.w(TAG, "gotBotList() called with: jsonObject = [" + jsonObject + "]");
                            }

                            @Override
                            public void gotRecentChatsList(JSONObject jsonObject) {
                                Log.w(TAG, "gotRecentChatsList() called with: jsonObject = [" + jsonObject + "]");
                            }

                            @Override
                            public void onError(JSONObject jsonObject) {
                                Log.w(TAG, "onError() called with: jsonObject = [" + jsonObject + "]");
                                AndroidUtil.showErrorToast(LoginActivity.this);
                                mProgressDialogUtil.dismissProgressDialog();
                            }

                            @Override
                            public void onMessageReceived(JSONObject jsonObject) {
                                Log.w(TAG, "onMessageReceived() called with: jsonObject = [" + jsonObject + "]");
                            }

                            @Override
                            public void gotProfileInfo(JSONObject jsonObject) {
                                Log.w(TAG, "gotProfileInfo() called with: jsonObject = [" + jsonObject + "]");
                                if (!isFinishing() && !isDestroyed() && jsonObject != null) {
                                    String pushChannel = jsonObject.optString("push_channel");
                                    SharedPrefHelper.getInstance().setPushChannel(LoginActivity.this, pushChannel);
                                    FirebaseMessaging.getInstance().subscribeToTopic(pushChannel);
                                    // mCometChatUtil.cometChat.unsubscribe();
                                    mProgressDialogUtil.dismissProgressDialog();
                                    afterLogin();
                                }


                            }

                            @Override
                            public void gotAnnouncement(JSONObject jsonObject) {
                                Log.w(TAG, "gotAnnouncement() called with: jsonObject = [" + jsonObject + "]");
                            }

                            @Override
                            public void onAVChatMessageReceived(JSONObject jsonObject) {
                                Log.w(TAG, "onAVChatMessageReceived() called with: jsonObject = [" + jsonObject + "]");
                            }

                            @Override
                            public void onActionMessageReceived(JSONObject jsonObject) {
                                Log.w(TAG, "onActionMessageReceived() called with: jsonObject = [" + jsonObject + "]");
                            }

                            @Override
                            public void onGroupMessageReceived(JSONObject jsonObject) {

                            }

                            @Override
                            public void onGroupsError(JSONObject jsonObject) {

                            }

                            @Override
                            public void onLeaveGroup(JSONObject jsonObject) {

                            }

                            @Override
                            public void gotGroupList(JSONObject jsonObject) {

                            }

                            @Override
                            public void gotGroupMembers(JSONObject jsonObject) {

                            }

                            @Override
                            public void onGroupAVChatMessageReceived(JSONObject jsonObject) {

                            }

                            @Override
                            public void onGroupActionMessageReceived(JSONObject jsonObject) {

                            }

                            @Override
                            public void onLogout() {

                            }
                        });


                    } else {
                        AndroidUtil.showErrorToast(this);
                        mProgressDialogUtil.dismissProgressDialog();
                    }

                    break;
            }
        }
    }

    private LoginResponse parseLoginFailedResponse(Response response) {
        LoginResponse result = null;
        if (response != null) {
            Object body = response.body();
            if (body != null && body instanceof LoginResponse) {
                result = (LoginResponse) body;
            }
        }
        return result;
    }

    private void afterLogin() {
        if (mLoginResponse != null) {
            mSharedPrefHelper.setLoginResponse(this, mLoginResponse.mTFLUser);
            //Check is User First Time Login Aftre Regsiter in App
            String firtsTimeRegister = mSharedPrefHelper.getFirstTimeRegister(this);

            Intent intent = new Intent();
            intent.putExtra(Constants.KEY_TFL_USER, mLoginResponse.mTFLUser);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            if (TextUtils.isEmpty(firtsTimeRegister)) {
                intent.setClass(this, HomeActivity.class);
            } else {
                intent.setClass(this, TutorialActivity.class);
            }
            startActivity(intent);
            finishAffinity();

        }
    }
}
