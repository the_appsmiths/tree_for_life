package com.treeforlife.retrofit.responses;

import com.google.gson.annotations.SerializedName;
import com.treeforlife.dataobjects.Video;

import java.util.ArrayList;

/**
 * Created by TheAppsmiths on 4/5/2018.
 */

public class GetVideoResponse extends WebServiceResponse {

    @SerializedName("videos")
    public ArrayList<Video>  videosList=new ArrayList<Video>();

    @SerializedName("total_records")
    public int totalRecords;
}
