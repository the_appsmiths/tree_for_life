package com.treeforlife.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.treeforlife.R;
import com.treeforlife.Utils.FragmentUtil;
import com.treeforlife.Utils.ProgressDialogUtil;
import com.treeforlife.activities.ChangePasswordActivity;
import com.treeforlife.adapters.AdapterRvValutDetail;
import com.treeforlife.commons.Constants;
import com.treeforlife.commons.FragmentCallBack;
import com.treeforlife.commons.SharedPrefHelper;
import com.treeforlife.dataobjects.TFLUser;

/**
 * Created by TheAppsmiths on 3/19/2018.
 */

/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class VaultDetailFragment extends Fragment
        implements AdapterRvValutDetail.VaultClickDetail {

    public static final String TAG = "VaultDetailFragment";
    private Context mContext;
    private Activity mActivity;
    private Bundle mBundle;
    private FragmentCallBack mCallback;

    private RecyclerView mRecyclerView;

    private AdapterRvValutDetail adapterRvValutDetail;

    private SharedPrefHelper mSharedPrefHelper;
    private ProgressDialogUtil mProgressDialogUtil;

    private TFLUser mTflUser;

    private final String[] arrays =
            {
                    "Albums",
                    "Videos",
                    "Groups",
                    "Friends",
                    "Family",
                    "Converter",
                    "My QR Code",
                    "Customized",
                    "Wish Status",
                    "Change Values Password"
            };
    private final int[] mVaultImageList =
            {
                    R.drawable.album_cover,
                    R.drawable.videos_cover,
                    R.drawable.groups_cover,
                    R.drawable.friends_cover,
                    R.drawable.family_cover,
                    R.drawable.converter_cover,
                    R.drawable.my_qr_cover,
                    R.drawable.bg_home, //_customize_btn
                    R.drawable.bg_home,
                    R.drawable.bg_home
            };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
        this.mActivity = getActivity();
        this.mBundle = getArguments();

        if (mActivity != null && mActivity instanceof FragmentCallBack) {
            mCallback = (FragmentCallBack) mActivity;
        }
        this.mSharedPrefHelper = SharedPrefHelper.getInstance();
        this.mProgressDialogUtil = ProgressDialogUtil.getInstance();

        this.adapterRvValutDetail = new AdapterRvValutDetail(this);

        extractDataFromBundle();
        ensureTflUser();
    }


    private void extractDataFromBundle() {
        if (mBundle == null) {
            mBundle = getArguments();
        }

        if (mBundle != null) {
            mTflUser = mBundle.getParcelable(Constants.KEY_TFL_USER);

        }

    }

    private void ensureTflUser() {
        if (mTflUser == null) {
            mTflUser = mSharedPrefHelper.getLoginResponse(mContext);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_vault_detail, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initComponents(view);
    }


    @Override
    public void onResume() {
        super.onResume();
        if (mCallback != null) {
            Bundle bundle = new Bundle();
            bundle.putString(Constants.KEY_FRAGMENT_CALLBACK, TAG);
            mCallback.updateBottomBar(bundle);
        }
        if (mActivity != null) {
            mActivity.setTitle(R.string.values);
        }
    }

    private void initComponents(View view) {
        if (view == null) {
            return;
        }

        mRecyclerView = view.findViewById(R.id.values_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mRecyclerView.setAdapter(adapterRvValutDetail);

        adapterRvValutDetail.updateList(arrays, mVaultImageList);

    }

    @Override
    public void clickItem(int position, View view) {
        Bundle bundle = new Bundle();
        bundle.putString(Constants.KEY_CALLER_COMPONENT, TAG);
        bundle.putParcelable(Constants.KEY_TFL_USER, mTflUser);
        switch (position) {
            case 0:
                FragmentUtil.loadFragment((AppCompatActivity) mContext, FragmentUtil.FRAGMENT_VAULT_ALBUM, bundle, true);
                break;
            case 1:
                FragmentUtil.loadFragment((AppCompatActivity) mContext, FragmentUtil.FRAGMENT_VIDEO, bundle, true);
                break;
            case 2:
                FragmentUtil.loadFragment((AppCompatActivity) mContext, FragmentUtil.FRAGMENT_GROUP, bundle, true);
                break;
            case 3:
                FragmentUtil.loadFragment((AppCompatActivity) mContext, FragmentUtil.FRAGMENT_FRIENDS, bundle, true);
                break;
            case 4:
                FragmentUtil.loadFragment((AppCompatActivity) mContext, FragmentUtil.FRAGMENT_FAMILY, bundle, true);
                break;
            case 5:
                FragmentUtil.loadFragment((AppCompatActivity) mContext, FragmentUtil.FRAGMENT_CONVERTER, bundle, true);
                break;
            case 6:
                FragmentUtil.loadFragment((AppCompatActivity) mContext, FragmentUtil.FRAGMENT_QR_CODE, bundle, true);
                break;
            case 7:
                FragmentUtil.loadFragment((AppCompatActivity) mActivity, FragmentUtil.FRAGMENT_CUSTOMIZED, bundle, true);
                break;
            case 8:
                FragmentUtil.loadFragment((AppCompatActivity) mActivity, FragmentUtil.FRAGMENT_WISH_STATUS, bundle, true);
                break;
            case 9:
                Intent changePasswordIntent = new Intent(mActivity, ChangePasswordActivity.class);
                changePasswordIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                changePasswordIntent.putExtra(Constants.KEY_CALLER_COMPONENT, TAG);
                changePasswordIntent.putExtra(Constants.KEY_TFL_USER, mTflUser);
                startActivity(changePasswordIntent);
                break;
        }


    }

}
