package com.treeforlife.retrofit.responses;

import com.google.gson.annotations.SerializedName;
import com.treeforlife.dataobjects.Group;

import java.util.ArrayList;

/**
 * Created by TheAppsmiths on 4/9/2018.
 */

public class GetGroupResponse extends WebServiceResponse {

    @SerializedName("groups")
    public ArrayList<Group> listGroup=new ArrayList<Group>();

    @SerializedName("total_records")
    public int totalRecords;
}
