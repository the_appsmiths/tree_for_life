package com.treeforlife.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.text.method.TransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.treeforlife.R;
import com.treeforlife.Utils.AndroidUtil;
import com.treeforlife.Utils.DialogUtil;
import com.treeforlife.Utils.FragmentUtil;
import com.treeforlife.Utils.ImageLoaderUtil;
import com.treeforlife.Utils.ProgressDialogUtil;
import com.treeforlife.activities.ChangePasswordActivity;
import com.treeforlife.commons.Constants;
import com.treeforlife.commons.FragmentCallBack;
import com.treeforlife.commons.SharedPrefHelper;
import com.treeforlife.customview.CustomTextView;
import com.treeforlife.dataobjects.TFLUser;
import com.treeforlife.retrofit.ResponseId;
import com.treeforlife.retrofit.RetrofitManager;
import com.treeforlife.retrofit.RetrofitResponseValidator;
import com.treeforlife.retrofit.responses.WebServiceResponse;

import java.net.SocketTimeoutException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by TheAppsmiths on 3/19/2018.
 * updated By TheAppsmiths on 25th Apr. 2018.
 */

/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class VaultFragment extends Fragment implements View.OnClickListener,
        RetrofitResponseValidator.ValidationListener {

    public static final String TAG = "VaultFragment";

    private Context mContext;
    private Activity mActivity;
    private Bundle mBundle;
    private FragmentCallBack mCallBack;

    private EditText mPasswordEditText, mConfirmPasswordEditText;
    private CustomTextView mPasswordTextView, mConfirmPassTextView,
            mHeadingTextView, mForgetPasswordTextView;

    private SharedPrefHelper mSharedPrefHelper;
    private ProgressDialogUtil mProgressDialogUtil;
    private DialogUtil mDialogUtil;

    private TFLUser mTflUser;
    private String mPassword, mConfirmPassword;
    private ImageView mHeaderImageView;

    private boolean setVaultPassword;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
        this.mActivity = getActivity();
        this.mBundle = getArguments();
        if (mActivity instanceof FragmentCallBack) {
            this.mCallBack = (FragmentCallBack) mActivity;
        }

        this.mSharedPrefHelper = SharedPrefHelper.getInstance();
        this.mProgressDialogUtil = ProgressDialogUtil.getInstance();
        this.mDialogUtil = DialogUtil.getInstance();
        extractDataFromBundle();
        ensureTflUser();
    }

    private void extractDataFromBundle() {

        if (mBundle == null) {
            mBundle = getArguments();
        }

        if (mBundle != null) {
            if (mTflUser == null) {
                mTflUser = mBundle.getParcelable(Constants.KEY_TFL_USER);
            }
        }

    }

    private void ensureTflUser() {
        if (mTflUser == null) {
            mTflUser = mSharedPrefHelper.getLoginResponse(mContext);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_vault, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initComponents(view);
        manageContentItemsVisibility();
    }

    private void initComponents(View view) {

        if (view != null) {
            mHeadingTextView = view.findViewById(R.id.heading_text_view);

            mPasswordTextView = view.findViewById(R.id.password_text_view);
            mPasswordEditText = view.findViewById(R.id.password_edit_text);

            mConfirmPassTextView = view.findViewById(R.id.confirm_password_text_view);
            mConfirmPasswordEditText = view.findViewById(R.id.confirm_password_edit_text);

            Button mSubmitButton = view.findViewById(R.id.submit_button);
            mSubmitButton.setOnClickListener(this);

            mForgetPasswordTextView = view.findViewById(R.id.forgot_password_text_view);
            mForgetPasswordTextView.setOnClickListener(this);

            View mChangePasswordTextView = view.findViewById(R.id.change_password_text_view);
            mChangePasswordTextView.setOnClickListener(this);

            mHeaderImageView = view.findViewById(R.id.header_tree_image_view);

            setImageHeader();

        }
    }

    private void setImageHeader() {

        if (mTflUser != null) {
            ImageLoaderUtil.getLoader(mContext).displayImage(mTflUser.profileImage, mHeaderImageView,
                    ImageLoaderUtil.getDefaultHeaderImageOption());
        }
    }

    private void manageContentItemsVisibility() {
        setVaultPassword = false;
        if (mConfirmPassTextView != null) {

            if (mTflUser != null && !TextUtils.isEmpty(mTflUser.vaultMpin)) {

                mHeadingTextView.setText(getString(R.string.four_digit_password));
                mPasswordTextView.setText(getString(R.string.enter_here));

                mConfirmPassTextView.setVisibility(View.GONE);
                mConfirmPasswordEditText.setVisibility(View.GONE);
                mPasswordEditText.setTransformationMethod(new AsteriskPasswordTransformationMethod());

            } else {

                mHeadingTextView.setText(R.string.set_values_password);
                mPasswordTextView.setText(R.string.password);

                mConfirmPassTextView.setVisibility(View.VISIBLE);
                mConfirmPasswordEditText.setVisibility(View.VISIBLE);
                setVaultPassword = true;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mActivity != null) {
            mActivity.setTitle(R.string.values);
        }
        if (mCallBack != null) {
            Bundle bundle = new Bundle();
            bundle.putString(Constants.KEY_FRAGMENT_CALLBACK, TAG);
            mCallBack.updateBottomBar(bundle);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == null) {
            return;
        }

        AndroidUtil.hideKeyPad(mActivity, v);
        switch (v.getId()) {
            case R.id.submit_button:

                if (setVaultPassword) {
                    callCreateVaultPassword();
                } else {
                    callVaultLogin();
                }

                break;
            case R.id.forgot_password_text_view:
                if (setVaultPassword) {
                    AndroidUtil.showToast(mActivity, "Please Set Password");

                } else {
                    callForgotPassword();
                }

                break;

            case R.id.change_password_text_view:
                Intent changePasswordIntent = new Intent(mActivity, ChangePasswordActivity.class);
                changePasswordIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                changePasswordIntent.putExtra(Constants.KEY_CALLER_COMPONENT, TAG);
                changePasswordIntent.putExtra(Constants.KEY_TFL_USER, mTflUser);
                startActivity(changePasswordIntent);
                break;
        }
    }

    private void clearEditText() {
        if (mPasswordEditText != null) {
            mPasswordEditText.setText(null);
            mConfirmPasswordEditText.setText(null);
        }

    }


    private boolean isValidInput() {
        mPassword = mPasswordEditText.getText().toString().trim();
        mConfirmPassword = mConfirmPasswordEditText.getText().toString().trim();

        if (TextUtils.isEmpty(mPassword)) {
            mPasswordEditText.setError(getString(R.string.error_no_password));
            mPasswordEditText.requestFocus();
            return false;

        } else if (mPassword.length() != 4) {
            mPasswordEditText.setError(getString(R.string.four_digit_password));
            mPasswordEditText.requestFocus();
            return false;

        }

        if (setVaultPassword) {

            if (TextUtils.isEmpty(mConfirmPassword)) {
                mConfirmPasswordEditText.setError(getString(R.string.error_no_password));
                mConfirmPasswordEditText.requestFocus();
                return false;

            } else if (!mPassword.equals(mConfirmPassword)) {
                mConfirmPasswordEditText.setError(getString(R.string.error_password_mismatch));
                mConfirmPasswordEditText.requestFocus();
                return false;
            }

        }

        return isValidUser();
    }

    private boolean isValidUser() {

        if (mTflUser == null) {

            AndroidUtil.showToast(mContext, "Invalid User");
            return false;

        } else if (TextUtils.isEmpty(mTflUser.userId)) {

            AndroidUtil.showToast(mContext, "You need to Login !");
            return false;
        }

        return true;
    }

    private void callForgotPassword() {
        if (isValidUser() && AndroidUtil.hasInternetConnectivity(mActivity)) {

            mProgressDialogUtil.showProgressDialog(mActivity);

            Call<WebServiceResponse> forgetPasswordApi = RetrofitManager.getRetrofitWebService()
                    .forgotVaultPassword(mTflUser.userId);
            forgetPasswordApi.enqueue(new Callback<WebServiceResponse>() {
                @Override
                public void onResponse(Call<WebServiceResponse> call, Response<WebServiceResponse> response) {
                    new RetrofitResponseValidator(ResponseId.FORGET_VAULT_PASSWORD, response, VaultFragment.this);

                }

                @Override
                public void onFailure(Call<WebServiceResponse> call, Throwable t) {
                    String errorMessage = "";
                    if (t != null) {
                        Log.e(TAG, "onFailure: Error : " + t.getMessage());
                        if (t instanceof SocketTimeoutException) {
                            errorMessage = "Please make sure that your device has an active internet connection.";
                        }
                    }

                    mProgressDialogUtil.dismissProgressDialog();


                    if (TextUtils.isEmpty(errorMessage)) {
                        AndroidUtil.showErrorToast(mContext);
                    } else {
                        mDialogUtil.showErrorDialog(mContext, errorMessage);
                    }
                }
            });

        }
    }

    private void callCreateVaultPassword() {
        if (isValidInput() && AndroidUtil.hasInternetConnectivity(mActivity)) {

            mProgressDialogUtil.showProgressDialog(mActivity);

            Call<WebServiceResponse> vaultLoginService = RetrofitManager.getRetrofitWebService()
                    .setVaultPassword(mTflUser.userId, mPassword);
            vaultLoginService.enqueue(new Callback<WebServiceResponse>() {
                @Override
                public void onResponse(Call<WebServiceResponse> call, Response<WebServiceResponse> response) {
                    new RetrofitResponseValidator(ResponseId.SET_VAULT_PASSWORD, response, VaultFragment.this);

                }

                @Override
                public void onFailure(Call<WebServiceResponse> call, Throwable t) {
                    String errorMessage = "";
                    if (t != null) {
                        Log.e(TAG, "onFailure: Error : " + t.getMessage());
                        if (t instanceof SocketTimeoutException) {
                            errorMessage = "Please make sure that your device has an active internet connection.";
                        }
                    }

                    mProgressDialogUtil.dismissProgressDialog();


                    if (TextUtils.isEmpty(errorMessage)) {
                        AndroidUtil.showErrorToast(mActivity);
                    } else {
                        mDialogUtil.showErrorDialog(mActivity, errorMessage);
                    }
                }
            });

        }
    }

    private void callVaultLogin() {
        if (isValidInput() && AndroidUtil.hasInternetConnectivity(mActivity)) {

            mProgressDialogUtil.showProgressDialog(mActivity);

            Call<WebServiceResponse> vaultLoginService = RetrofitManager.getRetrofitWebService()
                    .vaultLogin(mTflUser.userId, mPassword);
            vaultLoginService.enqueue(new Callback<WebServiceResponse>() {
                @Override
                public void onResponse(Call<WebServiceResponse> call, Response<WebServiceResponse> response) {
                    new RetrofitResponseValidator(ResponseId.VAULT_LOGIN, response, VaultFragment.this);

                }

                @Override
                public void onFailure(Call<WebServiceResponse> call, Throwable t) {
                    String errorMessage = "";
                    if (t != null) {
                        Log.e(TAG, "onFailure: Error : " + t.getMessage());
                        if (t instanceof SocketTimeoutException) {
                            errorMessage = "Please make sure that your device has an active internet connection.";
                        }
                    }

                    mProgressDialogUtil.dismissProgressDialog();


                    if (TextUtils.isEmpty(errorMessage)) {
                        AndroidUtil.showErrorToast(mActivity);
                    } else {
                        mDialogUtil.showErrorDialog(mActivity, errorMessage);
                    }
                }
            });

        }
    }

    @Override
    public void onValidResponse(@NonNull ResponseId responseId, @NonNull Object responseBody, @NonNull Response response) {

        switch (responseId) {
            case VAULT_LOGIN:
                afterVaultLogin((WebServiceResponse) responseBody);
                break;
            case SET_VAULT_PASSWORD:
                afterSetVaultPassword((WebServiceResponse) responseBody);
                break;
            case FORGET_VAULT_PASSWORD:
                afterForgetPassword((WebServiceResponse) responseBody);
                break;

        }
        mProgressDialogUtil.dismissProgressDialog();
    }


    @Override
    public void onInvalidResponse(@NonNull ResponseId responseId, @NonNull String errorMessage, Response response) {

        mProgressDialogUtil.dismissProgressDialog();
        if (response == null || !response.isSuccessful()) {
            mDialogUtil.showErrorDialog(mActivity, errorMessage);
        } else {
            mDialogUtil.showOkAlertDialog(mActivity, errorMessage);
        }
    }

    private void afterSetVaultPassword(WebServiceResponse responseBody) {
        if (responseBody.status == true) {
            ensureTflUser();
            mTflUser.vaultMpin = mPassword;
            mSharedPrefHelper.setLoginResponse(mContext, mTflUser);

            Intent broadCastIntent = new Intent(Constants.ACTION_USER_PROFILE_UPDATED);
            broadCastIntent.putExtra(Constants.KEY_TFL_USER, mTflUser);
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(broadCastIntent);
            AndroidUtil.showToast(mContext, responseBody.message);

            clearEditText();
            manageContentItemsVisibility();

            Bundle bundle = new Bundle();
            bundle.putParcelable(Constants.KEY_TFL_USER, mTflUser);
            FragmentUtil.loadFragment((AppCompatActivity) mActivity,
                    FragmentUtil.FRAGMENT_VAULT_DETAIL, bundle, true);


        }

    }


    private void afterVaultLogin(WebServiceResponse responseBody) {
        if (responseBody.status == true) {
            clearEditText();
            Bundle bundle = new Bundle();
            bundle.putParcelable(Constants.KEY_TFL_USER, mTflUser);
            FragmentUtil.loadFragment((AppCompatActivity) mContext,
                    FragmentUtil.FRAGMENT_VAULT_DETAIL, bundle, false);
        }
    }

    private void afterForgetPassword(WebServiceResponse responseBody) {
        if (responseBody.status == true) {
            mDialogUtil.showOkAlertDialog(mActivity, responseBody.message);
        }
        // AndroidUtil.showToast(mActivity, responseBody.message);

    }

    private class AsteriskPasswordTransformationMethod implements TransformationMethod {
        @Override
        public CharSequence getTransformation(CharSequence source, View view) {
            return new AsteriskPasswordTransformationMethod.PasswordCharSequence(source);
        }

        @Override
        public void onFocusChanged(View view, CharSequence sourceText, boolean focused, int direction, Rect previouslyFocusedRect) {

        }

        private class PasswordCharSequence implements CharSequence {
            private CharSequence mSource;

            public PasswordCharSequence(CharSequence source) {
                mSource = source; // Store char sequence
            }

            @Override
            public int length() {
                return mSource.length(); // Return default
            }

            @Override
            public char charAt(int index) {
                return '*'; // This is the important part
            }

            @Override
            public CharSequence subSequence(int start, int end) {
                return mSource.subSequence(start, end); // Return default
            }
        }
    }
}
