package com.treeforlife.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.treeforlife.R;
import com.treeforlife.Utils.FragmentUtil;
import com.treeforlife.commons.Constants;
import com.treeforlife.commons.FragmentCallBack;
import com.treeforlife.commons.SharedPrefHelper;
import com.treeforlife.dataobjects.TFLUser;

/**
 * Created by TheAppsmiths on 3/19/2018.
 */

/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class HomeFragment extends Fragment implements View.OnClickListener {

    public static final String TAG = "HomeFragment";

    private FragmentCallBack mCallBack;

    private Context mContext;
    private Activity mActivity;
    private Bundle mBundle;

    private SharedPrefHelper mSharedPrefHelper;

    private TFLUser mTflUser;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
        this.mActivity = getActivity();
        this.mBundle = getArguments();

        if (mActivity instanceof FragmentCallBack) {
            this.mCallBack = (FragmentCallBack) mActivity;
        }

        this.mSharedPrefHelper = SharedPrefHelper.getInstance();
        extractDataFromBundle();
        ensureTflUser();
    }


    private void extractDataFromBundle() {
        if (mBundle == null) {
            mBundle = getArguments();
        }

        if (mBundle != null) {
            mTflUser = mBundle.getParcelable(Constants.KEY_TFL_USER);
        }
    }

    private void ensureTflUser() {
        if (mTflUser == null) {
            mTflUser = mSharedPrefHelper.getLoginResponse(mContext);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initComponents(view);
    }

    private void initComponents(View view) {

        View mTruthAppleView = view.findViewById(R.id.truth_view);
        mTruthAppleView.setOnClickListener(this);

        View mTrustOrangeView = view.findViewById(R.id.trust_view);
        mTrustOrangeView.setOnClickListener(this);

        View mLoyaltyPearView = view.findViewById(R.id.loyalty_view);
        mLoyaltyPearView.setOnClickListener(this);

        View mToleranceMangoView = view.findViewById(R.id.tolerance_view);
        mToleranceMangoView.setOnClickListener(this);

    }

    @Override
    public void onResume() {
        super.onResume();

        if (mActivity != null) {
            mActivity.setTitle(R.string.home);
        }
        if (mCallBack != null) {
            Bundle bundle = new Bundle();
            bundle.putString(Constants.KEY_FRAGMENT_CALLBACK, TAG);
            mCallBack.updateBottomBar(bundle);
        }

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.truth_view:
                showComments(Constants.FRUIT_TYPE_APPLE);
                break;

            case R.id.trust_view:
                showComments(Constants.FRUIT_TYPE_ORANGE);
                break;

            case R.id.loyalty_view:
                showComments(Constants.FRUIT_TYPE_PEAR);
                break;

            case R.id.tolerance_view:
                showComments(Constants.FRUIT_TYPE_MANGO);
                break;

        }
    }

    private void showComments(int fruitType) {
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.KEY_FRUIT_TYPE, fruitType);
        bundle.putParcelable(Constants.KEY_TFL_USER, mTflUser);

        FragmentUtil.loadFragment((AppCompatActivity) mActivity,
                FragmentUtil.FRAGMENT_COMMENTS, bundle, true);
    }
}
