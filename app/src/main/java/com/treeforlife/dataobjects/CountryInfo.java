package com.treeforlife.dataobjects;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by TheAppsmiths on 4/10/2018.
 *
 * @author TheAppsmiths
 */

public class CountryInfo implements Parcelable {

    @SerializedName("country_id")
    public String countryId;
    @SerializedName(value = "country_name", alternate = {"name"})
    public String countryName;
    @SerializedName(value = "country_code", alternate = {"code"})
    public String countryCode;
    @SerializedName(value = "country_dial_code", alternate = {"dial_code"})
    public String dialCode;


    protected CountryInfo(Parcel in) {
        countryId = in.readString();
        countryName = in.readString();
        countryCode = in.readString();
        dialCode = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(countryId);
        dest.writeString(countryName);
        dest.writeString(countryCode);
        dest.writeString(dialCode);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CountryInfo> CREATOR = new Creator<CountryInfo>() {
        @Override
        public CountryInfo createFromParcel(Parcel in) {
            return new CountryInfo(in);
        }

        @Override
        public CountryInfo[] newArray(int size) {
            return new CountryInfo[size];
        }
    };
}
