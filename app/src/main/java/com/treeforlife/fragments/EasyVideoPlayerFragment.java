package com.treeforlife.fragments;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.easyvideoplayer.EasyVideoCallback;
import com.afollestad.easyvideoplayer.EasyVideoPlayer;
import com.treeforlife.R;
import com.treeforlife.Utils.AndroidUtil;
import com.treeforlife.commons.Constants;


/**
 * Created by TheAppsmiths on 3/20/2018.
 *
 * @author TheAppsmiths
 * @link : https://github.com/afollestad/easy-video-player
 */
/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */

public class EasyVideoPlayerFragment extends Fragment implements EasyVideoCallback {
    private static final String TAG = "EasyVideoPlayerFragment";

    private Context mContext;
    private Activity mActivity;
    private Bundle mBundle;
    private EasyVideoPlayerFragmentCallback mCallback;

    private EasyVideoPlayer mEasyVideoPlayer;


    private String mVideoUrl;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
        this.mActivity = getActivity();
        this.mBundle = getArguments();
        if (context instanceof EasyVideoPlayerFragmentCallback) {
            this.mCallback = (EasyVideoPlayerFragmentCallback) context;

        } else {
            Fragment parentFragment = getParentFragment();
            if (parentFragment != null && parentFragment instanceof EasyVideoPlayerFragmentCallback) {
                this.mCallback = (EasyVideoPlayerFragmentCallback) parentFragment;
            }
            extractDataFromBundle();
        }
    }

    private void extractDataFromBundle() {

        if (mBundle == null) {
            mBundle = getArguments();
        }

        if (mBundle != null) {
            mVideoUrl = mBundle.getString(Constants.KEY_VIDEO_URL);

        }

    }

    public void update(Bundle bundle) {
        this.mBundle = bundle;
        extractDataFromBundle();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_video_player, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initWidgets(view);
    }

    private void initWidgets(View view) {
        if (view != null) {
            mEasyVideoPlayer = view.findViewById(R.id.easy_video_player);
            setUpVideoPlayer();
        }

    }

    private void setUpVideoPlayer() {
        if (mEasyVideoPlayer != null) {
            mEasyVideoPlayer.setSource(Uri.parse(mVideoUrl));
            mEasyVideoPlayer.setCallback(this);
            mEasyVideoPlayer.setAutoPlay(true);
        }
    }

    public void stopVideo() {
        if (mEasyVideoPlayer != null && mEasyVideoPlayer.isPlaying()) {
            mEasyVideoPlayer.stop();
        }
    }

    public void playVideo() {
        stopVideo();
        if (TextUtils.isEmpty(mVideoUrl)) {
            extractDataFromBundle();
        }
        if (mEasyVideoPlayer != null) {
            mEasyVideoPlayer.start();
        } else {
            AndroidUtil.showToast(mContext, "Empty Video Url");
        }
    }

    public boolean isPlaying() {
        if (mEasyVideoPlayer != null) {
            return mEasyVideoPlayer.isPlaying();
        }
        return false;
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mEasyVideoPlayer != null) {
            mEasyVideoPlayer.pause();
        }
    }

    @Override
    public void onStarted(EasyVideoPlayer player) {
        if (mCallback != null) {
            mCallback.onPlayVideo();
        }
    }

    @Override
    public void onPaused(EasyVideoPlayer player) {
        if (mCallback != null) {
            mCallback.onStopVideo();
        }
    }

    @Override
    public void onPreparing(EasyVideoPlayer player) {

    }

    @Override
    public void onPrepared(EasyVideoPlayer player) {

    }

    @Override
    public void onBuffering(int percent) {

    }

    @Override
    public void onError(EasyVideoPlayer player, Exception e) {

    }

    @Override
    public void onCompletion(EasyVideoPlayer player) {
        if (mCallback != null) {
            mCallback.onStopVideo();
        }
    }

    @Override
    public void onRetry(EasyVideoPlayer player, Uri source) {

    }

    @Override
    public void onSubmit(EasyVideoPlayer player, Uri source) {

    }

    public interface EasyVideoPlayerFragmentCallback {
        void onStopVideo();

        void onPlayVideo();
    }
}
