package com.treeforlife.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.treeforlife.R;
import com.treeforlife.Utils.AndroidUtil;
import com.treeforlife.Utils.AttachFileUtil;
import com.treeforlife.Utils.DialogUtil;
import com.treeforlife.Utils.ImageLoaderUtil;
import com.treeforlife.Utils.ProgressDialogUtil;
import com.treeforlife.Utils.ValidationUtil;
import com.treeforlife.commons.Constants;
import com.treeforlife.commons.SharedPrefHelper;
import com.treeforlife.customview.CustomEditText;
import com.treeforlife.customview.CustomTextView;
import com.treeforlife.dataobjects.CountryInfo;
import com.treeforlife.fragments.ImageViewDialogFragment;
import com.treeforlife.retrofit.ResponseId;
import com.treeforlife.retrofit.RetrofitManager;
import com.treeforlife.retrofit.RetrofitResponseValidator;
import com.treeforlife.retrofit.responses.RegisterResponse;

import java.io.File;
import java.net.SocketTimeoutException;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener,
        View.OnTouchListener,
        AttachFileUtil.AttachFileCallback,
        RetrofitResponseValidator.ValidationListener {

    public static final String TAG = "RegisterActivity";

    private Intent mActivityIntent;
    private ActionBar mActionBar;
    private Toolbar mToolbar;

    private TextView mToolbarTitle;
    private ImageView mToolbarIconRhs, mToolbarIconLhs;


    private CircleImageView mRegisterImage;
    private CustomTextView mLoginTextView, mEmailTextView, mPhoneTextView;
    private Button mRegisterButton;
    private final int SELECT_ADDRESS_COUNTRY = 103;
    private ScrollView mScrollView;
    private CustomEditText mCountryEditText, mFirstNameEditText, mLastNameEditText, mEmailEditText,
            mPasswordEditText, mConfirmPassEditText, mPhoneDialCodeEditText, mPhoneEditText,
            mStreetEditText, mCityEditText, mStateEditText, mAddressCountryEditText;

    private DialogUtil mDialogUtil;
    private ProgressDialogUtil mProgressDialogUtil;
    private ImageLoader mImageLoader;
    private AttachFileUtil mAttachFileUtil;
    private View mSelectDialCodeView, mSelectPlantCountryCodeView, mSelectAddressCountryView;

    private Uri mAttachedFileUri;
    private File mAttachedFile;
    private AttachFileUtil.FileType mAttachedFileType;
    private CountryInfo mPhoneCodeCountryInfo, mPlantTreeCountry, mAddressCountryInfo;
    private SharedPrefHelper mSharedPrefHelper;

    private final int SELECT_COUNTRY_DIAL_CODE = 101;
    private final int SELECT_WHERE_TO_PLANT_COUNTRY = 102;
    private String mRegisterBy, mFirstName, mLastName, mEmail, mPassword, mConfirmPassword, mPhone,
            mPhoneDialCode, mAddressStreet, mAddressCity, mAddressState;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(Constants.KEY_COUNTRY_INFO, mPlantTreeCountry);
        outState.putString("phone_dial_code", mPhoneDialCode);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            mPlantTreeCountry = savedInstanceState.getParcelable(Constants.KEY_COUNTRY_INFO);
            mPhoneDialCode = savedInstanceState.getString("phone_dial_code");
        }
        setContentView(R.layout.activity_register);
        initComponents();
        initWidgets();
        initToolBar();
    }

    private void initComponents() {
        mActivityIntent = getIntent();
        mProgressDialogUtil = ProgressDialogUtil.getInstance();
        mDialogUtil = DialogUtil.getInstance();
        mImageLoader = ImageLoaderUtil.getLoader(this);
        mAttachFileUtil = AttachFileUtil.getInstance();
        mSharedPrefHelper = SharedPrefHelper.getInstance();

        if (mActivityIntent != null) {
            mRegisterBy = mActivityIntent.getStringExtra(Constants.KEY_REGISTER_BY);
        }
    }

    private void initWidgets() {

        mRegisterImage = (CircleImageView) findViewById(R.id.register_image);
        mRegisterImage.setOnClickListener(this);

        View mUploadImageView = findViewById(R.id.upload_image_btn);
        mUploadImageView.setOnClickListener(this);

        mScrollView = findViewById(R.id.register_scroll_view);

        mFirstNameEditText = findViewById(R.id.first_name_edit_text);
        mLastNameEditText = findViewById(R.id.last_name_edit_text);
        mEmailEditText = findViewById(R.id.email_edit_text);
        mPasswordEditText = findViewById(R.id.password_edit_text);
        mConfirmPassEditText = findViewById(R.id.con_password_edit_text);

        mSelectDialCodeView = findViewById(R.id.select_country_code_view);
        mSelectDialCodeView.setOnClickListener(this);

        mPhoneDialCodeEditText = findViewById(R.id.phone_dial_code_edit_text);
        mPhoneDialCodeEditText.setOnTouchListener(this);

        mPhoneEditText = findViewById(R.id.phone_number_edit_text);
        mStreetEditText = findViewById(R.id.street_edit_text);
        mCityEditText = findViewById(R.id.city_edit_text);
        mStateEditText = findViewById(R.id.state_edit_text);

        mSelectAddressCountryView = findViewById(R.id.select_address_country_view);
        mSelectAddressCountryView.setOnClickListener(this);
        mAddressCountryEditText = findViewById(R.id.edit_text_address_country);
        mAddressCountryEditText.setOnTouchListener(this);

        mCountryEditText = findViewById(R.id.edit_text_country);
        mCountryEditText.setOnTouchListener(this);
        mSelectPlantCountryCodeView = findViewById(R.id.select_where_to_plant_country_view);
        mSelectPlantCountryCodeView.setOnClickListener(this);

        mRegisterButton = findViewById(R.id.register_button);
        mRegisterButton.setOnClickListener(this);

        mLoginTextView = findViewById(R.id.login_text_view);
        mLoginTextView.setOnClickListener(this);
        decorateLoginTextView();

        mEmailTextView = findViewById(R.id.email_text_view);
        mPhoneTextView = findViewById(R.id.phone_text_view);
        updateText();

        setListeners();

    }

    private void updateText() {
        if (mEmailTextView != null) {
            switch (mRegisterBy) {

                case Constants.REGISTER_BY_EMAIL:
                    mEmailTextView.setText(R.string.email_address);
                    mPhoneTextView.setText(R.string.phone_number_optional);
                    break;

                case Constants.REGISTER_BY_PHONE:
                    mEmailTextView.setText(R.string.email_address_optional);
                    mPhoneTextView.setText(R.string.phone_number);
                    break;
            }

        }
        if (mCountryEditText != null && mPlantTreeCountry != null) {
            mCountryEditText.setText(mPlantTreeCountry.countryName);
            mCountryEditText.setError(null);
        }

        if (!TextUtils.isEmpty(mPhoneDialCode) && mPhoneDialCodeEditText != null) {
            mPhoneDialCodeEditText.setText(mPhoneDialCode);

        }

    }

    private void setListeners() {
        mEmailEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    AndroidUtil.hideKeyPad(RegisterActivity.this, mEmailEditText);
                    textView.clearFocus();
                    if (Constants.REGISTER_BY_PHONE.equals(mRegisterBy)) {
                        if (TextUtils.isEmpty(mPhoneDialCodeEditText.getText())) {
                            mSelectDialCodeView.performClick();

                        } else {
                            mPhoneEditText.requestFocus();
                            AndroidUtil.showKeyPad(RegisterActivity.this, mPhoneEditText);

                        }
                    } else {
                        mPasswordEditText.requestFocus();
                        AndroidUtil.showKeyPad(RegisterActivity.this, mPasswordEditText);
                    }
                }
                return true;
            }
        });

        mStateEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    AndroidUtil.hideKeyPad(RegisterActivity.this, mStateEditText);
                    textView.clearFocus();
                    if (TextUtils.isEmpty(mAddressCountryEditText.getText())) {
                        mSelectAddressCountryView.performClick();
                    }
                }
                return true;
            }
        });

        mAddressCountryEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    AndroidUtil.hideKeyPad(RegisterActivity.this, mEmailEditText);
                    textView.clearFocus();
                    if (TextUtils.isEmpty(mCountryEditText.getText())) {
                        mSelectPlantCountryCodeView.performClick();
                    }
                }
                return true;
            }
        });
    }

    private void decorateLoginTextView() {
        if (mLoginTextView != null) {
            String text = mLoginTextView.getText().toString();
            String subText = getString(R.string.login_here);
            if (!TextUtils.isEmpty(text) &&
                    !TextUtils.isEmpty(subText) &&
                    text.contains(subText)) {

                SpannableString spannableString = new SpannableString(text);
                spannableString.setSpan(
                        new ForegroundColorSpan(ContextCompat.getColor(this, R.color.appColor_774c2b_brown)),
                        text.indexOf(subText),
                        text.lastIndexOf(subText) + subText.length(),
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                mLoginTextView.setText(spannableString);
            }
        }
    }

    private void initToolBar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        if (mToolbar != null) {
            mToolbarTitle = mToolbar.findViewById(R.id.toolbar_title);
            mToolbarIconLhs = mToolbar.findViewById(R.id.toolbar_icon_back);
            mToolbarIconLhs.setVisibility(View.GONE);
            mToolbarIconRhs = mToolbar.findViewById(R.id.toolbar_icon_hamberg);
            mToolbarIconRhs.setVisibility(View.GONE);

        }
        setSupportActionBar(mToolbar);
        mActionBar = getSupportActionBar();

        setToolBarTitle();
    }

    private void setToolBarTitle() {
        if (mToolbarTitle != null) {
            mToolbarTitle.setText(R.string.register);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mAttachFileUtil != null) {
            mAttachFileUtil.onResume();
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        if (view != null && event != null) {
            switch (event.getAction()) {

                case MotionEvent.ACTION_DOWN:
                    switch (view.getId()) {

                        case R.id.edit_text_address_country:
                            mSelectAddressCountryView.performClick();
                            break;

                        case R.id.edit_text_country:
                            mSelectPlantCountryCodeView.performClick();
                            break;

                        case R.id.phone_dial_code_edit_text:
                            mSelectDialCodeView.performClick();
                            break;
                    }
                    break;
            }
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.register_image:
                showProfileImage();
                break;

            case R.id.upload_image_btn:
                selectImage();
                break;

            case R.id.select_country_code_view:
                Intent intent = new Intent(this, CountriesListActivity.class);
                intent.putExtra(Constants.KEY_REQ_CODE, SELECT_COUNTRY_DIAL_CODE);
                intent.putExtra(Constants.KEY_COUNTRY_INFO, mPhoneCodeCountryInfo);
                startActivityForResult(intent, Constants.RC_START_ACTIVITY_SELECT_COUNTRY);
                overridePendingTransition(R.anim.slide_in_from_right, R.anim.fade_in);
                break;

            case R.id.select_where_to_plant_country_view:
                intent = new Intent(this, CountriesListActivity.class);
                intent.putExtra(Constants.KEY_REQ_CODE, SELECT_WHERE_TO_PLANT_COUNTRY);
                intent.putExtra(Constants.KEY_COUNTRY_INFO, mPlantTreeCountry);
                startActivityForResult(intent, Constants.RC_START_ACTIVITY_SELECT_COUNTRY);
                overridePendingTransition(R.anim.slide_in_from_right, R.anim.fade_in);
                break;
            case R.id.select_address_country_view:
                intent = new Intent(this, CountriesListActivity.class);
                intent.putExtra(Constants.KEY_REQ_CODE, SELECT_ADDRESS_COUNTRY);
                intent.putExtra(Constants.KEY_COUNTRY_INFO, mAddressCountryInfo);
                startActivityForResult(intent, Constants.RC_START_ACTIVITY_SELECT_COUNTRY);
                overridePendingTransition(R.anim.slide_in_from_right, R.anim.fade_in);
                break;

            case R.id.register_button:
                callRegister();
                break;

            case R.id.login_text_view:
                goToLoginPage(false);
                break;
        }

    }

    private void showProfileImage() {
        String imageUriOrUrl = "";

        if (mAttachedFileUri != null) {
            imageUriOrUrl = mAttachedFileUri.toString();
        }


        if (!TextUtils.isEmpty(imageUriOrUrl)) {
            FragmentManager fragmentManager = getSupportFragmentManager();

            if (fragmentManager != null) {
                Bundle bundle = new Bundle();
                bundle.putString(Constants.KEY_CALLER_COMPONENT, TAG);
                bundle.putString(Constants.KEY_IMAGE_URL, imageUriOrUrl);

                ImageViewDialogFragment mImageViewDialogFragment;
                Fragment fragment =
                        fragmentManager.findFragmentByTag(ImageViewDialogFragment.TAG);
                if (fragment != null && fragment instanceof ImageViewDialogFragment) {
                    mImageViewDialogFragment = (ImageViewDialogFragment) fragment;
                    mImageViewDialogFragment.update(bundle);
                } else {
                    mImageViewDialogFragment = new ImageViewDialogFragment();
                    mImageViewDialogFragment.setArguments(bundle);
                }

                mImageViewDialogFragment.show(fragmentManager, ImageViewDialogFragment.TAG);
            }
        }
    }

    private void callRegister() {

        if (isValidInput() && AndroidUtil.hasInternetConnectivity(this)) {
            mProgressDialogUtil.showProgressDialog(RegisterActivity.this);
            MultipartBody.Builder builder = new MultipartBody.Builder();
            builder.setType(MultipartBody.FORM);

            builder.addFormDataPart("first_name", mFirstName);
            builder.addFormDataPart("last_name", mLastName);
            builder.addFormDataPart("password", mPassword);
            builder.addFormDataPart("where_to_plant", mPlantTreeCountry.countryName);
            builder.addFormDataPart("country_id", mPlantTreeCountry.countryId);


            builder.addFormDataPart("street", mAddressStreet);
            builder.addFormDataPart("city", mAddressCity);
            builder.addFormDataPart("state", mAddressState);
            builder.addFormDataPart("address_country_id", mAddressCountryInfo != null ? mAddressCountryInfo.countryId : "");
            builder.addFormDataPart("address_country_name", mAddressCountryInfo != null ? mAddressCountryInfo.countryName : "");

            builder.addFormDataPart("email_address", mEmail);
            if (!TextUtils.isEmpty(mPhone)) {
                builder.addFormDataPart("phone_number", mPhoneDialCode + mPhone);
            } else {
                builder.addFormDataPart("phone_number", mPhone);
            }
            builder.addFormDataPart("phone_dial_code", mPhoneDialCode);

            // Single Image

            if (mAttachedFile != null) {
                File file1 = new File(mAttachedFile.getName());
                builder.addFormDataPart("profile_image", file1.getName(), (RequestBody.create(MediaType.parse("image"), mAttachedFile)));

            }

            MultipartBody requestBody = builder.build();

            Call<RegisterResponse> normalRegisterCall = null;
            mRegisterButton.setEnabled(false);

            if (!TextUtils.isEmpty(mRegisterBy)) {
                switch (mRegisterBy) {

                    case Constants.REGISTER_BY_EMAIL:
                        normalRegisterCall = RetrofitManager.getRetrofitWebService()
                                .registerEmail(requestBody);
                        break;

                    case Constants.REGISTER_BY_PHONE:
                        normalRegisterCall = RetrofitManager.getRetrofitWebService()
                                .registerPhone(requestBody);
                        break;

                }
            }

            if (normalRegisterCall != null) {
                normalRegisterCall.enqueue(new Callback<RegisterResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<RegisterResponse> call, @NonNull Response<RegisterResponse> response) {
                        new RetrofitResponseValidator(ResponseId.REGISTER_PHONE, response, RegisterActivity.this);

                    }

                    @Override
                    public void onFailure(@NonNull Call<RegisterResponse> call, @NonNull Throwable t) {
                        String errorMessage = "";
                        if (t != null) {
                            Log.e(TAG, "onFailure: Error : " + t.getMessage());
                            if (t instanceof SocketTimeoutException) {
                                errorMessage = "Please make sure that your device has an active internet connection.";
                            }
                        }

                        mProgressDialogUtil.dismissProgressDialog();
                        mRegisterButton.setEnabled(true);


                        if (TextUtils.isEmpty(errorMessage)) {
                            AndroidUtil.showErrorToast(RegisterActivity.this);
                        } else {
                            mDialogUtil.showErrorDialog(RegisterActivity.this, errorMessage);
                        }
                    }
                });

            } else {
                mRegisterButton.setEnabled(true);
            }

        }
    }


    private boolean isValidInput() {

        String mCountryName = mCountryEditText.getText().toString().trim();
        mFirstName = mFirstNameEditText.getText().toString().trim();
        mLastName = mLastNameEditText.getText().toString().trim();
        mEmail = mEmailEditText.getText().toString().trim();
        mPassword = mPasswordEditText.getText().toString().trim();
        mConfirmPassword = mConfirmPassEditText.getText().toString().trim();
        mPhoneDialCode = mPhoneDialCodeEditText.getText().toString().trim();
        mPhone = mPhoneEditText.getText().toString().trim();
        mAddressStreet = mStreetEditText.getText().toString().trim();
        mAddressCity = mCityEditText.getText().toString().trim();
        mAddressState = mStateEditText.getText().toString().trim();
        String mAddressCountryName = mAddressCountryEditText.getText().toString().trim();

        ValidationUtil.ValidationResult firstNameResult = ValidationUtil.validateName(mFirstName, true);
        ValidationUtil.ValidationResult lastNameResult = ValidationUtil.validateName(mLastName, false);

        if (firstNameResult != null && !firstNameResult.isValidInput) {
            mFirstNameEditText.setError(firstNameResult.errorMessage);
            mFirstNameEditText.requestFocus();
            return false;

        } else if (lastNameResult != null && !lastNameResult.isValidInput) {
            mLastNameEditText.setError(lastNameResult.errorMessage);
            mLastNameEditText.requestFocus();
            return false;
        } else if (!hasValidEmailOrPhone()) {
            return false;

        } else if (TextUtils.isEmpty(mPassword)) {
            mPasswordEditText.setError(getString(R.string.empty_password));
            mPasswordEditText.requestFocus();
            return false;

        } else if (mPassword.length() < 10) {
            mPasswordEditText.setError(getString(R.string.error_password_length));
            mPasswordEditText.requestFocus();
            return false;

        } else if (TextUtils.isEmpty(mConfirmPassword)) {
            mConfirmPassEditText.setError(getString(R.string.empty_confirm_password));
            mConfirmPassEditText.requestFocus();
            return false;

        } else if (!(mPassword).equals(mConfirmPassword)) {
            mConfirmPassEditText.setError(getString(R.string.error_password_mismatch));
            mConfirmPassEditText.requestFocus();
            return false;

        } else if (TextUtils.isEmpty(mAddressStreet)) {
            mStreetEditText.setError(getString(R.string.empty_address_street));
            mStreetEditText.requestFocus();
            return false;

        } else if (TextUtils.isEmpty(mAddressCity)) {
            mCityEditText.setError(getString(R.string.empty_address_city));
            mCityEditText.requestFocus();
            return false;

        } else if (TextUtils.isEmpty(mAddressState)) {
            mStateEditText.setError(getString(R.string.empty_address_state));
            mStateEditText.requestFocus();
            return false;

        } else if (TextUtils.isEmpty(mAddressCountryName)) {
            mAddressCountryEditText.setError(getString(R.string.no_address_country));
            mAddressCountryEditText.clearFocus();
            AndroidUtil.showToast(this, getString(R.string.no_address_country));
            return false;

        } else if (TextUtils.isEmpty(mCountryName)) {
            mCountryEditText.setError("Please select country where to plant tree");
            mCountryEditText.clearFocus();
            AndroidUtil.showToast(this, "Please select country where to plant tree");
            return false;

        }

        return true;
    }

    private boolean hasValidEmailOrPhone() {
        if (TextUtils.isEmpty(mRegisterBy)) {
            AndroidUtil.showToast(this, "Invalid Registration type");
            return false;
        }

        switch (mRegisterBy) {

            case Constants.REGISTER_BY_EMAIL:
                if (TextUtils.isEmpty(mEmail)) {
                    mEmailEditText.setError(getString(R.string.empty_email));
                    mEmailEditText.requestFocus();
                    return false;

                } else if (!ValidationUtil.isValidEmail(mEmail)) {
                    mEmailEditText.setError(getString(R.string.error_email));
                    mEmailEditText.requestFocus();
                    return false;

                } else if (!TextUtils.isEmpty(mPhone)) {
                    if (TextUtils.isEmpty(mPhoneDialCode)) {

                        mPhoneDialCodeEditText.setError(getString(R.string.no_phone_dial_code));
                        mPhoneDialCodeEditText.clearFocus();
                        AndroidUtil.showToast(this, getString(R.string.no_phone_dial_code));
                        return false;
                    } else if (!ValidationUtil.isValidPhoneNumber(mPhone, false, false)) {
                        mPhoneEditText.setError(getString(R.string.error_valid_phone));
                        mPhoneEditText.requestFocus();
                        return false;

                    }

                } else if (!TextUtils.isEmpty(mPhoneDialCode)) {
                    if (TextUtils.isEmpty(mPhone)) {
                        mPhoneEditText.setError(getString(R.string.empty_phone_number));
                        mPhoneEditText.requestFocus();
                        return false;

                    } else if (!ValidationUtil.isValidPhoneNumber(mPhone, false, false)) {
                        mPhoneEditText.setError(getString(R.string.error_valid_phone));
                        mPhoneEditText.requestFocus();
                        return false;
                    }

                }
                break;

            case Constants.REGISTER_BY_PHONE:
                if (!TextUtils.isEmpty(mEmail)
                        && !ValidationUtil.isValidEmail(mEmail)) {
                    mEmailEditText.setError(getString(R.string.error_email));
                    mEmailEditText.requestFocus();
                    return false;

                } else if (TextUtils.isEmpty(mPhoneDialCode)) {
                    mPhoneDialCodeEditText.setError(getString(R.string.no_phone_dial_code));
                    mPhoneDialCodeEditText.clearFocus();
                    AndroidUtil.showToast(this, getString(R.string.no_phone_dial_code));
                    return false;
                } else if (TextUtils.isEmpty(mPhone)) {
                    mPhoneEditText.setError(getString(R.string.empty_phone_number));
                    mPhoneEditText.requestFocus();
                    return false;

                } else if (!ValidationUtil.isValidPhoneNumber(mPhone, false, false)) {
                    mPhoneEditText.setError(getString(R.string.error_valid_phone));
                    mPhoneEditText.requestFocus();
                    return false;

                }
                break;

            default:
                AndroidUtil.showToast(this, "Invalid Registration type("
                        + mRegisterBy + ")");
                return false;


        }

        return true;
    }

    private void goToLoginPage(boolean sendEmail) {
        Intent in = new Intent(this, LoginActivity.class);
        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        if (sendEmail) {
            in.putExtra(Constants.KEY_EMAIL, mEmail);
            //Set Value in share on First Time Register
            mSharedPrefHelper.setFirstTimeRegister(this, mEmail);
        }
        startActivity(in);
        finish();
    }


    private void selectImage() {

        mAttachFileUtil.start(AttachFileUtil.FileType.FILE_TYPE_IMAGE, RegisterActivity.this,
                mAttachedFile != null, this);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {

                case Constants.RC_START_IMPLICIT_ACTIVITY_GALLERY:
                case Constants.RC_START_IMPLICIT_ACTIVITY_CAMERA:
                    if (mAttachFileUtil != null) {
                        mAttachFileUtil.onActivityResult(requestCode, resultCode, data);
                    }
                    break;

                case Constants.RC_START_ACTIVITY_SELECT_COUNTRY:
                    if (data != null) {
                        CountryInfo countryInfo = data.getParcelableExtra(Constants.KEY_COUNTRY_INFO);
                        if (countryInfo != null) {
                            int reqCode = data.getIntExtra(Constants.KEY_REQ_CODE, -1);
                            switch (reqCode) {

                                case SELECT_COUNTRY_DIAL_CODE:
                                    mPhoneCodeCountryInfo = countryInfo;
                                    mPhoneDialCodeEditText.setText(mPhoneCodeCountryInfo.dialCode);
                                    mPhoneDialCodeEditText.setError(null);
                                    if (TextUtils.isEmpty(mPhoneEditText.getText())) {
                                        mPhoneEditText.requestFocus();
                                        AndroidUtil.showKeyPad(this, mPhoneEditText);

                                    }
                                    break;

                                case SELECT_WHERE_TO_PLANT_COUNTRY:
                                    mPlantTreeCountry = countryInfo;
                                    mCountryEditText.setText(mPlantTreeCountry.countryName);
                                    mCountryEditText.setError(null);
                                    mRegisterButton.requestFocus();
                                    break;

                                case SELECT_ADDRESS_COUNTRY:
                                    mAddressCountryInfo = countryInfo;
                                    mAddressCountryEditText.setText(mAddressCountryInfo.countryName);
                                    mAddressCountryEditText.setError(null);
                                    break;

                            }
                        }

                    }
                    break;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (mAttachFileUtil != null) {
            mAttachFileUtil.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    @Override
    public void onNewFile(@NonNull AttachFileUtil.FileType fileType, String attachedVia, Uri fileUri, File file) {
        Log.d(TAG, "onNewFile() called with: fileType = [" + fileType + "], attachedVia = ["
                + attachedVia + "], fileUri = [" + fileUri + "], file = [" + file + "]");

        this.mAttachedFileUri = fileUri;
        this.mAttachedFile = file;
        this.mAttachedFileType = fileType;

        if (mAttachedFileUri != null) {
            mImageLoader.displayImage(String.valueOf(mAttachedFileUri), mRegisterImage);
        } else {
            mRegisterImage.setImageResource(R.mipmap.ic_ad_image);
        }
        if (mAttachedFile == null && fileUri != null) {
            AndroidUtil.showToast(this, "File path could not be fetched.");
        }


    }

    @Override
    public void onError(@NonNull String errorMessage) {
        Log.e(TAG, "Attach File: errorMessage = [" + errorMessage + "]");
    }


    @Override
    public void onValidResponse(@NonNull ResponseId responseId, @NonNull Object responseBody, @NonNull Response response) {

        switch (responseId) {
            case REGISTER_PHONE:
                final RegisterResponse registerResponse = (RegisterResponse) responseBody;
                afterRegister(registerResponse);
                mProgressDialogUtil.dismissProgressDialog();
                mRegisterButton.setEnabled(true);
               /* CometChatUtil.getInstance().createCometChatUser(this,
                        registerResponse.userId, mFirstName + " " + mLastName,
                        null,
                        new CometChatResponseListener() {
                            @Override
                            public void onCometChatResponse(ResponseId responseId, boolean isSuccess, JSONObject response) {
                                Log.d(TAG, "onCometChatResponse() called with: responseId = [" + responseId + "], isSuccess = [" + isSuccess + "], response = [" + response + "]");

                            }
                        });*/
                break;
        }

    }

    private void afterRegister(RegisterResponse responseBody) {
        if (responseBody != null) {
            final String userId = responseBody.userId;

            Runnable okButtonAction = null;

            if (!TextUtils.isEmpty(mRegisterBy)) {

                switch (mRegisterBy) {

                    case Constants.REGISTER_BY_EMAIL:
                        okButtonAction = new Runnable() {
                            @Override
                            public void run() {
                                goToLoginPage(true);
                            }
                        };
                        break;

                    case Constants.REGISTER_BY_PHONE:
                        okButtonAction = new Runnable() {
                            @Override
                            public void run() {
                                Intent in = new Intent(RegisterActivity.this, OtpActivity.class);
                                if (!TextUtils.isEmpty(mPhone)) {
                                    in.putExtra(Constants.KEY_PHONE, mPhoneDialCode + mPhone);
                                }
                                in.putExtra(Constants.KEY_USER_ID_REGISTER, userId);
                                startActivity(in);
                                finishAffinity();
                            }
                        };

                        break;

                }

            }
            mDialogUtil.showOkAlertDialog(this, responseBody.message, okButtonAction,
                    false);

        }

    }

    @Override
    public void onInvalidResponse(@NonNull ResponseId responseId, @NonNull String errorMessage, Response response) {
        switch (responseId) {
            case REGISTER_PHONE:
                mRegisterButton.setEnabled(true);
                break;
        }

        mProgressDialogUtil.dismissProgressDialog();
        if (response == null || !response.isSuccessful()) {
            mDialogUtil.showErrorDialog(this, errorMessage);
        } else {
            mDialogUtil.showOkAlertDialog(this, errorMessage);
        }

    }
}
