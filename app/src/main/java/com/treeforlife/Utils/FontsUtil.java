package com.treeforlife.Utils;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import java.util.HashMap;

/**
 * Created by TheAppsmiths on 3/21/2018.
 */
/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class FontsUtil {

    private static final String TAG="FontsUtil";
    private static final String ROBOTO_BOLD= "Roboto-Bold.ttf";
    private static final String ROBOTO_LIGHT= "Roboto-Light.ttf";
    private static final String ROBOTO_REGULAR_WEBFONT= "Roboto-Regular-webfont.ttf";
    private static final String TITILLIUM_WEB_BOLD= "TitilliumWeb-Bold.ttf";
    private static final String TITILLIUM_WEB_LIGHT= "TitilliumWeb-Light.ttf";
    private static final String TITILLIUM_WEB_REGULAR= "TitilliumWeb-Regular.ttf";
    private static final String TITILLIUM_WEB_SEMIBOLD= "TitilliumWeb-SemiBold.ttf";
    private static HashMap<String, Typeface> sFontStore = new HashMap<>();

    public static Typeface getTypeface(@NonNull Context context, int fontIndex) {
        return getTypeface(context, FontName.getFontName(fontIndex));
    }

    public static Typeface getTypeface(@NonNull Context context, FontName fontName) {
        if (context != null && fontName != null) {
            return getTypeface(context, fontName.mFontName);

        }
        return null;
    }


    private static Typeface getTypeface(@NonNull Context context, @NonNull String fontName) {
        Typeface typeface = null;
        if (context != null && !TextUtils.isEmpty(fontName)) {
            typeface = sFontStore.get(fontName);

            if (typeface == null) {
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/" + fontName);
                if (typeface != null) {
                    sFontStore.put(fontName, typeface);
                    Log.w(TAG, "getTypeface: " + fontName + " has been stored");
                }
            }
        }
        return typeface;
    }

    public enum FontName {

        ROBOTO_BOLD(FontsUtil.ROBOTO_BOLD),
        ROBOTO_LIGHT(FontsUtil.ROBOTO_LIGHT),
        ROBOTO_REGULAR_WEBFONT(FontsUtil.ROBOTO_REGULAR_WEBFONT),
        TITILLIUM_WEB_BOLD(FontsUtil.TITILLIUM_WEB_BOLD),
        TITILLIUM_WEB_LIGHT(FontsUtil.TITILLIUM_WEB_LIGHT),
        TITILLIUM_WEB_REGULAR(FontsUtil.TITILLIUM_WEB_REGULAR),
        TITILLIUM_WEB_SEMIBOLD(FontsUtil.TITILLIUM_WEB_SEMIBOLD);

         String mFontName;
        FontName(String fontName) {
            mFontName = fontName;
        }

       static FontName getFontName(int fontIndex) {

            switch (fontIndex) {
                case 0:
                    return ROBOTO_BOLD;

                case 1:
                    return ROBOTO_LIGHT;

                case 2:
                    return ROBOTO_REGULAR_WEBFONT;

                case 3:
                    return TITILLIUM_WEB_BOLD;

                case 4:
                    return TITILLIUM_WEB_LIGHT;

                case 5:
                    return TITILLIUM_WEB_REGULAR;

                case 6:
                    return TITILLIUM_WEB_SEMIBOLD;

                default:
                    return null;
            }
        }
    }
}
