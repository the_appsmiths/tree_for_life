package com.treeforlife.retrofit.responses;

import com.google.gson.annotations.SerializedName;
import com.treeforlife.dataobjects.TreeInfo;

/**
 * Created by TheAppsmiths on 4/20/2018.
 *
 * @author TheAppsmiths
 */

public class TreeInfoResponse extends WebServiceResponse {

    @SerializedName("data")
    public TreeInfo treeInfo;
}
