package com.treeforlife.dataobjects;

import com.google.gson.annotations.SerializedName;

/**
 * Created by TheAppsmiths on 4/5/2018.
 */

public class AlbumImage {
    @SerializedName("image_id")
    public int imageId;

    @SerializedName("image_description")
    public String imageDescription;

    @SerializedName("image_url")
    public String imageUrl;

    @SerializedName("is_shared")
    public String shared;

    @SerializedName("album_id")
    public int albumId;
}
