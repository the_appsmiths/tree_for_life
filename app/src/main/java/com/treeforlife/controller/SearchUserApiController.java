package com.treeforlife.controller;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;

import com.treeforlife.Utils.AndroidUtil;
import com.treeforlife.Utils.DialogUtil;
import com.treeforlife.Utils.ProgressDialogUtil;
import com.treeforlife.Utils.ValidationUtil;
import com.treeforlife.dataobjects.SearchedTflUser;
import com.treeforlife.dataobjects.TFLUser;
import com.treeforlife.retrofit.ResponseId;
import com.treeforlife.retrofit.RetrofitManager;
import com.treeforlife.retrofit.RetrofitResponseValidator;
import com.treeforlife.retrofit.responses.SearchTflUserResponse;

import java.net.SocketTimeoutException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TheAppsmiths on 4/12/2018.
 *
 * @author TheAppsmiths
 */

/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class SearchUserApiController implements RetrofitResponseValidator.ValidationListener {
    private final String TAG;

    private final Fragment mFragment;
    private final Context mContext;
    private final Activity mActivity;
    private final TFLUser mTflUser;
    private final ResultCallback mCallback;

    private boolean isSearching;

    private DialogUtil mDialogUtil;
    private ProgressDialogUtil mProgressDialogUtil;

    public SearchUserApiController(String tag, Fragment fragment, Context context,
                                   Activity activity, TFLUser tflUser, ResultCallback mCallback) {
        this.TAG = tag;
        this.mFragment = fragment;
        this.mContext = context;
        this.mActivity = activity;
        this.mTflUser = tflUser;
        this.mCallback = mCallback;

        this.mDialogUtil = DialogUtil.getInstance();
        this.mProgressDialogUtil = ProgressDialogUtil.getInstance();
    }

    public boolean searchUser(@NonNull String treeIdOrEmail) {
        return searchUser(treeIdOrEmail, true);
    }

    public boolean searchUser(@NonNull String treeIdOrEmail, boolean selfSearchEnabled) {
        if (ValidationUtil.isValidEmail(treeIdOrEmail)) {
            return searchUser(treeIdOrEmail, null, null, selfSearchEnabled);
        } else if (ValidationUtil.isValidPhoneNumber(treeIdOrEmail, true, true)) {
            return searchUser(null, null, treeIdOrEmail, selfSearchEnabled);
        } else {
            return searchUser(null, treeIdOrEmail, null, selfSearchEnabled);
        }
    }

    public boolean searchUser(String email, String treeId, String phoneNumber, boolean selfSearchEnabled) {
        if (!isSearching && isValidData(email, treeId, phoneNumber, selfSearchEnabled)
                && AndroidUtil.hasInternetConnectivity(mContext)) {

            mProgressDialogUtil.showProgressDialog(mActivity);
            isSearching = true;

            Call<SearchTflUserResponse> mSearchUserCall = RetrofitManager.getRetrofitWebService()
                    .searchUser(mTflUser.userId, email, treeId, phoneNumber);

            mSearchUserCall.enqueue(new Callback<SearchTflUserResponse>() {
                @Override
                public void onResponse(Call<SearchTflUserResponse> call, Response<SearchTflUserResponse> response) {
                    new RetrofitResponseValidator(ResponseId.SEARCH_TFL_USER, response,
                            SearchUserApiController.this);
                }

                @Override
                public void onFailure(Call<SearchTflUserResponse> call, Throwable t) {
                    String errorMessage = "";
                    if (t != null) {
                        Log.e(TAG, "onFailure: Error : " + t.getMessage());
                        if (t instanceof SocketTimeoutException) {
                            errorMessage = "Please make sure that your device has an active internet connection.";
                        }
                    }

                    mProgressDialogUtil.dismissProgressDialog();
                    isSearching = false;
                    if (mFragment != null && !mFragment.isVisible()) {
                        return;
                    }

                    if (TextUtils.isEmpty(errorMessage)) {
                        AndroidUtil.showErrorToast(mContext);
                    } else {
                        mDialogUtil.showErrorDialog(mContext, errorMessage);
                    }
                }
            });
            return true;
        }
        return false;
    }

    private boolean isValidUser() {
        if (mTflUser == null) {
            AndroidUtil.showToast(mContext, "Invalid User");
            return false;

        } else if (TextUtils.isEmpty(mTflUser.userId)) {
            AndroidUtil.showToast(mContext, "You need to Login !");
            return false;
        }

        return true;
    }

    private boolean isValidData(String email, String treeId, String phoneNumber, boolean selfSearchEnabled) {
        if (TextUtils.isEmpty(email)
                && TextUtils.isEmpty(treeId)
                && TextUtils.isEmpty(phoneNumber)) {
            AndroidUtil.showToast(mContext, "Please provide Email,Phone Number or Tree ID to search a user.");
            return false;

        }
        if (isValidUser()) {

            if (!selfSearchEnabled &&
                    !TextUtils.isEmpty(treeId) && treeId.equals(mTflUser.treeId)) {
                AndroidUtil.showToast(mContext, "this is your Tree ID, search with another Tree ID.");
                return false;
            }

        } else {
            return false;
        }

        return true;
    }

    @Override
    public void onValidResponse(@NonNull ResponseId responseId, @NonNull Object responseBody, @NonNull Response response) {
        mProgressDialogUtil.dismissProgressDialog();

        switch (responseId) {

            case SEARCH_TFL_USER:
                SearchTflUserResponse serviceResponse = (SearchTflUserResponse) responseBody;
                AndroidUtil.showToast(mContext, serviceResponse.message);
                if (mCallback != null) {
                    mCallback.onSearchUserApiResult(serviceResponse.searchedTflUser);
                }
                isSearching = false;
                break;
        }
    }

    @Override
    public void onInvalidResponse(@NonNull ResponseId responseId, @NonNull String errorMessage, Response response) {
        mProgressDialogUtil.dismissProgressDialog();

        switch (responseId) {

            case SEARCH_TFL_USER:
                isSearching = false;
                break;
        }

        if (mFragment != null && !mFragment.isVisible()) {
            return;
        }

        if (response == null || !response.isSuccessful()) {
            mDialogUtil.showErrorDialog(mActivity, errorMessage);
        } else {
            mDialogUtil.showOkAlertDialog(mActivity, errorMessage);
        }
    }

    public interface ResultCallback {
        void onSearchUserApiResult(SearchedTflUser searchedTflUser);
    }
}
