package com.treeforlife.services.fcm;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by TheAppsmiths on 2/16/2018.
 *
 * @author TheAppsmiths
 * @link : https://developers.google.com/cloud-messaging/android/android-migrate-fcm
 */

public class FcmInstanceIdService extends FirebaseInstanceIdService {
    private static final String TAG = "FcmInstanceIdService";

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is also called
     * when the InstanceID token is initially generated, so this is where
     * you retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        Log.w(TAG, "onTokenRefresh() called");
        FcmUtil.setFcmIdAndToken(this);

        // TODO: Write code to send any registration to your app's servers.
    }
}
