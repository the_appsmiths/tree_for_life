package com.treeforlife.Utils;

import android.content.Context;
import android.support.annotation.NonNull;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.CircleBitmapDisplayer;
import com.treeforlife.R;

/**
 * Created by TheAppsmiths on 1/6/2018.
 *
 * @author TheAppsmiths
 * @link : https://github.com/nostra13/Android-Universal-Image-Loader
 */

/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class ImageLoaderUtil {

    public static ImageLoader getLoader(@NonNull Context context) {
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .build();
        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);
        return imageLoader;
    }


    public static DisplayImageOptions getDefaultImageOption() {
        return new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.img_place_holder)
                .showImageForEmptyUri(R.drawable.img_place_holder)
                .showImageOnFail(R.drawable.img_place_holder)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();
    }

    public static DisplayImageOptions getDefaultCircularImageOption() {
        return new DisplayImageOptions.Builder()
                .imageScaleType(ImageScaleType.EXACTLY)
                .showImageOnLoading(R.drawable.img_place_holder)
                .showImageForEmptyUri(R.drawable.img_place_holder)
                .showImageOnFail(R.drawable.img_place_holder)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .displayer(new CircleBitmapDisplayer())
                .build();
    }

    public static DisplayImageOptions getDefaultProfileImageOption() {
        return new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.dummy_profile_image)
                .showImageForEmptyUri(R.drawable.dummy_profile_image)
                .showImageOnFail(R.drawable.dummy_profile_image)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();
    }

    public static DisplayImageOptions getDefaultHeaderImageOption() {
        return new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.appimage_1024_500)
                .showImageForEmptyUri(R.drawable.appimage_1024_500)
                .showImageOnFail(R.drawable.appimage_1024_500)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();
    }

    public static DisplayImageOptions getDefaultTutorialImageOption() {
        return new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.tutorial_dummy_image)
                .showImageForEmptyUri(R.drawable.tutorial_dummy_image)
                .showImageOnFail(R.drawable.tutorial_dummy_image)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();
    }

    public static DisplayImageOptions getDefaultAlbumImageOption() {
        return new DisplayImageOptions.Builder()
                .showImageOnLoading(android.R.drawable.ic_menu_camera)
                .showImageForEmptyUri(android.R.drawable.ic_menu_camera)
                .showImageOnFail(android.R.drawable.ic_menu_camera)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();
    }
}
