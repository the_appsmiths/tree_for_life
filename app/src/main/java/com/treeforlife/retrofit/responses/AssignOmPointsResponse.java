package com.treeforlife.retrofit.responses;

import com.google.gson.annotations.SerializedName;
import com.treeforlife.dataobjects.Comment;

/**
 * Created by TheAppsmiths on 4/5/2018.
 *
 * @author TheAppsmiths
 */

public class AssignOmPointsResponse extends WebServiceResponse {

    @SerializedName("comment")
    public Comment comment;

}
