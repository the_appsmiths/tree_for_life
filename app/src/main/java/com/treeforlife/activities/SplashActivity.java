package com.treeforlife.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;

import com.crashlytics.android.Crashlytics;
import com.treeforlife.R;
import com.treeforlife.commons.Constants;
import com.treeforlife.commons.SharedPrefHelper;
import com.treeforlife.controller.GetCountriesApiController;
import com.treeforlife.dataobjects.TFLUser;

import io.fabric.sdk.android.Fabric;

public class SplashActivity extends AppCompatActivity {
    private static final String TAG = "SplashActivity";
    private SharedPrefHelper mSharedPrefHelper;
    private TFLUser mTFLUser;
    private boolean hasBackPressed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        makeFullScreen();
        initComponents();
        setContentView(R.layout.activity_splash);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                nextStepAfterSplash();
            }
        }, 3 * 1000);

    }

    private void makeFullScreen() {
        Window window = getWindow();
        if (window != null) {
            View decorView = window.getDecorView();
            if (decorView != null) {
                decorView.setSystemUiVisibility(
                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                                | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                                | View.SYSTEM_UI_FLAG_IMMERSIVE);
            }
        }

    }

    //method to initialize the component
    private void initComponents() {
        mSharedPrefHelper = SharedPrefHelper.getInstance();
        GetCountriesApiController.getInstance().loadData(this);
    }

    //method to go to the required screen after splash wait time over
    private void nextStepAfterSplash() {
        if (hasBackPressed) {
            return;
        }
        mSharedPrefHelper.resetEmailPassword(SplashActivity.this);
        mTFLUser = mSharedPrefHelper.getLoginResponse(SplashActivity.this);
        Intent nextStepIntent;
        if (mTFLUser == null) {
            nextStepIntent = new Intent(this, LoginActivity.class);

        } else if (!hasUserWishData()) {
            nextStepIntent = new Intent(this, WishFormActivity.class);
            nextStepIntent.putExtra(Constants.KEY_TFL_USER, mTFLUser);

        } else {
            nextStepIntent = new Intent(this, HomeActivity.class);
            nextStepIntent.putExtra(Constants.KEY_TFL_USER, mTFLUser);

        }
        startActivity(nextStepIntent);
        finish();
    }

    private boolean hasUserWishData() {
        return mTFLUser != null
                && !TextUtils.isEmpty(mTFLUser.wishStatus)
                && !TextUtils.isEmpty(mTFLUser.wishStatus);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        hasBackPressed = true;
    }
}
