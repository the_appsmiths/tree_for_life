package com.treeforlife.retrofit.responses;

import com.google.gson.annotations.SerializedName;



public class WebServiceResponse {
    @SerializedName("status")
    public boolean status;
    @SerializedName("message")
    public String message;
    @SerializedName("error_code")
    public String errorCode;
}
