package com.treeforlife.services.fcm;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.treeforlife.R;
import com.treeforlife.Utils.FragmentUtil;
import com.treeforlife.Utils.NotificationsUtil;
import com.treeforlife.activities.HomeActivity;
import com.treeforlife.commons.Constants;
import com.treeforlife.commons.SharedPrefHelper;
import com.treeforlife.dataobjects.TFLUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import utils.CCNotificationHelper;

/**
 * Created by TheAppsmiths on 2/16/2018.
 *
 * @author TheAppsmiths
 * @link : https://developers.google.com/cloud-messaging/android/android-migrate-fcm
 */

public class FcmListenerService extends FirebaseMessagingService {
    private static final String TAG = "FcmListenerService";
    private SharedPrefHelper mSharedPrefHelper;

    private void ensureSharePefHelper() {
        if (mSharedPrefHelper == null) {
            mSharedPrefHelper = SharedPrefHelper.getInstance();
        }
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        //super.onMessageReceived(remoteMessage);
        if (remoteMessage == null) {
            return;
        }

        Log.e(TAG, "onMessageReceived() called with: remoteMessage = [" + remoteMessage + "]");

        String from = remoteMessage.getFrom();
        Map remoteMessageDataMap = remoteMessage.getData();
        RemoteMessage.Notification remoteMessageNotification = remoteMessage.getNotification();
        String remoteMessageNotificationBody = null;
        if (remoteMessageNotification != null) {
            remoteMessageNotificationBody = remoteMessageNotification.getBody();
        }

        Log.e(TAG, "onMessageReceived() called with: from = [" + from + "]");
        Log.e(TAG, "onMessageReceived: remoteMessageDataMap = [ " + remoteMessageDataMap + " ]");
        Log.e(TAG, "onMessageReceived: remoteMessageNotification = [ " + remoteMessageNotification + " ]");
        Log.e(TAG, "onMessageReceived: remoteMessageNotificationBody = [ " + remoteMessageNotificationBody + " ]");
        ensureSharePefHelper();
        TFLUser mTflUser = mSharedPrefHelper.getLoginResponse(this);
        if (mTflUser == null) {
            Log.e(TAG, "user is not logged in ! PUSH_RECEIVED_UNEXPECTEDLY on this device");
            return;
        }

        CCNotificationHelper.processCCNotificationData(this, remoteMessage,
                R.mipmap.ic_launcher, R.mipmap.ic_app_notification);
        String jsonData = null;
        if (remoteMessageDataMap != null && remoteMessageDataMap.containsKey("body")) {
            jsonData = String.valueOf(remoteMessageDataMap.get("body"));
        }
        if (TextUtils.isEmpty(jsonData)) {
            jsonData = remoteMessageNotificationBody;
        }

        processReceivedJsonData(jsonData);
    }

    //json processing of received data
    private void processReceivedJsonData(String jsonData) {

        if (!TextUtils.isEmpty(jsonData)) {
            try {
                JSONObject jsonObject = new JSONObject(jsonData);
                int notificationType = jsonObject.optInt("push_notification_type");
                String notificationTitle = jsonObject.optString("push_notification_title");
                String notificationMessage = jsonObject.optString("push_notification_message");
                String senderTreeId = jsonObject.optString("sender_tree_id");
                if (TextUtils.isEmpty(senderTreeId)) {
                    senderTreeId = jsonObject.optString("friend_tree_id");
                }

                if (TextUtils.isEmpty(notificationTitle)) {
                    notificationTitle = getString(R.string.app_name) + " server notification.";
                }
                if (TextUtils.isEmpty(notificationMessage)) {
                    notificationMessage = "There is a notification from server.";
                }
                Intent intent = null;

                switch (notificationType) {

                    case Constants.PUSH_NOTIFICATION_FRIEND_REQ_RECEIVED:
                    case Constants.PUSH_NOTIFICATION_FRIEND_REQ_ACCEPTED:
                    case Constants.PUSH_NOTIFICATION_NEW_IMAGE:
                    case Constants.PUSH_NOTIFICATION_NEW_VIDEO:
                        intent = new Intent(this, HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra(Constants.KEY_HAS_PUSH_NOTIFICATION, true);
                        intent.putExtra(Constants.KEY_PUSH_NOTIFICATION_TYPE, notificationType);
                        intent.putExtra(Constants.KEY_TREE_ID, senderTreeId);
                        intent.putExtra(Constants.KEY_GO_TO_FRAGMENT_NAME, FragmentUtil.FRAGMENT_USER_PROFILE);
                        break;

                    case Constants.PUSH_NOTIFICATION_RECEIVED_OM_POINTS:
                        intent = new Intent(this, HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra(Constants.KEY_HAS_PUSH_NOTIFICATION, true);
                        intent.putExtra(Constants.KEY_PUSH_NOTIFICATION_TYPE, notificationType);
                        intent.putExtra(Constants.KEY_GO_TO_FRAGMENT_NAME, FragmentUtil.FRAGMENT_CONVERTER);
                        break;

                    case Constants.PUSH_NOTIFICATION_TREE_STATE_CHANGED:
                        intent = new Intent(this, HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra(Constants.KEY_HAS_PUSH_NOTIFICATION, true);
                        intent.putExtra(Constants.KEY_PUSH_NOTIFICATION_TYPE, notificationType);
                        intent.putExtra(Constants.KEY_TREE_ID, senderTreeId);
                        intent.putExtra(Constants.KEY_GO_TO_FRAGMENT_NAME, FragmentUtil.FRAGMENT_TREE);
                        break;
                    case Constants.PUSH_NOTIFICATION_USER_BLOCKED:
                        intent = new Intent(this, HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra(Constants.KEY_HAS_PUSH_NOTIFICATION, true);
                        intent.putExtra(Constants.KEY_PUSH_NOTIFICATION_TYPE, notificationType);
                        intent.putExtra(Constants.KEY_GO_TO_FRAGMENT_NAME, FragmentUtil.FRAGMENT_HOME);
                        break;
                    case Constants.PUSH_NOTIFICATION_NEW_LOGIN:
                        intent = new Intent(this, HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra(Constants.KEY_HAS_PUSH_NOTIFICATION, true);
                        intent.putExtra(Constants.KEY_PUSH_NOTIFICATION_TYPE, notificationType);
                        intent.putExtra(Constants.KEY_GO_TO_FRAGMENT_NAME, FragmentUtil.FRAGMENT_HOME);
                        break;

                    default:
                        Log.e(TAG, "empty/undefined PUSH_NOTIFICATION_TYPE");
                        return;

                }

                if (intent != null) {
                    ensureSharePefHelper();
                    mSharedPrefHelper.increaseNotificationCount(this);
                    intent.putExtra("text", notificationMessage);
                    intent.putExtra("jsonData", jsonData);
                    Log.e(TAG, "onMessageReceived: notificationTitle = " + notificationTitle);
                    Log.e(TAG, "onMessageReceived: notificationMessage = " + notificationMessage);
                    NotificationsUtil.showNotification(this, notificationTitle, notificationMessage, intent);

                    Intent broadCastIntent = new Intent(Constants.ACTION_RECEIVED_PUSH_NOTIFICATION);
                    broadCastIntent.putExtras(intent);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(broadCastIntent);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
