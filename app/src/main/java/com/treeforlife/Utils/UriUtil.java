package com.treeforlife.Utils;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

/**
 * Created by TheAppsmiths on 3/13/2018.
 *
 * @author TheAppsmiths
 * @link : https://developer.android.com/reference/java/net/URI.html
 */
/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
@SuppressWarnings("ALL")
public class UriUtil {
    private static final String TAG = "UriUtil";

    public static void dumpUri(String TAG, Uri uri) {
        if (uri != null) {
            Log.w(TAG, "dumpUri: Uri Scheme = " + uri.getScheme());
            Log.w(TAG, "dumpUri: SchemeSpecificPart() = " + uri.getSchemeSpecificPart());
            Log.w(TAG, "dumpUri: Uri Authority = " + uri.getAuthority());
            Log.w(TAG, "dumpUri: Uri Host = " + uri.getHost());
            Log.w(TAG, "dumpUri: Uri Port = " + uri.getPort());
            Log.w(TAG, "dumpUri: Uri UserInfo = " + uri.getUserInfo());
            Log.w(TAG, "dumpUri: Uri Path = " + uri.getPath());
            Log.w(TAG, "dumpUri: Uri LastPathSegment()= " + uri.getLastPathSegment());
            Log.w(TAG, "dumpUri: Uri getQuery = " + uri.getQuery());

            try {
                Log.w(TAG, "dumpUri: Uri PathSegments =" + Arrays.toString(uri.getPathSegments().toArray()));
            } catch (Exception e) {

            }
            try {
                Log.w(TAG, "dumpUri: Uri " + Arrays.toString(uri.getQueryParameterNames().toArray()));
            } catch (Exception e) {

            }

            Log.w(TAG, "dumpUri: Uri isOpaque = " + uri.isOpaque());
            Log.w(TAG, "dumpUri: Uri isHierarchical = " + uri.isHierarchical());
            Log.w(TAG, "dumpUri: Uri isAbsolute = " + uri.isAbsolute());
            Log.w(TAG, "dumpUri: Uri isRelative = " + uri.isRelative());

        }

    }


    public static String getFilePath(@NonNull Context context, @NonNull Uri uri) {
        String path = null, errorMessage = null, displayName = null;

        if (uri != null) {

            if (uri.isHierarchical()) {
                String uriAuthority = uri.getAuthority();
                String uriScheme = uri.getScheme();
                String uriPath = uri.getPath();
                String uriLastPathSegment = uri.getLastPathSegment();

                if (!TextUtils.isEmpty(uriScheme)) {
                    switch (uriScheme) {

                        case "file":
                            path = uriPath;
                            break;


                        case "content":
                        default:

                            if (context != null) {
                                ContentResolver contentResolver = context.getContentResolver();
                                if (contentResolver != null) {
                                    Cursor cursor = contentResolver.query(uri,
                                            null,
                                            null,
                                            null,
                                            null);
                                    if (cursor != null) {
                                        DatabaseUtils.dumpCursor(cursor);
                                        if (cursor.moveToFirst()) {
                                            int columnIndex = cursor.getColumnIndex("_data");
                                            if (columnIndex != -1) {
                                                path = cursor.getString(columnIndex);
                                            }
                                            columnIndex = cursor.getColumnIndex("_display_name");
                                            if (columnIndex != -1) {
                                                displayName = cursor.getString(columnIndex);
                                            }
                                        }
                                        cursor.close();
                                    }
                                }
                            }


                            if (TextUtils.isEmpty(path)) {

                                if (!TextUtils.isEmpty(uriAuthority)) {

                                    switch (uriAuthority) {
                                        case "com.android.externalstorage.documents":
                                            String[] split = null;
                                            if (!TextUtils.isEmpty(uriLastPathSegment)) {
                                                split = uriLastPathSegment.split(":");
                                            }
                                            if (split == null || split.length < 2) {
                                                String docId = DocumentsContract.getDocumentId(uri);
                                                if (!TextUtils.isEmpty(docId))
                                                    split = docId.split(":");
                                            }

                                            if (split != null && split.length >= 2) {
                                                if ("primary".equalsIgnoreCase(split[0])) {
                                                    path = Environment.getExternalStorageDirectory() + "/" + split[1];
                                                }
                                            }

                                            break;

                                        case "com.android.providers.downloads.documents":
                                            Uri contentUri;
                                            try {
                                                final String id = DocumentsContract.getDocumentId(uri);
                                                contentUri = ContentUris.withAppendedId(
                                                        Uri.parse("content://downloads/public_downloads"),
                                                        Long.valueOf(id));
                                                path = getFilePath(context, contentUri);
                                            } catch (NumberFormatException e) {
                                                e.printStackTrace();
                                            }

                                            break;

                                        case "com.android.providers.media.documents":
                                            split = null;
                                            if (!TextUtils.isEmpty(uriLastPathSegment)) {
                                                split = uriLastPathSegment.split(":");
                                            }
                                            if (split == null || split.length < 2) {
                                                String docId = DocumentsContract.getDocumentId(uri);
                                                if (!TextUtils.isEmpty(docId))
                                                    split = docId.split(":");
                                            }

                                            contentUri = null;
                                            if (split != null && split.length >= 2) {
                                                if ("image".equalsIgnoreCase(split[0])) {
                                                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                                                } else if ("video".equalsIgnoreCase(split[0])) {
                                                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                                                } else if ("audio".equalsIgnoreCase(split[0])) {
                                                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                                                }

                                                final String selection = "_id=?";
                                                final String[] selectionArgs = new String[]{
                                                        split[1]
                                                };

                                                if (contentUri != null) {

                                                    if (context != null) {
                                                        ContentResolver contentResolver = context.getContentResolver();
                                                        if (contentResolver != null) {
                                                            Cursor cursor = contentResolver.query(contentUri,
                                                                    null,
                                                                    selection,
                                                                    selectionArgs,
                                                                    null);
                                                            if (cursor != null) {
                                                                DatabaseUtils.dumpCursor(cursor);
                                                                if (cursor.moveToFirst()) {
                                                                    int columnIndex = cursor.getColumnIndex("_data");
                                                                    if (columnIndex != -1) {
                                                                        path = cursor.getString(columnIndex);
                                                                    }
                                                                }
                                                                cursor.close();
                                                            }
                                                        }
                                                    }


                                                }
                                            }

                                            break;

                                        case "com.google.android.apps.photos.contentprovider":
                                            if (context != null) {
                                                File storageDir = /*mActivity.getFilesDir()*/
                                                        context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);

                                                if (storageDir != null) {
                                                    String destPath = storageDir.getAbsolutePath()
                                                            + File.separator
                                                            + "temp_" + displayName;
                                                    try {
                                                        /*https://stackoverflow.com/questions/30527045/choosing-photo-using-new-google-photos-app-is-broken/30567302*/
                                                        InputStream is = context.getContentResolver().openInputStream(uri);
                                                        Bitmap bitmap = BitmapFactory.decodeStream(is);
                                                        FileOutputStream fileOutputStream = new FileOutputStream(destPath);
                                                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
                                                        fileOutputStream.flush();
                                                        fileOutputStream.close();
                                                        path = destPath;

                                                    } catch (IOException e) {
                                                        e.printStackTrace();
                                                    }


                                                }
                                            }


                                            break;
                                        case "com.google.android.apps.docs.storage":
                                        default:
                                            List<String> pathSegments = uri.getPathSegments();
                                            if (pathSegments != null) {
                                                int size = pathSegments.size();
                                                String temp = "";
                                                for (int i = 1; i < size; i++) {
                                                    if (!TextUtils.isEmpty(pathSegments.get(i))) {
                                                        temp += "/" + pathSegments.get(i);
                                                    }
                                                }
                                                if (!TextUtils.isEmpty(temp)) {
                                                    path = Environment.getExternalStorageDirectory() + temp;

                                                }
                                            }

                                            break;

                                    }

                                } else {
                                    errorMessage = "No Uri Authority.";
                                }
                            }


                    }
                }

            } else {
                errorMessage = "Uri isn't Hierarchical.";
            }
        } else {
            errorMessage = "Uri is Null.";
        }

        if (!TextUtils.isEmpty(errorMessage)) {
            Log.e(TAG, "getFilePath: errorMessage = " + errorMessage);
        }

        Log.e(TAG, "getFilePath: Uri = " + uri + " , path = " + path);
        return path;
    }

    public static void copyFromUriToFile(@NonNull Context context,
                                         @NonNull Uri uri,
                                         @NonNull UriUtilCallback callback
    ) throws IOException {

        if (context != null) {

            String fileName = "";
            ContentResolver contentResolver = context.getContentResolver();
            if (contentResolver != null) {
                Cursor cursor = contentResolver.query(uri,
                        null,
                        null,
                        null,
                        null);
                if (cursor != null) {
                    DatabaseUtils.dumpCursor(cursor);
                    if (cursor.moveToFirst()) {
                        int columnIndex = cursor.getColumnIndex(" _display_name");
                        if (columnIndex != -1) {
                            fileName = cursor.getString(columnIndex);
                        }
                    }
                    cursor.close();
                }
            }
            if (TextUtils.isEmpty(fileName)) {
                fileName = "uri_file_content_" + System.currentTimeMillis();
                File contentFile = new File(/*context.getFilesDir()*/
                        context.getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                        fileName);

                BufferedInputStream bis = new BufferedInputStream(new FileInputStream(uri.getPath()));
                BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(contentFile, false));
                byte[] buf = new byte[1024];
                bis.read(buf);
                do {
                    bos.write(buf);
                } while (bis.read(buf) != -1);

                try {
                    if (bis != null) bis.close();
                    if (bos != null) bos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            if (callback != null) {

            }
        }
    }

    public interface UriUtilCallback {
        void afterCopyingUriToFile(Uri uri, File file);
    }
}

