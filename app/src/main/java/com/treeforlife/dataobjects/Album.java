package com.treeforlife.dataobjects;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by TheAppsmiths on 4/5/2018.
 */

public class Album implements Parcelable {

    public static final Creator<Album> CREATOR = new Creator<Album>() {
        @Override
        public Album createFromParcel(Parcel in) {
            return new Album(in);
        }

        @Override
        public Album[] newArray(int size) {
            return new Album[size];
        }
    };
    @SerializedName("album_id")
    public int albumId;
    @SerializedName("album_title")
    public String albumTitle;
    @SerializedName("album_image")
    public String albumCoverImage;

    protected Album(Parcel in) {
        albumId = in.readInt();
        albumTitle = in.readString();
        albumCoverImage = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(albumId);
        dest.writeString(albumTitle);
        dest.writeString(albumCoverImage);
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
