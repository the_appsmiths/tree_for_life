package com.treeforlife.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.treeforlife.R;
import com.treeforlife.Utils.ImageLoaderUtil;
import com.treeforlife.dataobjects.Album;

import java.util.ArrayList;

/**
 * Created by TheAppsmiths on 3/20/2018.
 */

public class AdapterRvVaultAlbum extends RecyclerView.Adapter<AdapterRvVaultAlbum.MyHolder> {

    private VaultAlbumCallBack callBack;
    private ArrayList<Album> mAlbumList;

    public AdapterRvVaultAlbum(VaultAlbumCallBack CallBack) {
        this.callBack=CallBack;
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_vault_album, parent, false));
    }

    @Override
    public void onBindViewHolder(MyHolder holder, int position) {
        holder.bindHoler(position);

    }

    @Override
    public int getItemCount() {
        return (mAlbumList != null) ? mAlbumList.size() : 0 ;
    }

    public void updateAdapter(ArrayList<Album> mArrays) {
        this.mAlbumList =mArrays;
        notifyDataSetChanged();

    }


    public interface VaultAlbumCallBack{
        void clickItem(int position,View view);
        void removeItem(int position,View view);
    }
    
    //Holder view to show the data in adapter
    public class MyHolder extends RecyclerView.ViewHolder {
        private TextView mAlbumText;
        private View view;
        private Context mContext;
        private ImageView mRemoveImage,mAlbumImage;
        private Album mAlubum;
        public MyHolder(View itemView) {
            super(itemView);
            this.view=itemView;
            this.mContext=itemView.getContext();
            mAlbumText=itemView.findViewById(R.id.album_text);
            mRemoveImage=itemView.findViewById(R.id.remove_item_image);
            mAlbumImage=itemView.findViewById(R.id.image_album);
        }

        public void bindHoler(int position) {
            if (mAlbumList != null && mAlbumList.size() > position) {
                mAlubum = mAlbumList.get(position);
                mAlbumText.setText(mAlubum.albumTitle);
                ImageLoaderUtil.getLoader(mContext).displayImage(mAlubum.albumCoverImage, mAlbumImage);
            }
            setLisener(position);
        }

        private void setLisener(final int position) {
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (callBack != null)
                    {
                        callBack.clickItem(position,itemView);
                    }
                }
            });

            mRemoveImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callBack.removeItem(position,v);
                }
            });

        }
    }
}
