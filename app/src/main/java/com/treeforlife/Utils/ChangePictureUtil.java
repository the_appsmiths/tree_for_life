package com.treeforlife.Utils;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.util.Log;

import com.treeforlife.BuildConfig;
import com.treeforlife.fragments.BottomSheetFragment;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

/**
 * Created by TheAppsmiths on 3/13/2018.
 *
 * @author TheAppsmiths
 */
/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class ChangePictureUtil implements BottomSheetFragment.BottomSheetFragmentCallback,
        PermissionsManager.PermissionsResultCallback {
    private static final String TAG = "ChangePictureUtil";
    private static ChangePictureUtil sChangePictureUtil;
    public final int RC_CHANGE_PIC_BOTTOM_SHEET = 501;
    public final String[] CHANGE_PIC_BOTTOM_SHEET_MENU_ARRAY = new String[]
            {
                    "Camera",
                    "Album",
                    "Remove",
                    "Cancel"
            };
    private final int RC_START_IMPLICIT_ACTIVITY_ALBUM = 3001;
    private final int RC_START_IMPLICIT_ACTIVITY_CAMERA = 3002;
    private final BottomSheetFragment mBottomSheetFragment;
    private final PermissionsManager mPermissionsManager;

    private final DialogUtil mDialogUtil;
    private Activity mActivity;
    private ChangePictureUtilCallback mCallback;

    private Uri mImageUri;
    private File mImageFile;

    private int mChangePicViaOption = -1;
    private boolean hasGoneToAppInfo;


    private ChangePictureUtil() {

        this.mBottomSheetFragment = BottomSheetFragment.newInstance(TAG,
                RC_CHANGE_PIC_BOTTOM_SHEET,
                CHANGE_PIC_BOTTOM_SHEET_MENU_ARRAY);
        this.mBottomSheetFragment.setBottomSheetFragmentCallback(this);
        this.mPermissionsManager = PermissionsManager.getInstance();
        this.mDialogUtil = DialogUtil.getInstance();
    }

    public static ChangePictureUtil getInstance() {
        if (sChangePictureUtil == null) {
            sChangePictureUtil = new ChangePictureUtil();
        }
        return sChangePictureUtil;
    }


    public boolean showOptionsToChangePicVia(@NonNull Activity activity,
                                             @NonNull FragmentManager fragmentManager,
                                             @NonNull ChangePictureUtilCallback callback) {
        this.mActivity = activity;
        this.mCallback = callback;
        return showBottomSheetFragment(fragmentManager);

    }


    private boolean showBottomSheetFragment(@NonNull FragmentManager fragmentManager) {

        if (mBottomSheetFragment != null) {
            mBottomSheetFragment.show(fragmentManager, TAG);
            return true;
        }
        return false;
    }

    private void hideBottomSheetFragment() {

        if (mBottomSheetFragment != null) {
            mBottomSheetFragment.dismiss();
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        mPermissionsManager.parsePermissionsResult(requestCode, permissions, grantResults, mActivity, this);

    }

    @Override
    public void deniedPermissions(int requestCode, @NonNull String[] deniedPermissions) {
        Log.d(TAG, "deniedPermissions = [" + Arrays.toString(deniedPermissions) + "]");
        AndroidUtil.showToast(mActivity, "Required permission(s) was/were denied !");
        // mPermissionsManager.requestPermissions(requestCode, mActivity, deniedPermissions);

    }

    @Override
    public void grantedPermissions(int requestCode, @NonNull String[] grantedPermissions) {
        Log.d(TAG, "grantedPermissions = [" + Arrays.toString(grantedPermissions) + "]");
        switch (requestCode) {
            case PermissionsManager.REQUEST_PERMISSION_READ_EXTERNAL_STORAGE:
            case PermissionsManager.REQUEST_PERMISSIONS_CAMERA_WRITE_EXTERNAL_STORAGE:
                proceedToChangePic();
                break;
        }

    }

    @Override
    public void deniedAndNeverAskPermissions(int requestCode, @NonNull String[] deniedAndNeverAskPermissions) {
        Log.d(TAG, "deniedAndNeverAskPermissions = [" + Arrays.toString(deniedAndNeverAskPermissions) + "]");
        StringBuilder message = mPermissionsManager.getMessageForNeverAskPermissions(deniedAndNeverAskPermissions);

        if (!TextUtils.isEmpty(message)) {
            mDialogUtil.showOkAlertDialog(mActivity,
                    message.toString(),
                    new Runnable() {
                        @Override
                        public void run() {
                            mPermissionsManager.openAppInfoPage(mActivity);
                            hasGoneToAppInfo = true;


                        }
                    },true
            );
        }

    }

    public void onResume() {
        if (hasGoneToAppInfo
                && mPermissionsManager.isPermissionAlreadyGranted(mActivity,
                Manifest.permission.CAMERA)
                && mPermissionsManager.isPermissionAlreadyGranted(mActivity,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            hasGoneToAppInfo = false;
            proceedToChangePic();

        }
    }


    @Override
    public void onBottomSheetItemClick(int bottomSheetRequestCode, int itemPosition, String itemText) {
        hideBottomSheetFragment();
        switch (bottomSheetRequestCode) {
            case RC_CHANGE_PIC_BOTTOM_SHEET:
                mChangePicViaOption = itemPosition;
                proceedToChangePic();
                break;

        }
    }

    private void proceedToChangePic() {
        switch (mChangePicViaOption) {
            case 0:
                manageCameraAction();
                break;

            case 1:
                manageAlbumAction();
                break;

            case 2:
                mImageFile = null;
                mImageUri = null;
                if (mCallback != null) {
                    mCallback.onNewImage(false, mImageUri, mImageFile);
                }
                break;
        }

    }

    private void manageAlbumAction() {
        if (mActivity != null) {


            boolean hasPermission = !mPermissionsManager.requestPermission(mActivity,
                    Manifest.permission.READ_EXTERNAL_STORAGE);

            if (hasPermission) {
                Intent albumIntent = AndroidUtil.IntentHelper.getIntentToOpenGalleryImages();
                Intent chooser = AndroidUtil.IntentHelper.createChooserIntent(mActivity, albumIntent);
                mActivity.startActivityForResult(chooser, RC_START_IMPLICIT_ACTIVITY_ALBUM);

            }
        }
    }

    private void manageCameraAction() {
        if (mActivity != null) {
            boolean hasPermission =
                    !mPermissionsManager.hasCameraAndExternalStorageWritePermission(mActivity);

            if (hasPermission) {

                String fileName = "camera_profile_image_" + System.currentTimeMillis();


                File storageDir = /*mActivity.getFilesDir()*/
                        mActivity.getExternalFilesDir(Environment.DIRECTORY_PICTURES);


                try {
                    mImageFile = File.createTempFile(
                            fileName,   //prefix
                            ".jpg",         // suffix
                            storageDir       //directory
                    );

                    // mImageFile = new File(storageDir, fileName+".jpg");

                    mImageUri = FileProvider.getUriForFile(mActivity,
                            BuildConfig.APPLICATION_ID + ".provider",
                            mImageFile);

                    Intent cameraIntent = AndroidUtil.IntentHelper.getIntentToOpenCamera(mImageUri);
                    Intent chooser = AndroidUtil.IntentHelper.createChooserIntent(mActivity, cameraIntent);
                    mActivity.startActivityForResult(chooser, RC_START_IMPLICIT_ACTIVITY_CAMERA);

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        AndroidUtil.dumpIntent(TAG, data);

        switch (resultCode) {
            case Activity.RESULT_OK:
                switch (requestCode) {

                    case RC_START_IMPLICIT_ACTIVITY_ALBUM:
                        processAlbumResult(data);
                        break;

                    case RC_START_IMPLICIT_ACTIVITY_CAMERA:
                        processCameraResult(data);
                        break;
                }
                break;

            case Activity.RESULT_CANCELED:
                break;

        }

    }

    private void processAlbumResult(Intent data) {
        String errorMessage = null;
        if (data != null) {
            mImageUri = data.getData();
            if (mImageUri != null) {
                UriUtil.dumpUri(TAG + " : Album Result", mImageUri);
                String filepath = UriUtil.getFilePath(mActivity, mImageUri);
                if (!TextUtils.isEmpty(filepath)) {
                    mImageFile = new File(filepath);
                    if (mCallback != null) {
                        mCallback.onNewImage(false, mImageUri, mImageFile);
                    }

                }
            } else {
                errorMessage = "There is no uri of selected image data.";
            }
        } else {
            errorMessage = "There is no image data from album.";
        }

        if (!TextUtils.isEmpty(errorMessage)
                & mCallback != null) {
            mCallback.onError(errorMessage);
        }

    }

    private void processCameraResult(Intent data) {
        if (mCallback != null) {
            if (mImageUri == null && mImageFile == null) {
                mCallback.onError("could not capture the camera image result.");
            } else {
                mCallback.onNewImage(true, mImageUri, mImageFile);
            }
        }
    }

    public interface ChangePictureUtilCallback {
        void onNewImage(boolean isCameraImage, Uri imageUri, File imageFile);

        void onError(@NonNull String errorMessage);
    }


}
