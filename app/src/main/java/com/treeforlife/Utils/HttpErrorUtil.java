package com.treeforlife.Utils;

/**
 *copy from previous work
 *
 * @author TheAppsmiths
 */
/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class HttpErrorUtil {

    public static String getHttpCodeInfo(int httpCode) {
        String errorMessage;
        switch (httpCode) {

            case 200:
                errorMessage = "HTTP_OK";
                break;
            case 201:
                errorMessage = "HTTP_CREATED";
                break;
            case 202:
                errorMessage = "HTTP_ACCEPTED ";
                break;
            case 203:
                errorMessage = "HTTP_NOT_AUTHORITATIVE";
                break;
            case 204:
                errorMessage = "HTTP_NO_CONTENT";
                break;
            case 205:
                errorMessage = "HTTP_RESET";
                break;
            case 206:
                errorMessage = "HTTP_PARTIAL";
                break;


            case 300:
                errorMessage = "HTTP_MULT_CHOICE";
                break;
            case 301:
                errorMessage = "HTTP_MOVED_PERM";
                break;
            case 302:
                errorMessage = "HTTP_MOVED_TEMP";
                break;
            case 303:
                errorMessage = "HTTP_SEE_OTHER";
            case 304:
                errorMessage = "HTTP_NOT_MODIFIED";
                break;
            case 305:
                errorMessage = "HTTP_USE_PROXY";
                break;


            case 400:
                errorMessage = "HTTP_BAD_REQUEST";
                break;
            case 401:
                errorMessage = "HTTP_UNAUTHORIZED";
                break;
            case 402:
                errorMessage = "HTTP_PAYMENT_REQUIRED";
                break;
            case 403:
                errorMessage = "HTTP_FORBIDDEN";
                break;
            case 404:
                errorMessage = "HTTP_NOT_FOUND";
                break;
            case 405:
                errorMessage = "HTTP_BAD_METHOD";
                break;
            case 406:
                errorMessage = "HTTP_NOT_ACCEPTABLE";
                break;
            case 407:
                errorMessage = "HTTP_PROXY_AUTH";
                break;
            case 408:
                errorMessage = "HTTP_CLIENT_TIMEOUT";
                break;
            case 409:
                errorMessage = "HTTP_CONFLICT";
                break;
            case 410:
                errorMessage = "HTTP_GONE";
                break;
            case 411:
                errorMessage = "HTTP_LENGTH_REQUIRED";
                break;
            case 412:
                errorMessage = "HTTP_PRECON_FAILED";
                break;
            case 413:
                errorMessage = "HTTP_ENTITY_TOO_LARGE";
                break;
            case 414:
                errorMessage = "HTTP_REQ_TOO_LONG";
                break;
            case 415:
                errorMessage = "HTTP_UNSUPPORTED_TYPE";
                break;


            case 500:
                errorMessage = "HTTP_INTERNAL_SERVER_ERROR";
                break;
            case 501:
                errorMessage = "HTTP_NOT_IMPLEMENTED";
                break;
            case 502:
                errorMessage = "HTTP_BAD_GATEWAY";
                break;
            case 503:
                errorMessage = "HTTP_UNAVAILABLE";
                break;
            case 504:
                errorMessage = "HTTP_GATEWAY_TIMEOUT";
                break;
            case 505:
                errorMessage = "HTTP_VERSION";
                break;

            default:
                errorMessage = "UNDEFINED_HTTP_ERROR";
                break;
        }


        return errorMessage + " ( " + httpCode + " ) ";
    }
}
