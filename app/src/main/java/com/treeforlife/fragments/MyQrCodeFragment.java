package com.treeforlife.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.treeforlife.R;
import com.treeforlife.Utils.AndroidUtil;
import com.treeforlife.Utils.DialogUtil;
import com.treeforlife.Utils.ImageLoaderUtil;
import com.treeforlife.Utils.ProgressDialogUtil;
import com.treeforlife.commons.Constants;
import com.treeforlife.dataobjects.TFLUser;
import com.treeforlife.retrofit.ResponseId;
import com.treeforlife.retrofit.RetrofitManager;
import com.treeforlife.retrofit.RetrofitResponseValidator;
import com.treeforlife.retrofit.responses.MyQRCodeResponse;

import java.net.SocketTimeoutException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TheAppsmiths on 4/11/2018.
 */

public class MyQrCodeFragment extends Fragment implements RetrofitResponseValidator.ValidationListener {

    public static final String TAG="MyQrCodeFragment";

    private Context mContext;
    private Activity mActivity;
    private ImageView mQRCodeImage;
    private Bundle mBundle;
    private TFLUser mTflUser;
    private ProgressDialogUtil mProgressDialogUtil;
    private DialogUtil mDialogUtil;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext=context;
        this.mActivity=getActivity();
        this.mBundle = getArguments();
        this.mDialogUtil = DialogUtil.getInstance();
        this.mProgressDialogUtil=ProgressDialogUtil.getInstance();
        extractDataFromBundle();
    }
    private void extractDataFromBundle() {
        if (mBundle != null) {
            mTflUser = mBundle.getParcelable(Constants.KEY_TFL_USER);

        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_mygrcode,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initComponents(view);
        callGetQRCode();
    }


    @Override
    public void onResume() {
        super.onResume();
        if (mActivity != null)
        {
            mActivity.setTitle(R.string.my_qr_code);
        }
    }

    private void initComponents(View view) {
        mQRCodeImage=view.findViewById(R.id.image_qr_code);

    }

    //method to get the qr code using api
    private void callGetQRCode() {



        if (mTflUser != null && AndroidUtil.hasInternetConnectivity(mContext)) {
            mProgressDialogUtil.showProgressDialog(mActivity);

            Call<MyQRCodeResponse> getAlbum = RetrofitManager.getRetrofitWebService()
                    .getQRCode(mTflUser.userId);
            getAlbum.enqueue(new Callback<MyQRCodeResponse>() {
                @Override
                public void onResponse(Call<MyQRCodeResponse> call, Response<MyQRCodeResponse> response) {
                    new RetrofitResponseValidator(ResponseId.MY_QR_CODE, response, MyQrCodeFragment.this);

                }

                @Override
                public void onFailure(Call<MyQRCodeResponse> call, Throwable t) {
                    String errorMessage = "";
                    if (t != null) {
                        Log.e(TAG, "onFailure: Error : " + t.getMessage());
                        if (t instanceof SocketTimeoutException) {
                            errorMessage = "Please make sure that your device has an active internet connection.";
                        }
                    }

                    mProgressDialogUtil.dismissProgressDialog();


                    if (TextUtils.isEmpty(errorMessage)) {
                        AndroidUtil.showErrorToast(mContext);
                    } else {
                        mDialogUtil.showErrorDialog(mContext, errorMessage);
                    }
                }
            });

        }
    }

    @Override
    public void onValidResponse(@NonNull ResponseId responseId, @NonNull Object responseBody, @NonNull Response response) {
        switch (responseId)
        {
            case MY_QR_CODE:
                afterGetQR((MyQRCodeResponse)responseBody);
                break;
        }
        mProgressDialogUtil.dismissProgressDialog();
    }



    @Override
    public void onInvalidResponse(@NonNull ResponseId responseId, @NonNull String errorMessage, Response response) {
        mProgressDialogUtil.dismissProgressDialog();
        if (response == null || !response.isSuccessful()) {
            mDialogUtil.showErrorDialog(mActivity, errorMessage);
        } else {
            mDialogUtil.showOkAlertDialog(mActivity, errorMessage);
        }
    }

    //method after getting the qr code
    private void afterGetQR(MyQRCodeResponse responseBody) {
        if (responseBody.status==true)
        {
            ImageLoaderUtil.getLoader(mActivity).displayImage(responseBody.qrCode,mQRCodeImage);
        }

    }
}
