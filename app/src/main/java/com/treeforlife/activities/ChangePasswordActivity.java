package com.treeforlife.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.treeforlife.R;
import com.treeforlife.Utils.AndroidUtil;
import com.treeforlife.Utils.DialogUtil;
import com.treeforlife.Utils.ProgressDialogUtil;
import com.treeforlife.commons.Constants;
import com.treeforlife.customview.CustomButton;
import com.treeforlife.customview.CustomEditText;
import com.treeforlife.customview.CustomTextView;
import com.treeforlife.dataobjects.TFLUser;
import com.treeforlife.fragments.VaultDetailFragment;
import com.treeforlife.retrofit.ResponseId;
import com.treeforlife.retrofit.RetrofitManager;
import com.treeforlife.retrofit.RetrofitResponseValidator;
import com.treeforlife.retrofit.responses.WebServiceResponse;

import java.net.SocketTimeoutException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TheAppSmiths.
 * (TheAppSmiths is the Mobile App Development division of Adreno Technologies India Pvt. Ltd.)
 * *
 * Created on 21 June 2018 - 2:05 PM.
 *
 * @author Android Developer [AD143].
 **/


public class ChangePasswordActivity extends AppCompatActivity implements
        View.OnClickListener, RetrofitResponseValidator.ValidationListener {
    private static final String TAG = "ChangePasswordActivity";

    private Intent mActivityIntent;
    private Toolbar mToolbar;
    private ActionBar mActionBar;

    private CustomTextView mToolbarTitle, mHeadingTextView, mPasswordTextView, mOldPasswordTextView,
            mConfirmPasswordTextView, mForgotPasswordTextView, mChangePasswordTextView;
    private CustomEditText mOldPasswordEditText, mConfirmPasswordEditText, mPasswordEditText;
    private CustomButton mSubmitButton;
    private ImageView mLockImageView;

    private ProgressDialogUtil mProgressDialogUtil;
    private DialogUtil mDialogUtil;
    private TFLUser mTflUser;
    private String mCaller, mOldPassword, mPassword, mConfirmPassword;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initComponents();
        setContentView(R.layout.activity_change_password);
        initToolBar();
        initWidgets();
    }

    private void initComponents() {
        mActivityIntent = getIntent();
        if (mActivityIntent != null) {
            mCaller = mActivityIntent.getStringExtra(Constants.KEY_CALLER_COMPONENT);
            mTflUser = mActivityIntent.getParcelableExtra(Constants.KEY_TFL_USER);
        }

        mProgressDialogUtil = ProgressDialogUtil.getInstance();
        mDialogUtil = DialogUtil.getInstance();
    }

    //method to initialize the toolbar
    private void initToolBar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        if (mToolbar != null) {
            mToolbarTitle = mToolbar.findViewById(R.id.toolbar_title);
            ImageView mToolbarIconLhs = mToolbar.findViewById(R.id.toolbar_icon_back);
            mToolbarIconLhs.setOnClickListener(this);

            View mToolbarIconRhs = mToolbar.findViewById(R.id.toolbar_icon_hamberg);
            mToolbarIconRhs.setVisibility(View.GONE);
        }

        setSupportActionBar(mToolbar);
        mActionBar = getSupportActionBar();
        setTitle(R.string.change_password);


    }

    @Override
    public void setTitle(CharSequence title) {
        setTitleToolBar(title);
    }


    //method to set the title of toolbar
    private void setTitleToolBar(CharSequence title) {
        if (mToolbarTitle != null) {
            mToolbarTitle.setText(title);
        }
    }


    private void initWidgets() {
        mHeadingTextView = (CustomTextView) findViewById(R.id.heading_text_view);
        mLockImageView = findViewById(R.id.lock_image_view);

        mPasswordTextView = (CustomTextView) findViewById(R.id.password_text_view);
        mPasswordEditText = (CustomEditText) findViewById(R.id.password_edit_text);

        mOldPasswordTextView = (CustomTextView) findViewById(R.id.old_password_text_view);
        mOldPasswordEditText = (CustomEditText) findViewById(R.id.old_password_edit_text);

        mConfirmPasswordTextView = (CustomTextView) findViewById(R.id.confirm_password_text_view);
        mConfirmPasswordEditText = (CustomEditText) findViewById(R.id.confirm_password_edit_text);

        mSubmitButton = (CustomButton) findViewById(R.id.submit_button);
        mSubmitButton.setOnClickListener(this);

        mForgotPasswordTextView = (CustomTextView) findViewById(R.id.forgot_password_text_view);
        mChangePasswordTextView = (CustomTextView) findViewById(R.id.change_password_text_view);

        manageUi();
    }

    private void manageUi() {
        mForgotPasswordTextView.setVisibility(View.GONE);
        mChangePasswordTextView.setVisibility(View.GONE);

        mOldPasswordTextView.setVisibility(View.VISIBLE);
        mOldPasswordEditText.setVisibility(View.VISIBLE);
        mConfirmPasswordTextView.setVisibility(View.VISIBLE);
        mConfirmPasswordEditText.setVisibility(View.VISIBLE);


        if (!TextUtils.isEmpty(mCaller)) {
            switch (mCaller) {

                case VaultDetailFragment.TAG:
                    setTitle(R.string.change_values_password);
                    mHeadingTextView.setText(getString(R.string.change_values_password));
                    mPasswordTextView.setText(getString(R.string.new_values_password));
                    mOldPasswordTextView.setText(getString(R.string.old_values_password));
                    mConfirmPasswordTextView.setText(getString(R.string.confirm_new_values_password));
                    break;

                case HomeActivity.TAG:
                    setTitle(R.string.change_login_password);
                    mHeadingTextView.setVisibility(View.GONE);
                    mLockImageView.setVisibility(View.GONE);
                    mHeadingTextView.setText(getString(R.string.change_login_password));
                    mPasswordTextView.setText(getString(R.string.new_password));
                    mOldPasswordTextView.setText(getString(R.string.old_password));
                    mConfirmPasswordTextView.setText(getString(R.string.confirm_password));

                    mPasswordEditText.setFilters(new InputFilter[]{

                    });

                    mConfirmPasswordEditText.setFilters(new InputFilter[]{

                    });

                    mOldPasswordEditText.setFilters(new InputFilter[]{

                    });

                    mPasswordEditText.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    mConfirmPasswordEditText.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    mOldPasswordEditText.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    break;
            }
        }
    }


    @Override
    public void onClick(View view) {
        if (view != null) {

            switch (view.getId()) {

                case R.id.toolbar_icon_back:
                    onBackPressed();
                    break;

                case R.id.submit_button:
                    manageChangePassword();
                    break;
            }
        }

    }

    private boolean isValidUser() {
        if (mTflUser == null) {
            AndroidUtil.showToast(this, "Invalid User");
            return false;

        } else if (TextUtils.isEmpty(mTflUser.userId)) {
            AndroidUtil.showToast(this, "You need to Login !");
            return false;
        }

        return true;
    }

    public boolean isValidInput() {
        mOldPassword = mOldPasswordEditText.getText().toString().trim();
        mPassword = mPasswordEditText.getText().toString().trim();
        mConfirmPassword = mConfirmPasswordEditText.getText().toString().trim();

        if (VaultDetailFragment.TAG.equals(mCaller)) {

            if (TextUtils.isEmpty(mOldPassword)) {
                mOldPasswordEditText.setError(getString(R.string.error_no_old_password));
                mOldPasswordEditText.requestFocus();
                return false;

            } /*else if (mOldPassword.length() != 4) {
                mOldPasswordEditText.setError(getString(R.string.old_four_digit_password));
                mOldPasswordEditText.requestFocus();
                return false;

            }*/
            if (TextUtils.isEmpty(mPassword)) {
                mPasswordEditText.setError(getString(R.string.error_no_password));
                mPasswordEditText.requestFocus();
                return false;

            } else if (mPassword.length() != 4) {
                mPasswordEditText.setError(getString(R.string.four_digit_password));
                mPasswordEditText.requestFocus();
                return false;

            }

            if (TextUtils.isEmpty(mConfirmPassword)) {
                mConfirmPasswordEditText.setError(getString(R.string.error_no_password));
                mConfirmPasswordEditText.requestFocus();
                return false;

            } else if (!mPassword.equals(mConfirmPassword)) {
                mConfirmPasswordEditText.setError(getString(R.string.error_password_mismatch));
                mConfirmPasswordEditText.requestFocus();
                return false;
            }

        } else if (HomeActivity.TAG.equals(mCaller)) {
            if (TextUtils.isEmpty(mOldPassword)) {
                mOldPasswordEditText.setError(getString(R.string.error_no_old_password));
                mOldPasswordEditText.requestFocus();
                return false;

            }
            if (TextUtils.isEmpty(mPassword)) {
                mPasswordEditText.setError(getString(R.string.empty_password));
                mPasswordEditText.requestFocus();
                return false;

            } else if (mPassword.length() < 10) {
                mPasswordEditText.setError(getString(R.string.error_password_length));
                mPasswordEditText.requestFocus();
                return false;

            } else if (TextUtils.isEmpty(mConfirmPassword)) {
                mConfirmPasswordEditText.setError(getString(R.string.empty_confirm_password));
                mConfirmPasswordEditText.requestFocus();
                return false;

            } else if (!(mPassword).equals(mConfirmPassword)) {
                mConfirmPasswordEditText.setError(getString(R.string.error_password_mismatch));
                mConfirmPasswordEditText.requestFocus();
                return false;

            }
        } else {
            AndroidUtil.showToast(this, "Undefined/Invalid Caller component");
            return false;
        }

        return true;
    }

    private void manageChangePassword() {
        if (isValidInput() && isValidUser() && AndroidUtil.hasInternetConnectivity(this)) {

            mProgressDialogUtil.showProgressDialog(this);

            Call<WebServiceResponse> changePasswordCall = null;
            ResponseId responseId = null;
            if (!TextUtils.isEmpty(mCaller)) {
                switch (mCaller) {
                    case VaultDetailFragment.TAG:
                        changePasswordCall = RetrofitManager.getRetrofitWebService()
                                .changeVaultPassword(mTflUser.userId, mOldPassword, mPassword, mConfirmPassword);
                        responseId = ResponseId.CHANGE_VAULT_PASSWORD;
                        break;

                    case HomeActivity.TAG:
                        changePasswordCall = RetrofitManager.getRetrofitWebService()
                                .changeLoginPassword(mTflUser.userId, mOldPassword, mPassword, mConfirmPassword);
                        responseId = ResponseId.CHANGE_LOGIN_PASSWORD;
                        break;
                }
            }
            if (changePasswordCall != null) {
                changePasswordCall.enqueue(new Callback<WebServiceResponse>() {
                    @Override
                    public void onResponse(Call<WebServiceResponse> call, Response<WebServiceResponse> response) {
                        new RetrofitResponseValidator(ResponseId.CHANGE_VAULT_PASSWORD, response, ChangePasswordActivity.this);
                    }

                    @Override
                    public void onFailure(Call<WebServiceResponse> call, Throwable t) {
                        String errorMessage = "";
                        if (t != null) {
                            Log.e(TAG, "onFailure: Error : " + t.getMessage());
                            if (t instanceof SocketTimeoutException) {
                                errorMessage = "Please make sure that your device has an active internet connection.";
                            }
                        }

                        mProgressDialogUtil.dismissProgressDialog();


                        if (TextUtils.isEmpty(errorMessage)) {
                            AndroidUtil.showErrorToast(ChangePasswordActivity.this);
                        } else {
                            mDialogUtil.showErrorDialog(ChangePasswordActivity.this, errorMessage);
                        }
                    }
                });

            } else {
                AndroidUtil.showToast(this, "No Api for this action");
            }
        }
    }

    @Override
    public void onValidResponse(@NonNull ResponseId responseId, @NonNull Object responseBody, @NonNull Response response) {

        switch (responseId) {
            case CHANGE_LOGIN_PASSWORD:
            case CHANGE_VAULT_PASSWORD:
                WebServiceResponse serviceResponse = (WebServiceResponse) responseBody;
                AndroidUtil.showToast(this, serviceResponse.message);
                finish();
                break;
        }

        mProgressDialogUtil.dismissProgressDialog();
    }

    @Override
    public void onInvalidResponse(@NonNull ResponseId responseId, @NonNull String errorMessage, Response response) {
        mProgressDialogUtil.dismissProgressDialog();
        if (response == null || !response.isSuccessful()) {
            mDialogUtil.showErrorDialog(this, errorMessage);
        } else {
            AndroidUtil.showToast(this, errorMessage);
        }
    }
}
