package com.treeforlife.retrofit.responses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by TheAppsmiths on 4/10/2018.
 */

public class GetConverterResponse extends WebServiceResponse {

    @SerializedName("ordinary_point")
     public String ordinaryPoint;
    @SerializedName("copper_point")
    public String copperPoint;
    @SerializedName("silver_point")
    public String silverPoint;
    @SerializedName("gold_point")
    public String goldPoint;

}
