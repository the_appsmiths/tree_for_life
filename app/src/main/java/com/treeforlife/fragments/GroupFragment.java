package com.treeforlife.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.treeforlife.R;
import com.treeforlife.Utils.AndroidUtil;
import com.treeforlife.Utils.DialogUtil;
import com.treeforlife.Utils.FragmentUtil;
import com.treeforlife.Utils.ProgressDialogUtil;
import com.treeforlife.adapters.AdapterRvGroup;
import com.treeforlife.commons.Constants;
import com.treeforlife.commons.UploadImageVideoDialog;
import com.treeforlife.dataobjects.Group;
import com.treeforlife.dataobjects.TFLUser;
import com.treeforlife.retrofit.ResponseId;
import com.treeforlife.retrofit.RetrofitManager;
import com.treeforlife.retrofit.RetrofitResponseValidator;
import com.treeforlife.retrofit.responses.CreatedGroupResponse;
import com.treeforlife.retrofit.responses.GetGroupResponse;
import com.treeforlife.retrofit.responses.WebServiceResponse;

import java.net.SocketTimeoutException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TheAppsmiths on 4/9/2018.
 */
/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class GroupFragment extends Fragment implements View.OnClickListener,
        RetrofitResponseValidator.ValidationListener,
        AdapterRvGroup.GroupCallBack,
        NewGroupDialogFragment.NewGroupDialogFragmentCallback {

    public static final String TAG = "GroupFragment";

    private AdapterRvGroup mAdapter;
    private View mWishView;
    private TextView mNoDataTextView, mAddGroup;
    private ProgressDialogUtil mProgressDialogUtil;
    private DialogUtil mDialogUtil;
    private Bundle mBundle;
    private TFLUser mTflUser;
    private RecyclerView mRecycleview;
    private Activity mActivity;
    private Context mContext;
    private boolean isLoadingData;
    private int mPage;
    private ArrayList<Group> mGroupList = new ArrayList<Group>();
    private int mGroupId, mGroupPoistion;
    private UploadImageVideoDialog mDialog;
    private ArrayList<String> mList;
    private int mSpanCount;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mActivity = getActivity();
        this.mContext = context;
        this.mAdapter = new AdapterRvGroup(this);
        mProgressDialogUtil = ProgressDialogUtil.getInstance();
        mDialogUtil = DialogUtil.getInstance();
        mDialog = new UploadImageVideoDialog(mContext);
        mBundle = getArguments();
        mList = new ArrayList<String>();
        extractDataFromBundle();
    }

    private void extractDataFromBundle() {
        if (mBundle != null) {
            mTflUser = mBundle.getParcelable(Constants.KEY_TFL_USER);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_group, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initComponents(view);
        callGetGroups();
    }


    private void initComponents(View view) {

        if(getResources().getBoolean(R.bool.isTab)) {
            System.out.println("tablet");
            mSpanCount=3 ;
        } else {
            System.out.println("mobile");
            mSpanCount=2;
        }

        mNoDataTextView = view.findViewById(R.id.no_data_text_view);
        mWishView = view.findViewById(R.id.wish_layout);
        mAddGroup = view.findViewById(R.id.add_group_text);
        mAddGroup.setOnClickListener(this);
        mRecycleview = view.findViewById(R.id.recycleview_group);
        GridLayoutManager layoutManager = new GridLayoutManager(mContext, mSpanCount);
        mRecycleview.setLayoutManager(layoutManager);
        setInfiniteScroller(layoutManager);
        mRecycleview.setAdapter(mAdapter);
        setNoDataTextVisibiltiy();

    }

    private void setNoDataTextVisibiltiy() {
        if (mNoDataTextView != null && mAdapter != null) {
            mNoDataTextView.setVisibility(mAdapter.getItemCount() > 0 ? View.GONE : View.VISIBLE);
        }
    }

    private void setInfiniteScroller(final LinearLayoutManager layoutManager) {

        if (layoutManager != null && mRecycleview != null) {
            mRecycleview.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    int lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition();
                    int totalItemCount = layoutManager.getItemCount();
                    Log.e("TotalItemCountHome", "" + lastVisibleItemPosition + " " + Constants.ITEMS_LIMIT + " " + totalItemCount);

                    if ((lastVisibleItemPosition + Constants.ITEMS_LIMIT) > totalItemCount) {
                        Log.w(TAG, " RECYCLER ON SCROLL : loadData() called");
                        callGetGroups();
                    }
                }
            });
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if (mActivity != null) {
            mActivity.setTitle(R.string.groups);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mDialog != null) {
            mDialog.DismisDialog();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_group_text:
                showFriendsListInDialog();
                break;
        }
    }

    private void showFriendsListInDialog() {
        if (mActivity == null || !(mActivity instanceof AppCompatActivity)) {
            return;
        }
        FragmentManager fragmentManager = ((AppCompatActivity) mActivity).getSupportFragmentManager();

        if (fragmentManager != null) {

            Bundle bundle = new Bundle();
            bundle.putParcelable(Constants.KEY_TFL_USER, mTflUser);
            bundle.putString(Constants.KEY_CALLER_COMPONENT, TAG);

            SelectFriendDialogFragment selectFriendDialogFragment = null;
            Fragment fragment = fragmentManager.findFragmentByTag(SelectFriendDialogFragment.TAG);
            if (fragment != null && fragment instanceof SelectFriendDialogFragment) {
                selectFriendDialogFragment = (SelectFriendDialogFragment) fragment;
                selectFriendDialogFragment.update(bundle);
            }
            if (selectFriendDialogFragment == null) {
                selectFriendDialogFragment = new SelectFriendDialogFragment();
                selectFriendDialogFragment.setArguments(bundle);
            }
            selectFriendDialogFragment.setTargetFragment(GroupFragment.this, Constants.RC_SELECT_FRIEND_FRAGMENT);
            selectFriendDialogFragment.show(fragmentManager, SelectFriendDialogFragment.TAG);
        }
    }

    private void callGetGroups() {

        if (mTflUser != null && AndroidUtil.hasInternetConnectivity(mContext)
                && !isLoadingData) {

            isLoadingData = true;

            if (mPage == 0) {
                mProgressDialogUtil.showProgressDialog(mActivity);
            }


            Call<GetGroupResponse> getAlbum = RetrofitManager.getRetrofitWebService()
                    .getGroups(mTflUser.userId, mPage);
            getAlbum.enqueue(new Callback<GetGroupResponse>() {
                @Override
                public void onResponse(Call<GetGroupResponse> call, Response<GetGroupResponse> response) {
                    new RetrofitResponseValidator(ResponseId.GET_GROUP, response, GroupFragment.this);

                }

                @Override
                public void onFailure(Call<GetGroupResponse> call, Throwable t) {
                    String errorMessage = "";
                    if (t != null) {
                        Log.e(TAG, "onFailure: Error : " + t.getMessage());
                        if (t instanceof SocketTimeoutException) {
                            errorMessage = "Please make sure that your device has an active internet connection.";
                        }
                    }

                    mProgressDialogUtil.dismissProgressDialog();
                    isLoadingData = false;

                    if (!isVisible() || mPage != 0) {
                        return;
                    }

                    if (TextUtils.isEmpty(errorMessage)) {
                        AndroidUtil.showErrorToast(mContext);
                    } else {
                        mDialogUtil.showErrorDialog(mContext, errorMessage);
                    }
                }
            });

        }
    }


    @Override
    public void onValidResponse(@NonNull ResponseId responseId, @NonNull Object responseBody, @NonNull Response response) {
        switch (responseId) {

            case GET_GROUP:
                afterGetGroup((GetGroupResponse) responseBody);
                isLoadingData = false;
                break;
            case DELETE_GROUP:
                afterDeleteGroup((WebServiceResponse) responseBody);
                break;
        }
        mProgressDialogUtil.dismissProgressDialog();

    }

    @Override
    public void onInvalidResponse(@NonNull ResponseId responseId, @NonNull String errorMessage, Response response) {
        mProgressDialogUtil.dismissProgressDialog();
        switch (responseId) {
            case GET_GROUP:
                isLoadingData = false;
                if (!isVisible() || mPage != 0) {
                    return;
                }
                setNoDataTextVisibiltiy();
                break;
        }

        if (response == null || !response.isSuccessful()) {
            mDialogUtil.showErrorDialog(mActivity, errorMessage);
        } else {
            AndroidUtil.showToast(mContext, errorMessage);
        }
    }


    private void afterGetGroup(GetGroupResponse responseBody) {
        if (responseBody.status == true) {
            ArrayList<Group> list = responseBody.listGroup;
            if (list != null && !list.isEmpty()) {
                mGroupList.addAll(list);
            }
            if (mAdapter != null) {
                mAdapter.updateAdapter(mGroupList);
            }

            setNoDataTextVisibiltiy();

            if (mGroupList.size() <= responseBody.totalRecords) {
                mPage++;
            }

        }
    }

    private void afterDeleteGroup(WebServiceResponse responseBody) {
        if (responseBody.status == true) {
            mGroupList.remove(mGroupPoistion);
            mAdapter.notifyDataSetChanged();
            AndroidUtil.showToast(mActivity, responseBody.message);
        }
    }

    @Override
    public void clickItem(int position, Group group) {
        if (mTflUser != null) {
            Bundle bundle = new Bundle();
            bundle.putString(Constants.KEY_CALLER_COMPONENT, TAG);
            bundle.putParcelable(Constants.KEY_TFL_USER, mTflUser);
            bundle.putParcelable(Constants.KEY_GROUP, group);
            if (bundle != null) {
                FragmentUtil.loadFragment((AppCompatActivity) mContext, FragmentUtil.FRAGMENT_GROUP_MEMBER, bundle, true);
            }
        }
    }

    @Override
    public void removeItem(int position, Group group) {

        mGroupPoistion = position;
        mGroupId = group.groupId;

        Runnable OkButton = new Runnable() {
            @Override
            public void run() {
                callDeleteGroup(mGroupId);
            }
        };
        mDialogUtil.showOkCancelAlertDialog(mActivity, "Are You Sure to Delete Group?", OkButton);


    }

    private void callDeleteGroup(int mGroupId) {


        if (mTflUser != null && AndroidUtil.hasInternetConnectivity(mContext)) {
            mProgressDialogUtil.showProgressDialog(mActivity);

            Call<WebServiceResponse> getAlbum = RetrofitManager.getRetrofitWebService()
                    .deleteGroups(mTflUser.userId, mGroupId);
            getAlbum.enqueue(new Callback<WebServiceResponse>() {
                @Override
                public void onResponse(Call<WebServiceResponse> call, Response<WebServiceResponse> response) {
                    new RetrofitResponseValidator(ResponseId.DELETE_GROUP, response, GroupFragment.this);

                }

                @Override
                public void onFailure(Call<WebServiceResponse> call, Throwable t) {
                    String errorMessage = "";
                    if (t != null) {
                        Log.e(TAG, "onFailure: Error : " + t.getMessage());
                        if (t instanceof SocketTimeoutException) {
                            errorMessage = "Please make sure that your device has an active internet connection.";
                        }
                    }

                    mProgressDialogUtil.dismissProgressDialog();


                    if (TextUtils.isEmpty(errorMessage)) {
                        AndroidUtil.showErrorToast(mContext);
                    } else {
                        mDialogUtil.showErrorDialog(mContext, errorMessage);
                    }
                }
            });

        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {

                case Constants.RC_SELECT_FRIEND_FRAGMENT:
                    if (mActivity == null || !(mActivity instanceof AppCompatActivity)) {
                        return;
                    }
                    FragmentManager fragmentManager = ((AppCompatActivity) mActivity).getSupportFragmentManager();
                    //FragmentManager fragmentManager = getChildFragmentManager();

                    if (fragmentManager != null) {

                        Bundle bundle = new Bundle();
                        bundle.putParcelable(Constants.KEY_TFL_USER, mTflUser);
                        bundle.putString(Constants.KEY_CALLER_COMPONENT, TAG);
                        bundle.putAll(data.getExtras());
                        NewGroupDialogFragment newGroupDialogFragment = null;
                        Fragment fragment = fragmentManager.findFragmentByTag(NewGroupDialogFragment.TAG);
                        if (fragment != null && fragment instanceof NewGroupDialogFragment) {
                            newGroupDialogFragment = (NewGroupDialogFragment) fragment;
                            newGroupDialogFragment.update(bundle);
                        }
                        if (newGroupDialogFragment == null) {
                            newGroupDialogFragment = new NewGroupDialogFragment();
                            newGroupDialogFragment.setArguments(bundle);
                        }
                        newGroupDialogFragment.setCallback(this);
                        newGroupDialogFragment.show(fragmentManager, NewGroupDialogFragment.TAG);
                    }
                    break;

            }
        }
    }

    @Override
    public void onResult(@NonNull CreatedGroupResponse response) {
        if (response != null) {
            AndroidUtil.showToast(mContext, response.message);
            mGroupList.add(response.group);
            mAdapter.updateAdapter(mGroupList);
            setNoDataTextVisibiltiy();
            mPage++;
        }
    }
}
