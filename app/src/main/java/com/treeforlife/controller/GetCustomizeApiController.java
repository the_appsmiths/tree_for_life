package com.treeforlife.controller;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;

import com.treeforlife.Utils.AndroidUtil;
import com.treeforlife.Utils.DialogUtil;
import com.treeforlife.Utils.ProgressDialogUtil;
import com.treeforlife.commons.Constants;
import com.treeforlife.dataobjects.FriendTflUser;
import com.treeforlife.dataobjects.TFLUser;
import com.treeforlife.retrofit.ResponseId;
import com.treeforlife.retrofit.RetrofitManager;
import com.treeforlife.retrofit.RetrofitResponseValidator;
import com.treeforlife.retrofit.responses.GetCustomizedDataResponse;

import java.net.SocketTimeoutException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TheAppsmiths on 4/11/2018.
 *
 * @author TheAppsmiths
 */

/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class GetCustomizeApiController implements RetrofitResponseValidator.ValidationListener {
    private final String TAG;

    private final Fragment mFragment;
    private final Context mContext;
    private final Activity mActivity;
    private final TFLUser mTflUser;
    private final int mCustomizeType;
    private final ResultCallback mCallback;

    private int mPageNo;
    private boolean isLoadingData;

    private DialogUtil mDialogUtil;
    private ProgressDialogUtil mProgressDialogUtil;

    public GetCustomizeApiController(String tag, Fragment fragment, Context context,
                                     Activity activity, TFLUser tflUser, int customizeType,
                                     ResultCallback mCallback) {
        this.TAG = tag;
        this.mFragment = fragment;
        this.mContext = context;
        this.mActivity = activity;
        this.mTflUser = tflUser;
        this.mCustomizeType = customizeType;
        this.mCallback = mCallback;

        this.mDialogUtil = DialogUtil.getInstance();
        this.mProgressDialogUtil = ProgressDialogUtil.getInstance();
    }

    public void loadData(boolean resetData) {
        if (!isLoadingData && isValidData() && AndroidUtil.hasInternetConnectivity(mContext)) {
            if (resetData) {
                mPageNo = 0;
            }
            Log.d(TAG, "loadComments() called");
            isLoadingData = true;
            if (mPageNo == 0) {
                mProgressDialogUtil.showProgressDialog(mActivity);
            }

            Call<GetCustomizedDataResponse> mGetCustomizeDataCall =
                    RetrofitManager.getRetrofitWebService()
                            .getCustomizedData(mTflUser.userId, mCustomizeType, mPageNo);

            mGetCustomizeDataCall.enqueue(new Callback<GetCustomizedDataResponse>() {
                @Override
                public void onResponse(Call<GetCustomizedDataResponse> call, Response<GetCustomizedDataResponse> response) {
                    new RetrofitResponseValidator(ResponseId.GET_CUSTOMIZED_ITEM_DATA, response,
                            GetCustomizeApiController.this);

                }

                @Override
                public void onFailure(Call<GetCustomizedDataResponse> call, Throwable t) {
                    String errorMessage = "";
                    if (t != null) {
                        Log.e(TAG, "onFailure: Error : " + t.getMessage());
                        if (t instanceof SocketTimeoutException) {
                            errorMessage = "Please make sure that your device has an active internet connection.";
                        }
                    }

                    mProgressDialogUtil.dismissProgressDialog();
                    isLoadingData = false;

                    if (mPageNo != 0 || (mFragment != null && !mFragment.isVisible())) {
                        return;
                    }

                    if (TextUtils.isEmpty(errorMessage)) {
                        AndroidUtil.showErrorToast(mContext);
                    } else {
                        mDialogUtil.showErrorDialog(mContext, errorMessage);
                    }
                }
            });
        }
    }

    private boolean isValidUser() {
        if (mTflUser == null) {
            AndroidUtil.showToast(mContext, "Invalid User");
            return false;

        } else if (TextUtils.isEmpty(mTflUser.userId)) {
            AndroidUtil.showToast(mContext, "You need to Login !");
            return false;
        }

        return true;
    }

    private boolean isValidData() {
        switch (mCustomizeType) {
            case Constants.CUSTOMIZED_TYPE_PROFILE_SHARE:
            case Constants.CUSTOMIZED_TYPE_IMAGE_SHARE:
            case Constants.CUSTOMIZED_TYPE_VIDEO_SHARE:
            case Constants.CUSTOMIZED_TYPE_CONTACT_SHARE:
            case Constants.CUSTOMIZED_TYPE_BLOCKED_CONTACT:
                return isValidUser();

            default:
                AndroidUtil.showToast(mContext, "Invalid Customize Type");
                return false;
        }

    }

    @Override
    public void onValidResponse(@NonNull ResponseId responseId, @NonNull Object responseBody, @NonNull Response response) {
        mProgressDialogUtil.dismissProgressDialog();

        switch (responseId) {

            case GET_CUSTOMIZED_ITEM_DATA:
                GetCustomizedDataResponse dataResponse = (GetCustomizedDataResponse) responseBody;
                switch (mCustomizeType) {

                    case Constants.CUSTOMIZED_TYPE_PROFILE_SHARE:
                        if (mCallback != null) {
                            mCallback.onGetCustomizeApiResult(dataResponse.profileShareCount,
                                    dataResponse.profileShareList);
                            mPageNo++;
                        }
                        break;

                    case Constants.CUSTOMIZED_TYPE_IMAGE_SHARE:
                        if (mCallback != null) {
                            mCallback.onGetCustomizeApiResult(dataResponse.imageShareCount,
                                    dataResponse.imageShareList);
                            mPageNo++;
                        }
                        break;

                    case Constants.CUSTOMIZED_TYPE_VIDEO_SHARE:
                        if (mCallback != null) {
                            mCallback.onGetCustomizeApiResult(dataResponse.videoShareCount,
                                    dataResponse.videoShareList);
                            mPageNo++;
                        }
                        break;

                    case Constants.CUSTOMIZED_TYPE_CONTACT_SHARE:
                        if (mCallback != null) {
                            mCallback.onGetCustomizeApiResult(dataResponse.contactShareCount,
                                    dataResponse.contactShareList);
                            mPageNo++;
                        }
                        break;

                    case Constants.CUSTOMIZED_TYPE_BLOCKED_CONTACT:
                        if (mCallback != null) {
                            mCallback.onGetCustomizeApiResult(dataResponse.blockContactsCount,
                                    dataResponse.blockContactsList);
                            mPageNo++;
                        }
                        break;


                }
                isLoadingData = false;
                break;

        }
    }

    @Override
    public void onInvalidResponse(@NonNull ResponseId responseId, @NonNull String errorMessage, Response response) {
        mProgressDialogUtil.dismissProgressDialog();
        isLoadingData = false;
        if (mFragment != null && !mFragment.isVisible()) {
            return;
        }
        if (mPageNo == 0) {
            if (response == null || !response.isSuccessful()) {
                mDialogUtil.showErrorDialog(mActivity, errorMessage);
            } else {
                mDialogUtil.showOkAlertDialog(mActivity, errorMessage);
            }
        }
    }

    public interface ResultCallback {
        void onGetCustomizeApiResult(int noOfTotalRecords, ArrayList<FriendTflUser> customizeDataList);
    }
}
