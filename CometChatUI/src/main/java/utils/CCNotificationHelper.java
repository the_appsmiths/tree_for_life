package utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.text.Html;
import android.text.TextUtils;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.RemoteMessage;
import com.inscripts.enums.FeatureState;
import com.inscripts.enums.SettingSubType;
import com.inscripts.enums.SettingType;
import com.inscripts.helpers.EncryptionHelper;
import com.inscripts.helpers.PreferenceHelper;
import com.inscripts.keys.CometChatKeys;
import com.inscripts.keys.PreferenceKeys;
import com.inscripts.pojos.CCSettingMapper;
import com.inscripts.utils.Logger;
import com.inscripts.utils.SessionData;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import Keys.BroadCastReceiverKeys;
import Keys.JsonParsingKeys;
import activities.CometChatActivity;
import cometchat.inscripts.com.cometchatcore.coresdk.CometChat;
import models.OneOnOneMessage;
import videochat.CCIncomingCallActivity;

/**
 * Created by Jitvar on 3/28/2018.
 */

public class CCNotificationHelper {

    //asddas
    private static final java.lang.String TAG = CCNotificationHelper.class.getSimpleName();
    private static final String DELIMITER = "#:::#";
    private static Context context;
    public static final String MESSAGE = "m";
    public static final String DATA = "com.parse.Data";
    public static final String AV_CHAT = "avchat";
    public static final String FROM_ID = "fid";
    public static final String IS_CHATROOM = "isCR";
    public static final String ALERT = "alert";
    public static final String IS_ANNOUNCEMENT = "isANN";
    public static final String PUSH_CHANNEL = "push_channel";
    public static String TYPE = "t";

    private static String currentChannel = null;
    public static ArrayList<String> chatroomChannelList = new ArrayList<String>();
    public static int launcherIcon;
    public static int launcherScmallIcon;
    private static CometChat cometChat;
    private static FeatureState grpState;

    public static void processCCNotificationData(Context c, RemoteMessage remoteMessage, int LI, int LSI) {
        context = c;
        PreferenceHelper.initialize(context);
        initializeSessionData();
        launcherIcon = LI;
        launcherScmallIcon = LSI;
        cometChat = CometChat.getInstance(context);
        grpState = (FeatureState) cometChat.getCCSetting(new CCSettingMapper(SettingType.FEATURE, SettingSubType.CREATE_GROUPS_ENABLED));
        Logger.error(TAG, "onMessageReceive");
        try {
            Map<String, String> titleText = remoteMessage.getData();
            Logger.error(TAG, "Title text = " + titleText);
            JSONObject jsonData = new JSONObject();
            if (titleText.containsKey("action")) {
                jsonData.put("action", titleText.get("action"));
            }
            if (titleText.containsKey("t")) {
                jsonData.put("t", titleText.get("t"));
            }
            if (titleText.containsKey("alert")) {
                jsonData.put("alert", titleText.get("alert"));
            }
            if (titleText.containsKey("badge")) {
                jsonData.put("badge", titleText.get("badge"));
            }
            if (titleText.containsKey("sound")) {
                jsonData.put("sound", titleText.get("sound"));
            }
            if (titleText.containsKey("title")) {
                jsonData.put("title", titleText.get("title"));
            }
            if (titleText.containsKey("isCR")) {
                jsonData.put("isCR", titleText.get("isCR"));
            }
            if (titleText.containsKey("isANN")) {
                jsonData.put("isANN", titleText.get("isANN"));
            }

            Logger.error(TAG, "Json data = " + jsonData);



            Intent intent = new Intent(context, CometChatActivity.class);
            if (titleText.containsKey("m")) {
                jsonData.put("m", titleText.get("m").toString());
                JSONObject messageJson = new JSONObject(titleText.get("m"));
                String type = titleText.get(TYPE);
                String alert = titleText.get(ALERT);

                /*if (titleText.containsKey("isCR")
                        && titleText.get("isCR") == "1") {*/

                Logger.error(TAG, "type : " + type);
                Logger.error(TAG, "alert : " + alert);
                if (type.equals("C")) {
                    /*if (null != PreferenceHelper.get(PreferenceKeys.DataKeys.CURRENT_CHATROOM_ID) && !"0".equals(PreferenceHelper.get(PreferenceKeys.DataKeys.CURRENT_CHATROOM_ID))) {
                        // You're a part of a chatroom
                        if (!PreferenceHelper.get(PreferenceKeys.UserKeys.USER_ID).equals(messageJson.getString(FROM_ID))
                                && ("0".equals(PreferenceHelper.get(PreferenceKeys.DataKeys.ACTIVE_CHATROOM_ID))
                                || !(messageJson.getString("cid").equals(PreferenceHelper.get(PreferenceKeys.DataKeys.ACTIVE_CHATROOM_ID))))) {
                            intent.putExtra(DATA,jsonData.toString());
                            if(PreferenceHelper.get(PreferenceKeys.UserKeys.NOTIFICATION_ON).equals("1")){
                                showNotification(alert, intent);
                            }
                        }
                    }*/
                    Logger.error(TAG, "processCCNotificationData: isSelf" + PreferenceHelper.get(PreferenceKeys.UserKeys.USER_ID).equals(messageJson.getString(FROM_ID)));
                    Logger.error(TAG, "processCCNotificationData: groupWindowId: " + (PreferenceHelper.get(JsonParsingKeys.GRP_WINDOW_ID) == null ||
                            !PreferenceHelper.get(JsonParsingKeys.GRP_WINDOW_ID).equals(messageJson.getString("cid"))));
                    Logger.error(TAG, "processCCNotificationData: cid: " + messageJson.getString("cid"));
                    Logger.error(TAG, "processCCNotificationData: windowId: " + PreferenceHelper.get(JsonParsingKeys.GRP_WINDOW_ID));
                    if (!PreferenceHelper.get(PreferenceKeys.UserKeys.USER_ID).equals(messageJson.getString(FROM_ID)) && (PreferenceHelper.get(JsonParsingKeys.GRP_WINDOW_ID) == null || !PreferenceHelper.get(JsonParsingKeys.GRP_WINDOW_ID).equals(messageJson.getString("cid")))) {
                        if (PreferenceHelper.get(PreferenceKeys.UserKeys.NOTIFICATION_ON).equals("1")) {
                            if (jsonData.has("alert") && !jsonData.getString("alert").contains("CC^CONTROL_PLUGIN_")) {
                                intent.putExtra(DATA, jsonData.toString());
                                if (grpState == FeatureState.ACCESSIBLE) {
                                    showNotification(alert, intent);
                                }
                            }
                        }
                    }
                } else if (titleText.containsKey("isANN")) {
                    /*if (jp != null && null != jp.getLang()) {
                        intent.putExtra(NotificationHelper.PushNotificationKeys.DATA,jsonData.toString());
                        showNotification( alert, intent);
                    } else {
                        intent.putExtra(NotificationHelper.PushNotificationKeys.DATA,jsonData.toString());
                        showNotification(alert, intent);
                    }*/
                    intent.putExtra(DATA, jsonData.toString());
                    if (PreferenceHelper.get(PreferenceKeys.UserKeys.NOTIFICATION_ON).equals("1")) {
                        showNotification(alert, intent);
                    }
                } else {
                    Long buddyId = messageJson.getLong(FROM_ID);
                    Logger.error(TAG, "buddyId : " + buddyId);
                    Logger.error(TAG, "type : " + type);
                    Logger.error(TAG, "PreferenceHelper.contains(PreferenceKeys.DataKeys.ACTIVE_BUDDY_ID) : "
                            + PreferenceHelper.contains(PreferenceKeys.DataKeys.ACTIVE_BUDDY_ID));
                    //if (PreferenceHelper.contains(PreferenceKeys.DataKeys.ACTIVE_BUDDY_ID)) {
                    // if we are chatting with someone

                    long buddyWindowId = 0;
                    if (PreferenceHelper.contains(PreferenceKeys.DataKeys.ACTIVE_BUDDY_ID)) {
                        buddyWindowId = Long.parseLong(PreferenceHelper
                                .get(PreferenceKeys.DataKeys.ACTIVE_BUDDY_ID));
                    }
                    Logger.error(TAG, "Message Json has ? " + messageJson.has("m"));
                    if (messageJson.has("m")) {

                        if (type.contains("O_A")) {
                            Intent i = new Intent(context, CCIncomingCallActivity.class);
                            if (type.equals("O_AVC")) {
                                i.putExtra(CometChatKeys.AudiochatKeys.AUDIO_ONLY_CALL, false);
                            } else if (type.equals("O_AC")) {
                                i.putExtra(CometChatKeys.AudiochatKeys.AUDIO_ONLY_CALL, true);
                            }
                            if (titleText.containsKey("grp")) {

                                String originalRoomname = titleText.get("grp");
                                Logger.error(TAG, "originalRoomname : " + originalRoomname);
                                String webrtc_channel = PreferenceHelper.get(PreferenceKeys.UserKeys.WEBRTC_CHANNEL);
                                String roomNameMd5 = EncryptionHelper.encodeIntoMD5(webrtc_channel + originalRoomname);
                                Logger.error(TAG, "roomName md5 : " + roomNameMd5);
                                i.putExtra(CometChatKeys.AVchatKeys.ROOM_NAME, roomNameMd5);

                                SessionData session = SessionData.getInstance();
                                session.setAvChatRoomName(originalRoomname);
                                session.setActiveAVchatUserID(String.valueOf(buddyId));
                            }
                            Logger.error(TAG, "MessageJsom = " + messageJson);
                            if (messageJson.has("sent")) {
                                Long time = messageJson.getLong("sent") * 1000;
                                if ((System.currentTimeMillis() - time) < 60000) {
                                    Logger.error(TAG, "buddyWindowId = " + buddyWindowId);
                                    Logger.error(TAG, "buddyId = " + buddyId);

                                    if (buddyWindowId != buddyId || buddyWindowId == 0) {
                                        try {
                                            Long messageid = messageJson.getLong("id");
                                            OneOnOneMessage avmessage = OneOnOneMessage.findById(messageid);
                                            Logger.error(TAG, "avmessage = " + avmessage);
                                            if (type.equals("O_AVC_CANCEL") || type.equals("O_AC_CANCEL")) {
                                                Intent callCancelIntent = new Intent(BroadCastReceiverKeys.AvchatKeys.AVCHAT_CLOSE_ACTIVITY);
                                                callCancelIntent.putExtra(BroadCastReceiverKeys.AvchatKeys.CALL_CANCEL_FROM_NOTIFICATION, 1);
                                                context.sendBroadcast(callCancelIntent);
                                            } else if (type.equals("O_AVC_END") || type.equals("O_AC_END")) {
                                                Intent endCancelIntent = new Intent(BroadCastReceiverKeys.AvchatKeys.AVCHAT_CLOSE_ACTIVITY);
                                                endCancelIntent.putExtra(BroadCastReceiverKeys.AvchatKeys.CALL_END_FROM_NOTIFICATION, 1);
                                                context.sendBroadcast(endCancelIntent);
                                            } else if (avmessage == null) {
                                                    /*final OneOnOneMessage mess = new OneOnOneMessage(messageid, buddyId,
                                                            Long.parseLong(PreferenceHelper.get(PreferenceKeys.UserKeys.USER_ID)), messageJson.getString("m"), System.currentTimeMillis(), 1, 1,
                                                            CometChatKeys.MessageTypeKeys.AVCHAT, "", 1, CometChatKeys.MessageTypeKeys.MESSAGE_SENT, 1);
                                                    mess.save();*/

                                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                                                i.putExtra(CometChatKeys.AVchatKeys.CALLER_ID, String.valueOf(buddyId));
                                                i.putExtra(CometChatKeys.AVchatKeys.CALLER_NAME, messageJson.getString("name"));
                                                context.startActivity(i);
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                } else {
                                    //Logger.error("Notification is arrived late ");
                                }
                            }


                        } else {
                            if (buddyWindowId != buddyId || buddyWindowId == 0) {
                                intent.putExtra(DATA, jsonData.toString());
                                if (PreferenceHelper.get(PreferenceKeys.UserKeys.NOTIFICATION_ON).equals("1")) {
                                    showNotification(alert, intent);
                                }
                            }
                        }

                    } else {
                        intent.putExtra(DATA, jsonData.toString());
                        if (PreferenceHelper.get(PreferenceKeys.UserKeys.NOTIFICATION_ON).equals("1") &&

                                !PreferenceHelper.contains(PreferenceKeys.DataKeys.ACTIVE_BUDDY_ID)) {
                            showNotification(alert, intent);
                        }
                    }
                }
            }

        } catch (Exception e) {
            //Logger.error("In firebase exception");
            e.printStackTrace();

        }
    }

    private static void showNotification(String notificationText, Intent resultIntent) {
        Logger.error(TAG, "Notification text = " + String.valueOf(Html.fromHtml(notificationText)));
        notificationText = String.valueOf(Html.fromHtml(notificationText));
        String appName = context.getString(context.getApplicationInfo().labelRes);

        boolean isSoundActive = PreferenceHelper.get(PreferenceKeys.UserKeys.NOTIFICATION_SOUND).equals("1");
        boolean isVibrateActive = PreferenceHelper.get(PreferenceKeys.UserKeys.NOTIFICATION_VIBRATE).equals("1");

        boolean isUsingSystemSound = true;

        String storedNotificationStack = PreferenceHelper.get(PreferenceKeys.DataKeys.NOTIFICATION_STACK);
        if (TextUtils.isEmpty(storedNotificationStack)) {
            storedNotificationStack = notificationText;
        } else {
            storedNotificationStack = storedNotificationStack + DELIMITER + notificationText;
        }

        PreferenceHelper.save(PreferenceKeys.DataKeys.NOTIFICATION_STACK, storedNotificationStack);
        String[] allNotifications = storedNotificationStack.split(DELIMITER);

        String summaryText, contentText;
        if (allNotifications.length == 1) {
            summaryText = allNotifications.length + " new message";
            contentText = notificationText;
        } else {
            contentText = summaryText = allNotifications.length + " new messages";
        }

        NotificationCompat.InboxStyle style = new NotificationCompat.InboxStyle().setSummaryText(summaryText)
                .setBigContentTitle(appName);

        boolean isSinglePerson = true;
        boolean gotFirstValue = false;
        String oldPart = "";

        for (int i = allNotifications.length - 1; i >= 0; i--) {

            String notifi = allNotifications[i];
            String latestPart = notifi.substring(0, notifi.indexOf(":"));

            if (!gotFirstValue) {
                oldPart = latestPart;
                gotFirstValue = true;
            }
            if (!latestPart.contains("@") && !oldPart.equals(latestPart)) {
                isSinglePerson = false;
            } else if (latestPart.contains("@") && oldPart.contains("@")) {
                String old[] = oldPart.split("@");
                String lates[] = oldPart.split("@");
                if (!old[1].equals(lates[1])) {
                    isSinglePerson = false;
                }
            }

            oldPart = latestPart;

            // Adding the line to the notification
            style.addLine(notifi);
        }

        Logger.error(TAG, "isSinglePerson ? " + isSinglePerson);
//        Intent resultIntent = new Intent();
        if (isSinglePerson == false) {
            resultIntent = new Intent();
            resultIntent.setClass(context, CometChatActivity.class);
        }

        resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent pIntent = PendingIntent.getActivity(context, 1, resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Logger.error(TAG, "Content text = " + contentText);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);
        Notification summaryNotification = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationChannel mChannel;
            String CHANNEL_ID = "";
            CharSequence channelName = "";
            int importance = 0;

            if (isSoundActive && isVibrateActive) {
                Logger.error(TAG, "showNotification() 1: isSoundActive " + isSoundActive + " isVibrateActive " + isVibrateActive);
                CHANNEL_ID = "Notification Channel 1";
                channelName = "Notification Channel 1";   // The user-visible name of the channel.
                importance = NotificationManager.IMPORTANCE_DEFAULT;
            } else if (!isSoundActive && !isVibrateActive) {
                Logger.error(TAG, "showNotification() 2: isSoundActive " + isSoundActive + " isVibrateActive " + isVibrateActive);
                CHANNEL_ID = "Notification Channel 2";
                channelName = "Notification Channel 2";   // The user-visible name of the channel.
                importance = NotificationManager.IMPORTANCE_LOW;
            } else if (!isSoundActive && isVibrateActive) {
                Logger.error(TAG, "showNotification() 3: isSoundActive " + isSoundActive + " isVibrateActive " + isVibrateActive);
                CHANNEL_ID = "Notification Channel 3";
                channelName = "Notification Channel 3";   // The user-visible name of the channel.
                importance = NotificationManager.IMPORTANCE_DEFAULT;
            } else if (!isVibrateActive && isSoundActive) {
                Logger.error(TAG, "showNotification() 4: isSoundActive " + isSoundActive + " isVibrateActive " + isVibrateActive);
                CHANNEL_ID = "Notification Channel 4";
                channelName = "Notification Channel 4";   // The user-visible name of the channel.
                importance = NotificationManager.IMPORTANCE_DEFAULT;
            }

            mChannel = new NotificationChannel(CHANNEL_ID, channelName, importance);
            mChannel.setShowBadge(true);

            if (isSoundActive) {
                AudioAttributes attributes = new AudioAttributes.Builder()
                        .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                        .build();
                mChannel.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION), attributes);
            } else {
                mChannel.setSound(null, null);
            }

            if (isVibrateActive) {
                mChannel.enableVibration(true);
            } else {
                Logger.error(TAG, "showNotifications() : isVibrateActive : " + isVibrateActive);
                mChannel.enableVibration(true);
                mChannel.setVibrationPattern(new long[]{0});
            }

            notificationManager.createNotificationChannel(mChannel);
            summaryNotification = mBuilder.setContentTitle(appName).setSmallIcon(launcherScmallIcon)
                    .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), launcherIcon))
                    .setContentText(contentText).setContentIntent(pIntent)
                    .setAutoCancel(true)
                    .setStyle(style)
                    .setColor(Color.parseColor("#002832"))
                    .setChannelId(CHANNEL_ID)
                    .build();
        } else {
            summaryNotification = mBuilder.setContentTitle(appName).setSmallIcon(launcherScmallIcon)
                    .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), launcherIcon))
                    .setContentText(contentText).setContentIntent(pIntent)
                    .setAutoCancel(true)
                    .setStyle(style)
                    .setColor(Color.parseColor("#002832"))
                    .build();

            if (isUsingSystemSound && isSoundActive) {
                summaryNotification.defaults |= Notification.DEFAULT_SOUND;
            }

            if (isVibrateActive) {
                summaryNotification.defaults |= Notification.DEFAULT_VIBRATE;
            }
        }

        summaryNotification.flags |= Notification.FLAG_SHOW_LIGHTS;
        summaryNotification.ledARGB = 0xff00ff00;
        summaryNotification.ledOnMS = 300;
        summaryNotification.ledOffMS = 2000;

        if (android.os.Build.VERSION.SDK_INT >= 16) {
            summaryNotification.priority = Notification.PRIORITY_MAX;
        }


        notificationManager.notify(1, summaryNotification);
    }

    public static void unsubscribe(boolean isChatroom, boolean isclearAll) {
        try {
            if (isChatroom) {
                if (isclearAll) {
                    if (PreferenceHelper.contains(PreferenceKeys.UserKeys.CHATROOM_CHANNEL_LIST)) {
                        String channelListStr = PreferenceHelper.get(PreferenceKeys.UserKeys.CHATROOM_CHANNEL_LIST);
                        if (!TextUtils.isEmpty(channelListStr)) {
                            channelListStr = channelListStr.substring(1, channelListStr.length() - 1);
                            //Logger.error("channel List string :   " + channelListStr);
                        }
                        List<String> items = Arrays.asList(channelListStr.split("\\s*,\\s*"));
                        //Logger.error("list size channel "+items.size());
                        if (items.size() > 0) {
                            for (int i = 0; i < items.size(); i++) {
                                if (!items.get(i).isEmpty())
                                    FirebaseMessaging.getInstance().unsubscribeFromTopic(items.get(i));
                            }
                        }
                    }
                } else {
                    currentChannel = PreferenceHelper.get(PreferenceKeys.DataKeys.CURRENT_GROUP_CHANNEL);
                    if (chatroomChannelList.contains(currentChannel)) {
                        chatroomChannelList.remove(currentChannel);
                    } else if (PreferenceHelper.contains(PreferenceKeys.UserKeys.CHATROOM_CHANNEL_LIST)) {
                        String channelListStr = PreferenceHelper.get(PreferenceKeys.UserKeys.CHATROOM_CHANNEL_LIST);
                        if (!TextUtils.isEmpty(channelListStr)) {
                            channelListStr = channelListStr.substring(1, channelListStr.length() - 1);
                            //Logger.error("channel List string :   " + channelListStr);
                        }
                        List<String> items = Arrays.asList(channelListStr.split("\\s*,\\s*"));
                        if (items.size() > 0) {
                            for (int i = 0; i < items.size(); i++) {
                                if (!chatroomChannelList.contains(items.get(i))) {
                                    chatroomChannelList.add(items.get(i));
                                }

                            }
                        }
                        if (chatroomChannelList.contains(currentChannel)) {
                            chatroomChannelList.remove(currentChannel);
                        }
                    }
                    PreferenceHelper.save(PreferenceKeys.UserKeys.CHATROOM_CHANNEL_LIST, chatroomChannelList.toString());
                }
            }
            Logger.error(TAG, "Current Chatroom Push Channel1 : " + currentChannel);
            if (null != currentChannel) {
                Logger.error(TAG, "Current Chatroom Push Channel2 : " + currentChannel);
                FirebaseMessaging.getInstance().unsubscribeFromTopic(currentChannel);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Subscribes to a channel based on the parameter passed. Pass <b>true</b>
     * for chatrooms
     */
    public static void subscribe(boolean isChatroom, String channel) {
        Logger.error(TAG, "Firebase subcribe channel = " + channel);
        try {
            if (isChatroom) {
                if (chatroomChannelList == null) {
                    chatroomChannelList = new ArrayList<String>();
                }
                currentChannel = channel;
                if (!chatroomChannelList.contains(channel)) {
                    if (!PreferenceHelper.contains(PreferenceKeys.UserKeys.CHATROOM_CHANNEL_LIST)) {
                        chatroomChannelList.add(channel);
                    } else {
                        String channelListStr = PreferenceHelper.get(PreferenceKeys.UserKeys.CHATROOM_CHANNEL_LIST);
                        if (!TextUtils.isEmpty(channelListStr)) {
                            channelListStr = channelListStr.substring(1, channelListStr.length() - 1);
                            //Logger.error("channel List string :   " + channelListStr);
                        }
                        List<String> items = Arrays.asList(channelListStr.split("\\s*,\\s*"));
                        //Logger.error("list size channel " + items.size());

                        if (items.size() > 0) {
                            for (int i = 0; i < items.size(); i++) {
                                if (!chatroomChannelList.contains(items.get(i))) {
                                    chatroomChannelList.add(items.get(i));
                                }

                            }
                        }
                        if (!chatroomChannelList.contains(channel)) {
                            chatroomChannelList.add(channel);
                        }
                    }
                }

                PreferenceHelper.save(PreferenceKeys.UserKeys.CHATROOM_CHANNEL_LIST, chatroomChannelList.toString());
            } else {
                currentChannel = channel;
            }
            if (null != currentChannel) {
                try {
                    FirebaseMessaging.getInstance().subscribeToTopic(currentChannel);
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Clears all the notifications
     */
    public static void clearAllNotifications() {
        NotificationManager notificationManager = (NotificationManager) PreferenceHelper.getContext().getSystemService(
                Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
        PreferenceHelper.removeKey(PreferenceKeys.DataKeys.NOTIFICATION_STACK);
    }

    private static void initializeSessionData() {
        if (!PreferenceHelper.contains(PreferenceKeys.UserKeys.NOTIFICATION_ON)) {
            PreferenceHelper.save(PreferenceKeys.UserKeys.NOTIFICATION_ON, "1");
        }
        if (!PreferenceHelper.contains(PreferenceKeys.UserKeys.READ_TICK)) {
            PreferenceHelper.save(PreferenceKeys.UserKeys.READ_TICK, "1");
        }
        if (!PreferenceHelper.contains(PreferenceKeys.UserKeys.LAST_SEEN_SETTING)) {
            PreferenceHelper.save(PreferenceKeys.UserKeys.LAST_SEEN_SETTING, "1");
        }
        if (!PreferenceHelper.contains(PreferenceKeys.UserKeys.TYPING_SETTING)) {
            PreferenceHelper.save(PreferenceKeys.UserKeys.TYPING_SETTING, "1");
        }
        if (!PreferenceHelper.contains(PreferenceKeys.UserKeys.NOTIFICATION_SOUND)) {
            PreferenceHelper.save(PreferenceKeys.UserKeys.NOTIFICATION_SOUND, "1");
        }

        if (!PreferenceHelper.contains(PreferenceKeys.UserKeys.NOTIFICATION_VIBRATE)) {
            PreferenceHelper.save(PreferenceKeys.UserKeys.NOTIFICATION_VIBRATE, "1");
        }

        if (!PreferenceHelper.contains(PreferenceKeys.DataKeys.ACTIVE_CHATROOM_ID)) {
            PreferenceHelper.save(PreferenceKeys.DataKeys.ACTIVE_CHATROOM_ID, "0");
        }

        if (!PreferenceHelper.contains(PreferenceKeys.DataKeys.CURRENT_CHATROOM_ID)) {
            PreferenceHelper.save(PreferenceKeys.DataKeys.CURRENT_CHATROOM_ID, "1");
        }
    }
}
