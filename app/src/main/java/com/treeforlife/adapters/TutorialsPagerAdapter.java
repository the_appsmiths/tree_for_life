package com.treeforlife.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.treeforlife.commons.Constants;
import com.treeforlife.dataobjects.Tutorial;
import com.treeforlife.fragments.TutorialFragment;

import java.util.ArrayList;

/**
 * Created by TheAppSmiths.
 * (TheAppSmiths is the Mobile App Development division of Adreno Technologies India Pvt. Ltd.)
 * *
 * Created on 25 June 2018 - 5:30 PM.
 *
 * @author Android Developer [AD143].
 **/


public class TutorialsPagerAdapter extends FragmentPagerAdapter {
    private static final String TAG = "TutorialsPagerAdapter";
    private ArrayList<Tutorial> mTutorialList;
    private ArrayList<TutorialFragment> mTutorialFragments;

    public TutorialsPagerAdapter(FragmentManager fm) {
        super(fm);
        ensureTutorialsList();
    }

    @Override
    public Fragment getItem(int position) {
        if (position > -1 && position < mTutorialFragments.size()) {
            return mTutorialFragments.get(position);
        }

        return null;
    }

    @Override
    public int getCount() {
        return mTutorialFragments != null ? mTutorialFragments.size() : 0;
    }

    public void updateTutorialsList(ArrayList<Tutorial> tutorialList) {
        ensureTutorialsList();
        mTutorialList.clear();
        if (tutorialList != null && !tutorialList.isEmpty()) {
            mTutorialList.addAll(tutorialList);
        }
        notifyDataSetChanged();
    }

    private void ensureTutorialsList() {
        if (mTutorialList == null) {
            mTutorialList = new ArrayList<Tutorial>();
        }
    }

    private void ensureTutorialFragments() {
        if (mTutorialFragments == null) {
            mTutorialFragments = new ArrayList<TutorialFragment>();
        }
    }

    @Override
    public void notifyDataSetChanged() {

        ensureTutorialFragments();
        TutorialFragment tutorialFragment;
        Bundle bundle;
        for (int i = 0; i < mTutorialList.size(); i++) {
            bundle = new Bundle();
            bundle.putParcelable(Constants.KEY_TUTORIAL, mTutorialList.get(i));

            if (i < mTutorialFragments.size()) {
                tutorialFragment = mTutorialFragments.get(i);
                if (tutorialFragment != null) {
                    tutorialFragment.update(bundle);
                }
            } else {
                tutorialFragment = new TutorialFragment();
                tutorialFragment.setArguments(bundle);
                mTutorialFragments.add(tutorialFragment);
            }
        }

        super.notifyDataSetChanged();
    }
}
