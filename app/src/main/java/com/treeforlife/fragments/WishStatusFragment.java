package com.treeforlife.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.treeforlife.R;
import com.treeforlife.Utils.AndroidUtil;
import com.treeforlife.Utils.DialogUtil;
import com.treeforlife.commons.Constants;
import com.treeforlife.commons.FragmentCallBack;
import com.treeforlife.commons.SharedPrefHelper;
import com.treeforlife.customview.CustomTextView;
import com.treeforlife.dataobjects.TFLUser;
import com.treeforlife.retrofit.ResponseId;
import com.treeforlife.retrofit.RetrofitManager;
import com.treeforlife.retrofit.RetrofitResponseValidator;
import com.treeforlife.retrofit.responses.GetWishResponse;

import java.net.SocketTimeoutException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TheAppSmiths.
 * (TheAppSmiths is the Mobile App Development division of Adreno Technologies India Pvt. Ltd.)
 * *
 * Created on 19 July 2018 - 1:23 PM.
 *
 * @author Android Developer [AD143].
 **/


public class WishStatusFragment extends Fragment implements
        RetrofitResponseValidator.ValidationListener {

    public static final String TAG = "WishStatusFragment";

    private Activity mActivity;
    private Context mContext;
    private Bundle mBundle;
    private FragmentCallBack mCallback;

    private CustomTextView mUserWishTextView, mUserWishStatusTextView, mRedWishTextView,
            mYellowWishTextView, mGreenWishTextView;
    private SwipeRefreshLayout mWishRefreshLayout;

    private SharedPrefHelper mSharedPrefHelper;
    private DialogUtil mDialogUtil;
    private TFLUser mTflUser;
    private String mCaller;
    private boolean isLoadingData;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
        this.mActivity = getActivity();
        this.mBundle = getArguments();

        if (mActivity != null && mActivity instanceof FragmentCallBack) {
            mCallback = (FragmentCallBack) mActivity;
        }

        this.mSharedPrefHelper = SharedPrefHelper.getInstance();
        this.mDialogUtil = DialogUtil.getInstance();

        extractDataFromBundle(true);
        ensureTflUser(false);

    }

    private void extractDataFromBundle(boolean extractAfterGet) {

        if (extractAfterGet) {
            mBundle = getArguments();
        }

        if (mBundle != null) {
            mCaller = mBundle.getString(Constants.KEY_CALLER_COMPONENT);
            mTflUser = mBundle.getParcelable(Constants.KEY_TFL_USER);
        }
    }

    private void clearContent() {
        if (mUserWishTextView != null) {
            mUserWishTextView.setText(null);
            mUserWishStatusTextView.setText(null);
            mRedWishTextView.setText(null);
            mYellowWishTextView.setText(null);
            mGreenWishTextView.setText(null);

        }
    }

    private void displayContent() {
        if (mUserWishTextView != null) {
            clearContent();

            if (mTflUser != null) {
                String wishStatus = "", redWishText = "", yellowWishText = "", greenWishText = "";
                int mWishStatusTextColor = ContextCompat.getColor(mContext,
                        R.color.appColor_707070_black);

                if (!TextUtils.isEmpty(mTflUser.wishStatus)) {
                    switch (mTflUser.wishStatus) {

                        case Constants.WISH_STATUS_RED:
                            wishStatus = "RED";
                            redWishText = mTflUser.myWish;
                            mWishStatusTextColor = ContextCompat.getColor(mContext,
                                    R.color.appColor_dc4a25_red);
                            break;

                        case Constants.WISH_STATUS_YELLOW:
                            wishStatus = "YELLOW";
                            yellowWishText = mTflUser.myWish;
                            mWishStatusTextColor = ContextCompat.getColor(mContext,
                                    R.color.appColor_edb834_yellow);
                            break;

                        case Constants.WISH_STATUS_GREEN:
                            wishStatus = "GREEN";
                            greenWishText = mTflUser.myWish;
                            mWishStatusTextColor = ContextCompat.getColor(mContext,
                                    R.color.appColor_5cc034_green);
                            break;
                    }
                }

                mUserWishTextView.setText(mTflUser.myWish);
                mUserWishStatusTextView.setText(wishStatus);
                mUserWishStatusTextView.setTextColor(mWishStatusTextColor);
                mRedWishTextView.setText(redWishText);
                mYellowWishTextView.setText(yellowWishText);
                mGreenWishTextView.setText(greenWishText);

            } else {
                AndroidUtil.showToast(mContext, "Unexpected error : Invalid user data.");
            }

        }
    }

    private void ensureTflUser(boolean reset) {
        if (reset || mTflUser == null) {
            mTflUser = mSharedPrefHelper.getLoginResponse(mContext);
        }
    }

    public void update(Bundle bundle) {
        mBundle = bundle;
        extractDataFromBundle(false);
        clearContent();
        loadWishStatus();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_wish_status, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        clearContent();
        loadWishStatus();
    }

    private void initViews(@NonNull View view) {
        if (view != null) {
            mWishRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.wish_refresh_layout);
            mUserWishTextView = (CustomTextView) view.findViewById(R.id.user_wish_text_view);
            mUserWishStatusTextView = (CustomTextView) view.findViewById(R.id.user_wish_status_text_view);
            mRedWishTextView = (CustomTextView) view.findViewById(R.id.red_wish_text_view);
            mYellowWishTextView = (CustomTextView) view.findViewById(R.id.yellow_wish_text_view);
            mGreenWishTextView = (CustomTextView) view.findViewById(R.id.green_wish_text_view);
        }
        setListeners();
    }

    private void setListeners() {
        if (mWishRefreshLayout != null) {
            mWishRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    loadWishStatus();
                }
            });
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if (mCallback != null) {
            Bundle bundle = new Bundle();
            bundle.putString(Constants.KEY_FRAGMENT_CALLBACK, TAG);
            mCallback.updateBottomBar(bundle);
        }
        if (mActivity != null) {
            mActivity.setTitle(R.string.wish_status);
        }
    }


    private boolean isValidUser() {

        if (mTflUser == null) {

            AndroidUtil.showToast(mContext, "Invalid User");
            return false;

        } else if (TextUtils.isEmpty(mTflUser.userId)) {

            AndroidUtil.showToast(mContext, "You need to Login !");
            return false;
        }

        return true;
    }

    private void loadWishStatus() {
        if (!isValidUser() || !AndroidUtil.hasInternetConnectivity(mContext)) {
            mWishRefreshLayout.setRefreshing(false);
            return;
        }
        if (!isLoadingData) {
            isLoadingData = true;
            mWishRefreshLayout.setRefreshing(true);
            Call<GetWishResponse> getWishStatusCall = RetrofitManager.getRetrofitWebService()
                    .getWishStatus(mTflUser.userId);
            getWishStatusCall.enqueue(new Callback<GetWishResponse>() {
                @Override
                public void onResponse(Call<GetWishResponse> call, Response<GetWishResponse> response) {
                    new RetrofitResponseValidator(ResponseId.GET_WISH_STATUS, response, WishStatusFragment.this);
                }

                @Override
                public void onFailure(Call<GetWishResponse> call, Throwable t) {
                    String errorMessage = "";
                    if (t != null) {
                        Log.e(TAG, "onFailure: Error : " + t.getMessage());
                        if (t instanceof SocketTimeoutException) {
                            errorMessage = "Please make sure that your device has an active internet connection.";
                        }
                    }

                    isLoadingData = false;
                    mWishRefreshLayout.setRefreshing(false);
                    if (TextUtils.isEmpty(errorMessage)) {
                        AndroidUtil.showErrorToast(mContext);
                    }
                }
            });

        }
    }

    @Override
    public void onValidResponse(@NonNull ResponseId responseId, @NonNull Object responseBody, @NonNull Response response) {
        switch (responseId) {

            case GET_WISH_STATUS:
                GetWishResponse getWishResponse = (GetWishResponse) responseBody;
                mTflUser.myWish = getWishResponse.userWish;
                mTflUser.wishStatus = getWishResponse.wishStatus;
                mSharedPrefHelper.setLoginResponse(mContext, mTflUser);
                ensureTflUser(true);
                displayContent();
                isLoadingData = false;
                mWishRefreshLayout.setRefreshing(false);
                break;

        }
    }

    @Override
    public void onInvalidResponse(@NonNull ResponseId responseId, @NonNull String errorMessage, Response response) {
        switch (responseId) {

            case GET_WISH_STATUS:
                isLoadingData = false;
                mWishRefreshLayout.setRefreshing(false);
                break;
        }
    }
}
