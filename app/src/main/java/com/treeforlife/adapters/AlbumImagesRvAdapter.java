package com.treeforlife.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.treeforlife.R;
import com.treeforlife.Utils.ImageLoaderUtil;
import com.treeforlife.Utils.ImageRotationListener;
import com.treeforlife.customview.CustomTextView;
import com.treeforlife.dataobjects.AlbumImage;

import java.util.ArrayList;

/**
 * Created by TheAppsmiths on 3/20/2018.
 * updated on 14th June 2018. by TheAppsmiths
 */

public class AlbumImagesRvAdapter extends RecyclerView.Adapter<AlbumImagesRvAdapter.AlbumImageHolder> {

    private AlbumImageCallBack mCallBack;
    private ArrayList<AlbumImage> mImageList;

    public AlbumImagesRvAdapter(AlbumImageCallBack callBack) {
        this.mCallBack = callBack;
    }

    @Override
    public AlbumImageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AlbumImageHolder(LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_album_image, parent, false));
    }

    @Override
    public void onBindViewHolder(AlbumImageHolder holder, int position) {

        holder.bindData(position);
    }

    @Override
    public int getItemCount() {
        return (mImageList != null) ? mImageList.size() : 0;
    }

    private void ensureDataList() {
        if (mImageList == null) {
            mImageList = new ArrayList<AlbumImage>();
        }
    }

    //method to refresh the adapter
    public void update(int position) {
        if (getItemCount() > position) {
            notifyItemChanged(position);
        }
    }

    //method to remove the item from list
    public void removeItem(int position) {
        ensureDataList();
        if (mImageList.size() > position && position >= 0) {
            mImageList.remove(position);
            notifyDataSetChanged();
        }
    }

    //method to update the list
    public void update(ArrayList<AlbumImage> albumImages) {
        ensureDataList();
        this.mImageList.clear();
        if (albumImages != null && !albumImages.isEmpty()) {
            this.mImageList.addAll(albumImages);
        }
        notifyDataSetChanged();
    }

    //method to add the data in list
    public void addToList(ArrayList<AlbumImage> albumImagesList) {
        if (albumImagesList != null && !albumImagesList.isEmpty()) {
            if (mImageList == null || mImageList.isEmpty()) {
                update(albumImagesList);
            } else {
                int startPosition = mImageList.size();
                this.mImageList.addAll(albumImagesList);
                notifyItemRangeInserted(startPosition, mImageList.size());
                notifyItemRangeChanged(startPosition - 1, startPosition);
            }
        }

    }

    //method to add the image
    public void addImage(AlbumImage albumImage) {
        if (albumImage != null) {
            ensureDataList();
            mImageList.add(albumImage);
            notifyDataSetChanged();
        }
    }


    public interface AlbumImageCallBack {
        void removeAlbumImage(int position, AlbumImage albumImage);

        void shareAlbumImage(int position, AlbumImage albumImage);

        void albumImageClick(int position, AlbumImage albumImage);
    }

    //Holder view to show the data in adapter
    public class AlbumImageHolder extends RecyclerView.ViewHolder {
        private final Context mContext;
        private final ImageLoader mImageLoader;
        private final CustomTextView mAlbumImageTitleTextView;
        private final ImageView mShareAlbumImageView, mRemoveAlbumImageView, mAlbumImageView;
        private AlbumImage mAlbumImage;

        AlbumImageHolder(View itemView) {
            super(itemView);
            mContext = itemView.getContext();
            mImageLoader = ImageLoaderUtil.getLoader(mContext);

            mShareAlbumImageView = itemView.findViewById(R.id.share_album_image_view);
            mShareAlbumImageView.setVisibility(View.GONE);
            mRemoveAlbumImageView = itemView.findViewById(R.id.remove_album_image_view);
            mAlbumImageView = itemView.findViewById(R.id.album_image_view);
            mAlbumImageTitleTextView = itemView.findViewById(R.id.image_title_text_view);


        }

        public void bindData(int position) {
            if (mImageList != null && mImageList.size() > position && position > -1) {
                mAlbumImage = mImageList.get(position);

            }

            if (mAlbumImage != null) {
                mAlbumImageTitleTextView.setText(mAlbumImage.imageDescription);
                mImageLoader.displayImage(mAlbumImage.imageUrl, mAlbumImageView,
                        ImageLoaderUtil.getDefaultImageOption(), new ImageRotationListener());
            }

            setListener(position);

        }

        private void setListener(final int position) {
            mRemoveAlbumImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mCallBack != null) {
                        mCallBack.removeAlbumImage(position, mAlbumImage);
                    }
                }
            });

            mShareAlbumImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mCallBack != null) {
                        mCallBack.shareAlbumImage(position, mAlbumImage);
                    }
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mCallBack != null) {
                        mCallBack.albumImageClick(position, mAlbumImage);
                    }
                }
            });

        }
    }
}
