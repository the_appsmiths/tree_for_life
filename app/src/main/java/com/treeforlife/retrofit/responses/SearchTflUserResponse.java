package com.treeforlife.retrofit.responses;

import com.google.gson.annotations.SerializedName;
import com.treeforlife.dataobjects.SearchedTflUser;

/**
 * Created by TheAppsmiths on 4/12/2018.
 * @author TheAppsmiths
 */

public class SearchTflUserResponse extends WebServiceResponse {

    @SerializedName("data")
    public SearchedTflUser searchedTflUser;
}
