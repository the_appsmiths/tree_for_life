package com.treeforlife.retrofit.responses;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by TheAppsmiths on 4/20/2018.
 *
 * @author TheAppsmiths
 */

public class TreeInfoOfYearResponse extends WebServiceResponse {

    @SerializedName("data")
    public TreeInfoOfYear treeInfoOfYear;

    public static class TreeInfoOfYear {
        @SerializedName("year")
        public String year;
        @SerializedName("tree_id")
        public String treeId;
        @SerializedName("year_data")
        public ArrayList<TreeInfoOfMonth> monthWiseTreeDataList;
    }

    public static class TreeInfoOfMonth {
        @SerializedName("month")
        public int month;
        @SerializedName("tree_type")
        public String treeType;
        @SerializedName("is_empty_bucket")
        public String isEmptyBucket;
        @SerializedName("tree_country")
        public String treeCountry;
        @SerializedName("month_apple_om_point")
        public int appleOmPointsOfMonth;
        @SerializedName("month_mango_om_point")
        public int mangoOmPointsOfMonth;
        @SerializedName("month_pear_om_point")
        public int pearOmPointsOfMonth;
        @SerializedName("month_orange_om_point")
        public int orangeOmPointsOfMonth;
        @SerializedName("month_om_point")
        public int totalOmPointsOfMonth;

    }
}
