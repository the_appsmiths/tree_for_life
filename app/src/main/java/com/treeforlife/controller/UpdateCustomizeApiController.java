package com.treeforlife.controller;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;

import com.treeforlife.Utils.AndroidUtil;
import com.treeforlife.Utils.DialogUtil;
import com.treeforlife.Utils.ProgressDialogUtil;
import com.treeforlife.commons.Constants;
import com.treeforlife.dataobjects.FriendTflUser;
import com.treeforlife.dataobjects.SearchedTflUser;
import com.treeforlife.dataobjects.TFLUser;
import com.treeforlife.retrofit.ResponseId;
import com.treeforlife.retrofit.RetrofitManager;
import com.treeforlife.retrofit.RetrofitResponseValidator;
import com.treeforlife.retrofit.responses.WebServiceResponse;

import java.net.SocketTimeoutException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TheAppsmiths on 4/11/2018.
 *
 * @author TheAppsmiths
 */

/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class UpdateCustomizeApiController implements RetrofitResponseValidator.ValidationListener {
    private final String TAG;

    private final Fragment mFragment;
    private final Context mContext;
    private final Activity mActivity;
    private final TFLUser mTflUser;
    private final int mCustomizeType;
    private final ResultCallback mCallback;

    private DialogUtil mDialogUtil;
    private ProgressDialogUtil mProgressDialogUtil;

    public UpdateCustomizeApiController(String tag, Context context,
                                        Activity activity, TFLUser tflUser,
                                        ResultCallback mCallback) {
        this.TAG = tag;
        this.mFragment = null;
        this.mContext = context;
        this.mActivity = activity;
        this.mTflUser = tflUser;
        this.mCustomizeType = -1;
        this.mCallback = mCallback;

        this.mDialogUtil = DialogUtil.getInstance();
        this.mProgressDialogUtil = ProgressDialogUtil.getInstance();
    }

    public UpdateCustomizeApiController(String tag, Fragment fragment, Context context,
                                        Activity activity, TFLUser tflUser, int customizeType,
                                        ResultCallback mCallback) {
        this.TAG = tag;
        this.mFragment = fragment;
        this.mContext = context;
        this.mActivity = activity;
        this.mTflUser = tflUser;
        this.mCustomizeType = customizeType;
        this.mCallback = mCallback;

        this.mDialogUtil = DialogUtil.getInstance();
        this.mProgressDialogUtil = ProgressDialogUtil.getInstance();
    }

    public void updateCustomizeDataForFriend(@NonNull SearchedTflUser searchedTflUser,
                                             @NonNull String profileShare,
                                             @NonNull String photoShare,
                                             @NonNull String videoShare,
                                             @NonNull String contactShare) {
        if (isValidData(searchedTflUser) && AndroidUtil.hasInternetConnectivity(mContext)) {
            mProgressDialogUtil.showProgressDialog(mActivity);
            Call<WebServiceResponse> mUpdateCustomizeCall =
                    RetrofitManager.getRetrofitWebService()
                            .updateCustomize(mTflUser.userId, searchedTflUser.id, profileShare,
                                    photoShare, videoShare, contactShare);

            processRequest(mUpdateCustomizeCall);

        } else {
            if (mCallback != null) {
                mCallback.onUpdateCustomizeApiResult(false);
            }
        }
    }

    public void removeFriendFromCustomizeList(@NonNull FriendTflUser friendTflUser) {
        if (isValidData(friendTflUser) && AndroidUtil.hasInternetConnectivity(mContext)) {
            mProgressDialogUtil.showProgressDialog(mActivity);
            String profileShare = "0", videoShare = "0", photoShare = "0", contactShare = "0";
            switch (mCustomizeType) {
                case Constants.CUSTOMIZED_TYPE_PROFILE_SHARE:
                    videoShare = friendTflUser.videoShare;
                    photoShare = friendTflUser.photoShare;
                    contactShare = friendTflUser.contactShare;
                    break;

                case Constants.CUSTOMIZED_TYPE_IMAGE_SHARE:
                    profileShare = friendTflUser.profileShare;
                    videoShare = friendTflUser.videoShare;
                    contactShare = friendTflUser.contactShare;
                    break;

                case Constants.CUSTOMIZED_TYPE_VIDEO_SHARE:
                    profileShare = friendTflUser.profileShare;
                    photoShare = friendTflUser.photoShare;
                    contactShare = friendTflUser.contactShare;
                    break;

                case Constants.CUSTOMIZED_TYPE_CONTACT_SHARE:
                    profileShare = friendTflUser.profileShare;
                    videoShare = friendTflUser.videoShare;
                    photoShare = friendTflUser.photoShare;
                    break;

            }

            Call<WebServiceResponse> mUpdateCustomizeCall =
                    RetrofitManager.getRetrofitWebService()
                            .updateCustomize(mTflUser.userId, friendTflUser.id, profileShare,
                                    photoShare, videoShare, contactShare);

            processRequest(mUpdateCustomizeCall);

        } else {
            if (mCallback != null) {
                mCallback.onUpdateCustomizeApiResult(false);
            }
        }
    }

    private void processRequest(@NonNull Call<WebServiceResponse> mUpdateCustomizeCall) {
        mUpdateCustomizeCall.enqueue(new Callback<WebServiceResponse>() {
            @Override
            public void onResponse(Call<WebServiceResponse> call, Response<WebServiceResponse> response) {
                new RetrofitResponseValidator(ResponseId.UPDATED_CUSTOMIZE, response,
                        UpdateCustomizeApiController.this);

            }

            @Override
            public void onFailure(Call<WebServiceResponse> call, Throwable t) {
                String errorMessage = "";
                if (t != null) {
                    Log.e(TAG, "onFailure: Error : " + t.getMessage());
                    if (t instanceof SocketTimeoutException) {
                        errorMessage = "Please make sure that your device has an active internet connection.";
                    }
                }

                mProgressDialogUtil.dismissProgressDialog();

                if (mFragment != null && !mFragment.isVisible()) {
                    return;
                }

                if (TextUtils.isEmpty(errorMessage)) {
                    AndroidUtil.showErrorToast(mContext);
                } else {
                    mDialogUtil.showErrorDialog(mContext, errorMessage);
                }
            }
        });
    }

    private boolean isValidUser() {
        if (mTflUser == null) {
            AndroidUtil.showToast(mContext, "Invalid User");
            return false;

        } else if (TextUtils.isEmpty(mTflUser.userId)) {
            AndroidUtil.showToast(mContext, "You need to Login !");
            return false;
        }

        return true;
    }

    private boolean isValidData(FriendTflUser friendTflUser) {
        if (friendTflUser == null) {
            AndroidUtil.showToast(mContext, "Invalid Friend !");
            return false;

        } else if (TextUtils.isEmpty(friendTflUser.id)) {
            AndroidUtil.showToast(mContext, "Invalid Friend Id !");
            return false;
        }

        switch (mCustomizeType) {
            case Constants.CUSTOMIZED_TYPE_PROFILE_SHARE:
            case Constants.CUSTOMIZED_TYPE_IMAGE_SHARE:
            case Constants.CUSTOMIZED_TYPE_VIDEO_SHARE:
            case Constants.CUSTOMIZED_TYPE_CONTACT_SHARE:
            case Constants.CUSTOMIZED_TYPE_BLOCKED_CONTACT:
                return isValidUser();

            default:
                AndroidUtil.showToast(mContext, "Invalid Customize Type");
                return false;
        }

    }

    private boolean isValidData(SearchedTflUser searchedTflUser) {
        if (searchedTflUser == null) {
            AndroidUtil.showToast(mContext, "Invalid Friend !");
            return false;

        } else if (TextUtils.isEmpty(searchedTflUser.id)) {
            AndroidUtil.showToast(mContext, "Invalid Friend Id !");
            return false;
        }

        return isValidUser();

    }

    @Override
    public void onValidResponse(@NonNull ResponseId responseId, @NonNull Object responseBody, @NonNull Response response) {
        mProgressDialogUtil.dismissProgressDialog();
        switch (responseId) {
            case UPDATED_CUSTOMIZE:
                WebServiceResponse serviceResponse = (WebServiceResponse) responseBody;
                AndroidUtil.showToast(mContext, serviceResponse.message);
                if (mCallback != null) {
                    mCallback.onUpdateCustomizeApiResult(true);
                }
                break;
        }
    }

    @Override
    public void onInvalidResponse(@NonNull ResponseId responseId, @NonNull String errorMessage, Response response) {
        mProgressDialogUtil.dismissProgressDialog();
        if (mFragment != null && !mFragment.isVisible()) {
            return;
        }

        switch (responseId) {
            case UPDATED_CUSTOMIZE:
                if (mCallback != null) {
                    mCallback.onUpdateCustomizeApiResult(false);
                }
                break;
        }
        if (response == null || !response.isSuccessful()) {
            mDialogUtil.showErrorDialog(mActivity, errorMessage);
        } else {
            mDialogUtil.showOkAlertDialog(mActivity, errorMessage);

        }
    }

    public interface ResultCallback {
        void onUpdateCustomizeApiResult(boolean success);
    }
}
