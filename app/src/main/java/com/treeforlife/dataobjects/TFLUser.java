package com.treeforlife.dataobjects;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by TheAppsmiths on 4/4/2018.
 */

public class TFLUser implements Parcelable {

    @SerializedName("user_id")
    public String userId;

    @SerializedName("first_name")
    public String firstName;

    @SerializedName("last_name")
    public String lastName;

    @SerializedName("email")
    public String email;

    @SerializedName("phone")
    public String phone;

    public static final Creator<TFLUser> CREATOR = new Creator<TFLUser>() {
        @Override
        public TFLUser createFromParcel(Parcel in) {
            return new TFLUser(in);
        }

        @Override
        public TFLUser[] newArray(int size) {
            return new TFLUser[size];
        }
    };

    @SerializedName("vault_mpin")
    public String vaultMpin;

    @SerializedName("where_to_plant")
    public String whereToPlant;

    @SerializedName("profile_image")
    public String profileImage;

    @SerializedName("tree_id")
    public String treeId;

    @SerializedName("register_by")
    public String registerBy;
    @SerializedName(value = "biography", alternate = {"bio"})
    public String biography;

    @SerializedName("wish_status")
    public String wishStatus;

    @SerializedName("phone_dial_code")
    public String phoneDialCode;
    @SerializedName("wish_form")
    public String myWish;
    @SerializedName("street")
    public String street;
    @SerializedName("city")
    public String city;
    @SerializedName("state")
    public String state;
    @SerializedName("address_country_id")
    public String addressCountryId;
    @SerializedName("address_country_name")
    public String addressCountryName;

    protected TFLUser(Parcel in) {
        userId = in.readString();
        firstName = in.readString();
        lastName = in.readString();
        email = in.readString();
        phone = in.readString();
        biography = in.readString();
        vaultMpin = in.readString();
        whereToPlant = in.readString();
        profileImage = in.readString();
        treeId = in.readString();
        registerBy = in.readString();
        myWish = in.readString();
        wishStatus = in.readString();
        phoneDialCode = in.readString();
        street = in.readString();
        city = in.readString();
        state = in.readString();
        addressCountryId = in.readString();
        addressCountryName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userId);
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(email);
        dest.writeString(phone);
        dest.writeString(biography);
        dest.writeString(vaultMpin);
        dest.writeString(whereToPlant);
        dest.writeString(profileImage);
        dest.writeString(treeId);
        dest.writeString(registerBy);
        dest.writeString(myWish);
        dest.writeString(wishStatus);
        dest.writeString(phoneDialCode);
        dest.writeString(street);
        dest.writeString(city);
        dest.writeString(state);
        dest.writeString(addressCountryId);
        dest.writeString(addressCountryName);
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
