package com.treeforlife.activities;

import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.text.method.TransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.treeforlife.R;
import com.treeforlife.Utils.AndroidUtil;
import com.treeforlife.Utils.DialogUtil;
import com.treeforlife.Utils.ProgressDialogUtil;
import com.treeforlife.commons.Constants;
import com.treeforlife.commons.SharedPrefHelper;
import com.treeforlife.retrofit.ResponseId;
import com.treeforlife.retrofit.RetrofitManager;
import com.treeforlife.retrofit.RetrofitResponseValidator;
import com.treeforlife.retrofit.responses.WebServiceResponse;

import java.net.SocketTimeoutException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OtpActivity extends AppCompatActivity implements View.OnClickListener, RetrofitResponseValidator.ValidationListener {

    public static final String TAG = "OtpActivity";
    private Toolbar mToolbar;
    private ActionBar mActionBar;
    private Intent mActivityIntent;

    private TextView mToolbarTitle, mLoginTextView;
    private ImageView mToolbarIconRhs, mToolbarIconLhs;

    private EditText mInputEditText;
    private Button mSubmitButton, mResendButton, mCancelButton;

    private SharedPrefHelper mSharedPrefHelper;
    private ProgressDialogUtil mProgressDialogUtil;
    private DialogUtil mDialogUtil;

    private String mUserId, mInputOtp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        initComponents();
        intiWidgets();
        initToolBar();
    }

    //method to initialize the components
    private void initComponents() {
        mActivityIntent = getIntent();
        mProgressDialogUtil = ProgressDialogUtil.getInstance();
        mDialogUtil = DialogUtil.getInstance();
        mSharedPrefHelper = SharedPrefHelper.getInstance();

        if (mActivityIntent != null) {
            mUserId = mActivityIntent.getStringExtra(Constants.KEY_USER_ID_REGISTER);
        }
    }

    //method to initialize the widgets
    private void intiWidgets() {

        mInputEditText = (EditText) findViewById(R.id.opt_input_etext);
        mInputEditText.setTransformationMethod(new AsteriskPasswordTransformationMethod());

        mSubmitButton = (Button) findViewById(R.id.submit_button);
        mSubmitButton.setOnClickListener(this);

        mResendButton = (Button) findViewById(R.id.resend_button);
        mResendButton.setOnClickListener(this);

        mCancelButton = (Button) findViewById(R.id.cancel_button);
        mCancelButton.setOnClickListener(this);
    }

    //method to initialize the toolabr
    private void initToolBar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        if (mToolbar != null) {
            mToolbarTitle = mToolbar.findViewById(R.id.toolbar_title);
            mToolbarIconLhs = mToolbar.findViewById(R.id.toolbar_icon_back);
            mToolbarIconLhs.setVisibility(View.GONE);
            mToolbarIconRhs = mToolbar.findViewById(R.id.toolbar_icon_hamberg);
            mToolbarIconRhs.setVisibility(View.GONE);

        }
        setSupportActionBar(mToolbar);
        mActionBar = getSupportActionBar();

        setToolBarTitle();
    }

    //method to set the toolbar title
    private void setToolBarTitle() {
        if (mToolbarTitle != null) {
            mToolbarTitle.setText(R.string.verificaion);
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.submit_button:
                callSendOtp();
                break;
            case R.id.resend_button:
                callGenerateOtp();
                break;
            case R.id.cancel_button:
                mSharedPrefHelper.resetEmailPassword(OtpActivity.this);
                goToLoginPage(false);
                break;
        }
    }

    //method to move to the login page
    private void goToLoginPage(boolean sendPhone) {
        Intent in = new Intent(this, LoginActivity.class);
        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        if (sendPhone) {
            in.putExtras(mActivityIntent);
        }
        startActivity(in);
        finish();
    }

    //method to generate the otp
    private void callGenerateOtp() {

        if (!TextUtils.isEmpty(mUserId) && AndroidUtil.hasInternetConnectivity(this)) {
            mProgressDialogUtil.showProgressDialog(this);

            Call<WebServiceResponse> generateOtp = RetrofitManager.getRetrofitWebService()
                    .generateOtp(mUserId);
            generateOtp.enqueue(new Callback<WebServiceResponse>() {
                @Override
                public void onResponse(Call<WebServiceResponse> call, Response<WebServiceResponse> response) {
                    new RetrofitResponseValidator(ResponseId.GENERATE_OTP, response, OtpActivity.this);

                }

                @Override
                public void onFailure(Call<WebServiceResponse> call, Throwable t) {
                    String errorMessage = "";
                    if (t != null) {
                        Log.e(TAG, "onFailure: Error : " + t.getMessage());
                        if (t instanceof SocketTimeoutException) {
                            errorMessage = "Please make sure that your device has an active internet connection.";
                        }
                    }

                    mProgressDialogUtil.dismissProgressDialog();


                    if (TextUtils.isEmpty(errorMessage)) {
                        AndroidUtil.showErrorToast(OtpActivity.this);
                    } else {
                        mDialogUtil.showErrorDialog(OtpActivity.this, errorMessage);
                    }
                }
            });

        }
    }

    private void afterGenerateOtp(WebServiceResponse responseBody) {
        if (responseBody != null) {
            Toast.makeText(this, "" + responseBody.message, Toast.LENGTH_SHORT).show();
        }
    }

    //method to make api call for send otp
    private void callSendOtp() {

        if (!TextUtils.isEmpty(mUserId) && isValidInput() && AndroidUtil.hasInternetConnectivity(this)) {
            Log.e("OtpNumber", " " + mUserId + " " + mInputOtp);
            mProgressDialogUtil.showProgressDialog(this);

            Call<WebServiceResponse> otpVerify = RetrofitManager.getRetrofitWebService()
                    .verifyOtp(mUserId, mInputOtp);
            otpVerify.enqueue(new Callback<WebServiceResponse>() {
                @Override
                public void onResponse(Call<WebServiceResponse> call, Response<WebServiceResponse> response) {
                    new RetrofitResponseValidator(ResponseId.VERIFY_OTP, response, OtpActivity.this);

                }

                @Override
                public void onFailure(Call<WebServiceResponse> call, Throwable t) {
                    String errorMessage = "";
                    if (t != null) {
                        Log.e(TAG, "onFailure: Error : " + t.getMessage());
                        if (t instanceof SocketTimeoutException) {
                            errorMessage = "Please make sure that your device has an active internet connection.";
                        }
                    }

                    mProgressDialogUtil.dismissProgressDialog();


                    if (TextUtils.isEmpty(errorMessage)) {
                        AndroidUtil.showErrorToast(OtpActivity.this);
                    } else {
                        mDialogUtil.showErrorDialog(OtpActivity.this, errorMessage);
                    }
                }
            });

        }
    }

    private void afterOtpVerify(WebServiceResponse responseBody) {
        if (responseBody != null && responseBody.status == true) {
            //Set Value in share on First Time Register
            mSharedPrefHelper.setFirstTimeRegister(this, mInputOtp);

            goToLoginPage(true);
        }

    }

    private boolean isValidInput() {

        mInputOtp = mInputEditText.getText().toString().trim();
        if (TextUtils.isEmpty(mInputOtp)) {
            mInputEditText.setError(getString(R.string.enter_otp));
            mInputEditText.requestFocus();
            return false;
        }


        return true;
    }

    @Override
    public void onValidResponse(@NonNull ResponseId responseId, @NonNull Object responseBody, @NonNull Response response) {

        switch (responseId) {
            case VERIFY_OTP:
                afterOtpVerify((WebServiceResponse) responseBody);
                break;
            case GENERATE_OTP:
                afterGenerateOtp((WebServiceResponse) responseBody);
                break;
        }
        mProgressDialogUtil.dismissProgressDialog();
    }


    @Override
    public void onInvalidResponse(@NonNull ResponseId responseId, @NonNull String errorMessage, Response response) {

        mProgressDialogUtil.dismissProgressDialog();

        if (response == null || !response.isSuccessful()) {
            mDialogUtil.showErrorDialog(this, errorMessage);
        } else {
            mDialogUtil.showOkAlertDialog(this, errorMessage);
        }
    }


    private class AsteriskPasswordTransformationMethod implements TransformationMethod {
        @Override
        public CharSequence getTransformation(CharSequence source, View view) {
            return new PasswordCharSequence(source);
        }

        @Override
        public void onFocusChanged(View view, CharSequence sourceText, boolean focused, int direction, Rect previouslyFocusedRect) {

        }

        private class PasswordCharSequence implements CharSequence {
            private CharSequence mSource;

            public PasswordCharSequence(CharSequence source) {
                mSource = source; // Store char sequence
            }

            @Override
            public int length() {
                return mSource.length(); // Return default
            }

            @Override
            public char charAt(int index) {
                return '*'; // This is the important part
            }

            @Override
            public CharSequence subSequence(int start, int end) {
                return mSource.subSequence(start, end); // Return default
            }
        }
    }
}
