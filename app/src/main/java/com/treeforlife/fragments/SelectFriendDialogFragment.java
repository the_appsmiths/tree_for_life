package com.treeforlife.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.treeforlife.R;
import com.treeforlife.Utils.AndroidUtil;
import com.treeforlife.adapters.SelectFriendsRvAdapter;
import com.treeforlife.commons.Constants;
import com.treeforlife.commons.SharedPrefHelper;
import com.treeforlife.controller.FriendApisController;
import com.treeforlife.customview.CustomTextView;
import com.treeforlife.dataobjects.FriendTflUser;
import com.treeforlife.dataobjects.Group;
import com.treeforlife.dataobjects.TFLUser;
import com.treeforlife.retrofit.ResponseId;
import com.treeforlife.retrofit.responses.GetMembersListResponse;

import java.util.ArrayList;

/**
 * Created by TheAppsmiths on 4/30/2018.
 * updated By TheAppsmiths on 3rd May 2018.
 */

/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class SelectFriendDialogFragment extends DialogFragment implements View.OnClickListener,
        SelectFriendsRvAdapter.FriendItemCallback,
        FriendApisController.GetFriendsListApiResultCallback {

    public static final String TAG = "SelectFriendDialogFragm";

    private Context mContext;
    private Activity mActivity;
    private Bundle mBundle;

    private CustomTextView mToolbarTitleTextView;
    private TextView mNoDataTextView;
    private ProgressBar mProgressBar;
    private RecyclerView mRecyclerView;
    private Button mSubmitButton;

    private SelectFriendsRvAdapter mSelectFriendsRvAdapter;

    private FriendApisController mFriendApisController;
    private SharedPrefHelper mSharedPrefHelper;

    private TFLUser mTflUser;
    private Group mGroup;

    private ArrayList<String> mSelectedFriendIdsList;
    private ArrayList<FriendTflUser> mSelectedFriendsList;
    private String mCaller;

    private int mNoOfTotalRecords, mNoOfLoadedRecords;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
        this.mActivity = getActivity();
        this.mSharedPrefHelper = SharedPrefHelper.getInstance();

        mSelectFriendsRvAdapter = new SelectFriendsRvAdapter(this);
        setStyle(DialogFragment.STYLE_NO_FRAME, R.style.DialogTheme);
        extractDataFromBundle();

        mFriendApisController = new FriendApisController(TAG, this, mContext, mActivity,
                mTflUser, this);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(mContext != null ? mContext :
                mActivity != null ? mActivity : getActivity(),
                R.style.DialogTheme);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ensureComponents();
    }

    private void ensureComponents() {
        if (mContext == null) {
            mContext = getActivity();
        }
        if (mActivity == null) {
            mActivity = getActivity();
        }
        extractDataFromBundle();
    }

    private void extractDataFromBundle() {

        if (mBundle == null) {
            mBundle = getArguments();
        }

        if (mBundle != null) {
            mTflUser = mBundle.getParcelable(Constants.KEY_TFL_USER);
            mGroup = mBundle.getParcelable(Constants.KEY_GROUP);
            mCaller = mBundle.getString(Constants.KEY_CALLER_COMPONENT);
        }
        ensureTflUser();

    }

    private void ensureTflUser() {
        if (mTflUser == null) {
            mTflUser = mSharedPrefHelper.getLoginResponse(mContext);
        }
    }

    private void ensureSelectedFriendList() {
        if (mSelectedFriendIdsList == null) {
            mSelectedFriendIdsList = new ArrayList<String>();
        }
        if (mSelectedFriendsList == null) {
            mSelectedFriendsList = new ArrayList<FriendTflUser>();
        }
    }

    public void update(Bundle bundle) {
        this.mBundle = bundle;
        extractDataFromBundle();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_select_friend_dialog, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initComponents(view);
        manageNoDataTextVisibility();
        loadData(true);
    }

    private void initComponents(View view) {
        if (view != null) {
            Toolbar mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
            if (mToolbar != null) {
                mToolbarTitleTextView = mToolbar.findViewById(R.id.toolbar_title);
                mToolbarTitleTextView.setText(R.string.select_friends);
                View mToolbarIconLhs = mToolbar.findViewById(R.id.toolbar_icon_back);
                mToolbarIconLhs.setVisibility(View.GONE);
                View mToolbarIconRhs = mToolbar.findViewById(R.id.toolbar_icon_hamberg);
                mToolbarIconRhs.setVisibility(View.GONE);

            }

            mNoDataTextView = view.findViewById(R.id.no_data_text_view);
            mProgressBar = view.findViewById(R.id.progress_bar);

            mRecyclerView = view.findViewById(R.id.rv_friends);
            LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
            mRecyclerView.setLayoutManager(layoutManager);
            setInfiniteScroll(layoutManager);
            mRecyclerView.setAdapter(mSelectFriendsRvAdapter);

            mSubmitButton = view.findViewById(R.id.submit_button);
            mSubmitButton.setOnClickListener(this);


        }
    }

    private void setInfiniteScroll(final LinearLayoutManager layoutManager) {
        if (layoutManager != null && mRecyclerView != null) {
            mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    int lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition();
                    int totalItemCount = layoutManager.getItemCount();
                    Log.e("TotalItemCountHome", "" + lastVisibleItemPosition + " " + Constants.ITEMS_LIMIT + " " + totalItemCount);

                    if ((lastVisibleItemPosition + Constants.ITEMS_LIMIT) > totalItemCount) {
                        Log.w(TAG, " RECYCLER ON SCROLL : loadData() called");
                        loadData(false);
                    }
                }
            });
        }
    }

    private void manageNoDataTextVisibility() {
        if (isVisible() && mNoDataTextView != null && mSelectFriendsRvAdapter != null) {
            mNoDataTextView.setVisibility(mSelectFriendsRvAdapter.getItemCount() > 0 ? View.GONE : View.VISIBLE);
        }
    }

    private void loadData(boolean resetData) {
        if (resetData) {
            mNoOfTotalRecords = 0;
            mNoOfLoadedRecords = -1;
        }

        if (isAdded() && mNoOfLoadedRecords < mNoOfTotalRecords) {
            mSubmitButton.setText(R.string.submit);
            mProgressBar.setVisibility(View.VISIBLE);
            mNoDataTextView.setText(R.string.downloading_data);

            if (!TextUtils.isEmpty(mCaller)) {

                switch (mCaller) {

                    case GroupFragment.TAG:
                        mSubmitButton.setText(R.string.next);
                        mFriendApisController.loadMyFriends(resetData);
                        break;

                    case FamilyFragment.TAG:
                        mFriendApisController.loadNotInMyFamilyMembers(resetData);
                        break;

                    case GroupMemberFragment.TAG:
                        mFriendApisController.loadNotInMyGroupMembers(mGroup, resetData);
                        break;

                }
            }
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if (!TextUtils.isEmpty(mCaller)) {

            switch (mCaller) {


                case FamilyFragment.TAG:
                    mToolbarTitleTextView.setText(R.string.add_family_members);
                    break;


            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.submit_button:
                if (mSelectedFriendIdsList != null
                        && !mSelectedFriendIdsList.isEmpty()) {
                    Intent intent = new Intent();
                    intent.putStringArrayListExtra(Constants.KEY_FRIEND_IDS_LIST, mSelectedFriendIdsList);
                    intent.putParcelableArrayListExtra(Constants.KEY_FRIEND_LIST, mSelectedFriendsList);
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                    dismiss();
                } else {
                    String errMessage = "Please select at least one friend to add.";
                    if (mSelectFriendsRvAdapter != null && mSelectFriendsRvAdapter.getItemCount() == 0) {
                        errMessage = "You've no friend. You need to add friend first.";
                    }
                    AndroidUtil.showToast(mActivity, errMessage);
                }
                break;
        }

    }

    @Override
    public void onClick(int position, boolean isSelected, FriendTflUser friendTflUser) {
        if (friendTflUser != null && !TextUtils.isEmpty(friendTflUser.id)) {
            ensureSelectedFriendList();
            if (isSelected) {
                mSelectedFriendIdsList.add(friendTflUser.id);
                mSelectedFriendsList.add(friendTflUser);
            } else {
                mSelectedFriendIdsList.remove(friendTflUser.id);
                mSelectedFriendsList.remove(friendTflUser);
            }
        } else {
            AndroidUtil.showToast(mContext, "Invalid Friend Data!");
        }
    }

    @Override
    public void onFriendsListApiResult(ResponseId responseId, boolean success, GetMembersListResponse response) {
        if (isVisible()) {
            mProgressBar.setVisibility(View.GONE);
            mNoDataTextView.setText(R.string.no_data_to_display);
            if (success && response != null) {

                this.mNoOfTotalRecords = response.totalRecord;

                if (mSelectFriendsRvAdapter != null) {

                    if (mNoOfLoadedRecords == -1) {
                        mSelectFriendsRvAdapter.update(response.friendsOrFamilyMembersList);
                    } else {
                        mSelectFriendsRvAdapter.addToList(response.friendsOrFamilyMembersList);
                    }

                }

                if (mNoOfLoadedRecords == -1) {
                    mNoOfLoadedRecords = response.friendsOrFamilyMembersList.size();
                } else {
                    mNoOfLoadedRecords += response.friendsOrFamilyMembersList.size();
                }

                manageNoDataTextVisibility();

            }
        }
    }
}

