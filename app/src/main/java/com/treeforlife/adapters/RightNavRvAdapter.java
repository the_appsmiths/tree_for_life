package com.treeforlife.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.treeforlife.R;

/**
 * Created by TheAppsmiths on 3/16/2018.
 */

public class RightNavRvAdapter extends RecyclerView.Adapter<RightNavRvAdapter.Holder> {
   private String [] mNavArray;
   private NavItemCallBack mCallBack;

    public RightNavRvAdapter(NavItemCallBack callBack) {
        this.mNavArray=getRightNavItemsTextArray();
        this.mCallBack=callBack;
    }

    private String[] getRightNavItemsTextArray() {
        String[] result = null;

        result = new String[]
                {
                        "Your Comments",
                        "Invite",
                        /*"Converter",*/
                        "Add Friend",
                        "Change Login Password",
                        "Log out"
                };

        return result;
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.nav_item, parent, false));
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {

        holder.bindData(position);
    }

    @Override
    public int getItemCount() {
        return mNavArray.length;
    }

   public interface NavItemCallBack
    {
        void clickItem(View clickedView, int selectedItemPosition);
    }

    //Holder view to show the data in adapter
    public class Holder extends RecyclerView.ViewHolder {

        private TextView mNavText;
        private final View mItemView;

        public Holder(View itemView) {
            super(itemView);
            this.mItemView=itemView;
            this.mNavText=itemView.findViewById(R.id.nav_item_text_view);
        }

        public void bindData(int position) {
            mNavText.setText(mNavArray[position]);

            setListener(position);



        }

        private void setListener(final int position) {

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mCallBack != null)
                    {
                        mCallBack.clickItem(itemView,position);
                        notifyDataSetChanged();
                    }
                }
            });

        }
    }
}
