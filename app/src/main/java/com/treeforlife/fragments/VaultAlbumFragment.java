package com.treeforlife.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.treeforlife.R;
import com.treeforlife.Utils.AndroidUtil;
import com.treeforlife.Utils.AttachFileUtil;
import com.treeforlife.Utils.DialogUtil;
import com.treeforlife.Utils.FragmentUtil;
import com.treeforlife.Utils.ProgressDialogUtil;
import com.treeforlife.adapters.AdapterRvVaultAlbum;
import com.treeforlife.commons.Constants;
import com.treeforlife.commons.ListItemCallback;
import com.treeforlife.commons.UploadImageVideoDialog;
import com.treeforlife.dataobjects.Album;
import com.treeforlife.dataobjects.TFLUser;
import com.treeforlife.retrofit.ResponseId;
import com.treeforlife.retrofit.RetrofitManager;
import com.treeforlife.retrofit.RetrofitResponseValidator;
import com.treeforlife.retrofit.responses.CreateAlbumResponse;
import com.treeforlife.retrofit.responses.GetAlbumResponse;
import com.treeforlife.retrofit.responses.WebServiceResponse;

import java.io.File;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TheAppsmiths on 3/19/2018.
 */

/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class VaultAlbumFragment extends Fragment implements AdapterRvVaultAlbum.VaultAlbumCallBack, View.OnClickListener, ListItemCallback, RetrofitResponseValidator.ValidationListener, UploadImageVideoDialog.UploadDialogCallBack {

    public static final String TAG = "VaultAlbumFragment";

    private Context mContext;
    private Activity mActivity;
    private RecyclerView mRecyclerView;
    private AdapterRvVaultAlbum adapterRvVaultAlbum;
    private View mWishView;

    private TextView mNoDataTextView, mAddAlbum;
    private AttachFileUtil mAttachFileUtil;
    private ProgressDialogUtil mProgressDialogUtil;
    private DialogUtil mDialogUtil;
    private Bundle mBundle;
    private TFLUser mTflUser;
    private ArrayList<Album> mAlbumList = new ArrayList<Album>();
    private String mAlbumName;
    private int albumId, mAlbumPoistion;
    private int mPage;
    private boolean isLoadingData;
    private UploadImageVideoDialog mDialog;
    private int mSpanCount;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
        this.mActivity = getActivity();
        this.adapterRvVaultAlbum = new AdapterRvVaultAlbum(this);
        mAttachFileUtil = AttachFileUtil.getInstance();
        mProgressDialogUtil = ProgressDialogUtil.getInstance();
        mDialogUtil = DialogUtil.getInstance();
        mDialog = new UploadImageVideoDialog(mContext);

        extractDataFromBundle(true);
    }

    private void extractDataFromBundle(boolean extractAfterGet) {

        if (extractAfterGet && mBundle == null) {
            mBundle = getArguments();
        }

        if (mBundle != null) {
            mTflUser = mBundle.getParcelable(Constants.KEY_TFL_USER);

        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_vault_album, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        intiComponents(view);
        getVaultAlbum();
    }


    private void intiComponents(View view) {

        if (getResources().getBoolean(R.bool.isTab)) {
            System.out.println("tablet");
            mSpanCount = 3;
        } else {
            System.out.println("mobile");
            mSpanCount = 2;
        }

        mAddAlbum = view.findViewById(R.id.add_album_text);
        mAddAlbum.setOnClickListener(this);

        mNoDataTextView = view.findViewById(R.id.no_data_text_view);
        mWishView = view.findViewById(R.id.wish_layout);
        mRecyclerView = view.findViewById(R.id.recycleview_vault_album);
        GridLayoutManager layoutManager = new GridLayoutManager(mContext, mSpanCount);

        mRecyclerView.setLayoutManager(layoutManager);
        setinfiniteScroller(layoutManager);
        mRecyclerView.setAdapter(adapterRvVaultAlbum);
        setNoDataTextVisibiltiy();
    }

    private void setinfiniteScroller(final GridLayoutManager layoutManager) {

        if (layoutManager != null && mRecyclerView != null) {
            mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    int lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition();
                    int totalItemCount = layoutManager.getItemCount();
                    Log.e("TotalItemCountHome", "" + lastVisibleItemPosition + " " + Constants.ITEMS_LIMIT + " " + totalItemCount);

                    if ((lastVisibleItemPosition + Constants.ITEMS_LIMIT) > totalItemCount) {
                        Log.w(TAG, " RECYCLER ON SCROLL : loadData() called");
                        getVaultAlbum();
                    }
                }
            });
        }

    }

    private void setNoDataTextVisibiltiy() {
        if (mNoDataTextView != null && adapterRvVaultAlbum != null) {
            mNoDataTextView.setVisibility(adapterRvVaultAlbum.getItemCount() > 0 ? View.GONE : View.VISIBLE);
        }
    }

    private boolean isValidUser() {

        if (mTflUser == null) {

            AndroidUtil.showToast(mContext, "Invalid User");
            return false;

        } else if (TextUtils.isEmpty(mTflUser.userId)) {

            AndroidUtil.showToast(mContext, "You need to Login !");
            return false;
        }

        return true;
    }

    @Override
    public void clickItem(int position, View view) {
        if (isValidUser()) {
            if (mAlbumList != null && mAlbumList.size() > position && position > -1) {
                Bundle bundle = new Bundle();
                bundle.putString(Constants.KEY_CALLER_COMPONENT, TAG);
                bundle.putParcelable(Constants.KEY_TFL_USER, mTflUser);
                bundle.putParcelable(Constants.KEY_ALBUM, mAlbumList.get(position));

                FragmentUtil.loadFragment((AppCompatActivity) mContext,
                        FragmentUtil.FRAGMENT_VAULT_IMAGE, bundle, true);

            } else {
                AndroidUtil.showToast(mContext, "Invalid album data");
            }
        }
    }

    @Override
    public void removeItem(int position, View view) {

        mAlbumPoistion = position;
        if (mAlbumList != null && mAlbumList.size() > position && position > -1) {
            Album album = mAlbumList.get(position);
            if (album == null) {
                return;
            }
            String albumName = album.albumTitle;
            if (TextUtils.isEmpty(albumName)) {
                albumName = "this";
            } else {
                albumName = "\"" + albumName + "\"";
            }
            albumId = mAlbumList.get(position).albumId;

            Runnable OkButton = new Runnable() {
                @Override
                public void run() {
                    callDeleteAlbum(albumId);
                }
            };
            mDialogUtil.showOkCancelAlertDialog(mActivity, "Are you sure you want to delete "
                    + albumName + " album?", OkButton);
        } else {
            AndroidUtil.showToast(mContext, "Invalid album data");
        }


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_album_text:
                mDialog.UploadDialog(UploadImageVideoDialog.UploadFile.UPLOAD_ALBUM, this);
                break;

        }
    }


    private void submitAlbum(File mAttachedFile, String description) {

        if (AndroidUtil.hasInternetConnectivity(mContext)) {
            mProgressDialogUtil.showProgressDialog(mActivity);
            MultipartBody.Builder builder = new MultipartBody.Builder();
            builder.setType(MultipartBody.FORM);
            builder.addFormDataPart("user_id", mTflUser.userId);
            builder.addFormDataPart("album_name", description);

            if (mAttachedFile != null) {
                File file1 = new File(mAttachedFile.getName());
                builder.addFormDataPart("album_image", file1.getName(), (RequestBody.create(MediaType.parse("image"), mAttachedFile)));

            }

            MultipartBody requestBody = builder.build();

            Call<CreateAlbumResponse> createAlbumVault = RetrofitManager.getRetrofitWebService()
                    .createAlbum(requestBody);

            createAlbumVault.enqueue(new Callback<CreateAlbumResponse>() {
                @Override
                public void onResponse(Call<CreateAlbumResponse> call, Response<CreateAlbumResponse> response) {
                    new RetrofitResponseValidator(ResponseId.CREATE_ALBUM, response, VaultAlbumFragment.this);

                }

                @Override
                public void onFailure(Call<CreateAlbumResponse> call, Throwable t) {
                    String errorMessage = "";
                    if (t != null) {
                        Log.e(TAG, "onFailure: Error : " + t.getMessage());
                        if (t instanceof SocketTimeoutException) {
                            errorMessage = "Please make sure that your device has an active internet connection.";
                        }
                    }

                    mProgressDialogUtil.dismissProgressDialog();


                    if (TextUtils.isEmpty(errorMessage)) {
                        AndroidUtil.showErrorToast(mActivity);
                    } else {
                        mDialogUtil.showErrorDialog(mActivity, errorMessage);
                    }
                }
            });

        }


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (mDialog != null) {
            mDialog.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (mDialog != null) {
            mDialog.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    @Override
    public void onItemClicked(int which, String datum, int requestCode) {

    }


    private void getVaultAlbum() {

        if (mTflUser != null && AndroidUtil.hasInternetConnectivity(mContext)
                && !isLoadingData) {

            isLoadingData = true;

            if (mPage == 0) {
                mProgressDialogUtil.showProgressDialog(mActivity);
            }


            Call<GetAlbumResponse> getAlbum = RetrofitManager.getRetrofitWebService()
                    .getVaultAlbum(mTflUser.userId, mPage);
            getAlbum.enqueue(new Callback<GetAlbumResponse>() {
                @Override
                public void onResponse(Call<GetAlbumResponse> call, Response<GetAlbumResponse> response) {
                    new RetrofitResponseValidator(ResponseId.GET_VAULT_ALBUM, response, VaultAlbumFragment.this);

                }

                @Override
                public void onFailure(Call<GetAlbumResponse> call, Throwable t) {
                    String errorMessage = "";
                    if (t != null) {
                        Log.e(TAG, "onFailure: Error : " + t.getMessage());
                        if (t instanceof SocketTimeoutException) {
                            errorMessage = "Please make sure that your device has an active internet connection.";
                        }
                    }

                    mProgressDialogUtil.dismissProgressDialog();
                    isLoadingData = false;

                    if (!isVisible() || mPage != 0) {
                        return;
                    }

                    if (TextUtils.isEmpty(errorMessage)) {
                        AndroidUtil.showErrorToast(mContext);
                    } else {
                        mDialogUtil.showErrorDialog(mContext, errorMessage);
                    }
                }
            });

        }
    }


    private void callDeleteAlbum(int albumId) {

        Log.e("RemoceId", "" + albumId);

        if (mTflUser != null && AndroidUtil.hasInternetConnectivity(mContext)) {
            mProgressDialogUtil.showProgressDialog(mActivity);

            Call<WebServiceResponse> getAlbum = RetrofitManager.getRetrofitWebService()
                    .deleteAlbum(mTflUser.userId, albumId);
            getAlbum.enqueue(new Callback<WebServiceResponse>() {
                @Override
                public void onResponse(Call<WebServiceResponse> call, Response<WebServiceResponse> response) {
                    new RetrofitResponseValidator(ResponseId.DELETE_ALBUM, response, VaultAlbumFragment.this);

                }

                @Override
                public void onFailure(Call<WebServiceResponse> call, Throwable t) {
                    String errorMessage = "";
                    if (t != null) {
                        Log.e(TAG, "onFailure: Error : " + t.getMessage());
                        if (t instanceof SocketTimeoutException) {
                            errorMessage = "Please make sure that your device has an active internet connection.";
                        }
                    }

                    mProgressDialogUtil.dismissProgressDialog();


                    if (TextUtils.isEmpty(errorMessage)) {
                        AndroidUtil.showErrorToast(mContext);
                    } else {
                        mDialogUtil.showErrorDialog(mContext, errorMessage);
                    }
                }
            });

        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if (mActivity != null) {
            mActivity.setTitle(R.string.albums);
        }
        if (mAttachFileUtil != null) {
            mAttachFileUtil.onResume();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mDialog != null) {
            mDialog.DismisDialog();
        }
    }

    @Override
    public void onValidResponse(@NonNull ResponseId responseId, @NonNull Object responseBody, @NonNull Response response) {

        switch (responseId) {
            case GET_VAULT_ALBUM:
                afterGetAlbum((GetAlbumResponse) responseBody);
                isLoadingData = false;
                break;
            case CREATE_ALBUM:
                afterCreateAlbum((CreateAlbumResponse) responseBody);
                break;
            case DELETE_ALBUM:
                afterDeleteAlbum((WebServiceResponse) responseBody);
                break;
        }

        mProgressDialogUtil.dismissProgressDialog();


    }


    @Override
    public void onInvalidResponse(@NonNull ResponseId responseId, @NonNull String errorMessage, Response response) {

        mProgressDialogUtil.dismissProgressDialog();
        switch (responseId) {
            case GET_VAULT_ALBUM:
                isLoadingData = false;
                if (!isVisible() || mPage != 0) {
                    return;
                }
                setNoDataTextVisibiltiy();
                break;
        }
        if (response == null || !response.isSuccessful()) {
            mDialogUtil.showErrorDialog(mActivity, errorMessage);
        } else {
            AndroidUtil.showToast(mContext, errorMessage);
        }
    }

    private void afterGetAlbum(GetAlbumResponse responseBody) {
        if (responseBody != null) {

            ArrayList<Album> list = responseBody.album;
            if (list != null && !list.isEmpty()) {
                mAlbumList.addAll(list);
            }
            if (adapterRvVaultAlbum != null) {
                adapterRvVaultAlbum.updateAdapter(mAlbumList);
            }

            setNoDataTextVisibiltiy();

            if (mAlbumList.size() <= responseBody.totalRecords) {
                mPage++;
            }

        }
    }

    private void afterCreateAlbum(CreateAlbumResponse responseBody) {
        if (responseBody != null) {
            Album album = responseBody.mCreateAlbum;
            mAlbumList.add(album);
            adapterRvVaultAlbum.updateAdapter(mAlbumList);
            AndroidUtil.showToast(mActivity, responseBody.message);
            setNoDataTextVisibiltiy();
            mPage++;
        }
    }

    private void afterDeleteAlbum(WebServiceResponse responseBody) {
        if (responseBody != null && responseBody.status == true) {
            AndroidUtil.showToast(mActivity, responseBody.message);
            mAlbumList.remove(mAlbumPoistion);
            adapterRvVaultAlbum.notifyDataSetChanged();
            setNoDataTextVisibiltiy();

        }
    }

    @Override
    public void onitemCall(File mAttachedFile, String description) {
        if (mAttachedFile != null && !TextUtils.isEmpty(description)) {
            submitAlbum(mAttachedFile, description);
        }

    }
}
