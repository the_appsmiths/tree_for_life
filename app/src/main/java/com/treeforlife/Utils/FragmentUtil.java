package com.treeforlife.Utils;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;

import com.treeforlife.R;
import com.treeforlife.fragments.AddFriendFragment;
import com.treeforlife.fragments.AlbumImageFragment;
import com.treeforlife.fragments.BlockedContactsFragment;
import com.treeforlife.fragments.ChatFragment;
import com.treeforlife.fragments.CommentsFragment;
import com.treeforlife.fragments.ContactShareFragment;
import com.treeforlife.fragments.ContactUsFragment;
import com.treeforlife.fragments.ConverterFragment;
import com.treeforlife.fragments.CustomizedFragment;
import com.treeforlife.fragments.EasyVideoPlayerFragment;
import com.treeforlife.fragments.EditProfileFragment;
import com.treeforlife.fragments.FamilyFragment;
import com.treeforlife.fragments.FriendsFragment;
import com.treeforlife.fragments.GroupFragment;
import com.treeforlife.fragments.GroupMemberFragment;
import com.treeforlife.fragments.HomeFragment;
import com.treeforlife.fragments.MyCommentsFragment;
import com.treeforlife.fragments.MyProfileFragment;
import com.treeforlife.fragments.MyQrCodeFragment;
import com.treeforlife.fragments.NotificationFragment;
import com.treeforlife.fragments.PhotosShareFragment;
import com.treeforlife.fragments.ProfileShareFragment;
import com.treeforlife.fragments.TreeFragment;
import com.treeforlife.fragments.UserProfileFragment;
import com.treeforlife.fragments.VaultAlbumFragment;
import com.treeforlife.fragments.VaultDetailFragment;
import com.treeforlife.fragments.VaultFragment;
import com.treeforlife.fragments.VideoFragment;
import com.treeforlife.fragments.VideoShareFragment;
import com.treeforlife.fragments.VideoViewFragment;
import com.treeforlife.fragments.WishStatusFragment;

/**
 * Created by TheAppsmiths on 3/19/2018.
 */

/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class FragmentUtil {

    public static final String TAG = "FragmentUtil";
    public static final String FRAGMENT_HOME = "HomeFragment";
    public static final String FRAGMENT_TREE = "TreeFragment";
    public static final String FRAGMENT_VAULT = "VaultFragment";
    public static final String FRAGMENT_CHAT = "ChatFragment";
    public static final String FRAGMENT_NOTIFICATION = "NotificationFragment";
    public static final String FRAGMENT_VAULT_DETAIL = "VaultDetailFragment";
    public static final String FRAGMENT_VAULT_ALBUM = "VaultAlbumFragment";
    public static final String FRAGMENT_VAULT_IMAGE = "AlbumImageFragment";
    public static final String FRAGMENT_VIDEO = "VideoFragment";
    public static final String FRAGMENT_FRIENDS = "FriendsFragment";
    public static final String FRAGMENT_ADD_FRIEND = "AddFriendFragment";
    public static final String FRAGMENT_MY_PROFILE = "MyProfileFragment";
    public static final String FRAGMENT_EDIT_PROFILE = "EditProfileFragment";
    public static final String FRAGMENT_USER_PROFILE = "UserProfileFragment";
    public static final String FRAGMENT_CONVERTER = "ConverterFragment";
    public static final String FRAGMENT_COMMENTS = "CommentsFragment";
    public static final String FRAGMENT_MY_COMMENTS = "MyCommentsFragment";
    public static final String FRAGMENT_CUSTOMIZED = "CustomizedFragment";
    public static final String FRAGMENT_CONTACT_US = "ContactUsFragment";
    public static final String FRAGMENT_PROFILE_SHARE = "ProfileShareFragment";
    public static final String FRAGMENT_VIDEO_SHARE = "VideoShareFragment";
    public static final String FRAGMENT_PHOTO_SHARE = "PhotosShareFragment";
    public static final String FRAGMENT_CONTACT_SHARE = "ContactShareFragment";
    public static final String FRAGMENT_BLOCKED_CONTACT = "BlockedContactsFragment";
    public static final String FRAGMENT_VIDEO_VIEW = "VideoViewFragment";
    public static final String FRAGMENT_EASY_VIDEO_PLAYER = "EasyVideoPlayerFragment";
    public static final String FRAGMENT_GROUP = "GroupFragment";
    public static final String FRAGMENT_GROUP_MEMBER = "GroupMemberFragment";
    public static final String FRAGMENT_FAMILY = "FamilyFragment";
    public static final String FRAGMENT_QR_CODE = "MyQrCodeFragment";
    public static final String FRAGMENT_WISH_STATUS = "WishStatusFragment";

    public static void loadFragment(@NonNull AppCompatActivity activity,
                                    @NonNull String fragmentName,
                                    boolean addToBackStack) {
        loadFragment(activity, fragmentName, R.id.fragment_container, addToBackStack);

    }

    public static void loadFragment(@NonNull AppCompatActivity activity,
                                    @NonNull String fragmentName,
                                    @IdRes int containerId,
                                    boolean addToBackStack) {
        loadFragment(activity, fragmentName, containerId, null, addToBackStack);

    }

    public static void loadFragment(@NonNull AppCompatActivity activity,
                                    @NonNull String fragmentName,
                                    @Nullable Bundle bundle,
                                    boolean addToBackStack) {
        loadFragment(activity, fragmentName, R.id.fragment_container, bundle, addToBackStack);

    }

    private static void loadFragment(@NonNull AppCompatActivity activity,
                                     @NonNull String fragmentName,
                                     @IdRes int containerId,
                                     @Nullable Bundle bundle,
                                     boolean addToBackStack) {

        if (activity == null) {
            Log.e(TAG, "loadFragment: Error = activity is NULL");
            return;
        }

        if (TextUtils.isEmpty(fragmentName)) {
            Log.e(TAG, "loadFragment: Error = fragmentName is Null/Empty : " + fragmentName);
            return;
        }

        Log.d(TAG, "loadFragment() called with: activity = [" + activity +
                "], fragmentName = [" + fragmentName + "], containerId = [" + containerId + "]," +
                " bundle = [" + bundle + "], addToBackStack = [" + addToBackStack + "]");

        boolean isFragmentAlreadyActive = false;

        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        Fragment activeFragment = null;
        activeFragment = fragmentManager.findFragmentById(containerId);//getCurrentlyActiveFragment();

        isFragmentAlreadyActive = isInstanceOfRequireFragment(fragmentName, activeFragment);

        if (isFragmentAlreadyActive) {
            updateFragmentData(activeFragment, fragmentName, bundle);
        } else {

            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            Fragment fragment = null;
            if (!TextUtils.isEmpty(fragmentName)) {
                fragment = fragmentManager.findFragmentByTag(fragmentName);
            }

            if (fragment == null) {
                fragment = newInstanceOfRequiredFragment(fragmentName);
                fragment.setArguments(bundle);

                // Code to add the Fragment

                if (addToBackStack) {
                    fragmentTransaction.replace(containerId, fragment, fragmentName)
                            .addToBackStack(fragmentName)
                            // java.lang.IllegalStateException: This transaction is already being added to the back stack
                            .commitAllowingStateLoss();

                } else {

                    fragmentTransaction.replace(containerId, fragment, fragmentName)
                            .commitNowAllowingStateLoss();
                }

            } else {
                updateFragmentData(fragment, fragmentName, bundle);
                if (!fragmentManager.popBackStackImmediate(fragmentName,
                        0 )) {
                    fragmentTransaction.replace(containerId, fragment, fragmentName)
                            .commitNowAllowingStateLoss();
                }

            }
        }
    }

    private static Fragment newInstanceOfRequiredFragment(@NonNull String fragmentName) {

        Fragment fragment;
        switch (fragmentName) {

            case FRAGMENT_HOME:
                fragment = new HomeFragment();
                break;

            case FRAGMENT_TREE:
                fragment = new TreeFragment();
                break;

            case FRAGMENT_VAULT:
                fragment = new VaultFragment();
                break;

            case FRAGMENT_CHAT:
                fragment = new ChatFragment();
                break;

            case FRAGMENT_NOTIFICATION:
                fragment = new NotificationFragment();
                break;

            case FRAGMENT_VAULT_DETAIL:
                fragment = new VaultDetailFragment();
                break;

            case FRAGMENT_VAULT_ALBUM:
                fragment = new VaultAlbumFragment();
                break;

            case FRAGMENT_VAULT_IMAGE:
                fragment = new AlbumImageFragment();
                break;

            case FRAGMENT_VIDEO:
                fragment = new VideoFragment();
                break;

            case FRAGMENT_FRIENDS:
                fragment = new FriendsFragment();
                break;

            case FRAGMENT_ADD_FRIEND:
                fragment = new AddFriendFragment();
                break;

            case FRAGMENT_MY_PROFILE:
                fragment = new MyProfileFragment();
                break;

            case FRAGMENT_EDIT_PROFILE:
                fragment = new EditProfileFragment();
                break;

            case FRAGMENT_USER_PROFILE:
                fragment = new UserProfileFragment();
                break;

            case FRAGMENT_CONVERTER:
                fragment = new ConverterFragment();
                break;

            case FRAGMENT_COMMENTS:
                fragment = new CommentsFragment();
                break;

            case FRAGMENT_MY_COMMENTS:
                fragment = new MyCommentsFragment();
                break;

            case FRAGMENT_CUSTOMIZED:
                fragment = new CustomizedFragment();
                break;

            case FRAGMENT_CONTACT_US:
                fragment = new ContactUsFragment();
                break;

            case FRAGMENT_PROFILE_SHARE:
                fragment = new ProfileShareFragment();
                break;

            case FRAGMENT_VIDEO_SHARE:
                fragment = new VideoShareFragment();
                break;

            case FRAGMENT_PHOTO_SHARE:
                fragment = new PhotosShareFragment();
                break;

            case FRAGMENT_CONTACT_SHARE:
                fragment = new ContactShareFragment();
                break;

            case FRAGMENT_BLOCKED_CONTACT:
                fragment = new BlockedContactsFragment();
                break;

            case FRAGMENT_VIDEO_VIEW:
                fragment = new VideoViewFragment();
                break;

            case FRAGMENT_EASY_VIDEO_PLAYER:
                fragment = new EasyVideoPlayerFragment();
                break;


            case FRAGMENT_GROUP:
                fragment = new GroupFragment();
                break;

            case FRAGMENT_GROUP_MEMBER:
                fragment = new GroupMemberFragment();
                break;

            case FRAGMENT_FAMILY:
                fragment = new FamilyFragment();
                break;

            case FRAGMENT_QR_CODE:
                fragment = new MyQrCodeFragment();
                break;

            case FRAGMENT_WISH_STATUS:
                fragment = new WishStatusFragment();
                break;

            default:
                throw new RuntimeException("Illegal Fragment Name : " + fragmentName +
                        " No implementation has been found.");
        }

        return fragment;
    }

    private static boolean updateFragmentData(@NonNull Fragment activeFragment, @NonNull String fragmentName, @NonNull Bundle bundle) {
        if (activeFragment == null) {
            Log.e(TAG, "updateFragmentData: Error = fragment is Null ");
            return false;
        }

        if (TextUtils.isEmpty(fragmentName)) {
            Log.e(TAG, "updateFragmentData: Error = fragmentName is Null/Empty : " + fragmentName);
            return false;
        }
        switch (fragmentName) {
            case FRAGMENT_FRIENDS:
                ((FriendsFragment) activeFragment).update(bundle);
                return true;
            case FRAGMENT_USER_PROFILE:
                ((UserProfileFragment) activeFragment).update(bundle);
                return true;
        }

        return false;
    }

    private static boolean isInstanceOfRequireFragment(@NonNull String fragmentName, @NonNull Fragment activeFragment) {
        boolean result = false;
        if (activeFragment != null && !TextUtils.isEmpty(fragmentName)) {
            switch (fragmentName) {
                case FRAGMENT_HOME:
                    result = activeFragment instanceof HomeFragment;
                    break;
                case FRAGMENT_TREE:
                    result = activeFragment instanceof TreeFragment;
                    break;
                case FRAGMENT_VAULT:
                    result = activeFragment instanceof VaultFragment;
                    break;
                case FRAGMENT_CHAT:
                    result = activeFragment instanceof ChatFragment;
                    break;
                case FRAGMENT_NOTIFICATION:
                    result = activeFragment instanceof NotificationFragment;
                    break;
                case FRAGMENT_VAULT_DETAIL:
                    result = activeFragment instanceof VaultDetailFragment;
                    break;
                case FRAGMENT_VAULT_ALBUM:
                    result = activeFragment instanceof VaultAlbumFragment;
                    break;
                case FRAGMENT_VAULT_IMAGE:
                    result = activeFragment instanceof AlbumImageFragment;
                    break;
                case FRAGMENT_VIDEO:
                    result = activeFragment instanceof VideoFragment;
                    break;
                case FRAGMENT_FRIENDS:
                    result = activeFragment instanceof FriendsFragment;
                    break;
                case FRAGMENT_ADD_FRIEND:
                    result = activeFragment instanceof AddFriendFragment;
                    break;
                case FRAGMENT_MY_PROFILE:
                    result = activeFragment instanceof MyProfileFragment;
                    break;
                case FRAGMENT_EDIT_PROFILE:
                    result = activeFragment instanceof EditProfileFragment;
                    break;
                case FRAGMENT_USER_PROFILE:
                    result = activeFragment instanceof UserProfileFragment;
                    break;
                case FRAGMENT_CONVERTER:
                    result = activeFragment instanceof ConverterFragment;
                    break;

                case FRAGMENT_COMMENTS:
                    result = activeFragment instanceof CommentsFragment;
                    break;
                case FRAGMENT_MY_COMMENTS:
                    result = activeFragment instanceof MyCommentsFragment;
                    break;
                case FRAGMENT_CUSTOMIZED:
                    result = activeFragment instanceof CustomizedFragment;
                    break;
                case FRAGMENT_CONTACT_US:
                    result = activeFragment instanceof ContactUsFragment;
                    break;
                case FRAGMENT_VIDEO_VIEW:
                    result = activeFragment instanceof VideoViewFragment;
                    break;

                case FRAGMENT_EASY_VIDEO_PLAYER:
                    result = activeFragment instanceof EasyVideoPlayerFragment;
                    break;

                case FRAGMENT_GROUP:
                    result = activeFragment instanceof GroupFragment;
                    break;

                case FRAGMENT_GROUP_MEMBER:
                    result = activeFragment instanceof GroupMemberFragment;
                    break;

                case FRAGMENT_FAMILY:
                    result = activeFragment instanceof FamilyFragment;
                    break;

                case FRAGMENT_QR_CODE:
                    result = activeFragment instanceof MyQrCodeFragment;
                    break;

                case FRAGMENT_WISH_STATUS:
                    result = activeFragment instanceof MyQrCodeFragment;
                    break;
            }

        }
        return result;
    }

    public static void clearFragmentBackStack(@NonNull AppCompatActivity activity) {
        if (activity != null) {
            FragmentManager fragmentManager = activity.getSupportFragmentManager();
            if (fragmentManager != null) {
                try {
                    if (fragmentManager.getBackStackEntryCount() > 0) {
                        fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }


    public static void removeFragment(AppCompatActivity activity) {
        removeFragment(activity, "");
    }

    public static void removeFragment(AppCompatActivity activity, String fragmentName) {
        removeFragment(activity, fragmentName, R.id.fragment_container);

    }

    public static void removeFragment(AppCompatActivity activity, String fragmentName, int containerId) {
        if (activity == null) {
            Log.e(TAG, "loadFragment: Error = activity is NULL");
            return;
        }


        Log.d(TAG, "removeFragment() called with: activity = [" + activity +
                "], fragmentName = [" + fragmentName + "]");

        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        Fragment fragment = null;

        boolean mayRemove = true;
        if (!TextUtils.isEmpty(fragmentName)) {
            fragment = fragmentManager.findFragmentByTag(fragmentName);
            mayRemove = isInstanceOfRequireFragment(fragmentName, fragment);

        } else {

            fragment = fragmentManager.findFragmentById(containerId); // Currently Active Fragment
        }
        if (fragment != null && mayRemove) {
            fragmentManager.beginTransaction().remove(fragment).commit();
        }

    }

}
