package com.treeforlife.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.treeforlife.R;
import com.treeforlife.Utils.ImageLoaderUtil;
import com.treeforlife.customview.CustomTextView;
import com.treeforlife.dataobjects.FriendTflUser;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by TheAppsmiths on 4/16/2018.
 * updated By TheAppsmiths on 3rd May 2018.
 */

public class SelectFriendsRvAdapter extends RecyclerView.Adapter<SelectFriendsRvAdapter.SelectFriendHolder> {
    private static final String TAG = "SelectFriendsRvAdapter";

    private ArrayList<FriendTflUser> mList;

    private final FriendItemCallback mItemCallback;

    public SelectFriendsRvAdapter(FriendItemCallback itemCallback) {
        this.mItemCallback = itemCallback;
    }

    @Override
    public SelectFriendHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SelectFriendHolder(LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_friend_list_dialog, parent, false));
    }

    @Override
    public void onBindViewHolder(SelectFriendHolder holder, int position) {
        holder.bindData(position);
    }

    @Override
    public int getItemCount() {
        return (mList != null ? mList.size() : 0);
    }

    private void ensureDataList() {
        if (mList == null) {
            mList = new ArrayList<FriendTflUser>();
        }
    }

    //update list to the adapter
    public void update(ArrayList<FriendTflUser> commentsList) {
        ensureDataList();
        this.mList.clear();
        if (commentsList != null && !commentsList.isEmpty()) {
            this.mList.addAll(commentsList);
        }
        notifyDataSetChanged();
    }

    //add list in to the adapter
    public void addToList(ArrayList<FriendTflUser> list) {
        if (list != null && !list.isEmpty()) {
            if (mList == null || mList.isEmpty()) {
                update(list);
            } else {
                int startPosition = mList.size();
                this.mList.addAll(list);
                notifyItemRangeInserted(startPosition, mList.size());
                notifyItemRangeChanged(startPosition - 1, startPosition);
            }
        }

    }

    //Holder view to show the data in adapter
    public class SelectFriendHolder extends RecyclerView.ViewHolder {

        private final View mItemView, mDividerView;
        private final CircleImageView mFriendImageView;
        private final CustomTextView mFriendNameTextView;
        private final CheckBox mCheckBox;

        private final Context mContext;
        private final ImageLoader mImageLoader;

        private FriendTflUser mFriendTflUser;


        public SelectFriendHolder(View itemView) {
            super(itemView);
            this.mItemView = itemView;
            this.mFriendImageView = itemView.findViewById(R.id.friend_image);
            this.mFriendNameTextView = itemView.findViewById(R.id.name_text);
            this.mCheckBox = itemView.findViewById(R.id.check_box);
            this.mDividerView = itemView.findViewById(R.id.divider_view);

            this.mContext = itemView.getContext();
            this.mImageLoader = ImageLoaderUtil.getLoader(mContext);
        }

        public void bindData(final int position) {
            if (mList != null && mList.size() > position) {
                mFriendTflUser = mList.get(position);
            }

            if (mFriendTflUser != null) {
                mFriendNameTextView.setText(mFriendTflUser.name);
                mImageLoader.displayImage(mFriendTflUser.imageUrl, mFriendImageView,
                        ImageLoaderUtil.getDefaultProfileImageOption());
            }
            mDividerView.setVisibility(position == getItemCount() - 1 ? View.GONE : View.VISIBLE);


            mItemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mItemCallback != null) {
                        mCheckBox.setChecked(!mCheckBox.isChecked());
                        mItemCallback.onClick(position, mCheckBox.isChecked(), mFriendTflUser);
                    }
                }
            });
        }
    }


    public interface FriendItemCallback {
        void onClick(int position, boolean isSelected, FriendTflUser friendTflUser);
    }
}