package com.treeforlife.controller;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.treeforlife.Utils.AndroidUtil;
import com.treeforlife.Utils.ProgressDialogUtil;
import com.treeforlife.commons.Data;
import com.treeforlife.retrofit.ResponseId;
import com.treeforlife.retrofit.RetrofitManager;
import com.treeforlife.retrofit.RetrofitResponseValidator;
import com.treeforlife.retrofit.responses.GetCountriesListResponse;

import java.net.SocketTimeoutException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TheAppsmiths on 4/11/2018.
 *
 * @author TheAppsmiths
 */

/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class GetCountriesApiController implements RetrofitResponseValidator.ValidationListener {
    private static final String TAG = "GetCountriesApiControll";
    private static GetCountriesApiController mGetCountriesApiController;
    private ArrayList<ResultCallback> mCallbacksList;
    private ProgressDialogUtil mProgressDialogUtil;
    private boolean isLoadingData;

    private GetCountriesApiController() {
        mProgressDialogUtil = ProgressDialogUtil.getInstance();
    }

    public static GetCountriesApiController getInstance() {
        if (mGetCountriesApiController == null) {
            mGetCountriesApiController = new GetCountriesApiController();
        }
        return mGetCountriesApiController;
    }

    private void ensureCallBacksList() {
        if (mCallbacksList == null) {
            mCallbacksList = new ArrayList<ResultCallback>();
        }
    }

    public void loadData(@NonNull Context context) {
        if (!isLoadingData && context != null && AndroidUtil.hasInternetConnectivity(context,
                false)) {
            Log.w(TAG, "LOADING COUNTRIES LIST FROM SERVER");
            isLoadingData = true;

            Call<GetCountriesListResponse> mGetCountriesListCall
                    = RetrofitManager.getRetrofitWebService().getCountriesList();

            mGetCountriesListCall.enqueue(new Callback<GetCountriesListResponse>() {
                @Override
                public void onResponse(Call<GetCountriesListResponse> call, Response<GetCountriesListResponse> response) {
                    new RetrofitResponseValidator(ResponseId.GET_COUNTRIES_LIST, response,
                            GetCountriesApiController.this);
                }

                @Override
                public void onFailure(Call<GetCountriesListResponse> call, Throwable t) {
                    String errorMessage = "";
                    if (t != null) {
                        Log.e(TAG, "onFailure: Error : " + t.getMessage());
                        if (t instanceof SocketTimeoutException) {
                            errorMessage = "Please make sure that your device has an active internet connection.";
                        }
                    }
                    Log.e(TAG, "onFailure: " + errorMessage);
                    isLoadingData = false;
                }
            });
        }
    }

    public boolean loadData(@NonNull Activity activity, ResultCallback callback) {
        if (!isLoadingData && activity != null && AndroidUtil.hasInternetConnectivity(activity)) {
            ensureCallBacksList();
            mCallbacksList.add(callback);
            Log.w(TAG, "LOADING COUNTRIES LIST FROM SERVER");
            isLoadingData = true;
            mProgressDialogUtil.showProgressDialog(activity);

            Call<GetCountriesListResponse> mGetCountriesListCall
                    = RetrofitManager.getRetrofitWebService().getCountriesList();

            mGetCountriesListCall.enqueue(new Callback<GetCountriesListResponse>() {
                @Override
                public void onResponse(Call<GetCountriesListResponse> call, Response<GetCountriesListResponse> response) {
                    new RetrofitResponseValidator(ResponseId.GET_COUNTRIES_LIST, response,
                            GetCountriesApiController.this);
                }

                @Override
                public void onFailure(Call<GetCountriesListResponse> call, Throwable t) {
                    String errorMessage = "";
                    if (t != null) {
                        Log.e(TAG, "onFailure: Error : " + t.getMessage());
                        if (t instanceof SocketTimeoutException) {
                            errorMessage = "Please make sure that your device has an active internet connection.";
                        }
                    }
                    Log.e(TAG, "onFailure: " + errorMessage);
                    isLoadingData = false;
                    mProgressDialogUtil.dismissProgressDialog();
                }
            });
        }
        return isLoadingData;
    }

    @Override
    public void onValidResponse(@NonNull ResponseId responseId, @NonNull Object responseBody, @NonNull Response response) {

        GetCountriesListResponse countriesListResponse = (GetCountriesListResponse) responseBody;
        Data.countryCodesList = countriesListResponse.countriesInfoList;

        isLoadingData = false;
        mProgressDialogUtil.dismissProgressDialog();

        ensureCallBacksList();
        for (ResultCallback resultCallback : mCallbacksList) {
            if (resultCallback != null) {
                resultCallback.onResult(true, countriesListResponse.message);
            }
        }
        mCallbacksList.clear();
    }

    @Override
    public void onInvalidResponse(@NonNull ResponseId responseId, @NonNull String errorMessage, Response response) {
        isLoadingData = false;
        Log.e(TAG, "onInvalidResponse: " + errorMessage);
        mProgressDialogUtil.dismissProgressDialog();
        ensureCallBacksList();
        for (ResultCallback resultCallback : mCallbacksList) {
            if (resultCallback != null) {
                resultCallback.onResult(true, errorMessage);
            }
        }
        mCallbacksList.clear();

    }

    public interface ResultCallback {
        void onResult(boolean success, String message);
    }
}
