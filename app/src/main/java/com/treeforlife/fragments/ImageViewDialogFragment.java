package com.treeforlife.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.treeforlife.R;
import com.treeforlife.Utils.AndroidUtil;
import com.treeforlife.Utils.DialogUtil;
import com.treeforlife.Utils.ImageLoaderUtil;
import com.treeforlife.commons.Constants;

/**
 * Created by TheAppsmiths on 5/2/2018.
 *
 * @author TheAppsmiths
 */
/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class ImageViewDialogFragment extends DialogFragment {
    public static final String TAG = "ImageViewDialogFragment";

    private Context mContext;
    private Activity mActivity;
    private Bundle mBundle;

    private ImageView mImageView;
    private ProgressBar mProgressBar;

    private ImageLoader mImageLoader;
    private DialogUtil mDialogUtil;

    private String mCaller, mImageUrl;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        super.onAttach(context);
        this.mContext = context;
        this.mActivity = getActivity();
        this.mBundle = getArguments();

        this.mImageLoader = ImageLoaderUtil.getLoader(mContext);
        this.mDialogUtil = DialogUtil.getInstance();

        setStyle(DialogFragment.STYLE_NO_FRAME, R.style.DialogTheme);
        extractDataFromBundle();

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(mContext != null ? mContext :
                mActivity != null ? mActivity : getActivity(),
                R.style.DialogTheme);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ensureComponents();
    }

    private void ensureComponents() {
        if (mContext == null) {
            mContext = getActivity();
        }
        if (mActivity == null) {
            mActivity = getActivity();
        }
        extractDataFromBundle();
    }


    private void extractDataFromBundle() {

        if (mBundle == null) {
            mBundle = getArguments();
        }

        if (mBundle != null) {
            mCaller = mBundle.getString(Constants.KEY_CALLER_COMPONENT);
            mImageUrl = mBundle.getString(Constants.KEY_IMAGE_URL);

        }

    }

    public void update(Bundle bundle) {
        this.mBundle = bundle;
        extractDataFromBundle();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_image_view, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initWidgets(view);
        loadImage();
    }

    private void initWidgets(View view) {
        if (view != null) {
            mImageView = view.findViewById(R.id.image_view);
            mProgressBar = view.findViewById(R.id.progress_bar);
            mProgressBar.setVisibility(View.GONE);
        }

    }

    private void loadImage() {
        if (TextUtils.isEmpty(mImageUrl)) {
            AndroidUtil.showToast(mContext, "Empty Image Url");
            return;
        }

        if (AndroidUtil.hasInternetConnectivity(mContext)) {
            mImageLoader.displayImage(mImageUrl, mImageView, ImageLoaderUtil.getDefaultImageOption(),
                    new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String imageUri, View view) {
                            if (isVisible()) {
                                mProgressBar.setVisibility(View.VISIBLE);
                            }
                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                            if (isVisible()) {
                                String errMessage = "Image loading failed.\n";
                                if (failReason != null) {
                                    errMessage += failReason.getType();
                                    Throwable cause = failReason.getCause();
                                    if (cause != null) {
                                        errMessage += cause.getMessage();
                                    }
                                }
                                mDialogUtil.showErrorDialog(mContext, errMessage);
                                mProgressBar.setVisibility(View.GONE);
                            }

                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            if (isVisible()) {
                                mProgressBar.setVisibility(View.GONE);
                            }
                        }

                        @Override
                        public void onLoadingCancelled(String imageUri, View view) {
                            if (isVisible()) {
                                mProgressBar.setVisibility(View.GONE);
                            }
                        }
                    },
                    new ImageLoadingProgressListener() {
                        @Override
                        public void onProgressUpdate(String imageUri, View view, int current, int total) {
                            if (isVisible() && current < total
                                    && mProgressBar.getVisibility() != View.VISIBLE) {
                                mProgressBar.setVisibility(View.VISIBLE);
                            }
                        }
                    });
        }

    }
}
