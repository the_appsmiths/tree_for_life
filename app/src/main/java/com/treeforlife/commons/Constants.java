package com.treeforlife.commons;

import com.treeforlife.BuildConfig;

/**
 * Created by TheAppsmiths on 3/26/2018.
 *
 * @author TheAppsmiths
 */

public interface Constants {

    int ITEMS_LIMIT = 10;
    int FRUIT_TYPE_APPLE = 1;
    int FRUIT_TYPE_ORANGE = 2;
    int FRUIT_TYPE_PEAR = 3;
    int FRUIT_TYPE_MANGO = 4;
    int FRUIT_TYPE_ALL= 5;

    int CUSTOMIZED_TYPE_PROFILE_SHARE = 0;
    int CUSTOMIZED_TYPE_IMAGE_SHARE = 1;
    int CUSTOMIZED_TYPE_VIDEO_SHARE = 2;
    int CUSTOMIZED_TYPE_CONTACT_SHARE = 3;
    int CUSTOMIZED_TYPE_BLOCKED_CONTACT = 4;

    int NOTIFICATION_ID = 4444;
    int RC_START_IMPLICIT_ACTIVITY_CAMERA = 30001;
    int RC_START_IMPLICIT_ACTIVITY_GALLERY = 30002;
    int RC_START_ACTIVITY_SELECT_COUNTRY = 31001;
    int RC_START_ACTIVITY_CUSTOMIZE = 31002;

    int RC_SELECT_FRIEND_FRAGMENT = 32001;
    int RC_NEW_GROUP_FRAGMENT = 32002;

    int RC_ACTIVITY_PENDING_INTENT = 30011;

    long ALLOWED_ATTACHED_FILE_SIZE_IN_MB = 20;

    String CUSTOMIZED_STATUS_ALLOWED = "1";
    String CUSTOMIZED_STATUS_DENIED = "0";

    String ACTION_PREFIX = BuildConfig.APPLICATION_ID + ".";
    String ACTION_USER_PROFILE_UPDATED = ACTION_PREFIX + "ACTION_USER_PROFILE_UPDATED";
    String ACTION_RECEIVED_PUSH_NOTIFICATION = ACTION_PREFIX + "ACTION_RECEIVED_PUSH_NOTIFICATION";

    String KEY_GO_TO_FRAGMENT_NAME = "key_go_to_fragment_name";
    String KEY_CALLER_COMPONENT = "key_caller_component";
    String KEY_FRAGMENT_CALLBACK = "key_fragment_callback";
    String KEY_FRIEND_ID = "friend_id";
    String KEY_USER_ID_REGISTER = "key_user_id_register";
    String KEY_REGISTER_BY = "key_register_by";
    String KEY_EMAIL = "key_email";
    String KEY_PHONE = "key_phone";
    String KEY_REQ_CODE = "key_req_code";
    String KEY_COUNTRY_INFO = "key_country_info";
    String KEY_PHONE_DIAL_CODE = "key_phone_dial_code";
    String KEY_TFL_USER = "key_tfl_user";
    String KEY_SEARCHED_TFL_USER = "key_searched_tfl_user";
    String KEY_FRIEND_TFL_USER = "key_friend_tfl_user";
    String KEY_FRUIT_TYPE = "fruit_type";
    String KEY_CUSTOMIZE_TYPE = "customize_type";
    String KEY_HAS_CUSTOMIZE_UPDATED = "has_customize_updated";
    String KEY_WISH_NAME = "key_wish_name";
    String KEY_WISH_STATUS = "key_wish_status";
    String KEY_FRIEND_IDS_LIST = "friends_ids_list";
    String KEY_FRIEND_LIST = "friends_list";
    String KEY_TREE_ID = "tree_id";
    String KEY_EMAIL_ID = "email_id";
    String KEY_NOTIFICATION = "notification";
    String KEY_TUTORIAL = "tutorial";

    String KEY_ALBUM = "album";

    String KEY_GROUP = "group";
    String KEY_GROUP_NAME = "group_name";
    String KEY_GROUP_ID = "group_id";
    String KEY_GROUP_FILE = "group_file";

    String KEY_VIDEO_URL = "video_url";
    String KEY_IMAGE_URL = "image_url";
    String KEY_PUSH_NOTIFICATION_TYPE = "push_notification_type";
    String KEY_HAS_PUSH_NOTIFICATION = "has_push_notification";


    String DEVICE_TYPE_ANDROID = "1";
    String REGISTER_BY_EMAIL = "1";
    String REGISTER_BY_PHONE = "2";

    String ERR_CODE_ACCOUNT_NOT_VERIFIED = "0";

    String FRIENDSHIP_STATUS_FRIEND_REQUEST_SENT = "1";
    String FRIENDSHIP_STATUS_FRIEND_REQUEST_RECEIVED = "6";
    String FRIENDSHIP_STATUS_FRIEND_REQUEST_REJECTED = "3";
    String FRIENDSHIP_STATUS_FRIEND = "2";
    String FRIENDSHIP_STATUS_BLOCK = "4";
    String FRIENDSHIP_STATUS_NOT_FRIEND = "0";
    String FRIENDSHIP_STATUS_UN_FRIEND = "5";

    String TREE_TYPE_SEED = "1";
    String TREE_TYPE_PLANT = "2";
    String TREE_TYPE_HALF_TREE = "3";
    String TREE_TYPE_FULL_TREE = "4";

    String BUCKET_STATUS_FULL = "1";
    String BUCKET_STATUS_EMPTY = "0";

    String NOTIFICATION_FRIEND_REQ_RECEIVED = "1";
    String NOTIFICATION_FRIEND_REQ_ACCEPTED = "2";
    String NOTIFICATION_RECEIVED_OM_POINTS = "3";
    String NOTIFICATION_TREE_STATE_CHANGED = "4";
    String NOTIFICATION_NEW_IMAGE = "5";
    String NOTIFICATION_NEW_VIDEO = "6";
    String NOTIFICATION_USER_BLOCKED = "7";

    int PUSH_NOTIFICATION_FRIEND_REQ_RECEIVED = 1;
    int PUSH_NOTIFICATION_FRIEND_REQ_ACCEPTED = 2;
    int PUSH_NOTIFICATION_RECEIVED_OM_POINTS = 3;
    int PUSH_NOTIFICATION_TREE_STATE_CHANGED = 4;
    int PUSH_NOTIFICATION_NEW_IMAGE = 5;
    int PUSH_NOTIFICATION_NEW_VIDEO = 6;
    int PUSH_NOTIFICATION_USER_BLOCKED = 7;
    int PUSH_NOTIFICATION_NEW_LOGIN = 12;

    String COMMENT_TYPE_TEXT = "1";
    String COMMENT_TYPE_IMAGE = "2";
    String COMMENT_TYPE_VIDEO = "3";

    String WISH_STATUS_RED = "1";
    String WISH_STATUS_YELLOW = "2";
    String WISH_STATUS_GREEN = "3";
}
