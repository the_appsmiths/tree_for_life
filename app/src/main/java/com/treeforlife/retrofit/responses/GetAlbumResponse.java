package com.treeforlife.retrofit.responses;

import com.google.gson.annotations.SerializedName;
import com.treeforlife.dataobjects.Album;

import java.util.ArrayList;

/**
 * Created by TheAppsmiths on 4/5/2018.
 */

public class GetAlbumResponse extends WebServiceResponse{

    @SerializedName("albums")
    public ArrayList<Album>  album=new ArrayList<Album>() ;

    @SerializedName("total_records")
    public int totalRecords;

}
