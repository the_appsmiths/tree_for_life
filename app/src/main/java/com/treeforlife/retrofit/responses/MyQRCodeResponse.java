package com.treeforlife.retrofit.responses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by TheAppsmiths on 4/11/2018.
 */

public class MyQRCodeResponse extends WebServiceResponse {

    @SerializedName("qr_code")
    public String qrCode;
}
