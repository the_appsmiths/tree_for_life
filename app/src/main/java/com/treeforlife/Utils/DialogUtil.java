package com.treeforlife.Utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.widget.ArrayAdapter;

import com.treeforlife.adapters.CountriesArrayAdapter;
import com.treeforlife.commons.ListItemCallback;
import com.treeforlife.dataobjects.CountryInfo;

import java.util.ArrayList;
/**
 * All method features are having same naming conventions as their name.
 * No need to write the comments for particular file.
 */
public class DialogUtil {
    private static DialogUtil sDialogUtil;

    private DialogUtil() {

    }

    public static DialogUtil getInstance() {
        if (sDialogUtil == null) {
            sDialogUtil = new DialogUtil();
        }
        return sDialogUtil;
    }

    AlertDialog.Builder getAlertDialogBuilder(@NonNull Context context) {
        return new AlertDialog.Builder(context);
    }


    public AlertDialog showErrorDialog(@NonNull Context context,
                                       String message) {
        return showTwoButtonsAlertDialog(context, "Error", message, "Ok", "", null, true);
    }

    public AlertDialog showOkAlertDialog(@NonNull Context context,
                                         String message) {
        return showOkAlertDialog(context, "", message);
    }

    public AlertDialog showOkAlertDialog(@NonNull Context context,
                                         @NonNull String title,
                                         String message) {
        return showTwoButtonsAlertDialog(context, title, message, "Ok", "", null, true);
    }

    public AlertDialog showOkAlertDialog(@NonNull Context context,
                                         String message,
                                         final Runnable positiveButtonAction,
                                         boolean isCancelable) {
        return showTwoButtonsAlertDialog(context, "", message, "Ok", "", positiveButtonAction, isCancelable);

    }

    public AlertDialog showOkCancelAlertDialog(@NonNull Context context,
                                               String title,
                                               String message,
                                               final Runnable positiveButtonAction) {
        return showTwoButtonsAlertDialog(context, title, message, "Ok", "Cancel", positiveButtonAction);

    }

    public AlertDialog showOkCancelAlertDialog(@NonNull Context context,
                                               String title,
                                               String message,
                                               final Runnable positiveButtonAction,
                                               boolean isCancelable) {
        return showTwoButtonsAlertDialog(context, title, message, "Ok", "Cancel", positiveButtonAction,
                isCancelable);

    }

    public AlertDialog showOkCancelAlertDialog(@NonNull Context context,
                                               String message,
                                               final Runnable positiveButtonAction) {
        return showTwoButtonsAlertDialog(context, message, "Ok", "Cancel", positiveButtonAction);

    }

    public AlertDialog showOkCancelAlertDialog(@NonNull Context context,
                                               String message,
                                               final Runnable positiveButtonAction,
                                               boolean isCancelable) {
        return showTwoButtonsAlertDialog(context, message, "Ok", "Cancel", positiveButtonAction,
                isCancelable);

    }

    public AlertDialog showTwoButtonsAlertDialog(@NonNull Context context,
                                                 String message,
                                                 String positiveButtonText,
                                                 String negativeButtonText,
                                                 final Runnable positiveButtonAction) {
        return showTwoButtonsAlertDialog(context, message, positiveButtonText,
                negativeButtonText, positiveButtonAction, true);
        /*
         // or Alternative call#2
        return showTwoButtonsAlertDialog(context, "", message, positiveButtonText,
                negativeButtonText, positiveButtonAction);
        */

    }

    public AlertDialog showTwoButtonsAlertDialog(@NonNull Context context,
                                                 String title,
                                                 String message,
                                                 String positiveButtonText,
                                                 String negativeButtonText,
                                                 final Runnable positiveButtonAction) {
        return showTwoButtonsAlertDialog(context, title, message, positiveButtonText,
                negativeButtonText, positiveButtonAction, true);

    }

    public AlertDialog showTwoButtonsAlertDialog(@NonNull Context context,
                                                 String message,
                                                 String positiveButtonText,
                                                 String negativeButtonText,
                                                 final Runnable positiveButtonAction,
                                                 boolean isCancelable) {
        return showTwoButtonsAlertDialog(context, message, positiveButtonText,
                negativeButtonText, positiveButtonAction, null, isCancelable);

    }

    public AlertDialog showTwoButtonsAlertDialog(@NonNull Context context,
                                                 String title,
                                                 String message,
                                                 String positiveButtonText,
                                                 String negativeButtonText,
                                                 final Runnable positiveButtonAction,
                                                 boolean isCancelable) {
        return showTwoButtonsAlertDialog(context, title, message, positiveButtonText,
                negativeButtonText, positiveButtonAction, null, isCancelable);

    }

    public AlertDialog showTwoButtonsAlertDialog(@NonNull Context context,
                                                 String message,
                                                 String positiveButtonText,
                                                 String negativeButtonText,
                                                 final Runnable positiveButtonAction,
                                                 final Runnable negativeButtonAction) {
        return showTwoButtonsAlertDialog(context, "", message, positiveButtonText,
                negativeButtonText, positiveButtonAction, negativeButtonAction, true);

    }

    public AlertDialog showTwoButtonsAlertDialog(@NonNull Context context,
                                                 String title,
                                                 String message,
                                                 String positiveButtonText,
                                                 String negativeButtonText,
                                                 final Runnable positiveButtonAction,
                                                 final Runnable negativeButtonAction) {
        return showTwoButtonsAlertDialog(context, title, message, positiveButtonText,
                negativeButtonText, positiveButtonAction, negativeButtonAction, true);

    }


    public AlertDialog showTwoButtonsAlertDialog(@NonNull Context context,
                                                 String message,
                                                 String positiveButtonText,
                                                 String negativeButtonText,
                                                 final Runnable positiveButtonAction,
                                                 final Runnable negativeButtonAction,
                                                 boolean isCancelable) {
        return showTwoButtonsAlertDialog(context, "", message, positiveButtonText,
                negativeButtonText, positiveButtonAction, negativeButtonAction, isCancelable);

    }

    public AlertDialog showTwoButtonsAlertDialog(@NonNull Context context,
                                                 String title,
                                                 String message,
                                                 String positiveButtonText,
                                                 String negativeButtonText,
                                                 final Runnable positiveButtonAction,
                                                 final Runnable negativeButtonAction,
                                                 boolean isCancelable) {
        return showAlertDialog(context, title, message, positiveButtonText, negativeButtonText,
                null, positiveButtonAction, negativeButtonAction, null, isCancelable);

    }

    public AlertDialog showAlertDialog(@NonNull Context context,
                                       String title,
                                       String message,
                                       String positiveButtonText,
                                       String negativeButtonText,
                                       String neutralButtonText,
                                       final Runnable positiveButtonAction,
                                       final Runnable negativeButtonAction,
                                       final Runnable neutralButtonAction,
                                       boolean isCancelable) {

        AlertDialog.Builder builder = getAlertDialogBuilder(context);
        if (!TextUtils.isEmpty(title)) {
            builder.setTitle(title);
        }

        if (!TextUtils.isEmpty(message)) {
            builder.setMessage(message);
        }
        if (!TextUtils.isEmpty(positiveButtonText)) {
            builder.setPositiveButton(positiveButtonText, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (positiveButtonAction != null) {
                        positiveButtonAction.run();
                    }
                }
            });
        }

        if (!TextUtils.isEmpty(negativeButtonText)) {
            builder.setNegativeButton(negativeButtonText, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (negativeButtonAction != null) {
                        negativeButtonAction.run();
                    }

                }
            });
        }

        if (!TextUtils.isEmpty(neutralButtonText)) {
            builder.setNeutralButton(neutralButtonText, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (neutralButtonAction != null) {
                        neutralButtonAction.run();
                    }

                }
            });
        }

        builder.setCancelable(isCancelable);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        return alertDialog;
    }


    public AlertDialog showAlertList(@NonNull Context context,
                                     String title,
                                     final ArrayList<String> data,
                                     final int requestCode,
                                     final ListItemCallback callback) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setAdapter(new ArrayAdapter<String>(context,
                        android.R.layout.simple_list_item_1, data),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (null != callback) {
                            String datum = "";
                            if (data != null && data.size() > which) {
                                datum = data.get(which);
                            }
                            callback.onItemClicked(which, datum, requestCode);
                        }
                    }
                });
        builder.show();
        return builder.create();
    }

    public AlertDialog showAlertList(@NonNull Context context,
                                     String title,
                                     final String[] data,
                                     final int requestCode,
                                     final ListItemCallback callback) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setAdapter(new ArrayAdapter<String>(context,
                        android.R.layout.simple_list_item_1, data),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (null != callback)
                            callback.onItemClicked(which, data[which], requestCode);
                    }
                });
        builder.show();
        return builder.create();
    }

    public AlertDialog showCountriesAlertList(@NonNull Context context,
                                              String title,
                                              final int requestCode,
                                              final ListItemCallback callback) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        final CountriesArrayAdapter adapter = new CountriesArrayAdapter(context,
                new CountriesArrayAdapter.CountriesDialCodeItemCallback() {
                    @Override
                    public void onItemClick(int position, CountryInfo countryCode) {
                        if (null != callback && countryCode != null) {
                            callback.onItemClicked(position, countryCode.dialCode, requestCode);
                        }

                    }
                });

        builder.setAdapter(adapter, null);
        builder.setCancelable(true);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        return alertDialog;
    }

    public AlertDialog showCountriesAlertList(@NonNull Context context,
                                              String title,
                                              final CountriesArrayAdapter.CountriesDialCodeItemCallback callback) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setTitle(title);
        final CountriesArrayAdapter adapter = new CountriesArrayAdapter(context, callback);

        builder.setAdapter(adapter, null);
        builder.setCancelable(true);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        return alertDialog;
    }

}
