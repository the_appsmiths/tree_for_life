package com.treeforlife.dataobjects;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by TheAppsmiths on 4/12/2018.
 *
 * @author TheAppsmiths
 */

public class SearchedTflUser implements Parcelable {

    @SerializedName("id")
    public String id;
    @SerializedName("first_name")
    public String firstName;
    @SerializedName("last_name")
    public String lastName;
    @SerializedName("email")
    public String email;
    @SerializedName("phone")
    public String phone;
    @SerializedName("bio")
    public String biography;

    @SerializedName("profile_image")
    public String profileImageUrl;
    @SerializedName("tree_id")
    public String treeId;
    @SerializedName("is_friend")
    public String friendShipStatus;
    @SerializedName("blocked_by")
    public String blockedByUserId;
    @SerializedName("images")
    public ArrayList<Image> imagesList;
    @SerializedName(value = "video", alternate = {"videos"})
    public ArrayList<Video> videosList;
    @SerializedName("contacts")
    public ArrayList<FriendTflUser> friendsList;
    @SerializedName("my_profile_share")
    public String myProfileShare;
    @SerializedName("my_video_share")
    public String myVideoShare;
    @SerializedName("my_photo_share")
    public String myPhotoShare;
    @SerializedName("my_contacts_share")
    public String myContactShare;
    @SerializedName("friend_profile_share")
    public String friendProfileShare;
    @SerializedName("friend_video_share")
    public String friendVideoShare;
    @SerializedName("friend_photo_share")
    public String friendPhotoShare;
    @SerializedName("friend_contacts_share")
    public String friendContactShare;
    @SerializedName("contacts_count")
    public int noOfFriends;

    @SerializedName("street")
    public String street;
    @SerializedName("city")
    public String city;
    @SerializedName("state")
    public String state;
    @SerializedName("address_country")
    public String addressCountry;


    protected SearchedTflUser(Parcel in) {
        id = in.readString();
        firstName = in.readString();
        lastName = in.readString();
        email = in.readString();
        phone = in.readString();
        biography = in.readString();
        profileImageUrl = in.readString();
        treeId = in.readString();
        friendShipStatus = in.readString();
        blockedByUserId = in.readString();
        videosList = in.createTypedArrayList(Video.CREATOR);
        friendsList = in.createTypedArrayList(FriendTflUser.CREATOR);
        myProfileShare = in.readString();
        myVideoShare = in.readString();
        myPhotoShare = in.readString();
        myContactShare = in.readString();
        friendProfileShare = in.readString();
        friendVideoShare = in.readString();
        friendPhotoShare = in.readString();
        friendContactShare = in.readString();
        noOfFriends = in.readInt();
        street = in.readString();
        city = in.readString();
        state = in.readString();
        addressCountry = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(email);
        dest.writeString(phone);
        dest.writeString(biography);
        dest.writeString(profileImageUrl);
        dest.writeString(treeId);
        dest.writeString(friendShipStatus);
        dest.writeString(blockedByUserId);
        dest.writeTypedList(videosList);
        dest.writeTypedList(friendsList);
        dest.writeString(myProfileShare);
        dest.writeString(myVideoShare);
        dest.writeString(myPhotoShare);
        dest.writeString(myContactShare);
        dest.writeString(friendProfileShare);
        dest.writeString(friendVideoShare);
        dest.writeString(friendPhotoShare);
        dest.writeString(friendContactShare);
        dest.writeInt(noOfFriends);
        dest.writeString(street);
        dest.writeString(city);
        dest.writeString(state);
        dest.writeString(addressCountry);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SearchedTflUser> CREATOR = new Creator<SearchedTflUser>() {
        @Override
        public SearchedTflUser createFromParcel(Parcel in) {
            return new SearchedTflUser(in);
        }

        @Override
        public SearchedTflUser[] newArray(int size) {
            return new SearchedTflUser[size];
        }
    };
}
