package com.treeforlife.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.messaging.FirebaseMessaging;
import com.inscripts.interfaces.LaunchCallbacks;
import com.treeforlife.R;
import com.treeforlife.Utils.AndroidUtil;
import com.treeforlife.Utils.CometChatUtil;
import com.treeforlife.Utils.DeviceInfoUtil;
import com.treeforlife.Utils.DialogUtil;
import com.treeforlife.Utils.FragmentUtil;
import com.treeforlife.Utils.ProgressDialogUtil;
import com.treeforlife.adapters.RightNavRvAdapter;
import com.treeforlife.commons.CometChatResponseListener;
import com.treeforlife.commons.Constants;
import com.treeforlife.commons.CustomizeChangeListener;
import com.treeforlife.commons.FragmentCallBack;
import com.treeforlife.commons.SharedPrefHelper;
import com.treeforlife.commons.UploadImageVideoDialog;
import com.treeforlife.customview.CustomTextView;
import com.treeforlife.dataobjects.TFLUser;
import com.treeforlife.fragments.ChatFragment;
import com.treeforlife.fragments.CommentsFragment;
import com.treeforlife.fragments.CustomizedFragment;
import com.treeforlife.fragments.HomeFragment;
import com.treeforlife.fragments.NotificationFragment;
import com.treeforlife.fragments.RightNavigationBarFragment;
import com.treeforlife.fragments.TreeFragment;
import com.treeforlife.fragments.VaultDetailFragment;
import com.treeforlife.fragments.VaultFragment;
import com.treeforlife.fragments.WishStatusFragment;
import com.treeforlife.retrofit.ResponseId;
import com.treeforlife.retrofit.RetrofitManager;
import com.treeforlife.retrofit.RetrofitResponseValidator;
import com.treeforlife.retrofit.responses.WebServiceResponse;

import org.json.JSONObject;

import java.net.SocketTimeoutException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.treeforlife.retrofit.ResponseId.LOGOUT;


public class HomeActivity extends AppCompatActivity implements View.OnClickListener,
        RightNavRvAdapter.NavItemCallBack, FragmentCallBack,
        RetrofitResponseValidator.ValidationListener {

    public static final String TAG = "HomeActivity";

    private Intent mActivityIntent;

    private Toolbar mToolbar;
    private ActionBar mActionBar;

    private DrawerLayout mDrawerLayout;
    private CustomTextView mNotificationCountTextView;
    private TextView mToolbarTitle;
    private ImageView mToolbarIconRhs, mToolbarIconLhs;
    private View mHomeTab, mTreeTab, mVaultTab, mChatTab, mNotificationTab;
    private RightNavigationBarFragment mRightNavBarFragment;

    private BroadcastReceiver mProfileUpdatedReceiver;

    private Handler mClickHandler;
    private Runnable mClickAction;
    private TFLUser mTflUser;
    private SharedPrefHelper mSharedPrefHelper;
    private ProgressDialogUtil mProgressDialogUtil;
    private DialogUtil mDialogUtil;
    private CometChatUtil mCometChatUtil;

    private Runnable mAfterInitCometChatAction;
    private WebServiceResponse logoutResponse;

    private boolean isProfileUpdateReceiverRegistered;
    private UploadImageVideoDialog mDialog;


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(Constants.KEY_TFL_USER, mTflUser);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            mTflUser = savedInstanceState.getParcelable(Constants.KEY_TFL_USER);
        }
        setContentView(R.layout.activity_home);
        initComponents();
        initToolBar();
        initWidgets();
        initBottomBar();
        updateNotificationCount();
        registerLocalReceiver();
        testMyLogic();
    }

    //custom method to check some logic
    private void testMyLogic() {

    }


    //method to initialize register receiver
    private void registerLocalReceiver() {
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);
        if (localBroadcastManager != null) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(Constants.ACTION_USER_PROFILE_UPDATED);
            intentFilter.addAction(Constants.ACTION_RECEIVED_PUSH_NOTIFICATION);
            localBroadcastManager.registerReceiver(mProfileUpdatedReceiver, intentFilter);
            isProfileUpdateReceiverRegistered = true;

        }
    }

    //method to initialize unregister receiver
    private void unRegisterLocalReceiver() {
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);
        if (localBroadcastManager != null && isProfileUpdateReceiverRegistered) {
            localBroadcastManager.unregisterReceiver(mProfileUpdatedReceiver);
            isProfileUpdateReceiverRegistered = false;

        }
    }

    //method after receiving the broadcast
    private void afterReceivingBroadcast(Context context, Intent intent) {
        AndroidUtil.dumpIntent(TAG, intent);
        if (intent != null) {
            String mIntentAction = intent.getAction();

            if (!TextUtils.isEmpty(mIntentAction)) {

                switch (mIntentAction) {
                    case Constants.ACTION_USER_PROFILE_UPDATED:
                        mTflUser = intent.getParcelableExtra(Constants.KEY_TFL_USER);
                        if (mTflUser == null) {
                            mTflUser = mSharedPrefHelper.getLoginResponse(this);
                            Log.e(TAG, "afterReceivingBroadcast: FETCHING DATA FROM LOCAL DB");
                        }
                        if (mRightNavBarFragment != null) {
                            mRightNavBarFragment.update(mTflUser);
                        }
                        break;
                    case Constants.ACTION_RECEIVED_PUSH_NOTIFICATION:
                        updateNotificationCount();
                        if (intent.getIntExtra(Constants.KEY_PUSH_NOTIFICATION_TYPE, 0) == Constants.PUSH_NOTIFICATION_NEW_LOGIN) {
                            callLogout();
                        }
                        break;

                }
            }
        }

    }

    //method to initialize the components
    private void initComponents() {
        mClickHandler = new Handler();
        mActivityIntent = getIntent();
        mSharedPrefHelper = SharedPrefHelper.getInstance();
        mProgressDialogUtil = ProgressDialogUtil.getInstance();
        mDialogUtil = DialogUtil.getInstance();
        mCometChatUtil = CometChatUtil.getInstance();
        mDialog = new UploadImageVideoDialog(this);
        if (mActivityIntent != null) {
            mTflUser = mActivityIntent.getParcelableExtra(Constants.KEY_TFL_USER);
        }
        ensureTFLUser();
        this.mProfileUpdatedReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                afterReceivingBroadcast(context, intent);
            }
        };
    }

    //method to check the tfl user
    private void ensureTFLUser() {
        if (mTflUser == null) {
            mTflUser = mSharedPrefHelper.getLoginResponse(this);
        }
    }

    //method to initialize the toolbar
    private void initToolBar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        if (mToolbar != null) {
            mToolbarTitle = mToolbar.findViewById(R.id.toolbar_title);
            mToolbarIconLhs = mToolbar.findViewById(R.id.toolbar_icon_back);
            mToolbarIconLhs.setOnClickListener(this);
            mToolbarIconRhs = mToolbar.findViewById(R.id.toolbar_icon_hamberg);
            mToolbarIconRhs.setVisibility(View.VISIBLE);
            mToolbarIconRhs.setImageResource(R.mipmap.ic_hamburger);
            if (mToolbarIconRhs != null) {
                mToolbarIconRhs.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mDrawerLayout.openDrawer(GravityCompat.END);
                    }
                });
            }
        }

        setSupportActionBar(mToolbar);
        mActionBar = getSupportActionBar();
        setTitle(R.string.home);


    }

    @Override
    public void setTitle(CharSequence title) {
        setTitleToolBar(title);
    }


    //method to set the title of toolbar
    private void setTitleToolBar(CharSequence title) {
        if (mToolbarTitle != null) {
            mToolbarTitle.setText(title);
        }
    }

    //method to initialize the widgets
    private void initWidgets() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mRightNavBarFragment = (RightNavigationBarFragment) getSupportFragmentManager()
                .findFragmentById(R.id.right_side_navigation_bar);
    }

    //method to initialize the bottom bar
    private void initBottomBar() {

        mHomeTab = findViewById(R.id.home_tab);
        mTreeTab = findViewById(R.id.tree_tab);
        mVaultTab = findViewById(R.id.vault_tab);
        mChatTab = findViewById(R.id.chat_tab);
        mNotificationTab = findViewById(R.id.notification_tab);
        mNotificationCountTextView = findViewById(R.id.notification_count_text_view);

        mHomeTab.setOnClickListener(this);
        mTreeTab.setOnClickListener(this);
        mVaultTab.setOnClickListener(this);
        mChatTab.setOnClickListener(this);
        mNotificationTab.setOnClickListener(this);

        mHomeTab.performClick();

    }

    private void updateNotificationCount() {
        int count = mSharedPrefHelper.getNotificationCount(this);
        if (mNotificationCountTextView != null) {
            mNotificationCountTextView.setText(String.valueOf(count));
            mNotificationCountTextView.setVisibility(count > 0 ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout != null
                && mDrawerLayout.isDrawerOpen(GravityCompat.END)) {
            mDrawerLayout.closeDrawer(GravityCompat.END);

        } else {
            try {
                FragmentUtil.removeFragment(this);
                super.onBackPressed();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unRegisterLocalReceiver();
    }

    @Override
    public void onClick(final View v) {

        if (mClickAction != null) {
            mClickHandler.removeCallbacks(mClickAction);
        }

        switch (v.getId()) {
            case R.id.home_tab:
            case R.id.tree_tab:
            case R.id.vault_tab:
            case R.id.notification_tab:
                mClickAction = new Runnable() {
                    @Override
                    public void run() {
                        showTab(v);
                        mClickAction = null;
                    }
                };
                break;
            case R.id.chat_tab:
                mClickAction = new Runnable() {
                    @Override
                    public void run() {
                        launchChat();
                        mClickAction = null;
                    }
                };
                break;

            case R.id.toolbar_icon_back:
                mClickAction = new Runnable() {
                    @Override
                    public void run() {
                        onBackPressed();
                        mClickAction = null;
                    }
                };
                break;
        }

        if (mClickAction != null && mClickHandler != null) {
            mClickHandler.postDelayed(mClickAction, 200);
        }

    }

    //method to initialize the comet chat
    private boolean ensureCometChat() {
        if (mCometChatUtil == null) {
            mCometChatUtil = CometChatUtil.getInstance();
        }
        if (mCometChatUtil.cometChat == null) {
            mProgressDialogUtil.showProgressDialog(this);
            mCometChatUtil.initializeCometChat(this, new CometChatResponseListener() {
                @Override
                public void onCometChatResponse(ResponseId responseId, boolean isSuccess, JSONObject response) {
                    if (mAfterInitCometChatAction != null) {
                        mAfterInitCometChatAction.run();
                    }
                }
            });
        }
        return mCometChatUtil.cometChat != null;
    }

    //method to launch the comet chat
    private void launchChat() {
        if (ensureCometChat()) {
            mProgressDialogUtil.showProgressDialog(this);
            mCometChatUtil.cometChat.launchCometChat(this, true, new LaunchCallbacks() {
                @Override
                public void successCallback(JSONObject jsonObject) {
                    Log.w(TAG, "Launch Success : " + jsonObject);
                    mProgressDialogUtil.dismissProgressDialog();
                }

                @Override
                public void failCallback(JSONObject jsonObject) {
                    Log.e(TAG, "Launch Fail : " + jsonObject);
                    AndroidUtil.showErrorToast(HomeActivity.this);
                    mProgressDialogUtil.dismissProgressDialog();
                }

                @Override
                public void userInfoCallback(JSONObject jsonObject) {
                    Log.d(TAG, "User Info Received : " + jsonObject);

                }

                @Override
                public void chatroomInfoCallback(JSONObject jsonObject) {
                    Log.d(TAG, "Chatroom Info Received : " + jsonObject);
                }

                @Override
                public void onMessageReceive(JSONObject jsonObject) {
                    Log.d(TAG, "Message Received : " + jsonObject);
                }

                @Override
                public void error(JSONObject jsonObject) {
                    Log.d(TAG, "Error : " + jsonObject);
                }

                @Override
                public void onWindowClose(JSONObject jsonObject) {
                    Log.d(TAG, "Chat Window Closed : " + jsonObject);
                }

                @Override
                public void onLogout() {
                    Log.d(TAG, "Logout");
                    String pushCahnnel = SharedPrefHelper.getInstance().getPushChannel(HomeActivity.this);
                    if (!TextUtils.isEmpty(pushCahnnel)) {
                        FirebaseMessaging.getInstance().unsubscribeFromTopic(pushCahnnel);
                        SharedPrefHelper.getInstance().setPushChannel(HomeActivity.this, "");
                    }
                }
            });
        } else {

            mAfterInitCometChatAction = new Runnable() {
                @Override
                public void run() {
                    launchChat();
                    mAfterInitCometChatAction = null;
                }
            };
        }

    }

    //method to show the tabs
    private void showTab(View v) {

        if (v == null) {
            return;
        }

        String fragmentName = null;
        boolean addToBackStack = true;

        switch (v.getId()) {
            case R.id.home_tab:
                fragmentName = FragmentUtil.FRAGMENT_HOME;
                addToBackStack = false;
                break;

            case R.id.tree_tab:
                fragmentName = FragmentUtil.FRAGMENT_TREE;
                break;

            case R.id.vault_tab:
                fragmentName = FragmentUtil.FRAGMENT_VAULT;
                break;

            case R.id.notification_tab:
                mSharedPrefHelper.resetNotificationCount(this);
                updateNotificationCount();
                fragmentName = FragmentUtil.FRAGMENT_NOTIFICATION;
                break;
        }

        if (!addToBackStack) {
            FragmentUtil.clearFragmentBackStack(this);
        }
        openFragment(fragmentName, addToBackStack);
    }


    //method to open the fragment
    private void openFragment(String fragmentName, boolean addToBackStack) {
        if (!TextUtils.isEmpty(fragmentName)) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(Constants.KEY_TFL_USER, mTflUser);
            FragmentUtil.loadFragment(this, fragmentName, bundle, addToBackStack);
        }
    }

    //method after selecting the tab
    private void selectTab(View v) {
        int mSelectedItemBgColor = ContextCompat.getColor(this, R.color.appColor_5e3413_brown);
        int mBgColor = ContextCompat.getColor(this, android.R.color.transparent);
        int mUnSelectedItemBgColor = ContextCompat.getColor(this, R.color.appColor_774c2b_brown);
        mHomeTab.setBackgroundColor(mUnSelectedItemBgColor);
        mTreeTab.setBackgroundColor(mUnSelectedItemBgColor);
        mVaultTab.setBackgroundColor(mUnSelectedItemBgColor);
        mChatTab.setBackgroundColor(mUnSelectedItemBgColor);
        mNotificationTab.setBackgroundColor(mUnSelectedItemBgColor);

        if (v == null) {
            return;
        }

        switch (v.getId()) {
            case R.id.home_tab:
                mHomeTab.setBackgroundColor(mSelectedItemBgColor);
                break;

            case R.id.tree_tab:
                mTreeTab.setBackgroundColor(mSelectedItemBgColor);
                break;

            case R.id.vault_tab:
                mVaultTab.setBackgroundColor(mSelectedItemBgColor);
                break;

            case R.id.chat_tab:
                mChatTab.setBackgroundColor(mSelectedItemBgColor);
                break;

            case R.id.notification_tab:
                mNotificationTab.setBackgroundColor(mSelectedItemBgColor);
                break;
        }

        mDrawerLayout.setBackgroundColor(mBgColor);
    }

    @Override
    public void clickItem(View clickedView, int selectedItemPosition) {
        String fragmentName = "";
        boolean addToBackStack = true;
        switch (selectedItemPosition) {
            case 0:
                Bundle bundle = new Bundle();
                bundle.putInt(Constants.KEY_FRUIT_TYPE, Constants.FRUIT_TYPE_ALL);
                bundle.putParcelable(Constants.KEY_TFL_USER, mTflUser);

                FragmentUtil.loadFragment(HomeActivity.this,
                        FragmentUtil.FRAGMENT_MY_COMMENTS, bundle, true);
                break;
            case 1:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_SUBJECT, "TFL Android App Invitation");
                sendIntent.putExtra(Intent.EXTRA_TEXT, "Hey ! I'm using tree for life app. it's an amazing app, download it now from play store. https://play.google.com/store/apps/details?id=com.treeforlife");
                sendIntent.setType("text/plain");
                // startActivity(sendIntent);

                startActivity(Intent.createChooser(sendIntent, "Invite via"));
                break;

           /* case 1:
                fragmentName = FragmentUtil.FRAGMENT_CONVERTER;
                break;*/

            case 2:
                fragmentName = FragmentUtil.FRAGMENT_ADD_FRIEND;
                break;

            case 3:
                Intent changePasswordIntent = new Intent(this, ChangePasswordActivity.class);
                changePasswordIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                changePasswordIntent.putExtra(Constants.KEY_CALLER_COMPONENT, TAG);
                changePasswordIntent.putExtra(Constants.KEY_TFL_USER, mTflUser);
                startActivity(changePasswordIntent);
                break;

            case 4:
                callLogout();
                break;

            case RightNavigationBarFragment.PROFILE_POSITION:
                fragmentName = FragmentUtil.FRAGMENT_MY_PROFILE;
                break;

        }
        openFragment(fragmentName, addToBackStack);
        mDrawerLayout.closeDrawer(GravityCompat.END);
    }

    //method to call the api of logout
    public void callLogout() {

        if (mTflUser != null && AndroidUtil.hasInternetConnectivity(this)) {
            mProgressDialogUtil.showProgressDialog(this);

            Call<WebServiceResponse> logout = RetrofitManager.getRetrofitWebService()
                    .logout(mTflUser.userId, mTflUser.treeId, Constants.DEVICE_TYPE_ANDROID,
                            DeviceInfoUtil.getTelephonyDeviceIdOrIMEINo(this));
            logout.enqueue(new Callback<WebServiceResponse>() {
                @Override
                public void onResponse(Call<WebServiceResponse> call, Response<WebServiceResponse> response) {
                    new RetrofitResponseValidator(LOGOUT, response, HomeActivity.this);

                }

                @Override
                public void onFailure(Call<WebServiceResponse> call, Throwable t) {
                    String errorMessage = "";
                    if (t != null) {
                        Log.e(TAG, "onFailure: Error : " + t.getMessage());
                        if (t instanceof SocketTimeoutException) {
                            errorMessage = "Please make sure that your device has an active internet connection.";
                        }
                    }

                    mProgressDialogUtil.dismissProgressDialog();


                    if (TextUtils.isEmpty(errorMessage)) {
                        AndroidUtil.showErrorToast(HomeActivity.this);
                    } else {
                        mDialogUtil.showErrorDialog(HomeActivity.this, errorMessage);
                    }
                }
            });

        }
    }

    @Override
    public void updateBottomBar(Bundle bundle) {
        View view = null;
        if (bundle != null) {
            String fragmentTag = bundle.getString(Constants.KEY_FRAGMENT_CALLBACK, TAG);
            switch (fragmentTag) {
                case HomeFragment.TAG:
                case CustomizedFragment.TAG:
                case CommentsFragment.TAG:
                    view = mHomeTab;
                    break;
                case TreeFragment.TAG:
                    view = mTreeTab;
                    break;
                case WishStatusFragment.TAG:
                case VaultFragment.TAG:
                case VaultDetailFragment.TAG:
                    view = mVaultTab;
                    break;
                case ChatFragment.TAG:
                    view = mChatTab;
                    break;
                case NotificationFragment.TAG:
                    view = mNotificationTab;
                    break;


            }
            selectTab(view);
        }

        handlePushNotification();
    }

    //method to handle the push notification
    private void handlePushNotification() {
        if (mActivityIntent != null
                && mActivityIntent.hasExtra(Constants.KEY_HAS_PUSH_NOTIFICATION)
                && mActivityIntent.getBooleanExtra(Constants.KEY_HAS_PUSH_NOTIFICATION,
                false)) {
            AndroidUtil.dumpIntent(TAG + " : ON PUSH ", mActivityIntent);
            mActivityIntent.removeExtra(Constants.KEY_HAS_PUSH_NOTIFICATION);
            String fragmentName = mActivityIntent.getStringExtra(Constants.KEY_GO_TO_FRAGMENT_NAME);
            String senderTreeId = mActivityIntent.getStringExtra(Constants.KEY_TREE_ID);
            if (!TextUtils.isEmpty(fragmentName)) {
                Bundle bundle = new Bundle();
                bundle.putString(Constants.KEY_CALLER_COMPONENT, TAG);
                bundle.putParcelable(Constants.KEY_TFL_USER, mTflUser);
                bundle.putString(Constants.KEY_TREE_ID, senderTreeId);
                FragmentUtil.loadFragment(this, fragmentName, bundle, true);
                mSharedPrefHelper.decreaseNotificationCount(this);
                updateNotificationCount();
            }
        }
    }


    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                if (fragment != null) {
                    fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
                }
            }

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                if (fragment != null) {
                    fragment.onActivityResult(requestCode, resultCode, data);
                }
            }

        }

        if (resultCode == Activity.RESULT_OK) {

            switch (requestCode) {

                case Constants.RC_START_ACTIVITY_CUSTOMIZE:
                    if (data != null) {
                        boolean hasCustomizedUpdated = data.getBooleanExtra(Constants.KEY_HAS_CUSTOMIZE_UPDATED, false);
                        if (hasCustomizedUpdated) {
                            fragments = getSupportFragmentManager().getFragments();
                            if (fragments != null) {
                                for (Fragment fragment : fragments) {
                                    if (fragment != null && fragment instanceof CustomizeChangeListener) {
                                        ((CustomizeChangeListener) fragment).onCustomizeSettingsChange();
                                    }
                                }

                            }
                        }
                    }
                    break;
            }
        }

    }

    @Override
    public void onValidResponse(@NonNull ResponseId responseId, @NonNull final Object responseBody, @NonNull Response response) {

        switch (responseId) {
            case LOGOUT:
                logoutResponse = (WebServiceResponse) responseBody;
                //afterLogout();
                //mProgressDialogUtil.dismissProgressDialog();
                logoutFromCometChat();
                break;
        }

    }

    //method to logout from the comet chat
    private void logoutFromCometChat() {
        if (AndroidUtil.hasInternetConnectivity(this)) {
            if (ensureCometChat()) {
                mCometChatUtil.logoutCometChatUser(this, new CometChatResponseListener() {
                    @Override
                    public void onCometChatResponse(ResponseId responseId, boolean isSuccess, JSONObject response) {

                        String pushCahnnel = SharedPrefHelper.getInstance().getPushChannel(HomeActivity.this);
                        if (!TextUtils.isEmpty(pushCahnnel)) {
                            FirebaseMessaging.getInstance().unsubscribeFromTopic(pushCahnnel);
                            SharedPrefHelper.getInstance().setPushChannel(HomeActivity.this, "");
                        }
                        if (mCometChatUtil != null && mCometChatUtil.cometChat != null) {
                            mCometChatUtil.cometChat.unsubscribe();
                        }
                        afterLogout();
                        mProgressDialogUtil.dismissProgressDialog();
                    }
                });
            } else {
                mAfterInitCometChatAction = new Runnable() {
                    @Override
                    public void run() {
                        logoutFromCometChat();
                        mAfterInitCometChatAction = null;
                    }
                };
            }

        }
    }


    @Override
    public void onInvalidResponse(@NonNull ResponseId responseId, @NonNull String errorMessage, Response response) {

        switch (responseId) {
            case LOGOUT:
                if (response == null || !response.isSuccessful()) {
                    mDialogUtil.showErrorDialog(this, errorMessage);
                } else {
                    AndroidUtil.showToast(this, errorMessage);
                }
                break;
        }

        mProgressDialogUtil.dismissProgressDialog();

    }

    //method calls after logged-out
    private void afterLogout() {
        if (logoutResponse != null && logoutResponse.status) {
            mSharedPrefHelper.resetData(HomeActivity.this);
            Intent in = new Intent(this, LoginActivity.class);
            in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            ActivityCompat.finishAffinity(this);
            AndroidUtil.showToast(this, "You have been logged out successfully !");
            startActivity(in);
        }
    }

}
