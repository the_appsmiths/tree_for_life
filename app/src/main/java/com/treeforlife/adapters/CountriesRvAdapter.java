package com.treeforlife.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.treeforlife.R;
import com.treeforlife.customview.CustomTextView;
import com.treeforlife.dataobjects.CountryInfo;

import java.util.ArrayList;

/**
 * Created by TheAppsmiths on 4/26/2018.
 *
 * @author TheAppsmiths
 */

public class CountriesRvAdapter extends RecyclerView.Adapter<CountriesRvAdapter.CountryHolder> {
    private static final String TAG = "CountriesRvAdapter";

    private ArrayList<CountryInfo> mCountriesInfoList, mFilteredCountriesInfoList;
    private final CountriesItemCallback mItemCallback;
    private CountryInfo mSelectedCountryInfo;
    private String mSelectedPhoneDialCode;

    public CountriesRvAdapter(CountriesItemCallback callback) {
        this.mItemCallback = callback;
    }

    @Override
    public CountryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CountryHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_country_info, parent, false));
    }

    @Override
    public void onBindViewHolder(CountryHolder holder, int position) {
        holder.bindData(position);
    }

    @Override
    public int getItemCount() {
        return mFilteredCountriesInfoList != null ? mFilteredCountriesInfoList.size() : 0;
    }

    public void setSelectedCountryInfo(CountryInfo selectedCountryInfo) {
        this.mSelectedCountryInfo = selectedCountryInfo;
        notifyDataSetChanged();
    }

    public void setSelectedCountryInfo(String selectedPhoneDialCode) {
        this.mSelectedPhoneDialCode = selectedPhoneDialCode;
        notifyDataSetChanged();
    }

    //method to ensure the country list info
    private void ensureCountriesInfoLists() {
        if (mCountriesInfoList == null) {
            mCountriesInfoList = new ArrayList<CountryInfo>();
        }
        if (mFilteredCountriesInfoList == null) {
            mFilteredCountriesInfoList = new ArrayList<CountryInfo>();
        }
    }

    //method to update the whole list
    public void update(ArrayList<CountryInfo> countriesInfoList) {
        ensureCountriesInfoLists();

        mCountriesInfoList.clear();
        mCountriesInfoList.addAll(countriesInfoList);

        update("");
    }

    //method to update the data as per search query
    public void update(String searchQuery) {
        ensureCountriesInfoLists();
        mFilteredCountriesInfoList.clear();
        if (TextUtils.isEmpty(searchQuery)) {
            mFilteredCountriesInfoList.addAll(mCountriesInfoList);

        } else {
            if (mCountriesInfoList != null) {
                for (CountryInfo countryInfo : mCountriesInfoList) {
                    if (countryInfo != null) {
                        if (!TextUtils.isEmpty(countryInfo.countryCode)
                                && countryInfo.countryCode.toLowerCase()
                                .contains(searchQuery.toLowerCase())) {

                            mFilteredCountriesInfoList.add(countryInfo);

                        } else if (!TextUtils.isEmpty(countryInfo.countryCode)
                                && String.format("%s%s%s", "(", countryInfo.countryCode, ")").toLowerCase()
                                .contains(searchQuery.toLowerCase())) {

                            mFilteredCountriesInfoList.add(countryInfo);

                        } else if (!TextUtils.isEmpty(countryInfo.countryName)
                                && countryInfo.countryName.toLowerCase()
                                .contains(searchQuery.toLowerCase())) {

                            mFilteredCountriesInfoList.add(countryInfo);

                        } else if (!TextUtils.isEmpty(countryInfo.dialCode)
                                && countryInfo.dialCode.contains(searchQuery.toLowerCase())) {

                            mFilteredCountriesInfoList.add(countryInfo);
                        }
                    }
                }
            }
        }
        notifyDataSetChanged();
    }

    //Holder view to show the data in adapter
    public class CountryHolder extends RecyclerView.ViewHolder {
        private final View mItemView;
        private final CustomTextView mCountryNameTextView, mCountryDialCodeTextView;

        private final Context mContext;
        CountryInfo countryInfo;

        public CountryHolder(View itemView) {
            super(itemView);
            this.mItemView = itemView;
            this.mContext = itemView.getContext();

            mCountryNameTextView = itemView.findViewById(R.id.country_name_text_view);
            mCountryDialCodeTextView = itemView.findViewById(R.id.country_dial_code_text_view);
        }

        public void bindData(final int position) {
            if (mFilteredCountriesInfoList != null
                    && mFilteredCountriesInfoList.size() > position) {
                countryInfo = mFilteredCountriesInfoList.get(position);
            }


            if (countryInfo != null) {
                mCountryNameTextView.setText(countryInfo.countryName + "(" + countryInfo.countryCode + ")");
                mCountryDialCodeTextView.setText(countryInfo.dialCode);
                int itemBgColor = Color.TRANSPARENT;
                if ((!TextUtils.isEmpty(mSelectedPhoneDialCode)
                        && mSelectedPhoneDialCode.equals(countryInfo.countryCode))
                        || (mSelectedCountryInfo != null
                        && !TextUtils.isEmpty(mSelectedCountryInfo.countryCode)
                        && mSelectedCountryInfo.countryCode.equals(countryInfo.countryCode))) {
                    if (mContext != null) {
                        itemBgColor = ContextCompat.getColor(mContext, R.color.appColor_dab902_yellow);
                    } else {
                        itemBgColor = Color.YELLOW;
                    }
                }
                mItemView.setBackgroundColor(itemBgColor);
            }

            mItemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mItemCallback != null) {
                        mItemCallback.onItemClick(position, countryInfo);
                    }
                }
            });
        }
    }

    public interface CountriesItemCallback {
        void onItemClick(int position, CountryInfo countryInfo);
    }
}
