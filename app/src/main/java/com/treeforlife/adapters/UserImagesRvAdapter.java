package com.treeforlife.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.treeforlife.R;
import com.treeforlife.Utils.ImageLoaderUtil;
import com.treeforlife.dataobjects.Image;

import java.util.ArrayList;

/**
 * Created by TheAppsmiths on 4/12/2018.
 *
 * @author TheAppsmiths
 */

public class UserImagesRvAdapter extends RecyclerView.Adapter<UserImagesRvAdapter.ImageHolder> {
    private ArrayList<Image> mImagesList;
    private final ImageItemCallback mCallback;

    public UserImagesRvAdapter(ImageItemCallback callback) {
        this.mCallback = callback;
        setHasStableIds(true);
    }

    @Override
    public ImageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ImageHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_image, parent, false));
    }

    @Override
    public void onBindViewHolder(ImageHolder holder, int position) {
        holder.bindData(position);
    }

    @Override
    public int getItemCount() {
        return mImagesList != null ? mImagesList.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private void ensureImagesList() {
        if (mImagesList == null) {
            mImagesList = new ArrayList<>();
        }
    }

    public void clearImagesList() {
        if (mImagesList != null) {
            this.mImagesList.clear();
            notifyDataSetChanged();
        }
    }

    //method to update the list
    public void update(ArrayList<Image> imagesList) {
        ensureImagesList();
        this.mImagesList.clear();
        if (imagesList != null && !imagesList.isEmpty()) {
            this.mImagesList.addAll(imagesList);
        }
        notifyDataSetChanged();
    }

    //Holder view to show the data in adapter
    public class ImageHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "ImageHolder";
        final private View mItemView;
        final private ImageView mImageView;

        private final Context mContext;
        private final ImageLoader mImageLoader;
        private Image mImage;

        public ImageHolder(View itemView) {
            super(itemView);
            this.mItemView = itemView;
            this.mContext = itemView.getContext();
            this.mImageView = itemView.findViewById(R.id.item_image_view);

            this.mImageLoader = ImageLoaderUtil.getLoader(mContext);
        }


        public void bindData(final int position) {
            if (mImagesList != null && mImagesList.size() > position) {
                mImage = mImagesList.get(position);
                if (mImage != null) {
                    mImageLoader.displayImage(mImage.imageUrl,
                            mImageView, ImageLoaderUtil.getDefaultImageOption());
                }
            }

            mItemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mCallback != null) {
                        mCallback.onImageClick(position, mImage);
                    }
                }
            });
        }
    }

    public interface ImageItemCallback {
        void onImageClick(int position, Image image);
    }
}
