package com.treeforlife.retrofit.responses;

import com.google.gson.annotations.SerializedName;
import com.treeforlife.dataobjects.FriendTflUser;

import java.util.ArrayList;

/**
 * Created by TheAppsmiths on 4/9/2018.
 *
 * @author TheAppsmiths
 */

public class GetCustomizedDataResponse extends WebServiceResponse {

    @SerializedName("profile_share")
    public ArrayList<FriendTflUser> profileShareList;
    @SerializedName("profile_share_count")
    public int profileShareCount;

    @SerializedName("video_share")
    public ArrayList<FriendTflUser> videoShareList;
    @SerializedName("video_share_count")
    public int videoShareCount;

    @SerializedName("contacts_share")
    public ArrayList<FriendTflUser> contactShareList;
    @SerializedName("contacts_share_count")
    public int contactShareCount;

    @SerializedName("image_share")
    public ArrayList<FriendTflUser> imageShareList;
    @SerializedName("image_share_count")
    public int imageShareCount;

    @SerializedName("blocked_contacts")
    public ArrayList<FriendTflUser> blockContactsList;
    @SerializedName("blocked_contacts_count")
    public int blockContactsCount;

}
