package com.treeforlife.retrofit.responses;

import com.google.gson.annotations.SerializedName;
import com.treeforlife.dataobjects.AlbumImage;

import java.util.ArrayList;

/**
 * Created by TheAppsmiths on 4/5/2018.
 */

public class GetAlbumImagesResponse extends WebServiceResponse {

    @SerializedName("images")
    public ArrayList<AlbumImage> imagesList = new ArrayList<AlbumImage>();

    @SerializedName("total_records")
    public int totalRecords;

}
