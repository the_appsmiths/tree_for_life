package com.treeforlife.retrofit.responses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by TheAppsmiths on 4/3/2018.
 */

public class RegisterResponse extends WebServiceResponse {
    @SerializedName("user_id")
    public String userId;
}
