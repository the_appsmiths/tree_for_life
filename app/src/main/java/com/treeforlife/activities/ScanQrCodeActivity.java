package com.treeforlife.activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import com.treeforlife.Utils.AndroidUtil;
import com.treeforlife.Utils.DialogUtil;
import com.treeforlife.Utils.PermissionsManager;

import java.util.ArrayList;
import java.util.Arrays;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * Created by TheAppsmiths on 4/18/2018.
 *
 * @author TheAppsmiths
 *
 * @link : https://android-arsenal.com/details/1/387#!description
 * @link : https://github.com/dm77/barcodescanner
 */


public class ScanQrCodeActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler,
        PermissionsManager.PermissionsResultCallback {
    private static final String TAG = "ScanQrCodeActivity";

    private ZXingScannerView mScannerView;
    private AlertDialog neverAskedPermissionsInfoDialog;

    private PermissionsManager mPermissionsManager;
    private DialogUtil mDialogUtil;

    private boolean hasCameraPermission, isScanning;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
        mScannerView.setAutoFocus(true);
        ArrayList<BarcodeFormat> formats = new ArrayList<>();
        formats.add(BarcodeFormat.QR_CODE);
        mScannerView.setFormats(formats);
        mScannerView.setAspectTolerance(0.5f);
        //mScannerView.startCamera(0);

        setContentView(mScannerView);

        this.mPermissionsManager = PermissionsManager.getInstance();
        this.mDialogUtil = DialogUtil.getInstance();


    }

    @Override
    public void onResume() {
        super.onResume();
        if (neverAskedPermissionsInfoDialog == null
                || !neverAskedPermissionsInfoDialog.isShowing()) {
            hasCameraPermission = !mPermissionsManager.requestPermission(this,
                    android.Manifest.permission.CAMERA);
            startScanning();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        stopScanning();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mPermissionsManager.parsePermissionsResult(requestCode, permissions, grantResults, this, this);
    }

    @Override
    public void deniedPermissions(int requestCode, @NonNull String[] deniedPermissions) {
        AndroidUtil.showToast(this, "Required camera permission was denied.");
    }

    @Override
    public void grantedPermissions(int requestCode, @NonNull String[] grantedPermissions) {
        switch (requestCode) {
            case PermissionsManager.REQUEST_PERMISSION_CAMERA:
                hasCameraPermission = true;
                startScanning();
                break;
        }
    }

    @Override
    public void deniedAndNeverAskPermissions(int requestCode, @NonNull String[] deniedAndNeverAskPermissions) {
        Log.d(TAG, "deniedAndNeverAskPermissions = [" + Arrays.toString(deniedAndNeverAskPermissions) + "]");
        StringBuilder message = mPermissionsManager.getMessageForNeverAskPermissions(deniedAndNeverAskPermissions);

        if (!TextUtils.isEmpty(message)) {
            neverAskedPermissionsInfoDialog= mDialogUtil.showOkAlertDialog(this,
                    message.toString(),
                    new Runnable() {
                        @Override
                        public void run() {
                            mPermissionsManager.openAppInfoPage(ScanQrCodeActivity.this);


                        }
                    }, false
            );
        }
    }

    private void startScanning() {
        if (hasCameraPermission && !isScanning) {
            mScannerView.setResultHandler(this);
            mScannerView.startCamera();
            isScanning = true;
            Log.w(TAG, "SCANNING HAS BEEN STARTED: ");
        }
    }

    private void stopScanning() {
        if (isScanning) {
            mScannerView.stopCamera();
            isScanning = false;
            Log.w(TAG, "SCANNING HAS BEEN STOPPED: ");
        }
    }

    @Override
    public void handleResult(Result rawResult) {
        Intent resultIntent = new Intent();
        String scanResultText = rawResult.getText();
        Log.w(TAG, "scanResultText : " + scanResultText);
        if (!TextUtils.isEmpty(scanResultText)) {
            String keyValuePairs[] = scanResultText.split("&");
            if (keyValuePairs != null) {
                for (String keyValuePair : keyValuePairs) {
                    if (!TextUtils.isEmpty(keyValuePair)) {
                        String keyValue[] = keyValuePair.split("=");
                        if (keyValue != null && keyValue.length >= 2) {
                            resultIntent.putExtra(keyValue[0], keyValue[1]);
                        }
                    }
                }

            }
        }
        setResult(RESULT_OK, resultIntent);
        finish();
    }


}