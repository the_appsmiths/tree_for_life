package com.treeforlife.customview;

import android.content.Context;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

import com.treeforlife.Utils.FontsUtil;

/**
 * Created by TheAppsmiths on 3/20/2018.
 */

public class CustomButton extends AppCompatButton {
    private static final String TAG = "CustomButton";
    private static final String APP_SCHEMA = "http://schemas.android.com/apk/res-auto";
    private static final int DEFAULT_INT_VALUE = -1;
    private Context mContext;

    public CustomButton(Context context) {
        this(context, null);
    }

    public CustomButton(Context context, AttributeSet attrs) {
        this(context, attrs, android.support.v7.appcompat.R.attr.buttonStyle);
    }

    public CustomButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        applyFont(attrs);
    }

    //method to apply the font
    private void applyFont(AttributeSet attrs) {
        if (attrs != null && mContext != null) {
            int fontIndex = attrs.getAttributeIntValue(APP_SCHEMA, "fontName", DEFAULT_INT_VALUE);
            if (fontIndex != DEFAULT_INT_VALUE) {
                setTypeface(FontsUtil.getTypeface(mContext, fontIndex));
            }
        }
    }

    //method to apply the font name
    public void setFontName(FontsUtil.FontName fontName) {
        setTypeface(FontsUtil.getTypeface(mContext, fontName));
    }

}
