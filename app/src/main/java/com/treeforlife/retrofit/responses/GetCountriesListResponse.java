package com.treeforlife.retrofit.responses;

import com.google.gson.annotations.SerializedName;
import com.treeforlife.dataobjects.CountryInfo;

import java.util.ArrayList;

/**
 * Created by TheAppsmiths on 4/11/2018.
 * @author TheAppsmiths
 */

public class GetCountriesListResponse extends WebServiceResponse {

    @SerializedName("data")
    public ArrayList<CountryInfo> countriesInfoList;
}
