package com.treeforlife.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.treeforlife.R;
import com.treeforlife.Utils.ImageLoaderUtil;
import com.treeforlife.commons.Constants;
import com.treeforlife.customview.CustomTextView;
import com.treeforlife.dataobjects.Tutorial;

/**
 * Created by TheAppSmiths.
 * (TheAppSmiths is the Mobile App Development division of Adreno Technologies India Pvt. Ltd.)
 * *
 * Created on 25 June 2018 - 5:38 PM.
 *
 * @author Android Developer [AD143].
 **/


public class TutorialFragment extends Fragment {
    private static final String TAG = "TutorialFragment";

    private Context mContext;
    private Activity mActivity;
    private Bundle mBundle;

    private ImageView mTutorialImageView;
    private CustomTextView mTutorialTitleTextView, mTutorialDescriptionTextView;

    private ImageLoader mImageLoader;

    private Tutorial mTutorial;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mActivity = getActivity();
        this.mContext = context;
        this.mBundle = getArguments();
        this.mImageLoader = ImageLoaderUtil.getLoader(mContext);
        extractDataFromBundle(false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void extractDataFromBundle(boolean isBeingUpdated) {
        if (!isBeingUpdated && mBundle == null) {
            mBundle = getArguments();
        }

        if (mBundle != null) {
            mTutorial = mBundle.getParcelable(Constants.KEY_TUTORIAL);
        }
    }

    public void update(Bundle data) {
        this.mBundle = data;
        extractDataFromBundle(true);
        setData();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tutorial, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initWidgets(view);
        setData();
    }

    private void initWidgets(View view) {
        if (view != null) {
            mTutorialImageView = (ImageView) view.findViewById(R.id.tutorial_image_view);
            mTutorialTitleTextView = (CustomTextView) view.findViewById(R.id.tutorial_title_text_view);
            mTutorialDescriptionTextView = (CustomTextView) view.findViewById(R.id.tutorial_description_text_view);
            mTutorialDescriptionTextView.setMovementMethod(new ScrollingMovementMethod());
        }
    }

    private void setData() {
        String title = "", desc = "", imgUrl = "";
        if (mTutorial != null) {
            imgUrl = mTutorial.tutorialImage;
            title = mTutorial.tutorialTitle;
            desc = mTutorial.tutorailDesc;
        }
        if (TextUtils.isEmpty(title)) {
            title = getString(R.string.app_name);
        }
        mImageLoader.displayImage(imgUrl, mTutorialImageView,
                ImageLoaderUtil.getDefaultTutorialImageOption());

        mTutorialTitleTextView.setText(title);
        mTutorialDescriptionTextView.setText(desc);
    }
}
