package com.treeforlife.dataobjects;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by TheAppsmiths on 4/10/2018.
 */

public class Tutorial implements Parcelable {

    @SerializedName("tutorial_title")
    public String tutorialTitle;

    @SerializedName("tutorial_image")
    public String tutorialImage;

    @SerializedName("tutorial_desc")
    public String tutorailDesc;

    protected Tutorial(Parcel in) {
        tutorialTitle = in.readString();
        tutorialImage = in.readString();
        tutorailDesc = in.readString();
    }

    public static final Creator<Tutorial> CREATOR = new Creator<Tutorial>() {
        @Override
        public Tutorial createFromParcel(Parcel in) {
            return new Tutorial(in);
        }

        @Override
        public Tutorial[] newArray(int size) {
            return new Tutorial[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(tutorialTitle);
        dest.writeString(tutorialImage);
        dest.writeString(tutorailDesc);
    }
}
